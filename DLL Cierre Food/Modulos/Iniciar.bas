Attribute VB_Name = "Iniciar"
Global ConexionAdm                  As Object
Global ConexionPos                  As Object
Global gRutinas                     As New cls_Rutinas
Global gConexion                    As New Conexiones
Global gCierres                     As New Cls_Cierres
Global gReportes                    As New Cls_Reportes
Global GrupoCajas                   As String
Global autoDeclaracionFiscal        As String
Global Branch                       As String
Global Organizacion                 As String
Global Deposito                     As String
Global CodigoUsuario                As String
Global LocalidadUsuario             As String
Global NivelUsuario                 As Integer
Global ClaseLogin                   As Object 'New DLLLogin.Login
Global POS                          As StructPOS
Global gMesoneros                   As New cls_mesoneros
Global gDispositivos                As New clsDispositivos
Global gDllVerificacion             As Object
Global gVerificacion                As VerificacionElectronica

Global ImpresoraTicket_Tama�oFuente                 As Double

' Variables empleadas manejar frm_mensajeria.
Global Retorno As Boolean
 
Global PrimeraEjecucionDeModal As Boolean

Global ModalDisponible As Boolean

'Private Const gCodProducto      As Long = 853
Private Const gCodProducto      As Long = 859
Public Const ComandasLlevar     As Long = &H80000013
Public Const ComandasComer      As Long = &H80000018
Public Const SinComandar        As Long = &H80000005
Public Const DesCash As String = " Efectivo"

Public Type StructAutorizador
    CodigoUsuario As String
    NombreUsuario As String
    NivelUsuario As Integer
End Type

Public Type StructUsuario
    Codigo                          As String
    Descripcion                     As String
    Nivel                           As Integer
    PassWord                        As String
    LoggedIn                        As Boolean
    Localidad                       As String
    Vendedor                        As Boolean
    Global                          As Boolean
End Type

Public Type Dispositivos
    Activo                          As Boolean
    PNemotecnico                    As String
    Descripcion                     As String
    Puerto                          As String
    Baudios                         As String
    Paridad                         As String
    Parada                          As String
    BitDatos                        As Integer
    CarControl                      As String
    ComandoStr                      As String
    CarCorte                        As String
    Buffer                          As String
    OPOS                            As Boolean
    NombreOPOS                      As String
    UsaHojilla                      As Boolean
    ImpCheque                       As Boolean
    Rotacion                        As Integer
    FApuntador                      As Long
    OComApuntador                   As Object
    OOPOSApuntador                  As Object
    AdministraGaveta                As Boolean
End Type

Public Type StructcmdBotones
    Activo                          As Boolean
    Nivel                           As Integer
    Posicion                        As Integer
    HotKey                          As String
    Key                             As String
    Nombre                          As String
End Type

Public Type CaracteristicasPos
    Deposito                        As String
    Localidad                       As String
    Moneda                          As String
    MisDispositivos()               As Dispositivos
    BotonesPOs()                    As StructcmdBotones
End Type

Public Type ClienteDelPos
    Codigo                          As String
    Nombre                          As String
    Precio                          As Integer
    limite                          As Double
    Dias                            As Integer
    CodigoAfiliado                  As String
    CodigoAutorizado                As String
    Descuento                       As Double
    Rif                             As String
End Type

Public Type ReglasComercialesAdmin
    NivelParaVenderACredito         As Integer
End Type

Public Type VerificacionElectronica
    Verificacion            As Boolean
    Consorcio               As Integer '1 Megasoft 0 Credicard
    NivelAnulacion          As Integer
    DllCredicard            As Boolean
    SecuenciaCorteImpresora As String
End Type

Public Type StructPOS
    
    POSNo                           As String
    TipoCodigo                      As Boolean
    MonedaPref                      As String
    FactorMonedaFactura             As Double
    SRV_LOCAL                       As String
    SRV_REMOTO                      As String
    Branch                          As String
    Organizacion                    As String
    SECUENCE_TURN                   As Long
    ESPERA_TOTALIZAR                As Integer
    ESPERA_EVENTO                   As Integer
    POS_SECUENCE                    As Long
    SALE_WAITING                    As Long
    SALE_RETURN                     As Long
    APPSEtupIni                     As String
    UsuarioDelPos                   As StructUsuario
    TServerText                     As String
    CommandTimeOut                  As Long
    ConnectionTimeOut               As Long
    CnAdm                           As Object
    CnAdmLocal                      As Object
    CnPos                           As Object
    CnPosLocal                      As Object
    ActiveCnAdm                     As Object
    ActiveCnPos                     As Object
    EnLinea                         As Boolean
    Estado                          As String
    Cortes                          As Long
    Vendido                         As Double
    Compras                         As Long
    Suspendido                      As Long
    HoraInicio                      As Date
    TurnoDelPos                     As Double
    CadenadeBotones                 As String
    ClienteActual                   As ClienteDelPos
    Propiedades                     As CaracteristicasPos
    POE                             As Boolean  'Variable que indica si es punto de pedido o caja
    CodigoRelacionPos               As String
    PosEsRetail                     As Boolean
    ImprimirAgrupado                As Boolean
    ImprimirOrdenado                As Boolean
    ImpresoraFacturar               As String
    TextoFondo                      As String
    Forma                           As Object
    
    ComandarLocal                   As Boolean
    LlevarLocal                   As Boolean
    
    ComandarRemoto                  As Boolean
    LlevarRemoto                  As Boolean
    
    PreguntarImprimirFactura        As Boolean
    NoTransaccionActualizar         As Long
    ContTransacciones               As Long
    AutoDeclaracion                 As Boolean
    RequiereFondo                   As Boolean
    ReglasComerciales               As ReglasComercialesAdmin
    Deposito                        As String
    
    AdministraClientes              As Boolean
    TipoDePrecio                    As Integer
    TipoDeBusqueda                  As Integer
    
    SesionNueva                     As Boolean
    MaximoconsumoImprimir           As Integer
    HoraPermisoCierre               As Date
    HoraPermisoCierreMax            As Date
    
End Type

'Public Sub CerrarAuto()
'
'    Dim RsCajasXCerrar As New ADODB.Recordset, LConexion As New ADODB.Connection
'
'    LConexion.ConnectionString = ConexionPos.ConnectionString
'    LConexion.Open
'
'    RsCajasXCerrar.CursorLocation = adUseClient
'
'    RsCajasXCerrar.Open "SELECT TR_CAJA.*, MA_CAJA.C_RELACION " & _
'    "FROM TR_CAJA LEFT JOIN MA_CAJA " & _
'    "ON MA_CAJA.C_CODIGO = TR_CAJA.CS_CAJA " & _
'    "WHERE (ds_FECHA_INICIO <= '20031208 00:00:00.000') " & _
'    "AND (cs_ESTADO = 'C') ORDER BY ds_FECHA_INICIO ASC", _
'    LConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
'
'    If Not RsCajasXCerrar.EOF Then
'
'        FrmProgreso.Show
'        FrmProgreso.Barra.Min = 0
'        FrmProgreso.Barra.Value = 0
'        FrmProgreso.Barra.Max = RsCajasXCerrar.RecordCount
'
'        While Not RsCajasXCerrar.EOF
'
'            DoEvents
'
'            FrmProgreso.LblCaja = RsCajasXCerrar!cs_Caja
'            FrmProgreso.LblFecha = RsCajasXCerrar!ds_Fecha_Inicio
'            FrmProgreso.LblTurno = CDec(RsCajasXCerrar!ns_Turno)
'            FrmProgreso.LblOperador = RsCajasXCerrar!cs_Nombre_Cajero
'            FrmProgreso.Caption = "Cierre Autom�tico de cajas" & String(FrmProgreso.Barra.Value Mod 3, ".")
'
'            FrmProgreso.Refresh
'
'            Set Ficha_Cierre_total.ClaseCierres = gCierres
'            Set Ficha_Cierre_total.ConexionAdm = ConexionAdm
'            Set Ficha_Cierre_total.ConexionPos = ConexionPos
'
'            Ficha_Cierre_total.LCaja = RsCajasXCerrar!cs_Caja
'            Ficha_Cierre_total.LCajero = RsCajasXCerrar!cs_Cajero
'            Ficha_Cierre_total.LCajeroDescripcion = RsCajasXCerrar!cs_Nombre_Cajero
'            Ficha_Cierre_total.LGrupo = RsCajasXCerrar!c_Relacion
'            Ficha_Cierre_total.LLocalidad = Branch
'            Ficha_Cierre_total.LOrganizacion = Organizacion
'            Ficha_Cierre_total.LTurno = RsCajasXCerrar!ns_Turno
'            Ficha_Cierre_total.CodigoUsuario = CodigoUsuario
'
'            'Explicaci�n del siguiente c�digo: Es UN solo Campo y debe contener el mismo valor, solo que aparentemente
'            'surgi� una confusi�n por que en una BD aparec�a nu_ y en otras n_. Por lo cual con el paso del tiempo empezaron
'            'a existir los dos campos, pero solo uno funcionaba. Entonces al intentar corregirlo, se arregl� uno y se **** el otro.
'            'Conclusi�n: Dejamos existir ambos campos aunque tengan la misma funcionalidad y se cambia el c�digo para tratarlos a ambos.
'            'No es lo mas ortodoxo, pero ser�a complicado y ocasionar�a mas confusi�n enviarle scripts a los clientes actuales para solucionar.
'            'Es mejor corregir esto por ac� sin mucha complicaci�n ya que fue error nuestro.
'
'            Dim mVarFondo1 As Boolean
'            Dim mVarFondo2 As Boolean
'            Dim mMontoFondo As Double
'
'            mVarFondo1 = ExisteCampoTabla("nu_MontoFondo", RsCajasXCerrar)
'            mVarFondo2 = ExisteCampoTabla("n_MontoFondo", RsCajasXCerrar)
'
'            If mVarFondo1 And mVarFondo2 Then
'                If CDbl(RsCajasXCerrar!n_MontoFondo) = CDbl(RsCajasXCerrar!nu_MontoFondo) Then
'                    mMontoFondo = RsCajasXCerrar!nu_MontoFondo
'                ElseIf CDbl(RsCajasXCerrar!n_MontoFondo) > CDbl(RsCajasXCerrar!nu_MontoFondo) Then
'                    mMontoFondo = RsCajasXCerrar!n_MontoFondo
'                Else
'                    mMontoFondo = RsCajasXCerrar!nu_MontoFondo
'                End If
'            ElseIf mVarFondo1 And Not mVarFondo2 Then
'                mMontoFondo = RsCajasXCerrar!nu_MontoFondo
'            ElseIf Not mVarFondo1 And mVarFondo2 Then
'                mMontoFondo = RsCajasXCerrar!n_MontoFondo
'            Else
'                mMontoFondo = 0
'            End If
'
'            Ficha_Cierre_total.LFondo = mMontoFondo
'
'            Ficha_Cierre_total.Form_Load
'
'            Ficha_Cierre_total.Hide
'
'            Call Ficha_Cierre_total.Form_Activate
'
'            If Not gCierres.CierreTotal(Ficha_Cierre_total, Ficha_Cierre_total.LOrganizacion, _
'            Ficha_Cierre_total.LLocalidad, Ficha_Cierre_total.LGrupo, Ficha_Cierre_total.LCaja, _
'            Ficha_Cierre_total.LTurno, Ficha_Cierre_total.LFondo, Ficha_Cierre_total.LCajero, _
'            Ficha_Cierre_total.CodigoUsuario, Ficha_Cierre_total.ClaseMonedas.CodMoneda, False, True) Then
'                'MsgBox "Error......"
'                ClaseRutinas.Mensajes (WinApiFunc.Stellar_Mensaje(10097, True))
'                Exit Sub
'            End If
'
'            Set Ficha_Cierre_total = Nothing
'
'            FrmProgreso.Barra.Value = FrmProgreso.Barra.Value + 1
'
'            RsCajasXCerrar.MoveNext
'
'        Wend
'
'    End If
'
'End Sub
