Attribute VB_Name = "WinApiFunc"
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpstring _
    As Any, ByVal lpFileName As String) As Long

Declare Function ExitWindowsEx& Lib "user32" (ByVal uFlags&, ByVal dwReserved&)

Public Type WinSpoolDocInfo
    pDocName As String
    pOutputFile As String
    pDatatype As String
End Type

Public Declare Function ClosePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Public Declare Function EndDocPrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Public Declare Function EndPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Public Declare Function OpenPrinter Lib "winspool.drv" Alias _
                        "OpenPrinterA" (ByVal pPrinterName As String, phPrinter As Long, ByVal pDefault As Long) As Long
Public Declare Function StartDocPrinter Lib "winspool.drv" Alias _
                        "StartDocPrinterA" (ByVal hPrinter As Long, ByVal Level As Long, pDocInfo As WinSpoolDocInfo) As Long
Public Declare Function StartPagePrinter Lib "winspool.drv" (ByVal hPrinter As Long) As Long
Public Declare Function WritePrinter Lib "winspool.drv" (ByVal hPrinter As Long, pBuf As Any, ByVal cdBuf As Long, pcWritten As Long) As Long

Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public Function sGetIni(sIniFile As String, sSection As String, _
sKey As String, sDefault As String) As String
    
    Dim sTemp As String * 255
    Dim nLength As Integer
    
    sTemp = Space$(256)
    
    nLength = GetPrivateProfileString(sSection, sKey, sDefault, sTemp, 255, sIniFile)
    
    sGetIni = Left$(sTemp, nLength)
   
End Function
        
Public Function WriteIni(sIniFile As String, sSection As String, _
sKey As String, sValue As String) As String

    Dim sTemp As String
    Dim N As Integer

    sTemp = sValue
    N = WritePrivateProfileString(sSection, sKey, sValue, sIniFile)
    
End Function

Public Function ClientName() As String
    ClientName = Replace(RegConsult("HKEY_CURRENT_USER\Volatile Environment\CLIENTNAME"), "\\", "")
End Function

Public Function LogonServer() As String
    LogonServer = Replace(RegConsult("HKEY_CURRENT_USER\Volatile Environment\LOGONSERVER"), "\\", "")
End Function

Public Function RegConsult(PathReg As String) As Variant
    Dim ObjWSH As Object
    On Error GoTo RegError
    Set ObjWSH = CreateObject("WScript.Shell")
    RegConsult = ObjWSH.RegRead(PathReg)
    Exit Function
RegError:
    RegConsult = ""
    Exit Function
End Function

Public Sub AjustarPantalla(ByRef Forma As Form, Optional EsFormNavegador As Boolean = False)
    'Si la pantalla es igual a 1024x768px, se ajusta el alto del form para cubrir la pantalla.
    'Las medidas est�n en twips.
    If Screen.Height = "11520" Then 'And Screen.Width = "15360" Then
        Forma.Height = Screen.Height - (GetTaskBarHeight * Screen.TwipsPerPixelY)
        Forma.Top = 0
        Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
        'El form navegador tiene un footer que siempre debe quedar en bottom.
        If GetTaskBarHeight = 0 And EsFormNavegador Then
            Forma.Frame1.Top = (Screen.Height - Forma.Frame1.Height)
        End If
    Else
        'Si no es la resoluci�n m�nima de Stellar, se centra el form.
        Forma.Top = (Screen.Height / 2) - (Forma.Height / 2)
        Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
    End If
End Sub

Public Function StellarMensaje(mensaje As Integer, _
Optional pDevolver As Boolean = True) As String
    StellarMensaje = Stellar_Mensaje(mensaje, pDevolver)
End Function

Public Function Stellar_Mensaje(mensaje As Integer, _
Optional pDevolver As Boolean = True) As String
    
    On Error GoTo Error
    
    Dim Texto As String
    
    Texto = LoadResString(mensaje)

    If Not pDevolver Then
        'NoExiste.Label1.Caption = texto
        'NoExiste.Show vbModal
    Else
        Stellar_Mensaje = Texto
    End If
    
    Exit Function
    
Error:
    
    Stellar_Mensaje = "CHK_RES"
    
End Function

Public Function BuscarReglaNegocioBoolean(pConex As Object, pCampo, Optional pIgualA) As Boolean
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    On Error GoTo Errores
    mSQL = "SELECT * FROM MA_REGLASDENEGOCIO WHERE Campo = '" & pCampo & "'"
    mRs.Open mSQL, pConex, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not mRs.EOF Then
        BuscarReglaNegocioBoolean = CStr(mRs!Valor) = IIf(Not IsMissing(pIgualA), CStr(pIgualA), CStr(1))
    End If
    mRs.Close
    Exit Function
Errores:
    Err.Clear
End Function

Public Function BuscarReglaNegocioStr(pConex As Object, ByVal pCampo, _
Optional ByVal pDefault As String) As String
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    On Error GoTo Errores
    
    mSQL = "SELECT * FROM [" & Srv_Remote_BD_ADM & "].[DBO].[MA_REGLASDENEGOCIO] " & _
    "WHERE Campo = '" & pCampo & "' "
    
    mRs.Open mSQL, pConex, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarReglaNegocioStr = CStr(mRs!Valor)
    Else
        BuscarReglaNegocioStr = pDefault
    End If
    
    mRs.Close
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Function DtoC(Fecha As Date) As String
    'DtoC = Format(Day(Fecha), "00") & "-" & Format(Month(Fecha), "00") & "-" & Format(Year(Fecha), "0000")
    DtoC = CStr(CDate(Format(Fecha, "short date")))
End Function

Function DtoCLng(Fecha As Date) As String
    DtoCLng = CStr(CDate(Format(Fecha, "short date"))) & " " & Format(Fecha, "hh:mm:ss")
End Function

Public Sub LlamarTecladoNumerico(ByRef ValorDef As Double, Decimales As Integer, Optional Titulo As String = "")
    Dim Teclado As Object
    Set Teclado = CreateObject("DLLKeyboard.DLLTeclado")
    Call Teclado.ShowNumPad(ValorDef, 2) 'Call Teclado.ShowNumPad(ValorDef, Decimales)
    Set Teclado = Nothing
End Sub
