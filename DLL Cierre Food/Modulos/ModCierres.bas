Attribute VB_Name = "ModCierres"
Public ClaseCierres As New Cls_Cierres
Public ClaseRutinas As New cls_Rutinas
Public ClaseMonedaPred As cls_Monedas
Public ClaseMonedaTmp As cls_Monedas

Public ObjTeclado           As Object

Public Enum GridParcial
    ParMonedaDescripcion = 0
    ParFactorMoneda
    ParDenominacionDescripcion
    ParNumero
    ParEntidad
    ParTotalVendido
    ParTotalRecibio
    ParMonedaCodigo
    ParDenominacionCodigo
    ParEntidadCodigo
    ParValorReal
    ParDiferencia
    ParDetPagID
    ParDenominacionAutoDeclara
End Enum

Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_FULL
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Public Const ColorNormal                                As Long = &H80000005
Public Const ColorConfirmado                            As Long = &H80FF80

Public AutoDeclarar                                     As Boolean
Public AnchoSetup                                       As Integer
Public ImprimirCierreModoViejo                          As Boolean
Public ModoAgente                                       As Boolean

Public ImpresoraPOSEspecifica                           As String
Public ImpresoraPOSEspecifica_PorPuerto                 As Boolean

Global POS_CP_AutoDeposito_PorLote                      As Boolean
Global ManejaIGTF                                       As Boolean
Global POS_ManejaFondoMultiMoneda                       As Boolean
Global POS_DeclararEfectivoDesgloseAutomatico           As Integer

Global Srv_Remote_BD_ADM                                As String
Global Srv_Remote_BD_POS                                As String

Public FrmAppLinkADM                                    As Object

Public Function CargarConfiguracionPOS(ConexionPos As Object, Localidad As String, Caja As String) As Boolean
    
    Dim RsCaja As New ADODB.Recordset
    Set RsCaja = gRutinas.CrearAdoRs
    
    RsCaja.Open " SELECT * FROM MA_CAJA " & _
    " WHERE C_CODIGO = '" & Caja & "' AND C_CODLOCALIDAD = '" & Localidad & "'", _
    ConexionPos, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not RsCaja.EOF Then
        POS.ImpresoraFacturar = RsCaja!cu_Impresora_Facturar
        POS.ImprimirAgrupado = RsCaja!Imp_Agrupada
        POS.ImprimirOrdenado = RsCaja!Imp_Ordenada
        POS.PreguntarImprimirFactura = RsCaja!cu_Preguntar_Imprimir_Factura
        POS.NoTransaccionActualizar = RsCaja!n_Transacciones
        POS.AutoDeclaracion = RsCaja!bu_AutoDeclaracion
        POS.RequiereFondo = RsCaja!b_Solicitar_Fondo 'PENIENTE POR HACER: SOLICITAR EL MONTO DEL FONO Y EN EL CIERRE TOTAL CUADRAR
        POS.ComandarLocal = RsCaja!bu_Comanda_Local
        POS.LlevarLocal = RsCaja!bu_Llevar_Local
        POS.ComandarRemoto = RsCaja!bu_Comanda_Remota
        POS.LlevarRemoto = RsCaja!bu_Llevar_Remota
        POS.AdministraClientes = RsCaja!bu_Administra_Clientes
        POS.TipoDePrecio = RsCaja!nu_Tipo_Precio
        POS.TipoDeBusqueda = RsCaja!nu_Campo_Busqueda
        POS.Deposito = RsCaja!c_CodDeposito
        POS.CodigoRelacionPos = RsCaja!c_Relacion
        POS.TextoFondo = RsCaja!Observaciones
    Else
        gRutinas.Mensajes "No se encontr� la caja '" & Caja & "', el sistema se reiniciar�", False
        Call gRutinas.ReiniciarWindows(EWX_REBOOT)
    End If
    
End Function

'/***************************************************
'reimpresiones
'/***************************************************

Public Sub ReimprimirFactura(Branch As String, ConexionPos As Object, ConexionAdm As Object)
    
    Dim Datos As Variant, Recursos As Object, rsFacturas As Object
    
    On Error GoTo Falla_Local
    
'    Set Recursos = CreateObject("recsun.obj_busqueda")
'    Set Iniciar.ConexionPos = ConexionPos
'    Set Iniciar.ConexionAdm = ConexionAdm
'
'    Recursos.Inicializar "SELECT * FROM MA_VENTAS_POS WHERE cs_LOCALIDAD = '" & Branch & "' AND cs_TIPO_DOCUMENTO = 'VEN'", "F A C T U R A S  DEL  P O S", ConexionPos.ConnectionString, , False
'
'    Recursos.Add_Campos "CAJA", "CS_CAJA", 1000, 0
'    Recursos.Add_Campos "TURNO", "nu_TURNO", 1000, 0
'    Recursos.Add_Campos "DOCUMENTO", "CS_DOCUMENTO", 2400, 0
'    Recursos.Add_Campos "FECHA", "DS_FECHA", 1600, 0
'    Recursos.Add_Campos "CLIENTE", "CU_NOMBRE_CLIENTE", 2400, 0
'    Recursos.Add_Campos "RIF", "CU_RIFCLIENTE", 2400, 0
'    Recursos.Add_Campos "USUARIO", "CS_USUARIO", 2400, 0
'    Recursos.Add_Campos "VENDEDOR", "CS_CODIGO_VENDEDOR", 1400, 0
'    Recursos.Add_Campos "SERVICIO", "CS_CODIGOSERVICIO", 1400, 0
'
'    Recursos.Add_CamposBusqueda "CAJA", "CS_CAJA"
'    Recursos.Add_CamposBusqueda "TURNO", "nu_TURNO"
'    Recursos.Add_CamposBusqueda "DOCUMENTO", "CS_DOCUMENTO"
'    Recursos.Add_CamposBusqueda "FECHA", "DS_FECHA"
'    Recursos.Add_CamposBusqueda "CLIENTE", "CU_NOMBRE_CLIENTE"
'    Recursos.Add_CamposBusqueda "RIF", "CU_RIFCLIENTE"
'    Recursos.Add_CamposBusqueda "USUARIO", "CS_USUARIO"
'    Recursos.Add_CamposBusqueda "VENDEDOR", "CS_CODIGO_VENDEDOR"
'    Recursos.Add_CamposBusqueda "SERVICIO", "CS_CODIGOSERVICIO"
'    Datos = Recursos.Ejecutar
    
    Set Iniciar.ConexionPos = ConexionPos
    Set Iniciar.ConexionAdm = ConexionAdm
    
    Dim TmpNewFrmSuperConsultas As Object
    
    Set TmpNewFrmSuperConsultas = FrmAppLinkADM.GetFrmSuperConsultas
    
    With TmpNewFrmSuperConsultas 'Frm_Super_Consultas
        
        .Inicializar "SELECT * FROM MA_VENTAS_POS " & _
        "WHERE cs_LOCALIDAD = '" & Branch & "' " & _
        "AND cs_TIPO_DOCUMENTO = 'VEN'", _
        WinApiFunc.Stellar_Mensaje(2029, True), ConexionPos
        
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10098, True), "CS_CAJA", 1100, 0 '1000, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10099, True), "nu_TURNO", 1425, 0, , Array("Numerico", "0", "0") '1000, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10100, True), "CS_DOCUMENTO", 2130, 0 ' 2400, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10101, True), "DS_FECHA", 3465, 0, , Array("Fecha", "VbGeneralDate") '1600, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10102, True), "CU_NOMBRE_CLIENTE", 2985, 0 ' 2400, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10103, True), "CU_RIFCLIENTE", 2730, 0 '2400, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10104, True), "CS_USUARIO", 2655, 0 '2400, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10105, True), "CS_CODIGO_VENDEDOR", 2370, 0 ' 1400, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10106, True), "CS_CODIGOSERVICIO", 2265, 0 '1400, 0
        
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10098, True), "CS_CAJA"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10099, True), "nu_TURNO"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10100, True), "CS_DOCUMENTO"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10101, True), "DS_FECHA"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10102, True), "CU_NOMBRE_CLIENTE"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10103, True), "CU_RIFCLIENTE"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10104, True), "CS_USUARIO"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10105, True), "CS_CODIGO_VENDEDOR"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10106, True), "CS_CODIGOSERVICIO"
        
        .txtDato.Text = "%"
        
        .Show vbModal
        
        Datos = .ArrResultado
        
    End With
    
    If Not IsEmpty(Datos) Then
        
        If Trim(Datos(0)) <> Empty Then
            
            POS.POSNo = Datos(0)
            
            Call CargarConfiguracionPOS(ConexionPos, Branch, POS.POSNo)
            
            Set rsFacturas = ClaseRutinas.CrearAdoRs
            
            rsFacturas.Open "SELECT * FROM MA_VENTAS_POS " & _
            "WHERE CS_DOCUMENTO = '" & Trim(Datos(2)) & "' " & _
            "AND cs_LOCALIDAD = '" & Branch & "' " & _
            "AND cs_TIPO_DOCUMENTO = 'VEN' ", _
            ConexionPos, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            If rsFacturas.EOF Then
                ' IMPRIMIR DIRECTAMENTE.
                Set rsFacturas = Nothing
                ClaseRutinas.Mensajes WinApiFunc.Stellar_Mensaje(10107, True), False
                Exit Sub
            End If
            
            ' Buscar datos del usuario del pos.
            
            Call ObtenerDatosDelUsuario(rsFacturas!cs_Usuario)
            Call ObtenerDatosDelVendedor(rsFacturas!cs_Codigo_Vendedor)
            
            If Not rsFacturas Is Nothing Then
                gReportes.InicializarConexiones ConexionAdm, ConexionPos
                ClaseCierres.InicializarConexiones ConexionAdm, ConexionPos
                Call gReportes.Imprimir_Facturacion(rsFacturas!cs_organizacion, _
                rsFacturas!cs_localidad, rsFacturas!cs_Documento, "VEN", ConexionPos, _
                True, , Srv_Remote_BD_ADM, Srv_Remote_BD_POS)
            End If
            
        End If
        
    End If
    
    Set TmpNewFrmSuperConsultas = Nothing
    
    Exit Sub
    
Falla_Local:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(ReimprimirFactura)"
    
End Sub

Public Sub ObtenerDatosDelUsuario(Usuario As String)
    
    Dim RsUsuarios As New ADODB.Recordset
    
    ClaseRutinas.Apertura_Recordset RsUsuarios, adUseClient
    
    RsUsuarios.Open "SELECT * FROM MA_USUARIOS WHERE CODUSUARIO = '" & Usuario & "'", _
    ConexionAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not RsUsuarios.EOF Then
        POS.UsuarioDelPos.Codigo = RsUsuarios!CodUsuario
        POS.UsuarioDelPos.Descripcion = RsUsuarios!Descripcion
        POS.UsuarioDelPos.Global = RsUsuarios!TIPO_USUARIO
        POS.UsuarioDelPos.Localidad = RsUsuarios!Localidad
        POS.UsuarioDelPos.LoggedIn = False
        POS.UsuarioDelPos.Nivel = RsUsuarios!Nivel
        POS.UsuarioDelPos.PassWord = RsUsuarios!PassWord
        POS.UsuarioDelPos.Vendedor = RsUsuarios!Vendedor
    End If
    
    RsUsuarios.Close
    
End Sub

Public Sub ObtenerDatosDelVendedor(Vendedor As String)
    
    Dim RsUsuarios As New ADODB.Recordset
    
    ClaseRutinas.Apertura_Recordset RsUsuarios, adUseClient
    
    RsUsuarios.Open "SELECT * FROM MA_VENDEDORES WHERE CU_VENDEDOR_COD = '" & Vendedor & "'", _
    ConexionAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not RsUsuarios.EOF Then
        gMesoneros.VendedorCod = RsUsuarios!CU_VENDEDOR_COD
        gMesoneros.VendedorDes = RsUsuarios!CU_VENDEDOR_DES
    Else
        ClaseRutinas.Apertura_Recordset RsUsuarios, adUseClient
        
        RsUsuarios.Open "SELECT * FROM MA_USUARIOS WHERE CODUSUARIO = '" & Vendedor & "'", _
        ConexionAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsUsuarios.EOF Then
            gMesoneros.VendedorCod = RsUsuarios!CodUsuario
            gMesoneros.VendedorDes = RsUsuarios!Descripcion
        End If
    End If
    
    RsUsuarios.Close
    
End Sub

Public Sub ReimprimirCierreTotal(Branch As String, ConexionPos As Object, ConexionAdm As Object)
    
'    Dim Datos As Variant, Recursos As Object, RsFacturas As Object
'    Set Recursos = CreateObject("recsun.obj_busqueda")
'    Set Iniciar.ConexionPos = ConexionPos
'    Set Iniciar.ConexionAdm = ConexionAdm
'
'    Recursos.Inicializar "SELECT * FROM MA_CIERRE_POS_TOTAL WHERE cs_LOCALIDAD = '" & Branch & "'", "CIERRE DEL POS", ConexionPos.ConnectionString, , False
'    Recursos.Add_Campos "CAJA", "CS_CAJA", 1000, 0
'    Recursos.Add_Campos "TURNO", "CS_TURNO", 1000, 0
'    Recursos.Add_Campos "DOCUMENTO", "CS_DOCUMENTO", 2400, 0
'    Recursos.Add_Campos "FECHA", "DS_FECHA", 2400, 0
'    Recursos.Add_Campos "CAJERO", "CS_CAJERO", 2400, 0
'
'    Recursos.Add_CamposBusqueda "CAJA", "CS_CAJA"
'    Recursos.Add_CamposBusqueda "TURNO", "CS_TURNO"
'    Recursos.Add_CamposBusqueda "DOCUMENTO", "CS_DOCUMENTO"
'    Recursos.Add_CamposBusqueda "FECHA", "DS_FECHA"
'    Recursos.Add_CamposBusqueda "CAJERO", "CS_CAJERO"
'    Datos = Recursos.Ejecutar
    
    Dim Datos As Variant, Recursos As Object, rsFacturas As Object
    
    Set Iniciar.ConexionPos = ConexionPos
    Set Iniciar.ConexionAdm = ConexionAdm
    
    Dim TmpNewFrmSuperConsultas As Object
    
    Set TmpNewFrmSuperConsultas = FrmAppLinkADM.GetFrmSuperConsultas
    
    With TmpNewFrmSuperConsultas 'Frm_Super_Consultas
        
        .Inicializar "SELECT * FROM MA_CIERRE_POS_TOTAL " & _
        "WHERE cs_LOCALIDAD = '" & Branch & "' ", _
        WinApiFunc.Stellar_Mensaje(2032, True), ConexionPos
        
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10098, True), "CS_CAJA", 1185, 0 '1000, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10099, True), "CS_TURNO", 1920, 0, , Array("Numerico", "0", "0") '1200, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10100, True), "CS_DOCUMENTO", 2355, 0 '2400, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10101, True), "DS_FECHA", 1680, 0, , Array("Fecha", "VbShortDate") '1840, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10108, True), "CS_NOMBRE_CAJERO", 3960, 0 ' 3450, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10108, True), "CS_CAJERO", 0, 0
        
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10098, True), "CS_CAJA"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10099, True), "CS_TURNO"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10100, True), "CS_DOCUMENTO"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10101, True), "DS_FECHA"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10108, True), "CS_NOMBRE_CAJERO"
        .Add_ItemSearching "COD." & WinApiFunc.Stellar_Mensaje(10108, True), "CS_CAJERO"
        
        .txtDato.Text = "%"
        
        .Show vbModal
        
        Datos = .ArrResultado
        
    End With
    
    If Not IsEmpty(Datos) Then
        
        If Trim(Datos(0)) <> Empty Then
            
            POS.POSNo = Datos(0)
            
            Call CargarConfiguracionPOS(ConexionPos, Branch, POS.POSNo)
            
            gReportes.InicializarConexiones ConexionAdm, ConexionPos
            
            ClaseCierres.InicializarConexiones ConexionAdm, ConexionPos
            
            ImprimirCierreModoViejo = ImprimirCierreModoViejo Or _
            Val(BuscarReglaNegocioStr(ConexionAdm, "ImprimirCierreFOODModoAntiguo", "0")) = 1
            
            If ImprimirCierreModoViejo Then
                Call ClaseCierres.ImprimirCierreTotal(POS.Organizacion, Branch, _
                POS.POSNo, POS.CodigoRelacionPos, CDbl(Datos(1)), CStr(Datos(5)), CStr(Datos(2)), False)
            Else
                Call ClaseCierres.ImprimirCierreTotalNuevo(Branch, POS.POSNo, _
                POS.CodigoRelacionPos, CDbl(Datos(1)), CStr(Datos(5)), CStr(Datos(2)), False)
            End If
            
        End If
        
    End If
    
    Set TmpNewFrmSuperConsultas = Nothing 'Frm_Super_Consultas = Nothing
    
End Sub

Public Sub ReimprimirRetiroParcial(Branch As String, ConexionPos As Object, ConexionAdm As Object)
    
    Dim Datos As Variant, Recursos As Object, rsFacturas As Object
    
'    Set Recursos = CreateObject("recsun.obj_busqueda")
'    Set Iniciar.ConexionPos = ConexionPos
'    Set Iniciar.ConexionAdm = ConexionAdm
'    Recursos.Inicializar "SELECT * FROM MA_CIERRES_POS_PARCIAL WHERE cs_LOCALIDAD = '" & Branch & "'", "RETIRO DEL POS", ConexionPos.ConnectionString, , False
'
'    Recursos.Add_Campos "CAJA", "CS_CAJA", 1000, 0
'    Recursos.Add_Campos "TURNO", "CS_TURNO", 1000, 0
'    Recursos.Add_Campos "DOCUMENTO", "CS_DOCUMENTO", 2400, 0
'    Recursos.Add_Campos "FECHA", "DS_FECHA", 1400, 0
'    Recursos.Add_Campos "CAJERO", "CS_CAJERO", 2400, 0
'
'    Recursos.Add_CamposBusqueda "CAJA", "CS_CAJA"
'    Recursos.Add_CamposBusqueda "TURNO", "CS_TURNO"
'    Recursos.Add_CamposBusqueda "DOCUMENTO", "CS_DOCUMENTO"
'    Recursos.Add_CamposBusqueda "FECHA", "DS_FECHA"
'    Recursos.Add_CamposBusqueda "CAJERO", "CS_CAJERO"
'    Datos = Recursos.Ejecutar
    
    Set Iniciar.ConexionPos = ConexionPos
    Set Iniciar.ConexionAdm = ConexionAdm
    
    Dim TmpNewFrmSuperConsultas As Object
    
    Set TmpNewFrmSuperConsultas = FrmAppLinkADM.GetFrmSuperConsultas
    
    With TmpNewFrmSuperConsultas 'Frm_Super_Consultas
        
        .Inicializar "SELECT * FROM MA_CIERRES_POS_PARCIAL " & _
        "WHERE cs_LOCALIDAD = '" & Branch & "'", _
        WinApiFunc.Stellar_Mensaje(10109, True), ConexionPos
        
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10098, True), "CS_CAJA", 1185, 0 '1320, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10099, True), "CS_TURNO", 1920, 0, , Array("Numerico", "0", "0") '1250, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10100, True), "CS_DOCUMENTO", 2355, 0 '2400, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10101, True), "DS_FECHA", 1680, 0, , Array("Fecha", "VbShortDate") ', 2350, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10108, True), "CS_NOMBRE_CAJERO", 3960, 0 '2550, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10108, True), "CS_CAJERO", 0, 0
        
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10098, True), "CS_CAJA"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10099, True), "CS_TURNO"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10100, True), "CS_DOCUMENTO"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10101, True), "DS_FECHA"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10108, True), "CS_NOMBRE_CAJERO"
        .Add_ItemSearching "COD." & WinApiFunc.Stellar_Mensaje(10108, True), "CS_CAJERO"
        
        .txtDato.Text = "%"
        
        .Show vbModal
        
        Datos = .ArrResultado
        
    End With
    
    If Not IsEmpty(Datos) Then
        
        If Trim(Datos(0)) <> Empty Then
            
            POS.POSNo = Datos(0)
            
            Call CargarConfiguracionPOS(ConexionPos, Branch, POS.POSNo)
            
            gReportes.InicializarConexiones ConexionAdm, ConexionPos
            
            ClaseCierres.InicializarConexiones ConexionAdm, ConexionPos
            
            Call ClaseCierres.ImprimirCierreParcial(POS.Organizacion, Branch, _
            POS.POSNo, POS.CodigoRelacionPos, CDbl(Datos(1)), _
            CStr(Datos(5)), CStr(Datos(2)), False)
            
        End If
        
    End If
    
    Set TmpNewFrmSuperConsultas = Nothing 'Frm_Super_Consultas = Nothing
    
End Sub

Public Sub ReimprimirSeriales(Branch As String, ConexionPos As Object, ConexionAdm As Object)
    
    'SELECT CIERRES.ds_FECHA, CIERRES.cs_CAJA, CIERRES.cs_TURNO, CIERRES.cs_CAJERO, CIERRES.cs_DOCUMENTO, CIERRES.Tipo, MA_USUARIOS.Descripcion FROM (SELECT     ds_FECHA, cs_CAJA, cs_TURNO, cs_CAJERO, cs_DOCUMENTO, 'TOTAL' AS Tipo From MA_CIERRE_POS_TOTAL Union SELECT     ds_FECHA, cs_CAJA, cs_TURNO, cs_CAJERO, cs_DOCUMENTO, 'PARCIAL' AS Tipo FROM MA_CIERRES_POS_PARCIAL) CIERRES LEFT OUTER JOIN " & Srv_Remote_BD_ADM & ".dbo.MA_USUARIOS MA_USUARIOS ON MA_USUARIOS.codusuario = CIERRES.cs_CAJERO ORDER BY CIERRES.ds_FECHA, CIERRES.cs_CAJA, CIERRES.cs_TURNO, CIERRES.cs_CAJERO, CIERRES.Tipo, CIERRES.cs_DOCUMENTO
    'ESTA PENDIENTE AGREGAR AL OBJETO DE BUSQUEDA GENERICA DEL RECSUN LA PROPIEDAD QUE UNA CONSULTA PUEDE CONTENER LA SENTENCIA ORDER BY Y AGREGARLES CRITERIOS AL WHERE DE ESA CONSULTA
    
    Dim Datos As Variant, Recursos As Object, rsFacturas As Object
    
'    Set Recursos = CreateObject("recsun.obj_busqueda")
'    Set Iniciar.ConexionPos = ConexionPos
'    Set Iniciar.ConexionAdm = ConexionAdm
'    Recursos.Inicializar "SELECT CIERRES.ds_FECHA, CIERRES.cs_CAJA, CIERRES.cs_TURNO, CIERRES.cs_CAJERO, CIERRES.cs_DOCUMENTO, CIERRES.CS_ORGANIZACION, CIERRES.CS_LOCALIDAD, CIERRES.Tipo, MA_USUARIOS.Descripcion FROM (SELECT ds_FECHA, cs_CAJA, cs_TURNO, cs_CAJERO, cs_DOCUMENTO, CS_ORGANIZACION, CS_LOCALIDAD, 'TOTAL' AS Tipo From MA_CIERRE_POS_TOTAL Union SELECT ds_FECHA, cs_CAJA, cs_TURNO, cs_CAJERO, cs_DOCUMENTO, CS_ORGANIZACION, CS_LOCALIDAD, 'PARCIAL' AS Tipo FROM MA_CIERRES_POS_PARCIAL) CIERRES LEFT OUTER JOIN " & Srv_Remote_BD_ADM & ".dbo.MA_USUARIOS MA_USUARIOS ON MA_USUARIOS.codusuario = CIERRES.cs_CAJERO WHERE CS_LOCALIDAD = '" & Branch & "'", "CIERRES TOTALES Y PARCIALES", ConexionPos.ConnectionString, , False
'
'    Recursos.Add_Campos "FECHA", "DS_FECHA", 1400, 0
'    Recursos.Add_Campos "CAJA", "CS_CAJA", 1000, 0
'    Recursos.Add_Campos "TURNO", "CS_TURNO", 1000, 0
'    Recursos.Add_Campos "CAJERO", "CS_CAJERO", 1400, 0
'    Recursos.Add_Campos "NOMBRE", "Descripcion", 2400, 0
'    Recursos.Add_Campos "DOCUMENTO", "CS_DOCUMENTO", 1400, 0
'    Recursos.Add_Campos "TIPO", "TIPO", 1400, 0
'
'    Recursos.Add_CamposBusqueda "CAJA", "CS_CAJA"
'    Recursos.Add_CamposBusqueda "TURNO", "CS_TURNO"
'    Recursos.Add_CamposBusqueda "CAJERO", "CS_CAJERO"
'    Recursos.Add_CamposBusqueda "NOMBRE", "Descripcion"
'    Recursos.Add_CamposBusqueda "DOCUMENTO", "CS_DOCUMENTO"
'    Recursos.Add_CamposBusqueda "TIPO", "TIPO"
'
'    Datos = Recursos.Ejecutar
    
    Set Iniciar.ConexionPos = ConexionPos
    Set Iniciar.ConexionAdm = ConexionAdm
    
    With Frm_Super_Consultas
        
        .Inicializar "SELECT CIERRES.ds_FECHA, CIERRES.cs_CAJA, CIERRES.cs_TURNO, CIERRES.cs_CAJERO, CIERRES.cs_DOCUMENTO, CIERRES.CS_ORGANIZACION, CIERRES.CS_LOCALIDAD, CIERRES.Tipo, MA_USUARIOS.Descripcion FROM (SELECT ds_FECHA, cs_CAJA, cs_TURNO, cs_CAJERO, cs_DOCUMENTO, CS_ORGANIZACION, CS_LOCALIDAD, 'TOTAL' AS Tipo From MA_CIERRE_POS_TOTAL Union SELECT ds_FECHA, cs_CAJA, cs_TURNO, cs_CAJERO, cs_DOCUMENTO, CS_ORGANIZACION, CS_LOCALIDAD, 'PARCIAL' AS Tipo FROM MA_CIERRES_POS_PARCIAL) CIERRES LEFT OUTER JOIN " & Srv_Remote_BD_ADM & ".dbo.MA_USUARIOS MA_USUARIOS ON MA_USUARIOS.codusuario = CIERRES.cs_CAJERO WHERE CS_LOCALIDAD = '" & Branch & "'", WinApiFunc.Stellar_Mensaje(2032, True), ConexionPos
        
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10101, True), "DS_FECHA", 1400, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10098, True), "CS_CAJA", 1000, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10099, True), "CS_TURNO", 1000, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10108, True), "CS_CAJERO", 1400, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10110, True), "Descripcion", 2400, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10100, True), "CS_DOCUMENTO", 1400, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10111, True), "TIPO", 1400, 0
        
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10098, True), "CS_CAJA"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10099, True), "CS_TURNO"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10108, True), "CS_CAJERO"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10110, True), "Descripcion"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10100, True), "CS_DOCUMENTO"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10111, True), "TIPO"
        
        .txtDato.Text = "%"
        
        .Show vbModal
        
        Datos = .ArrResultado
    
    End With
    
    If Not IsEmpty(Datos) Then
        If Not Datos(0) = "" Then
            POS.POSNo = Datos(1)
            
            Call CargarConfiguracionPOS(ConexionPos, Branch, POS.POSNo)
            
            gReportes.InicializarConexiones ConexionAdm, ConexionPos
            
            ClaseCierres.InicializarConexiones ConexionAdm, ConexionPos
            
            Call ClaseCierres.ImprimirSerialesDeCierre(POS.Organizacion, Branch, CStr(Datos(1)), CLng(Datos(2)), CStr(Datos(5)), Left(CStr(Datos(6)), 3), False)
        End If
    End If

End Sub

Public Sub ReimprimirConsumos(Branch As String, ConexionPos As Object, ConexionAdm As Object)
    
    Dim Datos As Variant, Recursos As Object, RsConsumo As Object
    
    On Error GoTo Falla_Local
    
'    Set Recursos = CreateObject("recsun.obj_busqueda")
'    Set Iniciar.ConexionPos = ConexionPos
'    Set Iniciar.ConexionAdm = ConexionAdm
'
'    Recursos.Inicializar "SELECT * FROM MA_CONSUMO WHERE cs_LOCALIDAD = '" & Branch & "' AND BS_REINTEGRADA = 0 AND BS_CONSOLIDADA = 1", "F A C T U R A S  DEL  P O S", ConexionPos.ConnectionString, , False
'
'    Recursos.Add_Campos "CUENTA", "CS_CODIGOCUENTA", 1600, 0
'    Recursos.Add_Campos "SERVICIO", "CS_CODIGOSERVICIO", 1600, 0
'    Recursos.Add_Campos "FECHA", "FS_FECHA_HORA", 1600, 0
'    Recursos.Add_Campos "CODIGO", "CU_CODIGOCLIENTE", 1600, 0
'    Recursos.Add_Campos "CLIENTE", "CU_NOMBRECLIENTE", 2400, 0
'    Recursos.Add_Campos "RIF", "CU_RIFCLIENTE", 1600, 0
'    Recursos.Add_Campos "VENDEDOR", "CS_CODIGOMESERO", 1400, 0
'
'    Recursos.Add_CamposBusqueda "CUENTA", "CS_CODIGOCUENTA"
'    Recursos.Add_CamposBusqueda "SERVICIO", "CS_CODIGOSERVICIO"
'    Recursos.Add_CamposBusqueda "FECHA", "FS_FECHA_HORA"
'    Recursos.Add_CamposBusqueda "CLIENTE", "CU_NOMBRECLIENTE"
'    Recursos.Add_CamposBusqueda "RIF", "CU_RIFCLIENTE"
'    Recursos.Add_CamposBusqueda "VENDEDOR", "CS_CODIGOMESERO"
'    Datos = Recursos.Ejecutar
    
    Set Iniciar.ConexionPos = ConexionPos
    Set Iniciar.ConexionAdm = ConexionAdm
    
    Dim TmpNewFrmSuperConsultas As Object
    
    Set TmpNewFrmSuperConsultas = FrmAppLinkADM.GetFrmSuperConsultas
    
    With TmpNewFrmSuperConsultas 'Frm_Super_Consultas
        
        .Inicializar _
        "SELECT * FROM MA_CONSUMO " & _
        "WHERE cs_LOCALIDAD = '" & Branch & "' " & _
        "AND BS_REINTEGRADA = 0 " & _
        "AND BS_CONSOLIDADA = 1", _
        WinApiFunc.Stellar_Mensaje(2034, True), ConexionPos
        
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10112, True), "CS_CODIGOCUENTA", 2220, 0 '1600, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10106, True), "CS_CODIGOSERVICIO", 2200, 0 '1600, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10101, True), "FS_FECHA_HORA", 2025, 0, , Array("Fecha", "VbGeneralDate") '2310, 0
        .Add_ItemLabels UCase(WinApiFunc.Stellar_Mensaje(162, True)), "CU_CODIGOCLIENTE", 1800, 0 '1710, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10102, True), "CU_NOMBRECLIENTE", 2850, 0 '2400, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10103, True), "CU_RIFCLIENTE", 2835, 0 '1410, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10105, True), "CS_CODIGOMESERO", 2865, 0 '1890, 0
        
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10112, True), "CS_CODIGOCUENTA"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10106, True), "CS_CODIGOSERVICIO"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10101, True), "FS_FECHA_HORA"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10102, True), "CU_NOMBRECLIENTE"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10103, True), "CU_RIFCLIENTE"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10105, True), "CS_CODIGOMESERO"
        
        .txtDato.Text = "%"
        
        .Show vbModal
        
        Datos = .ArrResultado
        
    End With
    
    If Not IsEmpty(Datos) Then
        
        If Trim(Datos(0)) <> Empty Then
            
            Set TmpValoresPOS = ConexionPos.Execute( _
            "SELECT TOP (1) * FROM MA_CAJA ORDER BY c_Codigo")
            
            POS.POSNo = TmpValoresPOS!c_Codigo
            POS.MaximoconsumoImprimir = 1000
            
            Call CargarConfiguracionPOS(ConexionPos, Branch, POS.POSNo)
            
            Set RsConsumo = ClaseRutinas.CrearAdoRs
            
            RsConsumo.Open _
            "SELECT * FROM MA_CONSUMO " & _
            "WHERE CS_CODIGOCUENTA = '" & Trim(Datos(0)) & "' " & _
            "AND CS_CODIGOSERVICIO = '" & Datos(1) & "' " & _
            "AND cs_LOCALIDAD = '" & Branch & "' ", _
            ConexionPos, adOpenForwardOnly, adLockReadOnly, adCmdText
            
            If RsConsumo.EOF Then
                ' IMPRIMIR DIRECTAMENTE.
                Set RsConsumo = Nothing
                ClaseRutinas.Mensajes "No se encontr� el n�mero de consumo.", False
                Exit Sub
            End If
            
            ' Buscar datos del usuario del pos.
            Call ObtenerDatosDelVendedor(RsConsumo!cs_CODIGOMESERO)
            
            If Not RsConsumo Is Nothing Then
                gReportes.InicializarConexiones ConexionAdm, ConexionPos
                ClaseCierres.InicializarConexiones ConexionAdm, ConexionPos
                Call gReportes.Imprimir_Totalizar(RsConsumo!cs_organizacion, _
                RsConsumo!cs_localidad, RsConsumo!CS_CODIGOCUENTA, _
                RsConsumo!cs_CodigoServicio, RsConsumo!cs_RELACION, ConexionPos)
            End If
            
        End If
        
    End If
    
    Set TmpNewFrmSuperConsultas = Nothing 'Frm_Super_Consultas = Nothing
    
    Exit Sub
    
Falla_Local:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(ReimprimirConsumos)"
    
End Sub

Public Function ExisteCampoTabla(pCampo As String, pRs) As Boolean
    
    On Error GoTo Errores
    
    A = pRs.Fields(pCampo).Name
    
    ExisteCampoTabla = True
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Public Sub CambiaColorDeLineaGrid(Grid As MSFlexGrid, Color As Long)
    
    Dim LCont As Integer, ColAct As Integer
    
    ColAct = Grid.Col
    
    For LCont = 0 To Grid.Cols - 1
        Grid.Col = LCont
        Grid.CellBackColor = Color
    Next LCont
    
    Grid.Col = ColAct
    
End Sub

Public Function FechaBD(ByVal Expression, Optional pTipo As FechaBDPrecision = FBD_Fecha, Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                Dim dDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYYMMDD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_FULL), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_FULL
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYY-MM-DD HH:mm:ss") '& Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si a�n as� se desea utilizar este formato, volver a llamar a esta funci�n sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function

Public Function MsjErrorRapido(pDescripcion As String, Optional pMsjIntro As String = "[StellarMensaje]")
    
    If pMsjIntro = "[StellarMensaje]" Then
        pMsjIntro = Replace(pMsjIntro, "[StellarMensaje]", StellarMensaje(235)) & vbNewLine & vbNewLine '"Ha ocurrido un error, por favor reporte lo siguiente: " & vbNewLine & vbNewLine
    End If
    
    Call gRutinas.Mensajes(pMsjIntro & pDescripcion)
    
End Function

Public Sub AbrirTeclado(Optional pControl As Object)
    
    If ObjTeclado Is Nothing Then
        
        On Error GoTo Falla_Local
        
        Dim Teclado As Object
        
        Set Teclado = CreateObject("DLLKeyboard.DLLTeclado")
        
        Teclado.ShowKeyboard pControl
        
        Exit Sub
        
Falla_Local:
        
    Else ' Si el Caller asign� ObjTeclado, ejecutamos esa l�gica y reutilizamos c�digo.
        
        ObjTeclado.Parameters.Initialize
        ObjTeclado.Parameters.SetParam "pControl", "Control", pControl
        ObjTeclado.bTeclado_Click
        
    End If
    
End Sub

Public Sub TecladoWindows(Optional pControl As Object)
    ' Implementado por el Caller.
    If Not ObjTeclado Is Nothing Then
        ObjTeclado.Parameters.Initialize
        ObjTeclado.Parameters.SetParam "pControl", "Control", pControl
        ObjTeclado.CmdTecladoTabTip_Click
    End If
End Sub

' Asigna el valor de una variable sin importar si es objeto o no
' ignorando la palabra SET

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    On Error Resume Next
    Set pVar = pValue
    If Err.Number = 0 Then Exit Sub
    pVar = pValue
End Sub

' Sintaxis para obtener un elemento �ndice de un objeto de manera segura,
' Retornando un Valor por Defecto en caso de que el elemento no exista,
' Para evitar errores en tiempo de ejecuci�n y no tener que separar las instrucciones en IFs.

' Ejemplo, Caso Recordset

' Cambiaria:

'If ExisteCampoTabla(Campo, Recordset) Then
'   Variable = Recordset!Campo
'Else
'   Variable = 0
'End If

' Por:

' Variable = sProp(Recordset, Campo, 0)
' pObj(pItem) ser�a equivalente a regresar : pObj.Fields(pItem).Value)

' Todos los par�metros tanto de Entrada/Salida son Variant (Acepta Cualquier Cosa)

Public Function SafeItem(pObj, pItem, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeItem, pObj(pItem)
    Exit Function
Default:
    SafeVarAssign SafeItem, pDefaultValue
End Function

' Mismo prop�sito que la anterior, SafeItem()
' Solo que con la sintaxis para acceder a la propiedad de un objeto
' en vez de a un elemento de una colecci�n.

' Quizas para uso futuro.

Public Function SafeProp(pObj, pProperty, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeProp, CallByName(pObj, pProperty, VbGet)
    Exit Function
Default:
    SafeVarAssign SafeProp, pDefaultValue
End Function

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a un elemento.

Public Sub SafeItemAssign(pObj, pItem, pValue)
    On Error Resume Next
    Set pObj(pItem) = pValue
    If Err.Number = 0 Then Exit Sub
    pObj(pItem) = pValue
End Sub

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a una propiedad.

Public Sub SafePropAssign(pObj, pProperty, pValue)
    On Error Resume Next
    CallByName pObj, pProperty, VbLet, pValue
End Sub

Public Function CambiarImpresora(ByVal pDeviceNameOrPort As String, _
Optional ByVal pReestablecerOriginal As Boolean = False, _
Optional ByVal pPorPuerto As Boolean = False) As Boolean
    
    If Not pReestablecerOriginal And Trim(pDeviceNameOrPort) = vbNullString Then
        Exit Function
    End If
    
    On Error GoTo ErrorSel
    
    Dim TmpPrinter, TmpNameOrPort, TmpMatch As Boolean
    
    Static ImpresoraPredeterminadaOriginal As String
    
    If pReestablecerOriginal Then
        pDeviceNameOrPort = ImpresoraPredeterminadaOriginal
        ImpresoraPredeterminadaOriginal = vbNullString
        Printer.TrackDefault = True
        If Trim(pDeviceNameOrPort) = vbNullString Then
            Exit Function
        End If
    Else
        If pPorPuerto Then
            ImpresoraPredeterminadaOriginal = Printer.Port
        Else
            ImpresoraPredeterminadaOriginal = Printer.DeviceName
        End If
    End If
    
    For Each TmpPrinter In Printers
        
        If pPorPuerto Then
            TmpMatch = (UCase(TmpPrinter.Port) = UCase(pDeviceNameOrPort))
        Else
            TmpMatch = (UCase(TmpPrinter.DeviceName) = UCase(pDeviceNameOrPort))
        End If
        
        If TmpMatch Then
            Printer.TrackDefault = False
            Set Printer = TmpPrinter
            Printer.TrackDefault = False
            CambiarImpresora = True
            Exit For
        End If
        
    Next
    
    Exit Function
    
ErrorSel:
    
End Function

Public Function GetLines(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyLines As Integer
    
    HowManyLines = HowMany
    
    For i = 1 To HowManyLines
        GetLines = GetLines & vbNewLine
    Next i
    
End Function

Public Function GetTab(Optional ByVal HowMany As Long = 1)
    
    Dim HowManyTabs As Integer
    
    HowManyTabs = HowMany
    
    For i = 1 To HowManyTabs
        GetTab = GetTab & vbTab
    Next i
    
End Function

Public Function DebugColumnViewInfo(pObjTabular As Object, _
Optional ByVal pViewDataRow As Long = 0) As String
    
    Dim mStr As String
    
    With pObjTabular
        
        If TypeOf pObjTabular Is ListView Then
            
            For Each ColInfo In pObjTabular.ColumnHeaders
                mStr = mStr & GetLines & Rellenar_SpaceR(ColInfo.Width, 15, " ") & _
                Rellenar_SpaceR(ColInfo.Text, 50, " ") & _
                Rellenar_SpaceR(ColInfo.Alignment, 5, " ")
            Next
            
            DebugColumnViewInfo = IIf(Len(mStr) <> 0, mStr, GetLines)
            
            'Debug.Print DebugColumnViewInfo
        
        ElseIf TypeOf pObjTabular Is MSFlexGrid Then
            
            If .Rows > 0 Then
                
                mTmpRow = .Row
                mTmpCol = .Col
                
                .Row = 0
                
                For i = 0 To .Cols - 1
                    .Col = i
                    mStr = mStr & GetLines & Rellenar_SpaceR(.ColWidth(.Col), 15, " ") & _
                    Rellenar_SpaceR(.Text, 25, " ") & IIf(pViewDataRow <> 0, _
                    Rellenar_SpaceR(.TextMatrix(pViewDataRow, .Col), 15, " "), Empty) & _
                    Rellenar_SpaceR(.CellAlignment, 15, " ") & _
                    Rellenar_SpaceR(.ColAlignment(.Col), 15, " ")
                    'If pViewDataRow <> -1 Then mStr = mStr & Rellenar_SpaceR(.TextMatrix(pViewDataRow, .Col), 50, " ")
                Next
                
                .Row = mTmpRow
                .Col = mTmpCol
                
            End If
            
            DebugColumnViewInfo = IIf(Len(mStr) <> 0, mStr, GetLines)
            
            'Debug.Print DebugColumnViewInfo
        
        End If
        
    End With
    
End Function

Public Function Rellenar_SpaceR(intValue, intDigits, Car)
    '*** ESPACIO A LA DERECHA
    mValorLon = intDigits - Len(intValue)
    Rellenar_SpaceR = intValue & String(IIf(mValorLon < 0, intDigits, mValorLon), Car)
End Function

Public Function BuscarValorBD(ByVal Campo As String, ByVal SQL As String, _
Optional ByVal pDefault = "", Optional pCn As ADODB.Connection) As Variant
    
    On Error GoTo Err_Campo
    
    Dim mRs As ADODB.Recordset: Set mRs = New ADODB.Recordset
    
    mRs.Open SQL, pCn, adOpenStatic, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarValorBD = mRs.Fields(Campo).Value
    Else
        BuscarValorBD = pDefault
    End If
    
    mRs.Close
    
    Exit Function
    
Err_Campo:
    
    'Debug.Print Err.Description
    
    Err.Clear
    BuscarValorBD = pDefault
    
End Function

' Version Estricta de BuscarValorBD. Para cuando el valor a buscar es muy importante _
y no se pueda dar el lujo de reemplazar el dato con valor default por un fallo de conexi�n _
o de otra indole t�cnica. Si se usa esta versi�n sin control de errores, el manejo de errores _
lo debe tener el proceso que llam� a esta funci�n:

Public Function BuscarValorBD_Strict(ByVal Campo As String, ByVal SQL As String, _
Optional ByVal pDefault = "", Optional pCn As ADODB.Connection) As Variant
    
    Dim mRs As ADODB.Recordset: Set mRs = New ADODB.Recordset
    
    mRs.Open SQL, pCn, adOpenStatic, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarValorBD_Strict = mRs.Fields(Campo).Value
    Else
        BuscarValorBD_Strict = pDefault
    End If
    
    mRs.Close
    
    Exit Function
    
End Function

Public Function isDBNull(ByVal pValue, _
Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            isDBNull = pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

' pMonto es Variant en vez de Double ya que Double tiene una capacidad Limitada _
de decimales cuando hay muchos enteros. Es decir, o tiene muchos enteros o muchos decimales _
pero no ambos. En cambio un String / Variant puede convertirse al subtipo Decimal por medio _
de la funci�n CDec(Expression) sin perder la precisi�n del n�mero total.

Public Function FormatoDecimalesDinamicos(ByVal pMonto As Variant, _
Optional ByVal pMinimoDecimales As Integer = 0, _
Optional ByVal pMaxDecimales As Integer = 20) As String
    
    Const MaxDec = 25
    
    'pMaxDecimales = 20 por defecto para que sea preciso pero mejor no inventar con _
    mas decimales que esto, sino algo podr�a explotar.
    
    pMaxDecimales = IIf(pMaxDecimales < MaxDec, pMaxDecimales, MaxDec)
    
    If pMinimoDecimales < 0 Then pMinimoDecimales = 0 ' No permitir loqueras que exploten al m�todo.
    If pMaxDecimales < 0 Then pMaxDecimales = pMinimoDecimales
    
    Dim mFormato As String ', FormatoDec As String
    
    'Round(pMonto - Fix(pMonto), MaxDec) ' Explota. Round aguanta m�ximo hasta 22 decimales.
    
    If pMinimoDecimales = 0 And Round(pMonto - Fix(pMonto), 20) = 0 Then
        FormatoDecimalesDinamicos = Format(pMonto, "###,###,##0")
    Else
        If pMaxDecimales = 0 Then
            mFormato = "###,###,##0"
        Else
            mFormato = "###,###,##0." & _
            IIf(pMinimoDecimales > 0, String(pMinimoDecimales, "0"), Empty) & _
            String(pMaxDecimales - pMinimoDecimales, "#")
        End If
        FormatoDecimalesDinamicos = Format(pMonto, mFormato)
    End If
    
End Function

Public Function ESC_POS_CortarPapelImp( _
Optional ByVal pVarImpresora As String = "") As Boolean
    
    On Error GoTo Error
    
    Dim mDocInfo As WinSpoolDocInfo
    Dim mCadEnviar As String
    Dim lhPrinter As Long, lReturn As Long
    Dim lpcWritten As Long, lDoc As Long, lStart As Long
    Dim mCanal As Integer
    
    If pVarImpresora <> vbNullString Then
        Dim mVarCambioImpresora As Boolean
        mVarCambioImpresora = CambiarImpresora(pVarImpresora)
        If Not mVarCambioImpresora Then 'SeleccionarImpresora(CStr(pVarImpresora)) Then
            Exit Function
        End If
    End If
    
    lReturn = OpenPrinter(Printer.DeviceName, lhPrinter, 0)

    If lReturn = 0 Then Err.Raise 999, , StellarMensaje(809) '"Impresora no v�lida."
    
    mDocInfo.pDocName = "StellarPaperCut" 'vbNullString
    mDocInfo.pOutputFile = vbNullString
    mDocInfo.pDatatype = vbNullString '"RAW"
    
    lDoc = StartDocPrinter(lhPrinter, 1, mDocInfo)
    lStart = StartPagePrinter(lhPrinter)
    
    mCadEnviar = GetLines(4) & Chr(27) & Chr(109) ' A�adir 4 L�neas para que no corte el documento.
    
    lReturn = WritePrinter(lhPrinter, ByVal mCadEnviar, Len(mCadEnviar), lpcWritten)
    
    If lReturn = 0 Then Err.Raise 999, , StellarMensaje(810) '"Error al enviar secuencia de comandos."
    
    ESC_POS_CortarPapel = True
    
Finally:
    
    If CBool(lDoc) Or CBool(lStart) Then
        lReturn = EndPagePrinter(lhPrinter)
        lReturn = EndDocPrinter(lhPrinter)
        lReturn = ClosePrinter(lhPrinter)
    End If
    
    If mVarCambioImpresora Then
        CambiarImpresora Empty, True
    End If
    
    Exit Function
    
Error:
    
    ' Mejor no dar mensaje, si no se tuvieren impresoras que admitan estos comandos
    ' El POS va a dar el mensaje de error a cada rato y ser�a muy molesto...
    ' Dado que el sistema tiene muchas impresoras asociadas:
    ' Imp de Comandas Locales, Remotas, Facturacion, etc...
    ' El error podr�a salir N veces en cada rutina...
    ' gRutinas.Mensajes "Error en la rutina de Corte de Papel. " & err.Description
    
    GoTo Finally
    
End Function

Public Function PathExists(pPath As String, _
Optional ByVal pDirectory As Boolean = False) As Boolean
    
    On Error GoTo Error
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
    
    'If DebugMode Then Mensaje True, "PathExists: " & pPath
    
    'If pDirectory Then
        'If Dir(pPath, vbDirectory) <> Empty Then PathExists = True
        PathExists = GetDirectoryExists(pPath)
    'Else
        If Not PathExists Then
            'If Dir(pPath) <> Empty Then PathExists = True
            PathExists = GetFileExists(pPath)
        End If
    'End If
    
    'If DebugMode Then Mensaje True, "PathExists (" & pPath & "): " & PathExists
    
    Exit Function
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    
    If DebugMode Then
        'Mensaje True, "PathExists() " & mErrorDesc & "(" & mErrorNumber & ")"
    End If
    
End Function

Public Function GetFileExists(ByVal pFile As String) As Boolean
    
    On Error GoTo Error
    
    Static FSO As Object
    
    If FSO Is Nothing Then Set FSO = CreateObject("Scripting.FileSystemObject")
    GetFileExists = FSO.FileExists(pFile)
    
    Exit Function
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    
    If DebugMode Then
        'Mensaje True, "GetFileExists() " & mErrorDesc & "(" & mErrorNumber & ")"
    End If
    
End Function

Public Function GetDirectoryExists(ByVal pDir As String) As Boolean
    
    On Error GoTo Error
    
    Static FSO As Object
    
    If FSO Is Nothing Then Set FSO = CreateObject("Scripting.FileSystemObject")
    GetDirectoryExists = FSO.FolderExists(pDir)
    
    Exit Function
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    
    If DebugMode Then
        'Mensaje True, "GetDirectoryExists() " & mErrorDesc & "(" & mErrorNumber & ")"
    End If
    
End Function

'   ACA SE AGREGA ESTA FUNCI�N CON EL FIN DE GENERAR LAS AUDITORIAS QUE SE REQUIERAN PARA TODOS LOS CASOS.

Public Function InsertarAuditoria(ByVal TipoAuditoria As Long, _
ByVal DescripcionAuditoria As String, _
ByVal DescripcionEvento As String, _
ByVal Ventana As String, _
ByVal TipoObjeto As String, _
ByVal CodigoAfectado As String, _
ByRef Conexion As Object) As Boolean
    
    On Error GoTo Error
    
    Dim DB As ADODB.Connection
    'Set DB = New ADODB.Connection
    Set DB = Conexion
    
    Dim mDB As String
    
    If UCase(SafeProp(DB, "DefaultDatabase", Empty)) = UCase("" & Srv_Remote_BD_POS & "") Then
        mDB = "" & Srv_Remote_BD_ADM & ""
    End If
    
    Dim SQL As String, mRs As ADODB.Recordset
    
    'DB.ConnectionString = Conexion
    'DB.Open
    
    Set mRs = New ADODB.Recordset
    
    mDB = IIf(mDB <> Empty, mDB & ".DBO.", Empty)
    
    SQL = "SELECT * FROM " & mDB & "MA_AUDITORIAS WHERE 1 = 2"
    
    With mRs
        
        .Open SQL, DB, adOpenKeyset, adLockOptimistic, adCmdText
        
        .AddNew
        
        !Cod_Prod = 859 'gCodProducto
        !Nom_Prod = App.Title
        !Tipo = TipoAuditoria
        !Descripcion = DescripcionAuditoria
        !Evento = DescripcionEvento
        '!Fecha = Posee Valor Default GetDate()
        !CodUsuario = POS.UsuarioDelPos.Codigo
        !Usuario = POS.UsuarioDelPos.Descripcion
        !Ventana = Ventana
        !TipoObjAuditado = TipoObjeto
        !CodigoAuditado = CodigoAfectado
        
        .Update
        
        .Close
        
    End With
    
    'DB.Close
    
    InsertarAuditoria = True
    
    Exit Function
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    'If DebugMode Then
        'MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")." & GetLines(2) & "InsertarAuditoria()|" & mErrorSource
    'End If
    
End Function

' Safe Val() - Sin el problema de que Val no soporta numeros formateados.
' Ej: Val("10,000.00") = 10 | SVal("10,000.00") = 10000 ' Como debe ser.
Public Function SVal(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Double = 0) As Double
    On Error Resume Next
    If IsNumeric(pExpression) Then
        SVal = CDbl(pExpression)
    Else
        SVal = pDefaultValue
    End If
End Function

' Min Val()
Public Function MinVal(ByVal pExpression As Variant, _
Optional ByVal pMinVal As Double = 0) As Double
    MinVal = SVal(pExpression, pMinVal)
    MinVal = IIf(MinVal < pMinVal, pMinVal, MinVal)
End Function

' Max Val()
Public Function MaxVal(ByVal pExpression As Variant, _
Optional ByVal pMaxVal As Double = 0) As Double
    MaxVal = SVal(pExpression, MaxVal)
    MaxVal = IIf(MaxVal > pMaxVal, pMaxVal, MaxVal)
End Function

' Min Val > 0
Public Function MinVal0(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Double = 1) As Double
    MinVal0 = SVal(pExpression, pDefaultValue)
    MinVal0 = IIf(MinVal0 > 0, MinVal0, pDefaultValue)
End Function

Public Function SDec(ByVal pExpression As Variant, _
Optional ByVal pDefaultValue As Variant = 0) As Variant
    On Error Resume Next
    If IsNumeric(pExpression) Then
        SDec = CDec(pExpression)
    Else
        SDec = CDec(pDefaultValue)
    End If
End Function

Public Function ExisteCampoTablaV2(ByVal pTabla As String, ByVal pCampo As String, _
pCn As ADODB.Connection, _
Optional ByVal pBD As String = "", _
Optional ByVal pTipoCampoVerificar As String = "", _
Optional ByVal pCampoTextoCriterioCaracteres As String = "", _
Optional ByVal pCampoNumeroCriterioPrecisionEntera As String = "", _
Optional ByVal pCampoNumeroCriterioPrecisionDecimal As String = "", _
Optional ByVal pCampoDateTimeCriterioPrecision As String = "" _
) As Boolean
    
    ' Ej pTipoCampoVerificar: "char", "varchar", "text", "numeric", "float", "int", "date", "time", "datetime", _
    "ntext", "nchar", "nvarchar", "bigint", "binary", "varbinary", "bit", "money", "real", "xml", etc...
    
    ' Ej pCampoTextoCriterioCaracteres: " = 50", " <= 100", " >= 255", etc..
    
    ' Ej pCampoNumeroCriterioPrecisionEntera: " >= 10", " = 18", etc
    
    ' Ej pCampoNumeroCriterioPrecisionDecimal: " <= 2", " = 4", " = 0", "IS NULL", "IS NOT NULL", etc
    
    ' Ej pCampoDateTimeCriterioPrecision: " = 3", " = 0", etc...
    
    ' CHARACTER_MAXIMUM_LENGTH = -1 CUANDO EL CAMPO ES NVARCHAR(MAX). PARA NO ENTRAR EN CONFLICTO
    ' CON VALIDACION DE LONGITUD POR DEBAJO DE pCampoTextoCriterioCaracteres, ASUMIREMOS MAX COMO 999999
    ' SI QUEREMOS SABER SI UN CAMPO TIENE NVARCHAR(MAX) PASAMOS pCampoTextoCriterioCaracteres = 999999
    
    On Error Resume Next
    
    Dim pRs As ADODB.Recordset, mSQL As String, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    
    mSQL = "SELECT Column_Name FROM " & mBD & "INFORMATION_SCHEMA.COLUMNS " & _
    "WHERE Table_Name = '" & pTabla & "' AND Column_Name = '" & pCampo & "'" & _
    IIf(Len(pTipoCampoVerificar) > 0, " AND DATA_TYPE = '" & pTipoCampoVerificar & "'", "") & _
    IIf(Len(pCampoTextoCriterioCaracteres) > 0, " AND CASE WHEN CHARACTER_MAXIMUM_LENGTH = -1 THEN 999999 ELSE CHARACTER_MAXIMUM_LENGTH END " & pCampoTextoCriterioCaracteres, "") & _
    IIf(Len(pCampoNumeroCriterioPrecisionEntera) > 0, " AND NUMERIC_PRECISION " & pCampoNumeroCriterioPrecisionEntera, "") & _
    IIf(Len(pCampoNumeroCriterioPrecisionDecimal) > 0, " AND NUMERIC_SCALE " & pCampoNumeroCriterioPrecisionDecimal, "") & _
    IIf(Len(pCampoDateTimeCriterioPrecision) > 0, " AND DATETIME_PRECISION " & pCampoDateTimeCriterioPrecision, "")
    
    Set pRs = pCn.Execute(mSQL)
    
    ExisteCampoTablaV2 = Not (pRs.EOF And pRs.BOF)
    
    pRs.Close
    
End Function

Public Sub Apertura_Recordset(Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Rec.CursorLocation = adUseServer
End Sub

Public Sub Cerrar_Recordset(Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Set Rec = Nothing
End Sub

Public Sub Apertura_RecordsetC(ByRef Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Rec.CursorLocation = adUseClient
End Sub

Public Function Fecha() As String
'    If SRV_LOCAL <> "HGUERRERO" Then
        Fecha = Format(Year(Date), aaaa) & "-" & Format(Month(Date), "00") & "-" & Format(Day(Date), "00")
'    Else
'        FECHA = Day(Date) & "/" & Month(Date) & "/" & Year(Date)
'    End If
End Function

Public Function RoundUp(ByVal pNum As Variant, Optional ByVal pDecimals As Long) As Double
    RoundUp = CDbl(FormatNumber(pNum, pDecimals, vbTrue, vbFalse, vbFalse))
End Function

Public Function RoundDecimalUp(ByVal pNum As Variant, Optional ByVal pDecimals As Long) As Variant ' Para Max. Precisi�n.
    RoundDecimalUp = CDec(FormatNumber(pNum, pDecimals, vbTrue, vbFalse, vbFalse))
End Function

Public Function QuitarComillasDobles(ByVal pCadena As String) As String
    QuitarComillasDobles = Replace(pCadena, Chr(34), "")
End Function

Public Function QuitarComillasSimples(ByVal pCadena As String) As String
    QuitarComillasSimples = Replace(pCadena, Chr(39), "")
End Function

Public Function LimitarComillasSimples(ByVal pCadena As String) As String
    
    Dim Cad As String
    
    Cad = pCadena
    
    Do While (Cad Like "*''*")
        Cad = Replace(Cad, "''", "'")
    Loop
    
    LimitarComillasSimples = Cad
    
End Function

Public Function FixTSQL(ByVal pCadena As String) As String
    FixTSQL = Replace(pCadena, "'", "''")
End Function

' Formatea Fecha Corta o Larga si incluye hora, seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function GDate(ByVal pDate) As String: On Error Resume Next: GDate = FormatDateTime(pDate, vbGeneralDate): End Function

' Formatea Fecha Corta seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function SDate(ByVal pDate) As String: On Error Resume Next: SDate = FormatDateTime(pDate, vbShortDate): End Function

' Formatea Hora Corta o Larga si incluye hora, seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function GTime(ByVal pDate) As String: On Error Resume Next: GTime = FormatDateTime(pDate, vbLongTime): End Function

' Formatea Fecha Corta seg�n configuraci�n regional. _
Solo debe usarse en capa de presentaci�n, no en consultas a bases de datos !

Public Function STime(ByVal pDate) As String: On Error Resume Next: STime = FormatDateTime(pDate, vbShortTime): End Function

Public Function ExisteTablaV3(ByVal pTabla As String, _
pCn As Variant, _
Optional ByVal pBD As String = "") As Boolean
    ' pCn > Variant / Object / ADODB.Connection
    On Error Resume Next
    Dim pRs As ADODB.Recordset, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    Set pRs = pCn.Execute("SELECT Table_Name FROM " & mBD & "INFORMATION_SCHEMA.TABLES WHERE Table_Name = '" & pTabla & "'")
    ExisteTablaV3 = Not (pRs.EOF And pRs.BOF)
    pRs.Close
End Function

Public Function FactorMonedaTurno(ByVal pCodMoneda, ByVal pTurno, ByVal pCaja, pCn As ADODB.Connection) As Variant
    
    Resultado = CDec(BuscarValorBD_Strict("FactorEncontrado", _
    "SELECT isnull(sum((nu_MONTO - nu_FALTA) * ns_factor ) / sum((nu_MONTO - nu_FALTA)),1) as FactorEncontrado " & _
    "From TR_VENTAS_POS_PAGOS " & _
    "where cu_Denominacion =  'Efectivo' AND (nu_Monto - nu_Falta) > 0 " & _
    "AND cs_CAJA= '" & pCaja & "' AND ns_TURNO = " & pTurno & " and cu_MONEDA = '" & pCodMoneda & "' ", _
    1, pCn))
    
    FactorMonedaTurno = Resultado
    
End Function

Public Function FactorMonedaHistorico(ByVal pCodMoneda As String, ByVal pFecha As Date, _
pCn As ADODB.Connection) As Variant
    
    Dim CodigoMoneda As String
    
    CodigoMoneda = pCodMoneda
    
    'If ExisteTablaV3("MA_HISTORICO_MONEDAS", Ent.BDD) Then
        
        mFactorDestino = CDec(BuscarValorBD_Strict("FactorEncontrado", _
        "SELECT TOP 1 isNULL(HIS.n_FactorPeriodo, MON.n_Factor) AS FactorEncontrado " & _
        "FROM MA_MONEDAS MON LEFT JOIN MA_HISTORICO_MONEDAS HIS " & _
        "ON MON.c_CodMoneda = HIS.c_CodMoneda " & _
        "AND HIS.d_FechaCambioActual <= '" & FechaBD(pFecha, FBD_FULL) & "' " & _
        "WHERE MON.c_CodMoneda = '" & FixTSQL(CodigoMoneda) & "' " & _
        "ORDER BY HIS.d_FechaCambioActual DESC, HIS.ID DESC", 1, pCn))
        
    'Else
        
        'mFactorDestino = CDec(BuscarValorBD("FactorEncontrado", _
        "SELECT TOP 1 MON.n_Factor AS FactorEncontrado " & _
        "FROM MA_MONEDAS MON " & _
        "WHERE MON.c_CodMoneda = '" & QuitarComillasSimples(CodigoMoneda) & "' ", 1))
        
    'End If
    
    FactorMonedaHistorico = mFactorDestino
    
End Function

Public Function AsEnumerable(ArrayList As Variant) As Collection
    
    Set AsEnumerable = New Collection
    
    On Error Resume Next
    
    For i = LBound(ArrayList) To UBound(ArrayList)
        AsEnumerable.Add ArrayList(i), CStr(i)
    Next i
    
End Function

Public Sub SafeFocus(ByVal pObj As Object)
    On Error Resume Next
    If gRutinas.PuedeObtenerFoco(pObj) Then pObj.SetFocus
End Sub
