VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form Form_Ficha_Fondo 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6900
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   8985
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6900
   ScaleWidth      =   8985
   StartUpPosition =   1  'CenterOwner
   Visible         =   0   'False
   Begin VB.TextBox txt_FondoReal 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   390
      Left            =   5760
      MultiLine       =   -1  'True
      TabIndex        =   6
      Top             =   1560
      Width           =   3105
   End
   Begin VB.PictureBox MenuPrincipal 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   0
      ScaleHeight     =   1050
      ScaleWidth      =   8970
      TabIndex        =   3
      Top             =   360
      Width           =   9000
      Begin VB.Frame BarraHerramientas 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1080
         Left            =   30
         TabIndex        =   4
         Top             =   0
         Width           =   8775
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   120
            TabIndex        =   5
            Top             =   120
            Width           =   4395
            _ExtentX        =   7752
            _ExtentY        =   1429
            ButtonWidth     =   1984
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   4
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "(F4) &Grabar"
                  Key             =   "Grabar"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "(F12) &Salir"
                  Key             =   "Salir"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   -6600
      TabIndex        =   0
      Top             =   0
      Width           =   15600
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13395
         TabIndex        =   1
         Top             =   75
         Width           =   1815
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grd 
      Height          =   4575
      Left            =   120
      TabIndex        =   2
      Top             =   2160
      Width           =   8775
      _ExtentX        =   15478
      _ExtentY        =   8070
      _Version        =   393216
      FixedCols       =   0
      RowHeightMin    =   400
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   120
      Top             =   1080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Form_Ficha_Fondo.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Form_Ficha_Fondo.frx":1D92
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Form_Ficha_Fondo.frx":20AE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lbl_MontofOnfo 
      BackStyle       =   0  'Transparent
      Caption         =   "Fondo Real"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   375
      Left            =   4320
      TabIndex        =   7
      Top             =   1560
      Width           =   1215
   End
End
Attribute VB_Name = "Form_Ficha_Fondo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private LcLinea As Integer

Public ClaseRutinas             As New cls_Rutinas
Public ClaseCierres             As New Cls_Cierres

Public LCaja                    As String
Public LCajero                  As String
Public LTurno                   As Double
Public LLocalidad               As String

Public pb_Montofondo As Double
Public pb_Cambio As Boolean

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If Shift = vbAltMask Then
        
        Select Case KeyCode
            
            Case Is = vbKeyG                'ALT+D
                
                Call Form_KeyDown(vbKeyF4, 0)
                
            Case Is = vbKeyA                'ALT+A
                
            Case Is = vbKeyS                'ALT+S
                
                Call Form_KeyDown(vbKeyF12, 0)
                
        End Select
        
    End If
    
    Select Case KeyCode
        
        Case Is = vbKeyF12
            
            pb_Cambio = False
            Unload Me
            
        Case Is = vbKeyF4
            
            pb_Cambio = True
            Call Grabar
            
    End Select
    
End Sub

Private Sub Grabar()
    
    On Error GoTo Lc_Error
    
    Dim ActiveTrans As Boolean, AffectedRecords
        
    ConexionPos.BeginTrans
    
    ActiveTrans = True
   
    Lc_TrMontoFondo = 0
    
    For X = 1 To grd.Rows - 2
        
        With grd
            
            .Row = X
            
            .Col = 2
            Lc_Monto = CDec(.Text) 'fondo en moneda original
            
            .Col = 0
            Lc_Moneda = .Text
            
            .Col = 5
            Lc_Id = .Text
            
            .Col = 3
            Lc_Factor = CDec(.Text) ' factor inicial
            
            If Lc_Id <> 0 Then
                
                SQL = "UPDATE TR_CAJA_FONDO SET " & _
                "n_MontoFondo = " & Lc_Monto & ", " & _
                "n_Factor = " & Lc_Factor & " " & _
                "WHERE Turno =  " & LTurno & " " & _
                "AND c_Cajero = '" & LCajero & "' " & _
                "AND c_CodCaja = '" & LCaja & "' " & _
                "AND c_CodMoneda = '" & Lc_Moneda & "' " & _
                "AND c_CodDenomina = 'Efectivo' " & _
                "AND ID = " & CDec(Lc_Id) & " "
                
            Else
                
                SQL = "INSERT INTO TR_CAJA_FONDO " & _
                "([c_CodCaja], [c_Cajero], [Turno], [c_CodMoneda], [c_CodDenomina], " & _
                "[n_Factor], [n_FondoInicial], [n_MontoFondo]) " & _
                "VALUES ( " & _
                "'" & LCaja & "', '" & LCajero & "', " & _
                "" & CDec(LTurno) & ", '" & Lc_Moneda & "', 'Efectivo', " & _
                "" & CDec(Lc_Factor) & ", 0, " & CDec(Lc_Monto) & ") "
                
            End If
            
            Lc_TrMontoFondo = Lc_TrMontoFondo + (Lc_Monto * Lc_Factor)
            
            ConexionPos.Execute SQL, RowsAffected
            
        End With
        
    Next
    
    ConexionPos.Execute _
    "UPDATE TR_CAJA SET " & _
    "nu_MontoFondo = " & CDec(Lc_TrMontoFondo) & " " & _
    "WHERE Turno = " & LTurno & " " & _
    "AND c_Codigo = '" & LCaja & "' " & _
    "AND c_Cajero = '" & LCajero & "' ", RowsAffected
    
    ConexionPos.CommitTrans
    
    ActiveTrans = False
    
    pb_Montofondo = CDec(Lc_TrMontoFondo)
    
    Unload Me
    
    Exit Sub
    
Lc_Error:
    
    'Resume ' Debug
    
    If ActiveTrans Then
        ConexionPos.RollbackTrans
        ActiveTrans = False
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Grabar)"
    
End Sub

Private Sub Form_Load()
    
    Dim TempFondoCaja As New ADODB.Recordset
    
    pb_Cambio = False

    Call Apertura_Recordset(TempFondoCaja)
    
    SQL = "select isnull(t.id,0) as ID, isnull(t.n_MontoFondo, 0) as n_MontoFondo, m.c_codmoneda, d.c_coddenomina, " & _
    " isnull(t.n_factor,m.n_factor) as Nfactor , m.n_factor as FactorActual , m.c_descripcion , " & _
    " d.c_denominacion from " & Srv_Remote_BD_ADM & "..MA_MONEDAS m inner  join " & Srv_Remote_BD_ADM & "..MA_DENOMINACIONES d " & _
    " on m.c_codmoneda = d.c_codmoneda left join TR_CAJA_FONDO t on t.c_CodMoneda = m.c_codmoneda " & _
    " and d.c_coddenomina = t.c_CodDenomina and t.Turno = " & LTurno & " and t.c_CodDenomina = 'Efectivo'" & _
    " and t.c_Cajero = '" & LCajero & "' and t.c_CodCaja = '" & LCaja & "'" & _
    " Where d.c_coddenomina = 'Efectivo' and m.bUsoEnPOS = 1 "

    TempFondoCaja.Open SQL, ConexionPos, adOpenForwardOnly, adLockPessimistic, adCmdText
    
    With grd
        .Clear
        .Rows = 1
        .Cols = 8
        .RowHeightMin = 550
        .Width = 8775
        .AllowUserResizing = flexResizeColumns
        .WordWrap = True
    End With
    
    Call ClaseRutinas.MSGridAsign(grd, 0, 0, UCase(StellarMensaje(142)), 2300, flexAlignCenterCenter)  '"CODIGO"
    Call ClaseRutinas.MSGridAsign(grd, 0, 1, StellarMensaje(10051), 3000, flexAlignCenterCenter) '"MONEDA"
    Call ClaseRutinas.MSGridAsign(grd, 0, 2, UCase(StellarMensaje(10221)), 3000, flexAlignCenterCenter)  '"FONDO"
    Call ClaseRutinas.MSGridAsign(grd, 0, 3, "NFactor", 0, flexAlignCenterCenter)
    Call ClaseRutinas.MSGridAsign(grd, 0, 4, "FactorActual", 0, flexAlignCenterCenter)
    Call ClaseRutinas.MSGridAsign(grd, 0, 5, "ID", 0, flexAlignCenterCenter)
    Call ClaseRutinas.MSGridAsign(grd, 0, 6, "CodDenomina", 0, flexAlignCenterCenter)
    Call ClaseRutinas.MSGridAsign(grd, 0, 7, "Denominacion", 0, flexAlignCenterCenter)
    
    fila = 1
    
    While Not TempFondoCaja.EOF
        
        With grd
        
        .Rows = .Rows + 1
            
            .Row = fila
            
            .Col = 0 'MONEDA
            .CellAlignment = flexAlignLeftCenter
            .Text = TempFondoCaja!c_CodMoneda
            
            .Col = 1 'Descripcion moneda
            .Text = TempFondoCaja!c_Descripcion
            
            .Col = 2 'Fondo REAL
            .Text = FormatoDecimalesDinamicos(CDec(TempFondoCaja!n_MontoFondo), 2)
            
            .Col = 3 'factor inicial
            If ClaseCierres.TasaHistoricaMoneda Is Nothing Then
                GoTo DefaultFac
            Else
                If ClaseCierres.TasaHistoricaMoneda.Exists(UCase(TempFondoCaja!c_CodMoneda)) Then
                    .Text = FormatoDecimalesDinamicos( _
                    CDec(ClaseCierres.TasaHistoricaMoneda(UCase(TempFondoCaja!c_CodMoneda))), 0)
                Else
DefaultFac:
                    .Text = FormatoDecimalesDinamicos(CDec(TempFondoCaja!nFactor), 0)
                End If
            End If
            
            .Col = 4 'Factor moneda
            .Text = FormatoDecimalesDinamicos(CDec(TempFondoCaja!FactorActual), 0)
            
            .Col = 5 'id tr_caja_fondo
            .Text = FormatoDecimalesDinamicos(CDec(TempFondoCaja!Id), 2)
            
            .Col = 6 'codigo denominacion
            .Text = TempFondoCaja!c_CodDenomina
            
            .Col = 7 'descripcion denominacion
            .Text = TempFondoCaja!c_Denominacion
                    
        End With
        
        TempFondoCaja.MoveNext
        
        fila = fila + 1
        
    Wend
    
    TempFondoCaja.Close
    
End Sub

Private Sub grd_Click()
    With grd
        LcLinea = .Row
        .Col = 2
        Me.txt_FondoReal = FormatoDecimalesDinamicos(SDec(.Text), 2)
        SafeFocus txt_FondoReal
        Me.txt_FondoReal.SelStart = 0
        Me.txt_FondoReal.SelLength = Len(Me.txt_FondoReal.Text)
    End With
End Sub

Private Sub grd_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Call grd_Click
    End If
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case Button.Key
        
        Case Is = "Salir"
            
            Call Form_KeyDown(vbKeyF12, 1)
            
         Case Is = "Grabar"
            
            Call Form_KeyDown(vbKeyF4, 1)
            
    End Select
    
End Sub

Private Sub txt_FondoReal_KeyPress(KeyAscii As Integer)
    
    If LcLinea < 1 Then
        Exit Sub
    End If
    
    If KeyAscii = vbKeyReturn Then
        With grd
            .Row = LcLinea
            .Col = 2
            .Text = FormatoDecimalesDinamicos(txt_FondoReal.Text, 2)
            .SetFocus
        End With
    End If
    
End Sub
