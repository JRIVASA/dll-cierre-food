VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form frm_facPendienteProcesar 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7845
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   7155
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frm_facPendienteProcesar.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7845
   ScaleWidth      =   7155
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   1935
      Left            =   240
      TabIndex        =   6
      Top             =   720
      Width           =   6615
      Begin VB.CheckBox chk_todos 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Seleccionar todos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3960
         TabIndex        =   7
         Top             =   1440
         Width           =   2055
      End
      Begin VB.Label Label2 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1320
         TabIndex        =   11
         Top             =   720
         Width           =   4335
      End
      Begin VB.Label Label1 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1320
         TabIndex        =   10
         Top             =   240
         Width           =   4095
      End
      Begin VB.Label lbl_caja 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Caja:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   360
         TabIndex        =   9
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label lbl_Turno 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Turno:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   360
         TabIndex        =   8
         Top             =   720
         Width           =   1815
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Documentos Pendientes por Imprimir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   4515
         TabIndex        =   4
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Salir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   960
      Left            =   5640
      Picture         =   "frm_facPendienteProcesar.frx":74F2
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   6720
      Width           =   1215
   End
   Begin VB.CommandButton cmd_Cambiar 
      Caption         =   "Cambiar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   960
      Left            =   4320
      Picture         =   "frm_facPendienteProcesar.frx":9274
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   6720
      Width           =   1215
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   3495
      Left            =   240
      TabIndex        =   0
      Top             =   3000
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   6165
      _Version        =   393216
      Cols            =   5
      FixedCols       =   0
      RowHeightMin    =   350
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      GridLinesFixed  =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frm_facPendienteProcesar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public CajaDato As String
Public LocalidadDato As String
Public TurnoDato As String

Public Enum Frm_FacPendienteProcesar_OrigenDocumento
    FFPP_BIZ
    FFPP_POS
    FFPP_FOOD
End Enum

Private OrigenFac As Frm_FacPendienteProcesar_OrigenDocumento

Private Enum Columnas
    numero
    Cliente
    ConceptoCol
    Seleccionado
    Valor
End Enum

Public Property Let OrigenDocumento(pValor As Frm_FacPendienteProcesar_OrigenDocumento)
    OrigenFac = pValor
End Property

Private Sub chk_todos_Click()
    If chk_todos.Value = 1 Then
        For i = 1 To Grid.Rows - 2
            Grid.TextMatrix(i, Columnas.Seleccionado) = Chr(254)
            Grid.TextMatrix(i, Columnas.Valor) = 1
        Next i
    Else
        For i = 1 To Grid.Rows - 2
            Grid.TextMatrix(i, Columnas.Seleccionado) = Chr(168)
            Grid.TextMatrix(i, Columnas.Valor) = 0
        Next i
    End If
    'DoEvents
End Sub

Private Sub cmd_Cambiar_Click()
    
    Dim HizoBegin As Boolean
    
    On Error GoTo Error
    
    If OrigenFac = FFPP_BIZ Then
        ConexionAdm.BeginTrans
    ElseIf OrigenFac = FFPP_POS Or OrigenFac = FFPP_FOOD Then
        ConexionPos.BeginTrans
    End If
    
    HizoBegin = True
    
    For i = 1 To Grid.Rows - 2
        
        If Grid.TextMatrix(i, Columnas.Valor) = 1 Then ''CAMBIO
            
            If OrigenFac = FFPP_BIZ Then
                
                SQL = "UPDATE MA_VENTAS SET bu_Impresa = 1 WHERE c_Documento = '" & Grid.TextMatrix(i, Columnas.numero) & "' AND " & _
                "c_Concepto = '" & Grid.TextMatrix(i, Columnas.ConceptoCol) & "' AND c_CodLocalidad = '" & Sucursal & "' "
                
                ConexionAdm.Execute SQL
                
                InsertarAuditoria 51, "Doc. Fiscal Marcaje Manual ADM", "Documento " & Me.LocalidadDato & " " & _
                Grid.TextMatrix(i, Columnas.ConceptoCol) & " " & Grid.TextMatrix(i, Columnas.numero) & " sin imprimir fiscalmente. " & _
                "Marcado por el usuario [" & POS.UsuarioDelPos.Codigo & "][" & POS.UsuarioDelPos.Descripcion & "].", "Frm_FacPendienteProcesar.Cmd_Cambiar_Click()", _
                Grid.TextMatrix(i, Columnas.ConceptoCol), Grid.TextMatrix(i, Columnas.numero), ConexionAdm
                
            ElseIf OrigenFac = FFPP_POS Then
                
                SQL = "UPDATE MA_PAGOS SET bs_Impresa = 1 WHERE c_Numero = '" & Grid.TextMatrix(i, Columnas.numero) & "' AND " & _
                "Turno = '" & CDec(Me.TurnoDato) & "' AND c_Caja = '" & Me.CajaDato & "' AND " & _
                "c_Concepto = '" & Grid.TextMatrix(i, Columnas.ConceptoCol) & "' AND c_Sucursal = '" & Me.LocalidadDato & "' "
                
                ConexionPos.Execute SQL
                
                InsertarAuditoria 52, "Doc. Fiscal Marcaje Manual POS", "Documento " & Me.LocalidadDato & " " & _
                Grid.TextMatrix(i, Columnas.ConceptoCol) & " " & Grid.TextMatrix(i, Columnas.numero) & " sin imprimir fiscalmente. " & _
                "Marcado por el usuario [" & POS.UsuarioDelPos.Codigo & "][" & POS.UsuarioDelPos.Descripcion & "].", "Frm_FacPendienteProcesar.Cmd_Cambiar_Click()", _
                Grid.TextMatrix(i, Columnas.ConceptoCol), Grid.TextMatrix(i, Columnas.numero), ConexionPos
                
            ElseIf OrigenFac = FFPP_FOOD Then
                
                SQL = "UPDATE MA_VENTAS_POS SET bu_Impresa = 1 WHERE cs_Documento = '" & Grid.TextMatrix(i, Columnas.numero) & "' AND " _
                & "nu_Turno = '" & CDec(Me.TurnoDato) & "' AND cs_Caja = '" & Me.CajaDato & "' AND " _
                & "cs_Tipo_Documento = '" & Grid.TextMatrix(i, Columnas.ConceptoCol) & "' AND cs_Localidad = '" & Me.LocalidadDato & "' "
                
                ConexionPos.Execute SQL
                
                InsertarAuditoria 53, "Doc. Fiscal Marcaje Manual FOOD", "Documento " & Me.LocalidadDato & " " & _
                Grid.TextMatrix(i, Columnas.ConceptoCol) & " " & Grid.TextMatrix(i, Columnas.numero) & " sin imprimir fiscalmente. " & _
                "Marcado por el usuario [" & POS.UsuarioDelPos.Codigo & "][" & POS.UsuarioDelPos.Descripcion & "].", "Frm_FacPendienteProcesar.Cmd_Cambiar_Click()", _
                Grid.TextMatrix(i, Columnas.ConceptoCol), Grid.TextMatrix(i, Columnas.numero), ConexionPos
                
            End If
            
        End If
        
    Next i
    
    If OrigenFac = FFPP_BIZ Then
        ConexionAdm.CommitTrans
    ElseIf OrigenFac = FFPP_POS Or OrigenFac = FFPP_FOOD Then
        ConexionPos.CommitTrans
    End If
    
    Call gRutinas.Mensajes(StellarMensaje(346)) '"Proceso Finalizado")
    
    Unload Me
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Frm_FacPendienteProcesar_Click)"
    
    If HizoBegin Then
        If OrigenFac = FFPP_BIZ Then
            ConexionAdm.RollbackTrans
        ElseIf OrigenFac = FFPP_POS Or OrigenFac = FFPP_FOOD Then
            ConexionPos.RollbackTrans
        End If
    End If
    
End Sub

Private Sub Command2_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    lbl_Organizacion.Caption = Stellar_Mensaje(2040)
    
    If OrigenFac = FFPP_BIZ Then
        Label1.Visible = False
        Label2.Visible = False
        lbl_caja.Visible = False
        lbl_Turno.Visible = False
    ElseIf OrigenFac = FFPP_POS Or OrigenFac = FFPP_FOOD Then
        Label1.Caption = CajaDato
        Label2.Caption = TurnoDato
    End If
    
    Call PrepararGrid
    Call CargarDatos
    
    lbl_caja.Caption = Stellar_Mensaje(2017) 'n� caja
    lbl_Turno.Caption = Stellar_Mensaje(2022) 'turno
    chk_todos.Caption = Stellar_Mensaje(2041) 'seleccionar todos
    
    cmd_Cambiar.Caption = Stellar_Mensaje(2042) 'cambiar
    Command2.Caption = Stellar_Mensaje(107) 'salir
    
End Sub

Private Sub PrepararGrid()
    Grid.Clear
    Grid.Rows = 2
    Grid.ColWidth(Columnas.numero) = 1500
    Grid.ColWidth(Columnas.Cliente) = 2700
    Grid.ColWidth(Columnas.ConceptoCol) = 1000
    Grid.ColWidth(Columnas.Seleccionado) = 1000
    Grid.ColWidth(Columnas.Valor) = 0
    Grid.TextMatrix(0, Columnas.numero) = Stellar_Mensaje(2038) 'n� documento
    Grid.TextMatrix(0, Columnas.Cliente) = Stellar_Mensaje(2043) 'cliente
    Grid.TextMatrix(0, Columnas.Seleccionado) = Stellar_Mensaje(2044) 'selec.
    Grid.TextMatrix(0, Columnas.ConceptoCol) = Stellar_Mensaje(2036) 'concepto
    Grid.TextMatrix(0, Columnas.Valor) = Stellar_Mensaje(2045) 'valor
End Sub

Private Sub CargarDatos()
    
    Dim SQL As String, rs As New ADODB.Recordset, Linea As Integer
    
    If OrigenFac = FFPP_BIZ Then
        
        SQL = "SELECT c_Documento AS c_Numero, d_Fecha AS F_Fecha, d_Fecha AS F_Hora, c_Concepto, n_Total, c_Descripcion, 0 AS Turno, '' AS c_Caja " _
        & "FROM MA_VENTAS WHERE c_Concepto IN ('VEN', 'DEV') AND (bu_Impresa = 0) " _
        & "AND c_CodLocalidad = '" & Sucursal & "'"
        rs.Open SQL, ConexionAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
        Linea = 1
        While Not rs.EOF
            Grid.TextMatrix(Linea, Columnas.numero) = rs!c_Documento
            Grid.TextMatrix(Linea, Columnas.Cliente) = rs!c_Descripcion
            Grid.TextMatrix(Linea, Columnas.ConceptoCol) = rs!c_Concepto
            Grid.Col = Columnas.Seleccionado
            Grid.Row = Linea
            Grid.CellFontName = "Wingdings"
            Grid.CellFontSize = 14
            Grid.TextMatrix(Linea, Columnas.Seleccionado) = Chr(168)
            Grid.TextMatrix(Linea, Columnas.Valor) = 0
            rs.MoveNext
            Linea = Linea + 1
            Grid.Rows = Grid.Rows + 1
        Wend
        
    ElseIf OrigenFac = FFPP_POS Then
        
        SQL = "SELECT c_Numero, f_Fecha, f_Hora, c_Concepto, n_Total, c_Desc_Cliente, Turno, c_Caja " _
        & "FROM MA_PAGOS WHERE (bs_Impresa = 0) AND (Turno = '" & CDec(TurnoDato) & "') AND (c_Caja = '" & CajaDato & "') " _
        & "AND c_Sucursal = '" & LocalidadDato & "'"
        rs.Open SQL, ConexionPos, adOpenForwardOnly, adLockReadOnly, adCmdText
        Linea = 1
        While Not rs.EOF
            Grid.TextMatrix(Linea, Columnas.numero) = rs!c_Numero
            Grid.TextMatrix(Linea, Columnas.Cliente) = rs!c_Desc_cliente
            Grid.TextMatrix(Linea, Columnas.ConceptoCol) = rs!c_Concepto
            Grid.Col = Columnas.Seleccionado
            Grid.Row = Linea
            Grid.CellFontName = "Wingdings"
            Grid.CellFontSize = 14
            Grid.TextMatrix(Linea, Columnas.Seleccionado) = Chr(168)
            Grid.TextMatrix(Linea, Columnas.Valor) = 0
            rs.MoveNext
            Linea = Linea + 1
            Grid.Rows = Grid.Rows + 1
        Wend
        
    ElseIf OrigenFac = FFPP_FOOD Then
        
        SQL = "SELECT cs_Documento, ds_Fecha AS f_Fecha, ds_Fecha AS f_Hora, cs_Tipo_Documento, nu_Monto_Total, cu_Nombre_Cliente, nu_Turno, cs_Caja " _
        & "FROM MA_VENTAS_POS WHERE (bu_Impresa = 0) AND (nu_Turno = '" & CDec(TurnoDato) & "') AND (cs_Caja = '" & CajaDato & "') " _
        & "AND cs_Localidad = '" & LocalidadDato & "'"
        rs.Open SQL, ConexionPos, adOpenForwardOnly, adLockReadOnly, adCmdText
        Linea = 1
        While Not rs.EOF
            Grid.TextMatrix(Linea, Columnas.numero) = rs!cs_Documento
            Grid.TextMatrix(Linea, Columnas.Cliente) = rs!cu_Nombre_Cliente
            Grid.TextMatrix(Linea, Columnas.ConceptoCol) = rs!cs_Tipo_Documento
            Grid.Col = Columnas.Seleccionado
            Grid.Row = Linea
            Grid.CellFontName = "Wingdings"
            Grid.CellFontSize = 14
            Grid.TextMatrix(Linea, Columnas.Seleccionado) = Chr(168)
            Grid.TextMatrix(Linea, Columnas.Valor) = 0
            rs.MoveNext
            Linea = Linea + 1
            Grid.Rows = Grid.Rows + 1
        Wend
        
    End If
    
End Sub

Private Sub Grid_Click()
    fila = Grid.Row
    Columna = Grid.Col
    If Grid.TextMatrix(fila, 0) <> "" Then
        Select Case Columna
            Case Columnas.Seleccionado
                If Grid.TextMatrix(fila, Columnas.Valor) = 0 Then 'ESTA DESACTIVADA, ACTIVO
                    Grid.TextMatrix(fila, Columnas.Seleccionado) = Chr(254)
                    Grid.TextMatrix(fila, Columnas.Valor) = 1
                Else 'ESTA ACTIVADA, DESACTIVO
                    Grid.TextMatrix(fila, Columnas.Seleccionado) = Chr(168)
                    Grid.TextMatrix(fila, Columnas.Valor) = 0
                End If
        End Select
    End If
End Sub
