VERSION 5.00
Begin VB.Form Frm_Cierre_Mensual 
   Appearance      =   0  'Flat
   BackColor       =   &H00E7E8E8&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   2895
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   6150
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2895
   ScaleWidth      =   6150
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   4800
      Picture         =   "Frm_Cierre_Mensual.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   1800
      Width           =   1215
   End
   Begin VB.CommandButton aceptar 
      Caption         =   "Aceptar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   3360
      Picture         =   "Frm_Cierre_Mensual.frx":1D82
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   1800
      Width           =   1215
   End
   Begin VB.ComboBox cboAno 
      Appearance      =   0  'Flat
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "1234"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   3082
         SubFormatType   =   0
      EndProperty
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   435
      ItemData        =   "Frm_Cierre_Mensual.frx":3B04
      Left            =   4560
      List            =   "Frm_Cierre_Mensual.frx":3B06
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   960
      Width           =   1485
   End
   Begin VB.ComboBox cboperiodo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   960
      Width           =   4335
   End
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6600
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   75
         Width           =   4095
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   4155
         TabIndex        =   1
         Top             =   75
         Width           =   1815
      End
   End
End
Attribute VB_Name = "Frm_Cierre_Mensual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private ClaseCierres                 As New Cls_Cierres

Private Sub aceptar_Click()
    'Debug.Print cboperiodo.ListIndex + 1
    'Debug.Print cboAno.Text
    'Call ClaseCierres.ImprimirCierreMensual("", Branch, "", "")
    Call ClaseCierres.ImprimirCierreMensual("", Branch, cboAno.Text, cboperiodo.ListIndex + 1, ConexionAdm, ConexionPos)
End Sub

Private Sub cancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    aceptar.Caption = Stellar_Mensaje(5) 'aceptar
    cancelar.Caption = Stellar_Mensaje(6) 'cancelar
    LlenarCombo
End Sub

Private Sub LlenarCombo()
    
    With Me.cboperiodo
        .AddItem Stellar_Mensaje(10164) '"Enero"
        .AddItem Stellar_Mensaje(10165) '"Febrero"
        .AddItem Stellar_Mensaje(10166) '"Marzo"
        .AddItem Stellar_Mensaje(10167) '"Abril"
        .AddItem Stellar_Mensaje(10168) '"Mayo"
        .AddItem Stellar_Mensaje(10169) '"Junio"
        .AddItem Stellar_Mensaje(10170) '"Juio"
        .AddItem Stellar_Mensaje(10171) '"Agosto"
        .AddItem Stellar_Mensaje(10172) '"Septiembre"
        .AddItem Stellar_Mensaje(10173) '"Octubre"
        .AddItem Stellar_Mensaje(10174) '"Noviembre"
        .AddItem Stellar_Mensaje(10175) '"Diciembre"
    End With
        
    Dim AnoAct As String
    Dim AnoAnt As String
    
    AnoAct = Year(Date)
    AnoAnt = Year(Date) - 1
        
    With Me.cboAno
        .AddItem AnoAct, 0
        .AddItem AnoAnt, 1
        .AddItem Year(Date) - 2, 2 'Pruebas y para ser flexibles
        .ListIndex = 0
    End With
    
    cboperiodo.ListIndex = 0
    
End Sub
