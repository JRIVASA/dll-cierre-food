VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form Frm_Tips 
   Appearance      =   0  'Flat
   BackColor       =   &H00E7E8E8&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9165
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15315
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9165
   ScaleWidth      =   15315
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmd_aceptar 
      Caption         =   "&Aceptar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1110
      Left            =   12720
      Picture         =   "Frm_Tips.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   7920
      Visible         =   0   'False
      Width           =   1185
   End
   Begin VB.CommandButton cmd_Salir 
      Caption         =   "&Cancelar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1110
      Left            =   14040
      Picture         =   "Frm_Tips.frx":507F
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   7920
      Width           =   1185
   End
   Begin MSComctlLib.ListView lvTips 
      Height          =   7215
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   15100
      _ExtentX        =   26644
      _ExtentY        =   12726
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   10
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Documento"
         Object.Width           =   3881
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Caja"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Servicio N�"
         Object.Width           =   3881
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Fecha"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "N� Transaccion"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Text            =   "Monto"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Text            =   "Tip"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "CodDenomina"
         Object.Width           =   2
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Object.Width           =   0
      EndProperty
   End
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13275
         TabIndex        =   2
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   75
         Width           =   4095
      End
   End
End
Attribute VB_Name = "Frm_Tips"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fConexionPos         As Object
Public fConexionAdm         As Object
Public fBranch              As String
Public fCajero              As String
Public fCaja                As String
Public fTurno               As Double

Private ClaseDenominaciones As New cls_Denominaciones
Private ClaseMonedas        As New cls_Monedas

Private Sub cmd_Salir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    On Error Resume Next
    
    Set gDllVerificacion = CreateObject("VerificaElectron.cls_verificaEletronica")
    
    BuscarTipsPendientes
    
    gVerificacion.Verificacion = 1
    gVerificacion.Consorcio = 4
    
    Me.Width = 15350
    lvTips.Width = 15100
    
    cmd_aceptar.Caption = Stellar_Mensaje(5) 'aceptar
    cmd_Salir.Caption = Stellar_Mensaje(6) 'cancelar
    
    lvTips.ColumnHeaders(1).Text = Stellar_Mensaje(10016) 'documento
    lvTips.ColumnHeaders(2).Text = Stellar_Mensaje(2017) 'caja
    lvTips.ColumnHeaders(3).Text = Stellar_Mensaje(10026) & " N�" 'servicio n�
    lvTips.ColumnHeaders(4).Text = Stellar_Mensaje(128) 'fecha
    lvTips.ColumnHeaders(5).Text = Stellar_Mensaje(10163) 'n� transaccion
    lvTips.ColumnHeaders(6).Text = Stellar_Mensaje(2517) 'monto
    'lvTips.ColumnHeaders(7).Text = "" 'codDenomina
    
End Sub

Private Sub lvTips_DblClick()
    ProcesarTip
End Sub

Sub BuscarTipsPendientes()
    
    On Error Resume Next
    
    Dim rsFacturas As New ADODB.Recordset
    Dim lItem As ListItem
    Dim mSQL As String
    
    mSQL = "SELECT p.cs_CAJA, p.cs_DOCUMENTO, p.cs_CODIGOSERVICIO, p.ds_FECHA, pp.cu_NUMERO, pp.nu_MONTO, pp.cu_DENOMINACION,nu_MONTO_SERVICIO,cu_operador,cu_clave,nu_tip,pp.id " _
    & "FROM TR_CAJA t INNER JOIN MA_VENTAS_POS p ON t.cs_CAJA = p.cs_CAJA AND t.ns_TURNO = p.nu_TURNO " _
    & "INNER JOIN TR_VENTAS_POS_PAGOS pp ON p.cs_CAJA = pp.cs_CAJA AND p.cs_DOCUMENTO = pp.cs_DOCUMENTO AND p.nu_TURNO = pp.ns_TURNO " _
    & "AND p.cs_TIPO_DOCUMENTO = pp.cs_TIPO_DOCUMENTO INNER JOIN " & Srv_Remote_BD_ADM & ".DBO.MA_DENOMINACIONES d ON pp.cu_DENOMINACION = d.c_coddenomina " _
    & "WHERE (p.cs_usuario='" & fCajero & "') and (p.CS_CAJA='" & fCaja & "') AND (P.NU_TURNO=" & CDec(fTurno) & ") " _
    & "and (d.c_denominacion LIKE '%CREDIT%') AND (d.nu_verificacionElectronica = 1)  "
    
    Call ClaseRutinas.AbrirConexion(fConexionPos, ConexionPos.ConnectionString)
    
    lvTips.ListItems.Clear
    
    rsFacturas.CursorLocation = adUseClient
    rsFacturas.Open mSQL, fConexionPos, adOpenForwardOnly, adLockReadOnly, adCmdText
    rsFacturas.Sort = "cs_documento"
    
    Do While Not rsFacturas.EOF
        
        Set lItem = lvTips.ListItems.Add(, , rsFacturas!cs_Documento)
        
        lItem.SubItems(1) = rsFacturas!cs_Caja
        lItem.SubItems(2) = rsFacturas!cs_CodigoServicio
        lItem.SubItems(3) = rsFacturas!ds_Fecha
        lItem.SubItems(4) = rsFacturas!cu_Numero
        lItem.SubItems(5) = Format(rsFacturas!nu_Monto, "Standard")
        lItem.SubItems(6) = Format(rsFacturas!nu_tip, "Standard")
        lItem.SubItems(7) = rsFacturas!cu_Denominacion
        lItem.SubItems(8) = rsFacturas!cu_Clave
        lItem.SubItems(9) = rsFacturas!Id
        
        If rsFacturas!nu_tip > 0 Then
            lItem.ForeColor = vbRed
            For i = 1 To 7
                 lItem.ListSubItems(i).ForeColor = vbRed
            Next
        End If
        
        rsFacturas.MoveNext
        
    Loop
    
End Sub

Sub ProcesarTip()
    
    Dim Caja As String, Servicio As String, Documento As String
    Dim MontoServicio As Double, MontoDocumento As Double
    Dim mOperador As String, Preferencia As String
    Dim Id As Long, mInicio As Boolean
    
    On Error GoTo Error
    
    'MontoServicio = WinApiFunc.LlamarTecladoNumerico(MontoServicio, 2) 'CDbl(Val(InputBox("Enter the Tip Amount", "Stellar")))
    Call WinApiFunc.LlamarTecladoNumerico(MontoServicio, 2, Stellar_Mensaje(10151, True))
    
    If MontoServicio = 0 Then Exit Sub
    
    MontoDocumento = SVal(lvTips.SelectedItem.ListSubItems(5))
    
    If MontoServicio > MontoDocumento Then
        'If Not gRutinas.Mensajes("Are you Sure Amount: " & MontoDocumento & ", Tip " & MontoServicio, True) Then
        'El monto del documento es: $(Amount), la propina es: $(Tip).$(Line)$(Line)�Est� seguro de continuar?
        If Not gRutinas.Mensajes( _
        Replace( _
            Replace( _
                Replace(StellarMensaje(906), _
                "$(Amount)", FormatoDecimalesDinamicos(MontoDocumento)), _
            "$(Tip)", FormatoDecimalesDinamicos(MontoServicio)), _
        "$(Line)", vbNewLine), _
        True) Then
            Exit Sub
        End If
    End If
    
    ClaseDenominaciones.InicializarConexiones fConexionAdm, fConexionPos
    
    Documento = lvTips.SelectedItem.Text
    Caja = lvTips.SelectedItem.ListSubItems(1).Text
    Servicio = lvTips.SelectedItem.ListSubItems(2).Text
    Preferencia = lvTips.SelectedItem.ListSubItems(4).Text
    mOperador = lvTips.SelectedItem.SubItems(8)
    Id = lvTips.SelectedItem.SubItems(9)
    
    ClaseDenominaciones.CodDenomina = lvTips.SelectedItem.ListSubItems(7).Text
    gDllVerificacion.ConsorciodeAprobacion = 4
    
    If gDllVerificacion.AjustarOperacion(MontoServicio, Preferencia, ClaseDenominaciones, ClaseMonedas, mOperador) Then
        
        fConexionPos.BeginTrans: mInicio = True
        
        ActualizarTipFactura fConexionPos, Documento, Caja, Servicio, BuscarMontoServicio(Documento, Caja, Servicio, MontoServicio, lvTips.SelectedItem.Index)
        ActualizarTipFormaPagoFactura fConexionPos, Documento, Caja, Id, MontoServicio
        
        fConexionPos.CommitTrans: mInicio = False
        
        lvTips.SelectedItem.ListSubItems(6) = Format(MontoServicio, "Standard")
        lvTips.SelectedItem.ForeColor = vbRed
        
        For i = 1 To 7
            lvTips.SelectedItem.ListSubItems(i).ForeColor = vbRed
        Next
        
        'BuscarTipsPendientes
        
    End If
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If mInicio Then
        fConexionPos.RollbackTrans
        mInicio = False
    End If
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(ProcesarTip)"
    
End Sub

Private Function BuscarMontoServicio(Documento As String, Caja As String, _
Servicio As String, Monto As Double, IndexActual As Long) As Double
    
    Dim mItm As ListItem
    Dim mMontoTip As Double
    
    For Each mItm In Me.lvTips.ListItems
        If mItm.Text = Documento And mItm.ListSubItems(1).Text = Caja _
            And Servicio = mItm.ListSubItems(2).Text Then
            If IndexActual <> mItm.Index Then
                mMontoTip = mMontoTip + mItm.ListSubItems(6)
            End If
        End If
    Next
    
    BuscarMontoServicio = mMontoTip + Monto
    
End Function

Private Sub ActualizarTipFactura(Cn, Documento As String, Caja As String, Servicio As String, MontoTip As Double)
    
    Dim rsFacturas As ADODB.Recordset
    Dim mSQL As String
    
    mSQL = "SELECT * FROM MA_VENTAS_POS WHERE (cs_DOCUMENTO = '" & Documento & "') AND (CS_TIPO_DOCUMENTO = 'VEN') AND (cs_CAJA = '" & Caja & "') "
    
    Set rsFacturas = New ADODB.Recordset
    
    rsFacturas.Open mSQL, Cn, adOpenDynamic, adLockOptimistic, adCmdText
    
    rsFacturas!nu_Monto_Servicio = MontoTip
    
    rsFacturas.Update
    
End Sub

Private Sub ActualizarTipFormaPagoFactura(Cn, Documento As String, Caja As String, Id As Long, Monto As Double)
    
    Dim rsFacturas As ADODB.Recordset
    Dim mSQL As String
    
    mSQL = "SELECT * FROM tr_ventas_pos_pagos WHERE  (cs_DOCUMENTO = '" & Documento & "') AND (CS_TIPO_DOCUMENTO = 'VEN') AND (cs_CAJA = '" & Caja & "') " _
    & "and (id=" & Id & ")"
        
    Set rsFacturas = New ADODB.Recordset
    
    rsFacturas.Open mSQL, Cn, adOpenDynamic, adLockOptimistic, adCmdText
    
    rsFacturas!nu_tip = Monto
    
    rsFacturas.Update
    
End Sub
