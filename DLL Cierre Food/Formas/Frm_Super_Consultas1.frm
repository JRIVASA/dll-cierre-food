VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form Frm_Super_Consultas 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8790
   ClientLeft      =   1350
   ClientTop       =   1365
   ClientWidth     =   10635
   ControlBox      =   0   'False
   Icon            =   "Frm_Super_Consultas1.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8790
   ScaleMode       =   0  'User
   ScaleWidth      =   10635
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Medir 
      Height          =   135
      Left            =   9600
      ScaleHeight     =   75
      ScaleWidth      =   75
      TabIndex        =   22
      Top             =   1440
      Visible         =   0   'False
      Width           =   135
   End
   Begin VB.CommandButton CmdBuscar 
      Appearance      =   0  'Flat
      Caption         =   "&Buscar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   0
      Left            =   8160
      Picture         =   "Frm_Super_Consultas1.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   5520
      Width           =   1035
   End
   Begin VB.CommandButton CmdAceptarCancelar 
      Appearance      =   0  'Flat
      Caption         =   "&Cancelar"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Index           =   0
      Left            =   9285
      Picture         =   "Frm_Super_Consultas1.frx":800C
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   5520
      Width           =   1035
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   12120
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   8355
         TabIndex        =   17
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " B�squeda Avanzada "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1665
      Left            =   225
      TabIndex        =   12
      Top             =   6945
      Width           =   10155
      Begin VB.Frame Frame1 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   " Condici�n "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   765
         Left            =   7890
         TabIndex        =   9
         Top             =   780
         Width           =   2055
         Begin VB.OptionButton Opt_Operador 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "&O"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   285
            Index           =   1
            Left            =   1200
            TabIndex        =   5
            Top             =   330
            Width           =   705
         End
         Begin VB.OptionButton Opt_Operador 
            Appearance      =   0  'Flat
            BackColor       =   &H00E7E8E8&
            Caption         =   "&Y"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   285
            Index           =   0
            Left            =   180
            TabIndex        =   4
            Top             =   330
            Value           =   -1  'True
            Width           =   825
         End
      End
      Begin VB.CommandButton Btn_Agregar 
         Appearance      =   0  'Flat
         Caption         =   "A&gregar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   525
         Left            =   8130
         TabIndex        =   3
         Top             =   210
         Width           =   1815
      End
      Begin MSComctlLib.ListView Lst_Avanzada 
         CausesValidation=   0   'False
         Height          =   1215
         Left            =   210
         TabIndex        =   6
         Top             =   210
         Width           =   7125
         _ExtentX        =   12568
         _ExtentY        =   2143
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   5790296
         BackColor       =   16448250
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "StrCampo"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Campo"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Valor"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Operador"
            Object.Width           =   3351
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   900
      Left            =   -90
      TabIndex        =   10
      Top             =   421
      Width           =   10905
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   60
         TabIndex        =   11
         Top             =   30
         Width           =   10710
         _ExtentX        =   18891
         _ExtentY        =   1429
         ButtonWidth     =   2170
         ButtonHeight    =   1429
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Iconos_Encendidos"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   4
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "&Informaci�n"
               Key             =   "Informacion"
               Object.ToolTipText     =   "Informacion sobre el elemento Seleccionado"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Salir"
               Key             =   "Salir"
               Object.ToolTipText     =   "Salir de la B�squeda"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Caption         =   "&Ayuda"
               Key             =   "Ayuda"
               Object.ToolTipText     =   "Ayuda del Sistema"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Teclado"
               Key             =   "Teclado"
               ImageIndex      =   4
            EndProperty
         EndProperty
         Begin VB.Timer Tim_Progreso 
            Enabled         =   0   'False
            Interval        =   500
            Left            =   5760
            Top             =   120
         End
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Buscar "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1380
      Left            =   225
      TabIndex        =   8
      Top             =   5250
      Width           =   7755
      Begin VB.CheckBox Chk_Avanzada 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "&Avanzada"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   315
         Left            =   360
         TabIndex        =   2
         Top             =   915
         Width           =   1185
      End
      Begin VB.TextBox txtDato 
         Appearance      =   0  'Flat
         BackColor       =   &H00FAFAFA&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2670
         TabIndex        =   13
         Top             =   375
         Width           =   4785
      End
      Begin VB.ComboBox cmbItemBusqueda 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   375
         Width           =   2130
      End
      Begin MSComctlLib.ProgressBar Barra_Prg 
         Height          =   225
         Left            =   2670
         TabIndex        =   14
         Top             =   1005
         Width           =   4785
         _ExtentX        =   8440
         _ExtentY        =   397
         _Version        =   393216
         Appearance      =   0
         Max             =   1000
         Scrolling       =   1
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00AE5B00&
         X1              =   2250
         X2              =   7100
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Criterios de busqueda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   0
         Width           =   3015
      End
      Begin VB.Label Lbl_Progreso 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Progreso"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   195
         Left            =   2670
         TabIndex        =   15
         Top             =   780
         Width           =   4785
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   1125
      Top             =   960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Super_Consultas1.frx":9D8E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Super_Consultas1.frx":BB20
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Super_Consultas1.frx":11B22
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_Super_Consultas1.frx":138B4
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView GRID 
      CausesValidation=   0   'False
      Height          =   3285
      Left            =   225
      TabIndex        =   7
      Top             =   1770
      Width           =   10155
      _ExtentX        =   17912
      _ExtentY        =   5794
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   5790296
      BackColor       =   16448250
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00AE5B00&
      X1              =   1560
      X2              =   9820
      Y1              =   1620
      Y2              =   1620
   End
   Begin VB.Image Image1 
      Height          =   2160
      Left            =   3465
      Picture         =   "Frm_Super_Consultas1.frx":188A0
      Stretch         =   -1  'True
      Top             =   2580
      Width           =   2400
   End
   Begin VB.Label lblTitulo 
      Appearance      =   0  'Flat
      BackColor       =   &H00FAFAFA&
      BackStyle       =   0  'Transparent
      Caption         =   "#"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00AE5B00&
      Height          =   255
      Left            =   225
      TabIndex        =   1
      Top             =   1500
      Width           =   4095
   End
End
Attribute VB_Name = "Frm_Super_Consultas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim isPrimeravez As Boolean ' esta varible es para que cuando tenga registro y no este visible por primravez arraque el focus en el grid
Private StrSQL As String
Public frmPantallaInfo As Form
'Public Connection As New ADODB.Connection
Public FormPadre As Form
Public StrBotonPresionado As String
Public CampoCodigo As String
Public StrCondicion As String
Public StrCadBusCod As String
Public StrCadBusDes As String
Public StrOrderBy As String
Dim strSqlMasCondicion As String
Public StrTituloForma As String
Public UsaCodigosAlternos As Boolean
Public FormatoAplicar As Boolean
Public FormatoCantStr As Long
Public FormatoRelleno As String
Dim ArrCamposBusquedas()
Dim ArrCamposDeLaConsulta()
Dim ArrPropiedadCampo()
Dim ArrRegistroSeleccionado()
Dim ObjLw As New obj_listview
Private Conex_SC As ADODB.Connection
Private Band As Boolean
Public ArrResultado
Dim ContPuntos As Long, ContItems As Long, ContFound As Long, StrCont As String

Private Function Consulta_Avanzada() As String
    Dim SqlW As String, SQL As String
    SQL = StrSQL
    If Me.Lst_Avanzada.ListItems.Count = 0 Then
        ClaseRutinas.Mensajes WinApiFunc.Stellar_Mensaje(10115, True), True
        Consulta_Avanzada = ""
        Exit Function
    End If
    SqlW = " WHERE "
    For i = 1 To Me.Lst_Avanzada.ListItems.Count
        SqlW = SqlW + " (" + Me.Lst_Avanzada.ListItems(i) & " LIKE '" + Me.Lst_Avanzada.ListItems(i).SubItems(2) + "%') "
        If i < Me.Lst_Avanzada.ListItems.Count Then
            If Me.Lst_Avanzada.ListItems(i).SubItems(3) = "Y" Then
                SqlW = SqlW + " AND "
            Else
                SqlW = SqlW + " OR "
            End If
        End If
    Next
    If Trim(Me.StrCondicion) <> "" Then
        If SqlW = " WHERE " Then
            SqlW = SqlW + " (" + StrCondicion + ") "
        Else
            SqlW = SqlW + " AND (" + StrCondicion + ") "
        End If
    End If
    SQL = SQL + SqlW
    If Trim(Me.StrOrderBy) <> "" Then
        SQL = SQL + " ORDER BY " + Me.StrOrderBy
    End If
    Consulta_Avanzada = SQL
End Function

Private Sub Btn_Agregar_Click()
    Dim ItmX As ListItem
    Set ItmX = Me.Lst_Avanzada.ListItems.Add(, , ArrCamposBusquedas(Me.cmbItemBusqueda.ListIndex + 1))
    ItmX.ListSubItems.Add , , Me.cmbItemBusqueda.Text
    ItmX.ListSubItems.Add , , Me.txtDato
    If Me.Opt_Operador(0).Value = True Then
        ItmX.ListSubItems.Add , , "Y"
    Else
        ItmX.ListSubItems.Add , , "O"
    End If
End Sub

Private Sub cmbItemBusqueda_Click()
    
    Me.txtDato = Empty
    
    If Band = True Then
        If Me.txtDato.Enabled Then Me.txtDato.SetFocus
    End If
    
End Sub

Private Sub Lst_Avanzada_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        Me.Lst_Avanzada.ListItems.Remove Me.Lst_Avanzada.SelectedItem.Index
    End If
End Sub

Private Sub Tim_Progreso_Timer()
    ContPuntos = ContPuntos + 1
    If ContPuntos > 3 Then
        ContPuntos = 0
    End If
    Me.Lbl_Progreso.Caption = StrCont + "Buscando" + String(ContPuntos, ".")
End Sub

Private Sub Chk_Avanzada_Click()
    Me.Lst_Avanzada.ListItems.Clear
    If Me.Chk_Avanzada.Value = 0 Then
        Me.Height = 6900
    ElseIf Me.Chk_Avanzada.Value = 1 Then
        Me.Height = 8900
    End If
End Sub

Private Sub CmdAceptarCancelar_Click(Index As Integer)
'    txtDato_KeyDown vbKeyEscape, 0
    Me.StrBotonPresionado = "Cancelar"
End Sub

Private Sub CmdBuscar_Click(Index As Integer)
    Call txtDato_KeyPress(vbKeyReturn)
End Sub

Private Sub Form_Activate()

    Dim Longitud As Long
    
    Call AjustarPantalla(Me)
    Medir.FontName = lblTitulo.FontName
    Medir.FontSize = lblTitulo.FontSize
    Medir.FontBold = lblTitulo.FontBold
    Me.lbl_Organizacion.Caption = Stellar_Mensaje(51)
    'lblTitulo.Caption = "Er titulo de la remarparicion de relargo par co�o"
    Longitud = Medir.TextWidth(lblTitulo.Caption)
    If Longitud > 0 Then
        Line1.X1 = Line1.X1 + (Longitud - 500)
    End If
    
    If Me.Chk_Avanzada.Value = 0 Then
        Me.Height = 6900
    ElseIf Me.Chk_Avanzada.Value = 1 Then
        Me.Height = 8900
    End If
    If Me.txtDato.Enabled = True Then
        Me.txtDato.SetFocus
    End If
    
    If isPrimeravez Then
        If Me.Grid.ListItems.Count Then
            If Me.Grid.Enabled And Me.Grid.Visible Then
                Me.Grid.SetFocus
            End If
            isPrimeravez = False
        End If
    End If
    Band = True
End Sub

Private Sub Form_Load()
    ReDim ArrCamposBusquedas(0)
    ReDim ArrCamposDeLaConsulta(0)
    ReDim ArrRegistroSeleccionado(0)
    ReDim ArrPropiedadCampo(0)
    Band = True
    isPrimeravez = True
    
    Me.Toolbar1.Buttons(1).Visible = False
    Me.Toolbar1.Buttons(2).Caption = WinApiFunc.Stellar_Mensaje(54, True)
    Me.Toolbar1.Buttons(3).Caption = WinApiFunc.Stellar_Mensaje(7, True)
    Me.Label1.Caption = WinApiFunc.Stellar_Mensaje(10117, True)
    Me.CmdBuscar(0).Caption = WinApiFunc.Stellar_Mensaje(102, True)
    CmdAceptarCancelar(0).Caption = WinApiFunc.Stellar_Mensaje(105, True)
    Me.Chk_Avanzada.Caption = WinApiFunc.Stellar_Mensaje(10118, True)
    Frame1.Caption = WinApiFunc.Stellar_Mensaje(10121, True)
    Me.Opt_Operador(0).Caption = WinApiFunc.Stellar_Mensaje(10122, True)
    Me.Opt_Operador(1).Caption = WinApiFunc.Stellar_Mensaje(10123, True)
    Me.Lst_Avanzada.ColumnHeaders(1).Text = WinApiFunc.Stellar_Mensaje(10125, True) 'string campo
    Me.Lst_Avanzada.ColumnHeaders(2).Text = WinApiFunc.Stellar_Mensaje(10124, True) 'campo
    Me.Lst_Avanzada.ColumnHeaders(3).Text = WinApiFunc.Stellar_Mensaje(2045, True) 'valor
    Me.Lst_Avanzada.ColumnHeaders(4).Text = WinApiFunc.Stellar_Mensaje(10121, True) 'operador
    Lbl_Progreso.Caption = Stellar_Mensaje(10119) 'progreso
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
On Error GoTo Errores
    Select Case KeyCode
                
        Case Is = vbKeyF1
            ' Llamar Ayuda
            
        Case Is = vbKeyF2
'            Me.formPadre.Boton = 1
'            Call grid_DblClick
        
        Case Is = vbKeyF12
            txtDato_KeyDown vbKeyEscape, 0
    
    End Select
Exit Sub
Errores:
    'Call GRABAR_ERRORES(Err.Number, Err.Description, lccodusuario, Me.Name, Err.Source)
    Unload Me
End Sub


Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case UCase("Informaci�n")
            Me.FormPadre.boton = 1
            Call grid_DblClick
        
        Case UCase("Salir")
            txtDato_KeyDown vbKeyEscape, 0
        Case UCase("Teclado")
            AbrirTeclado txtDato
    End Select
End Sub

Private Sub txtDato_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 27 Then
       StrBotonPresionado = "Salir"
       ArrResultado = Array("", "")
       Unload Me
'       Me.Hide
    End If
End Sub

Private Sub txtDato_KeyPress(KeyAscii As Integer)
    On Error GoTo GetError
    
    Select Case KeyAscii
        Case Is = 39
            KeyAscii = 0
        Case Is = vbKeyReturn
            If txtDato = "" Then
                txtDato.SetFocus
                Exit Sub
            End If
            
            If Me.Chk_Avanzada.Value = 1 Then
                strSqlMasCondicion = Consulta_Avanzada
                Consulta_Mostrar
            Else
                If txtDato = "%" Then 'And strCadBusCod = "" And strCadBusDes = "" Then
                    strSqlMasCondicion = StrSQL
                    If Trim(Me.StrCondicion) <> "" Then
                        If InStr(StrSQL, "where") Or InStr(StrSQL, "WHERE") Then
                            strSqlMasCondicion = StrSQL + " AND (" + Me.StrCondicion + ") "
                        Else
                            strSqlMasCondicion = StrSQL + " WHERE (" + Me.StrCondicion + ") "
                        End If
                    End If
                    Consulta_Mostrar
                Else
                    'strCadBusDes = arrCamposDeLaConsulta(Me.cmbItemBusqueda.ListIndex + 1)
                    ' Ivan Espinoza 31/08/02
                    strSqlMasCondicion = StrSQL
                    ''''Debug.Print strSqlMasCondicion
                    If Trim(Me.StrCondicion) <> "" Then
                        If InStr(UCase(StrSQL), "WHERE") Then
                            strSqlMasCondicion = StrSQL + " AND (" + Me.StrCondicion + ") "
                        Else
                            strSqlMasCondicion = StrSQL + " WHERE (" + Me.StrCondicion + ") "
                        End If
                    End If
                    '''''Debug.Print strSqlMasCondicion
                    StrCadBusDes = ArrCamposBusquedas(Me.cmbItemBusqueda.ListIndex + 1)
                    If StrCadBusDes <> "" Then
                        'If InStr(UCase(strSQL), "WHERE") Then
                        If InStr(UCase(strSqlMasCondicion), "WHERE") Then
                            If StrCadBusDes = "DS_FECHA" Then
                                strSqlMasCondicion = strSqlMasCondicion + " AND CONVERT(DATE, " + StrCadBusDes + ") = CONVERT(DATE,'" + Me.txtDato + "')"
                            Else
                                strSqlMasCondicion = strSqlMasCondicion + " AND " + StrCadBusDes + " LIKE '" + Me.txtDato + "%'"
                            End If
                        Else
                        '19695633
                        'Solo busca productos activos, los inactivos NO.
                            strSqlMasCondicion = strSqlMasCondicion + " WHERE  " + StrCadBusDes + " LIKE '" + Me.txtDato + "%' "
                        End If
                    Else
                        'mensaje False, "Debe especificar Mejor la Consulta"
                        Screen.MousePointer = 0
                        Exit Sub
                    End If
                    '''''Debug.Print strSqlMasCondicion
                    '''''Debug.Print strSqlMasCondicion
                    Consulta_Mostrar
                End If
            End If
    End Select
    Screen.MousePointer = 0
    On Error GoTo 0
    Exit Sub
GetError:
    'Call mensaje(False, "Error N0." & Err.Number)
    Err.Clear
End Sub

Private Sub grid_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    'a = Abs((grid.SortOrder + 1) - 2)
    'grid.SortOrder = Abs((grid.SortOrder + 1) - 2)
    If Grid.SortOrder = lvwAscending Then
        Grid.SortOrder = lvwDescending
    Else
        Grid.SortOrder = lvwAscending
    End If
    Grid.SortKey = ColumnHeader.Index - 1
    Grid.Sorted = True
End Sub

Private Sub grid_DblClick()
    If Grid.ListItems.Count = 0 Then Exit Sub
     ArrResultado = ObjLw.TomardataLw(Me.Grid, Me.Grid.SelectedItem.Index)
     Unload Me
 End Sub

Private Sub grid_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        Call grid_DblClick
    End If
End Sub

Public Function buscar() As Variant
    Dim ArrResultados()
    Dim rsConsulta As New ADODB.Recordset
    Dim Cnx As New ADODB.Connection

    If Trim(StrSQL) = "" Then
        Exit Function
    End If
    If Trim(StrCondicion) <> "" Then
        If InStr(UCase(StrSQL), "WHERE") Then
            StrSQL = StrSQL + " AND (" + StrCondicion + ") "
        Else
            StrSQL = StrSQL + " WHERE (" + StrCondicion + ") "
        End If
    End If
    
    On Error GoTo GetError
    Cnx.ConnectionString = Conex_SC.ConnectionString
    Cnx.CursorLocation = adUseServer
    Cnx.Open
    rsConsulta.CursorLocation = adUseServer
    
    ClaseRutinas.Apertura_Recordset rsConsulta
    rsConsulta.Open StrSQL, Cnx, adOpenKeyset, adLockReadOnly
    ReDim ArrResultados(rsConsulta.Fields.Count - 1)
    If Not rsConsulta.EOF Then
        For i = 0 To rsConsulta.Fields.Count - 1
            ArrResultados(i) = rsConsulta.Fields(i)
        Next
    Else
        For i = 0 To rsConsulta.Fields.Count - 1
            ArrResultados(i) = ""
        Next
        Call ClaseRutinas.Mensajes(WinApiFunc.Stellar_Mensaje(10116, True))
    End If
    rsConsulta.Close
    buscar = ArrResultados
Exit Function
GetError:
    Call ClaseRutinas.Mensajes("Error N�" & Err.Number)
    Err.Clear
End Function

Public Function Consulta_Mostrar() As Boolean
    Dim rsConsulta As New ADODB.Recordset
    Dim Item As ListItem
    Dim lista As ListView
    Dim oField As ADODB.Field
    Dim Cnx As New ADODB.Connection
    Dim Cont As Long, ContItems As Long
    Dim SQL As String, rs As New ADODB.Recordset
    Dim CodigoBusq As String
    Set lista = Grid
    Cont = 0
    If Trim(strSqlMasCondicion) = "" Then
        Exit Function
    End If
    Me.StrBotonPresionado = ""
    Call Desactivar_Objetos(False)
    On Error GoTo GetError
    Screen.MousePointer = 11
    Grid.ListItems.Clear
    Cnx.ConnectionString = Conex_SC.ConnectionString
    Cnx.CursorLocation = adUseServer
    Cnx.Open
    
    If Trim(UCase(Me.cmbItemBusqueda.Text)) = "CODIGO" And UsaCodigosAlternos = True Then
        SQL = " SELECT c_codnasa AS CodMaestro, c_codigo AS CodAlterno From MA_CODIGOS"
'        If FormatoAplicar = True Then
'            CodigoBusq = Format(Me.txtDato, String(FormatoCantStr - 1, FormatoRelleno) + "#")
'        End If
        CodigoBusq = Me.txtDato
'        strSqlMasCondicion = strSqlMasCondicion + " WHERE " + Me.CampoCodigo + " LIKE '" + CodigoBusq + "%'"
        
        SQL = SQL + " WHERE c_codigo LIKE '" & CodigoBusq & "%'"
        rs.CursorLocation = adUseServer
'        '''''Debug.Print Sql
        rs.Open SQL, Cnx, adOpenKeyset, adLockReadOnly
        Do While Not rs.EOF
            CodigoBusq = rs!CodMaestro
'            If FormatoAplicar = True Then
'                CodigoBusq = Format(CodigoBusq, String(FormatoCantStr - 1, FormatoRelleno) + "#")
'            End If
            If InStr(UCase(strSqlMasCondicion), "WHERE") Then
                strSqlMasCondicion = strSqlMasCondicion + " OR " + Me.CampoCodigo + " LIKE '" + CodigoBusq + "%'"
            Else
                strSqlMasCondicion = strSqlMasCondicion + " WHERE " + Me.CampoCodigo + " LIKE '" + CodigoBusq + "%'"
            End If
            rs.MoveNext
        Loop
    End If
    If StrOrderBy <> "" Then
        strSqlMasCondicion = strSqlMasCondicion + " Order By  " + Me.StrOrderBy
    'Agregado el 10/10/2012
    Else
        If Replace(Mid(strSqlMasCondicion, 18, 9), ",", "", 1, 9) = "C_DESCRI" Then
            Debug.Print "Existe"
            strSqlMasCondicion = strSqlMasCondicion + " ORDER BY c_DESCRI"
        Else
            strSqlMasCondicion = strSqlMasCondicion
            Debug.Print "No Existe"
        End If
    End If
    rsConsulta.CursorLocation = adUseServer
    ClaseRutinas.Apertura_Recordset rsConsulta
    
    'MsgBox strSqlMasCondicion
    Debug.Print strSqlMasCondicion
    rsConsulta.Open strSqlMasCondicion, Cnx, adOpenKeyset, adLockReadOnly

     If Not rsConsulta.EOF Then
        rsConsulta.MoveLast
        rsConsulta.MoveFirst
        ContFound = 0
        Me.Barra_Prg.Min = 0
        Me.Barra_Prg.Value = 0
        Me.Tim_Progreso.Enabled = True
        Me.Barra_Prg.Max = rsConsulta.RecordCount
        ContFound = rsConsulta.RecordCount
        Do Until rsConsulta.EOF
            DoEvents
            Set Item = lista.ListItems.Add(, , rsConsulta.Fields(ArrCamposDeLaConsulta(1)))
            For A = 2 To Grid.ColumnHeaders.Count
                If Left(UCase(ArrCamposDeLaConsulta(A)), Len(ArrCamposDeLaConsulta(A)) - 1) = "N_PRECIO" Then
                    If Not ArrPropiedadCampo(A) Then
                        Item.SubItems(A - 1) = IIf(Not IsNull(rsConsulta.Fields(ArrCamposDeLaConsulta(A))), FormatNumber(rsConsulta.Fields(ArrCamposDeLaConsulta(A)), 2), "")
                    Else
                        Item.SubItems(A - 1) = IIf(Not IsNull(rsConsulta.Fields(ArrCamposDeLaConsulta(A))), Fix(rsConsulta.Fields(ArrCamposDeLaConsulta(A))), "")
                    End If
                Else
                    If Not ArrPropiedadCampo(A) Then
                        Item.SubItems(A - 1) = IIf(Not IsNull(rsConsulta.Fields(ArrCamposDeLaConsulta(A))), rsConsulta.Fields(ArrCamposDeLaConsulta(A)), "")
                    Else
                        Item.SubItems(A - 1) = IIf(Not IsNull(rsConsulta.Fields(ArrCamposDeLaConsulta(A))), Fix(rsConsulta.Fields(ArrCamposDeLaConsulta(A))), "")
                    End If
                End If
            Next A
            If Me.StrBotonPresionado = "Cancelar" Then
                Call Desactivar_Objetos(True)
                Screen.MousePointer = 0
                Me.Tim_Progreso.Enabled = False
                Me.Lbl_Progreso.Caption = "Progreso"
                Me.Barra_Prg.Value = 0
                Exit Do
            End If
            Me.Barra_Prg.Value = ContItems
            rsConsulta.MoveNext
            ContItems = ContItems + 1
            StrCont = CStr(FormatNumber(ContItems, 0)) + " de " + CStr(FormatNumber(ContFound, 0)) + " "
            Me.Lbl_Progreso = "Buscando" + String(ContPuntos, ".") + String(3 - ContPuntos, " ") + "  " + StrCont
        Loop
        Me.Barra_Prg.Value = Me.Barra_Prg.Max
        Call Desactivar_Objetos(True)
        If Me.Visible Then lista.SetFocus
    Else
        ClaseRutinas.Mensajes (Stellar_Mensaje(10116))
    End If
    rsConsulta.Close
    Screen.MousePointer = 0
    Me.Tim_Progreso.Enabled = False
    Me.Lbl_Progreso.Caption = CStr(FormatNumber(ContItems, 0)) + " Items Encontrados"
    Me.Barra_Prg.Value = 0
    On Error GoTo 0
    Consulta_Mostrar = True
    Me.StrBotonPresionado = ""
    Call Desactivar_Objetos(True)
Exit Function
GetError:
    Call ClaseRutinas.Mensajes(False, Err.Description)
    Call Desactivar_Objetos(True)
    Me.StrBotonPresionado = ""
    Screen.MousePointer = 0
    Me.Tim_Progreso.Enabled = False
    'Me.Lbl_Progreso.Caption = "Progreso"
    Me.Barra_Prg.Value = 0
    Call ClaseRutinas.Mensajes(False, "Error N0." & Err.Number)
    Err.Clear
    Consulta_Mostrar = False
    Me.Grid.SetFocus
End Function

Sub Desactivar_Objetos(Tipo As Boolean)
    Me.Frame3.Enabled = Tipo
    Me.txtDato.Enabled = Tipo
    Grid.Enabled = Tipo
    Grid.Visible = Tipo
    Me.CmdBuscar(0).Enabled = Tipo
End Sub

Public Function Inicializar(CadenaSql As String, srtTTitulo, ConexionBD As ADODB.Connection, Optional frmPantInfo As Form) As Boolean
    Dim m
    Me.cmbItemBusqueda.Clear
    Grid.ListItems.Clear
    Grid.ColumnHeaders.Clear
    Me.Lst_Avanzada.ListItems.Clear
    Me.StrCondicion = ""
    intNumCol = 0
    
    ArrResultado = Empty
    Me.Height = 7000
    ReDim ArrCamposBusquedas(0)
    ReDim ArrCamposDeLaConsulta(0)
    ReDim ArrRegistroSeleccionado(0)
    ReDim ArrPropiedadCampo(0)
    Me.UsaCodigosAlternos = False
    FormatoAplicar = False
    FormatoCantStr = 0
    FormatoRelleno = ""
    
    
    Set Conex_SC = ConexionBD
    StrSQL = CadenaSql
    
    If Not frmPantInfo Is Nothing Then
       Set Me.frmPantallaInfo = frmPantInfo
    End If
    'Me.Caption = UCase(srtTTitulo)
    lblTitulo = UCase(srtTTitulo)
End Function

Public Function Add_ItemSearching(strDescripcion, strCampoTabla)
         ReDim Preserve ArrCamposBusquedas(UBound(ArrCamposBusquedas) + 1)
         'para guardar los campos del registro seleccionados
         'ReDim Preserve arrRegistroSeleccionado(UBound(arrRegistroSeleccionado) + 1)
         'arrCamposDeLaConsulta(UBound(arrCamposDeLaConsulta)) = strCampoTabla
         Band = False
         ArrCamposBusquedas(UBound(ArrCamposBusquedas)) = strCampoTabla
         Me.cmbItemBusqueda.AddItem strDescripcion
         Me.cmbItemBusqueda.Text = Me.cmbItemBusqueda.List(0)
End Function

Public Function Add_ItemLabels(strDescripcion As String, strCampoTabla As String, intWidth As Integer, intAligmnet As Integer, Optional TruncarDatos As Boolean = False)
         ReDim Preserve ArrCamposDeLaConsulta(UBound(ArrCamposDeLaConsulta) + 1)
         ReDim Preserve ArrPropiedadCampo(UBound(ArrPropiedadCampo) + 1)
         ArrPropiedadCampo(UBound(ArrPropiedadCampo)) = TruncarDatos
         ArrCamposDeLaConsulta(UBound(ArrCamposDeLaConsulta)) = strCampoTabla
         Grid.ColumnHeaders.Add , , UCase(strDescripcion), intWidth, intAligmnet
End Function
