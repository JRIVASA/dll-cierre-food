VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Conexiones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Enum CCursorType
    adOpenUnspecified = -1
    adOpenForwardOnly = 0  'Valor predeterminado. Utiliza un cursor de recorrido s�lo hacia delante. Es id�ntico a un cursor est�tico, excepto en que s�lo permite desplazarse hacia delante en los registros. Esto mejora el rendimiento cuando s�lo se desea pasar una vez por cada registro del objeto Recordset.
    adOpenKeyset = 1  'Utiliza un cursor de conjunto claves. Es igual que un cursor din�mico, excepto en que no se pueden ver los registros que agregan otros usuarios, aunque los registros que eliminan otros usuarios son inaccesibles desde el Recordset. Los cambios que otros usuarios hacen en los datos permanecen visibles.
    adOpenDynamic = 2  'Utiliza un cursor din�mico. Las incorporaciones, cambios y eliminaciones que hacen otros usuarios permanecen visibles, y se permiten todo tipo de movimientos entre registros del objeto Recordset, a excepci�n de los marcadores, si el proveedor no los admite.
    adOpenStatic = 3  'Utiliza un cursor est�tico. Una copia est�tica de un conjunto de registros que se puede usar para buscar datos o generar informes. Las incorporaciones, cambios o eliminaciones que hacen otros usuarios no son visibles.
End Enum

Public Enum CLockType
    adLockBatchOptimistic = 4  'Indica actualizaciones optimistas por lotes. Requerido en el modo de actualizaci�n por lotes.
    adLockOptimistic = 3  'Indica el bloqueo optimista, registro por registro. El proveedor utiliza el bloqueo optimista y bloquea los registros s�lo cuando se llama al m�todo Update.
    adLockPessimistic = 2  'Indica el bloqueo pesimista, registro por registro. El proveedor hace lo necesario para asegurar la modificaci�n correcta de los registros, generalmente mediante el bloqueo de los registros en el origen de datos inmediatamente despu�s de su modificaci�n.
    adLockReadOnly = 1  'Indica que son registros de s�lo lectura. No puede modificar los datos.
    adLockUnspecified = -1  'No especifica ning�n tipo de bloqueo. En los clones, el clon se crea con el mismo tipo de bloqueo que el original.
End Enum

Public Enum COptions
    adCmdUnspecified = -1 'No especifica el argumento de tipo del comando.
    adCmdText = 1 'Eval�a CommandText como la definici�n de un comando o una llamada a un procedimiento almacenado.
    adCmdTable = 2 'Eval�a CommandText como un nombre de tabla cuyas columnas son devueltas en una consulta SQL generada internamente.
    adCmdStoredProc = 4 'Eval�a CommandText como un nombre de procedimiento almacenado.
    adCmdUnknown = 8 'Valor predeterminado. Indica que el tipo de comando de la propiedad CommandText es desconocido.
    adCmdFile = 256 'Eval�a CommandText como el nombre de archivo de un objeto Recordset almacenado de forma persistente.
    adCmdTableDirect = 512 'Eval�a CommandText como el nombre de una tabla de la que se devuelven todas las columnas.
End Enum

Public Function AbrirConexion(Server As String, ByRef Errores As Long, Optional Provider As String = "SQLOLEDB.1", Optional User As String = "sa", Optional PassWord As String = "", Optional IniCat As String = "master") As Object
    
    Dim LCn As Object
    
    Errores = 0
    
    On Error GoTo Falla_Cn
    
    Set LCn = CreateObject("ADODB.connection")
    
    LCn.ConnectionString = "Provider=" & Provider & ";Initial Catalog=" & IniCat & ";Data Source=" & Server & ";" _
    & IIf(User = vbNullString Or PassWord = vbNullString, _
    "Persist Security Info=False;User ID=" & User & ";", _
    "Persist Security Info=True;User ID=" & User & ";Password=" & PassWord & ";")
    
    LCn.Open
    
    Set AbrirConexion = LCn
    
    Exit Function
    
Falla_Cn:
    
    Set AbrirConexion = Nothing
    Errores = Err.Number
    
End Function

Public Function AbrirRecordset(SQLText As String, Conexion As Object, TOpen As CCursorType, TLock As CLockType, COpt As COptions, Errores As Long) As Object
    Dim lRs As Object
    On Error GoTo Falla_Recordset
    Errores = 0
    Set lRs = CreateObject("ADODB.recordset")
    lRs.Open SQLText, Conexion, TOpen, TLock, COpt
    Set AbrirRecordset = lRs
    Exit Function
Falla_Recordset:
    Set AbrirRecordset = Nothing
    Errores = Err.Number
End Function

Public Function FileToField(ImagenFilePath As String, lRs As Object, LCampo As String) As Boolean
    Dim Fhand As Long, LFile As Long, Buffers As Variant
    On Error GoTo Falla_Local
    FileToField = False
    Fhand = FreeFile()
    Open ImagenFilePath For Binary Access Read As #Fhand
    LFile = LOF(Fhand)
    Buffers = Input(LFile, Fhand)
    lRs.Fields(LCampo).AppendChunk (Buffers)
    Close Fhand
    FileToField = True
    Exit Function
Falla_Local:
    Exit Function
End Function

Public Function FieldToFile(ImagenFilePath As String, lRs As Object, LCampo As String) As Boolean
    Dim Fhand As Long, LFile As Long, Buffers As Variant
    On Error GoTo Falla_Local
    FieldToFile = False
    Fhand = FreeFile()
    Buffers = lRs.Fields(LCampo).GetChunk(lRs.Fields(LCampo).ActualSize - 1)
    Open ImagenFilePath For Output Access Write As #Fhand
    Print #Fhand, Buffers
    Close Fhand
    FieldToFile = True
    Exit Function
Falla_Local:
    Exit Function
End Function
