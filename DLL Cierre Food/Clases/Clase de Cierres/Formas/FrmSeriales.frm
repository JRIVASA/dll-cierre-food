VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{A0AD77AC-9E22-49A2-93ED-4F1729AD8BDE}#33.0#0"; "S_Grid_.ocx"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "ComCt232.ocx"
Begin VB.Form FrmSeriales 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7395
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   8955
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7395
   ScaleWidth      =   8955
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   23
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   6720
         TabIndex        =   24
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   5640
      Left            =   4665
      TabIndex        =   7
      Top             =   1605
      Width           =   4140
      Begin ComCtl2.UpDown Caracteres 
         Height          =   435
         Left            =   3105
         TabIndex        =   22
         Top             =   1110
         Width           =   930
         _ExtentX        =   1640
         _ExtentY        =   767
         _Version        =   327681
         Value           =   65
         Alignment       =   0
         Max             =   90
         Min             =   65
         Orientation     =   1
         Enabled         =   -1  'True
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   960
         Index           =   4
         Left            =   90
         TabIndex        =   21
         Top             =   2580
         Width           =   960
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   960
         Index           =   5
         Left            =   1095
         TabIndex        =   20
         Top             =   2580
         Width           =   960
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   960
         Index           =   6
         Left            =   2100
         TabIndex        =   19
         Top             =   2580
         Width           =   960
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   960
         Index           =   7
         Left            =   90
         TabIndex        =   18
         Top             =   1575
         Width           =   960
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "8"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   960
         Index           =   8
         Left            =   1095
         TabIndex        =   17
         Top             =   1575
         Width           =   960
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "9"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   960
         Index           =   9
         Left            =   2100
         TabIndex        =   16
         Top             =   1575
         Width           =   960
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   960
         Index           =   0
         Left            =   90
         TabIndex        =   15
         Top             =   4590
         Width           =   1965
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   960
         Index           =   1
         Left            =   90
         TabIndex        =   14
         Top             =   3585
         Width           =   960
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   960
         Index           =   2
         Left            =   1095
         TabIndex        =   13
         Top             =   3585
         Width           =   960
      End
      Begin VB.CommandButton btn_teclado 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   960
         Index           =   3
         Left            =   2100
         TabIndex        =   12
         Top             =   3585
         Width           =   960
      End
      Begin VB.CommandButton btn_teclado 
         BackColor       =   &H00C0C0C0&
         Caption         =   "<-----"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   960
         Index           =   11
         Left            =   3105
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1575
         Width           =   930
      End
      Begin VB.CommandButton btn_teclado 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Enter"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1980
         Index           =   12
         Left            =   3105
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   3585
         Width           =   930
      End
      Begin VB.CommandButton btn_teclado 
         BackColor       =   &H00C0C0C0&
         Caption         =   "CLS"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   960
         Index           =   13
         Left            =   3105
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   2580
         Width           =   930
      End
      Begin VB.CommandButton btn_teclado 
         BackColor       =   &H00C0C0C0&
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   960
         Index           =   14
         Left            =   3105
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   150
         Width           =   930
      End
   End
   Begin VB.TextBox TxtSerial 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   360
      Left            =   1305
      TabIndex        =   6
      Top             =   2310
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.PictureBox MenuPrincipal 
      Appearance      =   0  'Flat
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   9165
      TabIndex        =   1
      Top             =   421
      Width           =   9200
      Begin VB.Frame BarraHerramientas 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1080
         Left            =   30
         TabIndex        =   2
         Top             =   0
         Width           =   8800
         Begin MSComctlLib.Toolbar Barra 
            Height          =   810
            Left            =   150
            TabIndex        =   3
            Top             =   150
            Width           =   8500
            _ExtentX        =   15002
            _ExtentY        =   1429
            ButtonWidth     =   1561
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Enabled         =   0   'False
                  Caption         =   "&Grabar"
                  Key             =   "Grabar"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Salir"
                  Key             =   "Salir"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Ayuda"
                  Key             =   "Ayuda"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Teclado"
                  Key             =   "Teclado"
                  ImageIndex      =   5
               EndProperty
            EndProperty
         End
      End
   End
   Begin S_GRID_.S_GRID Seriales 
      CausesValidation=   0   'False
      Height          =   5010
      Left            =   135
      TabIndex        =   0
      Top             =   2205
      Width           =   4305
      _ExtentX        =   7594
      _ExtentY        =   8837
      BackColor       =   16448250
      ForeColor       =   4210752
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Appearance      =   0
      BackColorBkg    =   16448250
      BackColorFixed  =   5000268
      BackColorSel    =   16761024
      BackColorUnpopulated=   -2147483633
      Object.CausesValidation=   0   'False
      Sp_AutoIncrementar=   0   'False
      BackColor       =   16448250
      ForeColor       =   4210752
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Appearance      =   0
      BackColorFixed  =   5000268
      BackColorSel    =   16761024
      BackColorBkg    =   16448250
      BackColorUnpopulated=   -2147483633
      Object.CausesValidation=   0   'False
      ColAlignment0   =   9
      ColWidth0       =   -1
      ForeColorSel    =   -2147483634
      ForeColorFixed  =   -2147483630
      GridColor       =   -2147483630
      GridColorFixed  =   0
      GridLinesFixed  =   0
      GridLineWidthBand0=   1
      GridLineWidthHeader0=   1
      GridLineWidthIndent0=   1
      RowHeight0      =   345
      RowHeightMin    =   350
      sp_Autotab      =   0   'False
      sp_Del_EliminarFila=   0   'False
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   -165
      Top             =   1920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmSeriales.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmSeriales.frx":1D92
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmSeriales.frx":3B24
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmSeriales.frx":58B6
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmSeriales.frx":7648
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label LblDenominacion 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   330
      Left            =   1785
      TabIndex        =   5
      Top             =   1710
      Width           =   2655
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Denominación"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   180
      TabIndex        =   4
      Top             =   1710
      Width           =   1200
   End
End
Attribute VB_Name = "FrmSeriales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ClaseCierres             As New Cls_Cierres
Public ClaseRutinas             As New cls_Rutinas
Public ClaseDenominaciones      As New cls_Denominaciones

Public ConexionPos              As Object
Public ConexionAdm              As Object

Public Actualizar               As Boolean
Public LCaja                    As String
Public LCajero                  As String
Public LTurno                   As Double
Public LCodigoMoneda            As String
Public LCodigoDenominacion      As String
Public CantidadMaxima           As Integer
Public CantidadIngresada        As Integer
Private SerialActual            As String
Private FueraDeCelda            As Boolean

Private Sub Barra_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Dim PosAct As Variant
    
    PosAct = Seriales.sp_CeldaActual
    
    Select Case UCase(Button.Key)
        
        Case "SALIR"
            ClaseRutinas.SendKeys Chr(vbKeyF12), True
            
        Case "AYUDA"
            ClaseRutinas.SendKeys Chr(vbKeyF1), True
            
        Case UCase("Teclado")
            
            TxtSerial.Text = Seriales.S_TomarValor(CInt(PosAct(0)), 1)
            AbrirTeclado TxtSerial
            
            If Trim(TxtSerial.Text) <> Empty Then
                Seriales.S_FijarValor CInt(PosAct(0)), 1, TxtSerial
                Call Seriales_LeaveCell
            End If
            
    End Select
    
End Sub

Private Sub btn_teclado_Click(Index As Integer)
    
    Dim PosAct As Variant
    
    On Error GoTo Falla_Local
    
    PosAct = Seriales.sp_CeldaActual
    
    Select Case Index
        Case 14 'LETRAS ALFABETICAS
            
            If Not TxtSerial.Visible Then
                TxtSerial.Text = Seriales.S_TomarValor(CInt(PosAct(0)), 1)
                
                Call ClaseRutinas.MostarEdit(TxtSerial, Seriales, btn_teclado(Index).Caption, True)
            Else
                TxtSerial.Text = TxtSerial.Text & btn_teclado(Index).Caption
            End If
            
        Case 13 'CLS
        
            If Not TxtSerial.Visible Then
                TxtSerial.Text = Seriales.S_TomarValor(CInt(PosAct(0)), 1)
                
                Call ClaseRutinas.MostarEdit(TxtSerial, Seriales, vbNullString, True)
            Else
                TxtSerial.Text = Empty
            End If
            
        Case 12 'ENTER
            
            If Not TxtSerial.Visible Then
                TxtSerial.Text = Seriales.S_TomarValor(CInt(PosAct(0)), 1)
                
                Call ClaseRutinas.MostarEdit(TxtSerial, Seriales, TxtSerial.Text, True)
            Else
                If Trim(TxtSerial.Text) <> Empty Then
                    Seriales.S_FijarValor CInt(PosAct(0)), 1, TxtSerial
                    
                    Call Seriales_LeaveCell
                    
                    TxtSerial.Text = Empty
                    TxtSerial.Visible = False
                    
                    Seriales.Enabled = True
                    
                    If Seriales.Visible And Seriales.Enabled Then
                        Seriales.SetFocus
                    End If
                Else
                    TxtSerial.Text = Empty
                    TxtSerial.Visible = False
                    
                    Seriales.Enabled = True
                    
                    If Seriales.Visible And Seriales.Enabled Then
                        Seriales.SetFocus
                    End If
                End If
            End If
            
        Case 11 '<---
            
            If Not TxtSerial.Visible Then
                TxtSerial.Text = Seriales.S_TomarValor(CInt(PosAct(0)), 1)
                
                If Len(TxtSerial.Text) > 0 Then
                    TxtSerial.Text = Mid(TxtSerial.Text, 1, Len(TxtSerial.Text) - 1)
                Else
                    TxtSerial.Text = Empty
                End If
                
                Call ClaseRutinas.MostarEdit(TxtSerial, Seriales, TxtSerial.Text, True)
            Else
                If Len(TxtSerial.Text) > 0 Then
                    TxtSerial.Text = Mid(TxtSerial.Text, 1, Len(TxtSerial.Text) - 1)
                Else
                    TxtSerial.Text = Empty
                End If
            End If
            
        Case 0 To 9
            
            If Not TxtSerial.Visible Then
                TxtSerial.Text = Seriales.S_TomarValor(CInt(PosAct(0)), 1)
                Call ClaseRutinas.MostarEdit(TxtSerial, Seriales, TxtSerial.Text, True)
            Else
                TxtSerial.Text = TxtSerial.Text & btn_teclado(Index).Caption
            End If
            
    End Select
    
    Exit Sub
    
Falla_Local:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Teclado)"
    
End Sub

Private Sub Caracteres_Change()
    btn_teclado(14).Caption = Chr(Caracteres.Value)
End Sub

Private Sub Form_Activate()
    
    On Error GoTo Falla_Local
    
    Call ClaseDenominaciones.BuscarDenominacion(, LCodigoMoneda, LCodigoDenominacion, 2, False)
    
    LblDenominacion.Caption = ClaseDenominaciones.DesDenomina
    
    CantidadIngresada = Seriales.Rows - 2
    
    Exit Sub
    
Falla_Local:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(FrmSeriales_Activate)"
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Dim PosAct As Variant
    
    PosAct = Seriales.sp_CeldaActual
    
    Select Case Shift
        Case vbAltMask
        Case vbCtrlMask
        Case Else
            Select Case KeyCode
                Case vbKeyF12
                    If ValidarEntradas Then
                        Unload Me
                    End If
                Case vbKeyF4
                Case vbKeyF1
                    'AYUDA
                Case vbKeyDelete
                    Call EliminarFila
            End Select
    End Select
    
End Sub

Private Sub Form_Load()
    
    'Me.lbl_Organizacion.Caption = "Registro de Denominaciones"
    
    Me.lbl_Organizacion.Caption = WinApiFunc.Stellar_Mensaje(10089, True) 'Registro de denominaciones
    Me.Barra.Buttons(1).Caption = WinApiFunc.Stellar_Mensaje(103, True) 'Grabar
    Me.Barra.Buttons(1).Caption = WinApiFunc.Stellar_Mensaje(107, True) 'salir
    Me.Barra.Buttons(1).Caption = WinApiFunc.Stellar_Mensaje(7, True) 'ayuda
    Me.Barra.Buttons(1).Caption = WinApiFunc.Stellar_Mensaje(10080, True) 'Teclado
    Me.Label1.Caption = WinApiFunc.Stellar_Mensaje(2008, True) 'denominacion
    
    Call ClaseCierres.IniciarGridSeriales(Me.Seriales, LCodigoMoneda, LCodigoDenominacion, _
    LCaja, LCajero, LTurno)
    Call ClaseDenominaciones.InicializarConexiones(ConexionAdm, ConexionPos)
    
    btn_teclado(14).Caption = Chr(Caracteres.Value)
    
End Sub

Private Sub Seriales_Click()
    
    Dim PosAct As Variant
    
    PosAct = Seriales.sp_CeldaActual
    SerialActual = Seriales.S_TomarValor(CInt(PosAct(0)), 1)
    FueraDeCelda = False
    
End Sub

Private Sub Seriales_EnterCell()
    
    Dim PosAct As Variant
    
    PosAct = Seriales.sp_CeldaActual
    SerialActual = Seriales.S_TomarValor(CInt(PosAct(0)), 1)
    FueraDeCelda = False
    
End Sub

Private Sub Seriales_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Dim PosAct As Variant
    
    PosAct = Seriales.sp_CeldaActual
    
    If KeyCode = vbKeyDelete And Shift = 0 Then
        If ClaseCierres.BorrarSerialALista(Me.LCodigoMoneda, Me.LCodigoDenominacion, _
        Me.LCaja, Me.LCajero, Me.LTurno, Seriales.S_TomarValor(CInt(PosAct(0)), 1)) Then
            Seriales.sm_EliminarFila CInt(PosAct(0))
        End If
    End If
    
End Sub

Private Sub Seriales_LeaveCell()
    
    Dim PosAct As Variant
    
    PosAct = Seriales.sp_CeldaActual
    
    If Seriales.S_TomarValor(CInt(PosAct(0)), 1) <> Empty Then
        
        If ClaseCierres.AgregarSerialALista(Me.LCodigoMoneda, Me.LCodigoDenominacion, _
        Me.LCaja, Me.LCajero, Me.LTurno, Seriales.S_TomarValor(CInt(PosAct(0)), 1)) Then
            
            If Seriales.S_TomarValor(Seriales.Rows - 1, 1) <> Empty Then
                CantidadIngresada = CantidadIngresada + 1
                Seriales.Sm_adicionarFila
            End If
            
            If Seriales.S_TomarValor(CInt(PosAct(0)), 1) <> SerialActual _
            And SerialActual <> Empty Then
                
                Call ClaseCierres.BorrarSerialALista(Me.LCodigoMoneda, Me.LCodigoDenominacion, _
                Me.LCaja, Me.LCajero, Me.LTurno, SerialActual)
                
            End If
            
        Else
            Seriales.S_FijarValor CInt(PosAct(0)), 1, SerialActual
        End If
        
    Else
        Seriales.S_FijarValor CInt(PosAct(0)), 1, SerialActual
    End If
    
    FueraDeCelda = True
    
End Sub

Private Function ValidarEntradas() As Boolean
    
    Dim LCont As Integer
    'INSUFICIENTES
    
    ClaseRutinas.SendKeys Chr(vbKeyTab), True
    DoEvents
    
    If Seriales.S_TomarValor(Seriales.Rows - 1, 1) <> Empty Then
        Exit Function
    End If
    
    CantidadMaxima = CantidadIngresada
    
    ValidarEntradas = True
    
End Function

Private Sub EliminarFila()
    
    Dim PosAct As Variant
    
    PosAct = Seriales.sp_CeldaActual
    
    If Seriales.S_TomarValor(CInt(PosAct(0)), 1) <> Empty Then
        
        If ClaseCierres.BorrarSerialALista(Me.LCodigoMoneda, Me.LCodigoDenominacion, _
        Me.LCaja, Me.LCajero, Me.LTurno, Seriales.S_TomarValor(CInt(PosAct(0)), 1)) Then
            
            Seriales.sm_EliminarFila CInt(PosAct(0))
            CantidadIngresada = CantidadIngresada - 1
            
        End If
        
    End If
    
End Sub
