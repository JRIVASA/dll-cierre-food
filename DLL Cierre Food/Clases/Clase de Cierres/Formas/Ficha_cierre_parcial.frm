VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form Ficha_Cierre_parcial 
   Appearance      =   0  'Flat
   BackColor       =   &H00E7E8E8&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   120
   ClientTop       =   -210
   ClientWidth     =   15330
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Ficha_cierre_parcial.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   14
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13150
         TabIndex        =   15
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   2145
      Left            =   240
      TabIndex        =   1
      Top             =   1770
      Width           =   14775
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Cajero"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   510
         TabIndex        =   22
         Top             =   900
         Width           =   1095
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Diferencia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   9270
         TabIndex        =   10
         Top             =   1500
         Visible         =   0   'False
         Width           =   1740
      End
      Begin VB.Label diferencia 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   11140
         TabIndex        =   9
         Top             =   1500
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   2775
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Bs. Vendidos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   9270
         TabIndex        =   8
         Top             =   900
         Visible         =   0   'False
         Width           =   1740
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Caja N�"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   300
         Left            =   510
         TabIndex        =   7
         Top             =   390
         Width           =   1095
      End
      Begin VB.Label no_caja 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1700
         TabIndex        =   6
         Top             =   390
         Width           =   3000
      End
      Begin VB.Label bs_vendido 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   11140
         TabIndex        =   5
         Top             =   900
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   2775
      End
      Begin VB.Label cajero 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1700
         TabIndex        =   4
         Top             =   900
         Width           =   3000
      End
      Begin VB.Label bs_recibidos 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   11140
         TabIndex        =   3
         Top             =   390
         UseMnemonic     =   0   'False
         Width           =   2775
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Bs. Recibidos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   9270
         TabIndex        =   2
         Top             =   390
         Width           =   1740
      End
   End
   Begin MSFlexGridLib.MSFlexGrid monitor 
      Height          =   6535
      Left            =   240
      TabIndex        =   0
      Top             =   4110
      Width           =   14775
      _ExtentX        =   26061
      _ExtentY        =   11536
      _Version        =   393216
      Cols            =   9
      FixedCols       =   0
      RowHeightMin    =   350
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      FocusRect       =   2
      HighLight       =   2
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox CoolBar1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1080
      ScaleWidth      =   15510
      TabIndex        =   11
      Top             =   421
      Width           =   15510
      Begin VB.Frame Frame3 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1080
         Left            =   9960
         TabIndex        =   17
         Top             =   0
         Width           =   5055
         Begin VB.Label lbl_fecha 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "01/01/2015"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   2100
            TabIndex        =   21
            Top             =   690
            Width           =   2415
         End
         Begin VB.Label Label6 
            BackStyle       =   0  'Transparent
            Caption         =   "Fecha:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   345
            TabIndex        =   20
            Top             =   690
            Width           =   735
         End
         Begin VB.Label lbl_consecutivo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "000000000"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   375
            Left            =   2100
            TabIndex        =   19
            Top             =   210
            Width           =   2415
         End
         Begin VB.Label Label3 
            BackStyle       =   0  'Transparent
            Caption         =   "Retiro Parcial No."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00585A58&
            Height          =   255
            Left            =   330
            TabIndex        =   18
            Top             =   210
            Width           =   1695
         End
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1080
         Left            =   270
         TabIndex        =   12
         Top             =   0
         Width           =   9000
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   45
            TabIndex        =   13
            Top             =   105
            Width           =   8700
            _ExtentX        =   15346
            _ExtentY        =   1429
            ButtonWidth     =   1561
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "BarraMenu"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   6
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "Grabar"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Cambio"
                  Key             =   "Cambio"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Salir"
                  Key             =   "Salir"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Ayuda"
                  Key             =   "Ayuda"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  ImageIndex      =   5
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList BarraMenu 
      Left            =   120
      Top             =   1335
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_cierre_parcial.frx":1CCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_cierre_parcial.frx":3A5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_cierre_parcial.frx":57EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_cierre_parcial.frx":7580
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_cierre_parcial.frx":9312
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Ficha_Cierre_parcial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ClaseCierres             As New Cls_Cierres
Public ClaseRutinas             As New cls_Rutinas
Public ClaseMonedas             As New cls_Monedas
Public ConexionPos              As Object
Public ConexionAdm              As Object
Public CodigoUsuario            As String
Public Actualizar               As Boolean
Public EjecutadoRetiroParcial   As Boolean
Public LMensajes                As String
Public LCaja                    As String
Public LCajero                  As String
Public LCajeroDescripcion       As String
Public LTurno                   As Double
Public LLocalidad               As String
Public LOrganizacion            As String
Public LGrupo                   As String
Public RsCaja                   As Object
Public fCambio                  As Boolean

Private MontoTotal              As Double

Private FormaCargada            As Boolean
Private ErrorCargando           As Boolean

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        If ErrorCargando Then
            Form_KeyDown vbKeyF12, 0
            Exit Sub
        End If
        
        FormaCargada = True
            
        Call AjustarPantalla(Me)
        'Me.Caption = "Retiro parcial de la caja"
        Me.lbl_Organizacion.Caption = WinApiFunc.Stellar_Mensaje(10063, True) '"Retiro parcial de la caja"
        
        If monitor.Enabled And monitor.Visible Then
            monitor.SetFocus
        End If
        
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Static EnEjecucion As Boolean
    
    If EnEjecucion Then
        Exit Sub
    End If
    
    EnEjecucion = True
    
    Select Case Shift
        
        Case vbAltMask
            
            Select Case KeyCode
                Case vbKeyC
                    Call Form_KeyDown(vbKeyF6, 0)
                    EnEjecucion = False
                    Exit Sub
                Case vbKeyG
                    Call Form_KeyDown(vbKeyF4, 0)
                    EnEjecucion = False
                    Exit Sub
                Case vbKeyS
                    Call Form_KeyDown(vbKeyF12, 0)
                    EnEjecucion = False
                    Exit Sub
                Case vbKeyY
                    Call Form_KeyDown(vbKeyF1, 0)
                    EnEjecucion = False
                    Exit Sub
            End Select
            
        Case vbCtrlMask
        
        Case Else
            
            Select Case KeyCode
                
                Case vbKeyF12
                    
                    Actualizar = False
                    
                    'If Not gRutinas.Mensajes(WinApiFunc.Stellar_Mensaje(10064, True), True) Then '�Desea usted mantener los datos de los seriales almacenados?
                        Call ClaseCierres.BorrarDatosTemporalesDeLaCaja( _
                        LCaja, LCajero, LTurno)
                    'End If
                    
                    EnEjecucion = False
                    EjecutadoRetiroParcial = False
                    
                    Unload Me
                    
                Case vbKeyF4
                    
                    Actualizar = True
                    
                    'ACTUALIZAR Y HACER EL CIERRE PARCIAL
                    'PROBAR EL DIA 19-09-2003
                    
                    EjecutadoRetiroParcial = False
                    
                    If ClaseCierres.GrabarCierreParcial(Me, LOrganizacion, LLocalidad, LGrupo, LCaja, _
                    LTurno, LCajero, CodigoUsuario, ClaseMonedas.CodMoneda, LMensajes) Then
                        
                        Call ClaseCierres.BorrarDatosTemporalesDeLaCaja( _
                        LCaja, LCajero, LTurno)
                        
                        EjecutadoRetiroParcial = True
                        EnEjecucion = False
                        Unload Me
                        
                        Exit Sub
                        
                    End If
                    
                Case vbKeyF1
                    'AYUDA
                    
                Case vbKeyF6
                    Call CAMBIO
                    
            End Select
            
    End Select
    
    EnEjecucion = False
    
End Sub

Private Sub Form_Load()
    
    FormaCargada = False
    ErrorCargando = False
    
    POS_CP_AutoDeposito_PorLote = _
    Val(BuscarReglaNegocioStr(ConexionAdm, "POS_CP_AutoDeposito_PorLote", 0)) = 1 _
    And ExisteCampoTablaV2("TR_CIERRES", "c_ID_Lote_AutoDeposito", ConexionPos)
    
    POS_ManejaFondoMultiMoneda = _
    (Val(BuscarReglaNegocioStr(ConexionAdm, "POS_ManejaFondoMultiMoneda", "0")) = 1) _
    And ExisteTablaV3("TMP_CAM_ADV_MVD", ConexionAdm)
    
    POS_DeclararEfectivoDesgloseAutomatico = _
    Val(BuscarReglaNegocioStr(ConexionAdm, "POS_DeclararEfectivoDesgloseAutomatico", "0"))
    
    ManejaIGTF = Val(BuscarReglaNegocioStr(ConexionAdm, "ManejaImpuesto_IGTF_Ventas", "0")) = 1
    
    '** Cargar texto de los controles
    
    label3.Caption = WinApiFunc.Stellar_Mensaje(10045, True) 'retiro parcial N�
    Label6.Caption = WinApiFunc.Stellar_Mensaje(128, True) 'Fecha
    Label2.Caption = WinApiFunc.Stellar_Mensaje(2017, True) 'caja N�
    Label4.Caption = WinApiFunc.Stellar_Mensaje(2018, True) 'cajero
    Label5.Caption = WinApiFunc.Stellar_Mensaje(266, True) 'Bs.Recibidos-->total recibido
    Label5.Caption = WinApiFunc.Stellar_Mensaje(265, True) 'Bs.vendidos-->total vendido
    Label7.Caption = WinApiFunc.Stellar_Mensaje(2591, True) 'diferencia
    
    Toolbar1.Buttons(1).Caption = WinApiFunc.Stellar_Mensaje(103, True) 'grabar
    Toolbar1.Buttons(2).Caption = WinApiFunc.Stellar_Mensaje(10048, True) 'Cambio
    Toolbar1.Buttons(4).Caption = WinApiFunc.Stellar_Mensaje(107, True) 'salir
    Toolbar1.Buttons(5).Caption = WinApiFunc.Stellar_Mensaje(7, True) 'ayuda
    
    '**
    
    Me.no_caja.Caption = Me.LCaja
    Me.cajero.Caption = Me.LCajeroDescripcion
    Me.lbl_fecha.Caption = Date
    
    Me.lbl_consecutivo.Caption = ClaseRutinas.Consecutivos(ConexionPos, "cs_cierre_parcial", False)
    
    ClaseMonedas.InicializarConexiones ConexionAdm, ConexionPos
    ClaseMonedas.BuscarMonedas , , 1
    
    Set ClaseMonedaPred = ClaseMonedas
    
    Set ClaseMonedaTmp = New cls_Monedas
    ClaseMonedaTmp.InicializarConexiones ConexionAdm, ConexionPos
    
    Call ClaseCierres.GridCierreParcial(Me.monitor)
    
    If Not ClaseCierres.CargarCierreGridParcial(Me.monitor, Me.LOrganizacion, _
    Me.LLocalidad, Me.LCaja, Me.LTurno, Me.LCajero, MontoTotal) Then
        ErrorCargando = True
        ClaseRutinas.Mensajes WinApiFunc.Stellar_Mensaje(10049, True), False 'Ocurri� un error al cargar las denominaciones
    End If
    
    Call SumarColumna(Me.monitor)
    
    'Me.bs_recibidos.Caption = FormatNumber(MontoTotal, Clasemonedas.DecMoneda)
    
End Sub

Private Sub monitor_DblClick()
    
    Dim MontoLinea As Double, MontoVendido As Double, CodMonedaSel As String
    
    CodMonedaSel = ClaseRutinas.MSGridRecover(monitor, _
    monitor.Row, GridParcial.ParMonedaCodigo)
    
    MontoVendido = CDbl(ClaseRutinas.MSGridLeer(monitor, _
    monitor.Row, GridParcial.ParTotalVendido))
    
    ClaseMonedaTmp.BuscarMonedas , CodMonedaSel, , -1
    
    If DenominacionReal() And ESEfectivo() Then
    
        Set FrmDenominacionesCierres.ConexionAdm = ConexionAdm
        Set FrmDenominacionesCierres.ConexionPos = ConexionPos
        Set FrmDenominacionesCierres.ClaseCierres = ClaseCierres
        Set FrmDenominacionesCierres.ClaseRutinas = ClaseRutinas
        
        FrmDenominacionesCierres.LCaja = LCaja
        FrmDenominacionesCierres.LCajero = LCajero
        FrmDenominacionesCierres.LCajeroDescripcion = LCajeroDescripcion
        FrmDenominacionesCierres.LCodigoMoneda = CodMonedaSel
        FrmDenominacionesCierres.LGrupo = LGrupo
        FrmDenominacionesCierres.LLocalidad = LLocalidad
        FrmDenominacionesCierres.LOrganizacion = LOrganizacion
        FrmDenominacionesCierres.LTurno = LTurno
        
        FrmDenominacionesCierres.Show vbModal
        
        If FrmDenominacionesCierres.Actualizar Then
            
            MontoLinea = FrmDenominacionesCierres.MontoTotal
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParTotalRecibio, _
            FormatNumber(MontoLinea, ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParDiferencia, _
            FormatNumber(MontoLinea - MontoVendido, _
            ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            If MontoLinea <= 0 Then
                Call CambiaColorDeLineaGrid(monitor, ModCierres.ColorNormal)
            Else
                Call CambiaColorDeLineaGrid(monitor, ModCierres.ColorConfirmado)
            End If
            
        Else
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParTotalRecibio, _
            FormatNumber(0, ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParDiferencia, _
            FormatNumber(MontoVendido * -1, ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            Call CambiaColorDeLineaGrid(monitor, ModCierres.ColorNormal)
            
        End If
        
        Set FrmDenominacionesCierres = Nothing
        
    ElseIf DenominacionReal() And Not ESEfectivo() Then
        
        With Lista_Denominacionreal
            
            Set .CnAdm = ConexionAdm
            Set .CnPos = ConexionPos
            Set .Forma = Me
            
            .mMoneda = CodMonedaSel
            .mDenominacion = ClaseRutinas.MSGridRecover(monitor, _
            monitor.Row, GridParcial.ParDenominacionCodigo)
            
            .mDescriDenominacion = ClaseRutinas.MSGridRecover(monitor, _
            monitor.Row, GridParcial.ParDenominacionDescripcion)
            .mFactorMoneda = ClaseRutinas.MSGridRecover(monitor, _
            monitor.Row, GridParcial.ParFactorMoneda)
            
            .mUsuario = CodigoUsuario
            .mCajero = LCajero
            .mTurno = LTurno
            
            .NumCaja = LCaja
            
            .Show vbModal
            
            MontoLinea = CDbl(ClaseRutinas.MSGridLeer(monitor, _
            monitor.Row, GridParcial.ParTotalRecibio))
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParDiferencia, _
            FormatNumber((MontoLinea - MontoVendido), _
            ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            Call CambiaColorDeLineaGrid(monitor, _
            IIf(fCambio And MontoLinea > 0, _
            ModCierres.ColorConfirmado, ModCierres.ColorNormal))
            
        End With
        
        Set Lista_Denominacionreal = Nothing
        
    Else
        
        If CDbl(ClaseRutinas.MSGridRecover(monitor, monitor.Row, GridParcial.ParTotalRecibio)) <= 0 Then
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParTotalRecibio, _
            FormatNumber(MontoVendido, ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParDiferencia, _
            FormatNumber(0, ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            Call CambiaColorDeLineaGrid(monitor, ModCierres.ColorConfirmado)
            
        Else
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParTotalRecibio, _
            FormatNumber(0, ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParDiferencia, _
            FormatNumber(MontoVendido * -1, ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            Call CambiaColorDeLineaGrid(monitor, ModCierres.ColorNormal)
            
        End If
        
    End If
    
    Call SumarColumna(Me.monitor)
    
End Sub

Private Sub monitor_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
        Case vbCtrlMask
        Case Else
            Select Case KeyCode
                Case vbKeySpace, vbKeyReturn
                    Call monitor_DblClick
            End Select
    End Select
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case UCase(Button.Key)
        
        Case "GRABAR"
            ClaseRutinas.SendKeys Chr(vbKeyF4), True
            
        Case "SALIR"
            ClaseRutinas.SendKeys Chr(vbKeyF12), True
            
        Case "CAMBIO"
            Set FrmCambioDenominacion.ConexionAdm = ConexionAdm
            Set FrmCambioDenominacion.ConexionPos = ConexionPos
            Set FrmCambioDenominacion.ClaseCierres = ClaseCierres
            Set FrmCambioDenominacion.ClaseMonedas = ClaseMonedas
            
            FrmCambioDenominacion.LCaja = LCaja
            FrmCambioDenominacion.LCajero = LCajero
            FrmCambioDenominacion.LCajeroDescripcion = LCajeroDescripcion
            FrmCambioDenominacion.LGrupo = LGrupo
            FrmCambioDenominacion.LLocalidad = LLocalidad
            FrmCambioDenominacion.LOrganizacion = LOrganizacion
            FrmCambioDenominacion.LTurno = LTurno
            
            FrmCambioDenominacion.Show vbModal
            
            Set FrmCambioDenominacion = Nothing
            
            Call Form_Load
            
        Case "AYUDA"
            ClaseRutinas.SendKeys Chr(vbKeyF1), True
            
        Case UCase("Teclado")
            TecladoWindows
            
    End Select
    
End Sub

Private Sub SumarColumna(Grid As MSFlexGrid, _
Optional Columna As Integer = GridParcial.ParTotalRecibio)
    
    Dim LTotal As Double, LCont As Integer
    
    LTotal = 0
    
    For LCont = 1 To Grid.Rows - 1
        If ClaseRutinas.MSGridRecover(Grid, LCont, Columna) <> Empty Then
            LTotal = LTotal + _
            (ClaseRutinas.MSGridRecover(Grid, LCont, Columna) * ClaseRutinas.MSGridRecover(Grid, LCont, 1))
            'CMM1N => * => Factor OK
        End If
    Next LCont
    
    Me.bs_recibidos.Caption = FormatNumber(LTotal, ClaseMonedaPred.DecMoneda)
    
End Sub

Private Sub CAMBIO()
    
    Set FrmCambioDenominacion.ConexionAdm = ConexionAdm
    Set FrmCambioDenominacion.ConexionPos = ConexionPos
    Set FrmCambioDenominacion.ClaseCierres = ClaseCierres
    Set FrmCambioDenominacion.ClaseMonedas = ClaseMonedas
    
    FrmCambioDenominacion.LCaja = LCaja
    FrmCambioDenominacion.LCajero = LCajero
    FrmCambioDenominacion.LCajeroDescripcion = LCajeroDescripcion
    FrmCambioDenominacion.LGrupo = LGrupo
    FrmCambioDenominacion.LLocalidad = LLocalidad
    FrmCambioDenominacion.LOrganizacion = LOrganizacion
    FrmCambioDenominacion.LTurno = LTurno
    
    FrmCambioDenominacion.Show vbModal
    
    Set FrmCambioDenominacion = Nothing
    
    Call Form_Load
    
End Sub

Private Function DenominacionReal() As Boolean
    DenominacionReal = ClaseRutinas.MSGridRecover(monitor, _
    monitor.Row, GridParcial.ParValorReal) = "S"
End Function

Private Function ESEfectivo() As Boolean
    ESEfectivo = UCase(ClaseRutinas.MSGridRecover(monitor, _
    monitor.Row, GridParcial.ParDenominacionCodigo)) = UCase("Efectivo")
End Function
