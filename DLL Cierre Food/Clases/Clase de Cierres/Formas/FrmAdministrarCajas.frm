VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form FrmAdministrarCajas 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   15330
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FrmAdministrarCajas.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txt_montomax 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   360
      Left            =   11895
      TabIndex        =   12
      Top             =   1920
      Width           =   2775
   End
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13150
         TabIndex        =   9
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.PictureBox CoolBar1 
      Appearance      =   0  'Flat
      CausesValidation=   0   'False
      BeginProperty DataFormat 
         Type            =   0
         Format          =   ""
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   8202
         SubFormatType   =   0
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   15480
      TabIndex        =   4
      Top             =   421
      Width           =   15510
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   300
         TabIndex        =   5
         Top             =   75
         Width           =   14730
         _ExtentX        =   25982
         _ExtentY        =   1429
         ButtonWidth     =   2461
         ButtonHeight    =   1429
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Icono_Apagado"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   12
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Retiro Parcial"
               Key             =   "Parcial"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Cierre Caja"
               Key             =   "Total"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Cierre &Forzado"
               Key             =   "Forzado"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.Visible         =   0   'False
               Caption         =   "&Monitoreo"
               Key             =   "Monitor"
               Object.ToolTipText     =   "Monitorea las Cajas"
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Actualizar"
               Key             =   "Actualizar"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Reimpresi�n"
               Key             =   "Reimpresion"
               ImageIndex      =   5
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   7
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "facturas"
                     Text            =   "Facturas"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "retiro_parcial"
                     Text            =   "Retiros Parciales"
                  EndProperty
                  BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "cierre_caja"
                     Text            =   "Cierre Caja"
                  EndProperty
                  BeginProperty ButtonMenu4 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "seriales"
                     Text            =   "&Seriales"
                  EndProperty
                  BeginProperty ButtonMenu5 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "Consumos"
                     Text            =   "&Consumos"
                  EndProperty
                  BeginProperty ButtonMenu6 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "cierre_mensual"
                     Text            =   "Cierre Mensual"
                  EndProperty
                  BeginProperty ButtonMenu7 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "FacFiscalPend"
                     Text            =   "Doc. Fiscales Pendientes"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Object.Visible         =   0   'False
               Style           =   3
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Salir"
               Key             =   "Salir"
               Object.ToolTipText     =   "Salir de la Pantalla"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "A&yuda"
               Key             =   "Ayuda"
               Object.ToolTipText     =   "Ayuda del Sistema"
               ImageIndex      =   7
               Style           =   5
               BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                  NumButtonMenus  =   2
                  BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "Dblq_Cierre"
                     Text            =   "Desbloquear Cierre"
                  EndProperty
                  BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                     Key             =   "Fondo"
                     Text            =   "Fondo de Caja"
                  EndProperty
               EndProperty
            EndProperty
            BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Tips"
               Key             =   "Tips"
               ImageIndex      =   8
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H004C4C4C&
      BorderStyle     =   0  'None
      Caption         =   " Otras Teclas de Funci�n"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   600
      Left            =   0
      TabIndex        =   0
      Top             =   10320
      Width           =   15390
      Begin MSComctlLib.ProgressBar ProgresodeCarga 
         Height          =   225
         Left            =   4155
         TabIndex        =   7
         Top             =   -255
         Visible         =   0   'False
         Width           =   8295
         _ExtentX        =   14631
         _ExtentY        =   397
         _Version        =   393216
         Appearance      =   0
         Scrolling       =   1
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "DEL"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   240
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Presione DEL para borra al cajero asignado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   360
         TabIndex        =   1
         Top             =   150
         Width           =   4455
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   6060
      Top             =   1650
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAdministrarCajas.frx":1CCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAdministrarCajas.frx":3A5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAdministrarCajas.frx":57EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAdministrarCajas.frx":64C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAdministrarCajas.frx":825A
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAdministrarCajas.frx":9FEC
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAdministrarCajas.frx":BD7E
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAdministrarCajas.frx":DB10
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid Cajas 
      Bindings        =   "FrmAdministrarCajas.frx":F8A2
      Height          =   7515
      Left            =   180
      TabIndex        =   3
      Top             =   2610
      Width           =   14895
      _ExtentX        =   26273
      _ExtentY        =   13256
      _Version        =   393216
      Cols            =   16
      FixedCols       =   0
      RowHeightMin    =   350
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      WordWrap        =   -1  'True
      AllowBigSelection=   0   'False
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label5 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      Caption         =   "Monto M�ximo de Alerta"
      ForeColor       =   &H00585A58&
      Height          =   375
      Left            =   9120
      TabIndex        =   13
      Top             =   2000
      Width           =   2850
   End
   Begin VB.Label Label4 
      BackColor       =   &H00E7E8E8&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   240
      TabIndex        =   11
      Top             =   1680
      Width           =   14775
   End
   Begin VB.Label Label2 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3510
      TabIndex        =   6
      Top             =   2400
      Visible         =   0   'False
      Width           =   855
   End
End
Attribute VB_Name = "FrmAdministrarCajas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private ClaseRutinas                 As New cls_Rutinas
Private ClaseCierres                 As New Cls_Cierres
Private ClaseBuscarFactura           As New recsun.obj_busqueda
Private ClaseLogin                   As Object 'New DLLLogin.Login
Private NivelForzar                  As Integer
Private NivelCerrar                  As Integer
Private NivelRetiro                  As Integer
Private NivelBorrar                  As Integer

Public Organizacion As String, _
Localidad As String, _
Deposito As String, _
GrupoDeCajas As String, _
Usuario As String, _
LocalidadUsuario As String, _
NivelUsuario As Integer
        
Public ConexionAdm As Object, ConexionPos As Object

Private FormaCargada            As Boolean
Private ErrorCargando           As Boolean

Private mFilaActual             As Integer

Private Sub Cajas_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case Shift
        
        Case vbAltMask
        Case vbCtrlMask
        Case Else
            
            Select Case KeyCode
                
                Case vbKeyDelete
                    
                    ' Que no tenga transacciones (VEN / DEV) y este cerrada.
                    
                    If SVal(ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 5)) = 0 _
                    And SVal(ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 9)) = 0 _
                    And ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 3) = Stellar_Mensaje(10207) Then
                        
                        If FunctionAutorizar(NivelUsuario, NivelBorrar) Then
                            
                            If ClaseCierres.BorrarCaja( _
                            ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 0), _
                            Empty, Localidad, _
                            ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 14), _
                            ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 7), _
                            LocalidadUsuario, _
                            CDbl(ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 11))) Then
                                
                                ''Cajas.RemoveItem Cajas.Row
                                
                                'If Cajas.Rows - Cajas.FixedRows = 1 Then
                                    'Cajas.Rows = Cajas.Rows - 1
                                'Else
                                    'Cajas.RemoveItem Cajas.RowSel
                                'End If
                                
                                Form_KeyDown vbKeyF6, 0
                                
                            End If
                            
                        Else
                            ClaseRutinas.Mensajes Stellar_Mensaje(10153) '"El usuario no tiene el nivel suficiente para realizar esta operaci�n", False
                        End If
                        
                    Else
                        '"La caja debe estar cerrada y sin ninguna venta o devoluci�n realizada."
                        ClaseRutinas.Mensajes FrmAppLinkADM.StellarMensaje(2650)
                    End If
                    
            End Select
            
    End Select
    
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        If ConexionAdm Is Nothing Or ConexionPos Is Nothing Then
            ClaseRutinas.Mensajes Stellar_Mensaje(10153), False '"Error en las conexiones.", False
            Unload Me
        End If
        
        ObtenerDatosDelUsuario Me.Usuario
        
        Call ClaseCierres.InicializarConexiones(ConexionAdm, ConexionPos)
        
        Me.Enabled = False
        
        Cajas.Visible = False
        
        Call ClaseCierres.CargarCajasenGrid(Localidad, GrupoDeCajas, Cajas, ProgresodeCarga)
        
        Cajas.Visible = True
        
        Me.Enabled = True
        
        'CREAR LA RUTINA Y LAS ACTUALIZACIONES NECESARIAS PARA QUE SEA CONFIGURABLE
        
        NivelForzar = ClaseCierres.NivelCierreForzado
        NivelCerrar = 0
        NivelRetiro = 0
        NivelBorrar = 0
        
        If Cajas.Enabled And Cajas.Visible Then
            Cajas.SetFocus
        End If
        
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case Shift
        
        Case vbAltMask
            Case vbKeyR 'Retiro Parcial
                ClaseRutinas.SendKeys Chr(vbKeyF3), True
            Case vbKeyC 'Cierre total
                ClaseRutinas.SendKeys Chr(vbKeyF4), True
            Case vbKeyF 'Cierre Forzado
                ClaseRutinas.SendKeys Chr(vbKeyF7), True
            Case vbKeyR 'Reimpresion
                ClaseRutinas.SendKeys Chr(vbKeyF8), True
            Case vbKeyS 'Salir
                ClaseRutinas.SendKeys Chr(vbKeyF12), True
            Case vbKeyY 'Ayuda
                ClaseRutinas.SendKeys Chr(vbKeyF1), True
        
        Case vbCtrlMask
        
        Case Else
            
            Select Case KeyCode
                
                Case vbKeyF3 '"PARCIAL"
                    
                    ' Parcial o Abierta
                    
                    If UCase(ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 3)) = UCase(Stellar_Mensaje(10209)) _
                    Or UCase(ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 3)) = UCase(Stellar_Mensaje(10206)) Then
                        
                        Set Ficha_Cierre_parcial.ClaseCierres = ClaseCierres
                        Set Ficha_Cierre_parcial.ConexionAdm = ConexionAdm
                        Set Ficha_Cierre_parcial.ConexionPos = ConexionPos
                        
                        Ficha_Cierre_parcial.LCaja = ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 0)
                        Ficha_Cierre_parcial.LCajero = ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 7)
                        Ficha_Cierre_parcial.LCajeroDescripcion = ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 2)
                        Ficha_Cierre_parcial.LGrupo = ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 14)
                        Ficha_Cierre_parcial.LLocalidad = Localidad
                        Ficha_Cierre_parcial.LOrganizacion = Organizacion
                        Ficha_Cierre_parcial.LTurno = CDbl(ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 11))
                        Ficha_Cierre_parcial.CodigoUsuario = Usuario
                        
                        Ficha_Cierre_parcial.Show vbModal
                        
                        'If Ficha_Cierre_parcial.Actualizar Then
                            ClaseRutinas.SendKeys Chr(vbKeyF6), True
                        'End If
                        
                        Set Ficha_Cierre_parcial = Nothing
                        
                    Else
                    
                        ClaseRutinas.Mensajes Stellar_Mensaje(10154), False '"La caja debe estar en modo de cierre parcial para realizar este proceso.", False
                        
                    End If
                    
                    Exit Sub
                    
                Case vbKeyF4 '"TOTAL"
                    
                    ' Cerrada
                    
                    If UCase(ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 3)) = UCase(Stellar_Mensaje(10207)) Then
                        
                        Set Ficha_Cierre_total.ClaseCierres = ClaseCierres
                        Set Ficha_Cierre_total.ConexionAdm = ConexionAdm
                        Set Ficha_Cierre_total.ConexionPos = ConexionPos
                        
                        Ficha_Cierre_total.LCaja = ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 0)
                        Ficha_Cierre_total.LCajero = ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 7)
                        Ficha_Cierre_total.LCajeroDescripcion = ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 2)
                        Ficha_Cierre_total.LGrupo = ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 14)
                        Ficha_Cierre_total.LLocalidad = Localidad
                        Ficha_Cierre_total.LOrganizacion = Organizacion
                        Ficha_Cierre_total.LTurno = CDbl(ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 11))
                        Ficha_Cierre_total.LFondo = CDbl(ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 16))
                        Ficha_Cierre_total.CodigoUsuario = Usuario
                        
                        Ficha_Cierre_total.Show vbModal
                        
                        mFilaActual = Cajas.Row
                        
                        'If Ficha_Cierre_total.Actualizar Then
                            ClaseRutinas.SendKeys Chr(vbKeyF6), True
                        'End If
                        
                        Set Ficha_Cierre_total = Nothing
                        
                    Else
                    
                        ClaseRutinas.Mensajes Stellar_Mensaje(10154), False '"La caja debe estar en modo de cierre parcial para realizar este proceso.", False
                        
                    End If
                    
                    Exit Sub
        
                Case vbKeyF5 '"MONITOREO"
        
                Case vbKeyF8 '"REIMPRESION"
                
                Case vbKeyF12
                    
                    Unload Me
                    
                    Exit Sub
                    
                Case vbKeyF6                'ACTUALIZAR
                    
                    Me.Enabled = False
                    
                    Cajas.Visible = False
                    
                    Call ClaseCierres.CargarCajasenGrid(Localidad, GrupoDeCajas, Cajas, ProgresodeCarga)
                    
                    If mFilaActual > 0 Then
                        
                        If mFilaActual <= Cajas.Rows - 1 Then
                            
                            Cajas.Row = mFilaActual
                            Cajas.RowSel = Cajas.Row
                            Cajas.TopRow = Cajas.Row
                            Cajas.Col = 0
                            Cajas.ColSel = Cajas.Cols - 1
                            
                            Cajas.Refresh
                            Cajas.HighLight = flexHighlightAlways
                            
                        End If
                        
                    End If
                    
                    Me.Enabled = True
                    
                    Cajas.Visible = True
                    
                    SafeFocus Cajas
                    
                Case vbKeyF7 'FORZADO
                    
                    ' Cerrada
                    
                    If UCase(ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 3)) _
                    = UCase(Stellar_Mensaje(10207)) Then
                        ClaseRutinas.Mensajes Stellar_Mensaje(10155), False '"La caja ya se encuentra cerrada.", False
                        Exit Sub
                    End If
                    
                    If FunctionAutorizar(NivelUsuario, NivelForzar) Then
                        
                        If ClaseCierres.ForzarCierre(ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 0), _
                        "", Localidad, ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 14), _
                        Usuario, LocalidadUsuario, _
                        ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 7), _
                        CDec(ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 11))) Then
                            
                            ClaseRutinas.MSGridAsign Cajas, Cajas.Row, 3, _
                            Stellar_Mensaje(10207), , flexAlignCenterCenter ' Cerrada
                            
                            Call ClaseCierres.CambiarColorDeLinea(Cajas, Cajas.Row)
                            
                        End If
                        
                    Else
                        ClaseRutinas.Mensajes Stellar_Mensaje(10152), False '"El usuario no tiene el nivel suficiente para realizar esta operaci�n", False
                    End If
                    
            End Select
            
    End Select
    
    'If Cajas.Enabled And Cajas.Visible Then
        'Cajas.SetFocus
    'End If
    
End Sub

Private Sub Form_Load()
    
    POS_ManejaFondoMultiMoneda = _
    (Val(BuscarReglaNegocioStr(ConexionAdm, "POS_ManejaFondoMultiMoneda", "0")) = 1) _
    And ExisteTablaV3("TMP_CAM_ADV_MVD", ConexionAdm)
    
    FormaCargada = False
    ErrorCargando = False
    
    'AjustarPantalla (Me)
    'Me.Caption = "Monitor de Cajas"
    
    Me.lbl_Organizacion.Caption = WinApiFunc.Stellar_Mensaje(10076, True) ' "Monitor de Cajas"
    Me.Toolbar1.Buttons(1).Caption = WinApiFunc.Stellar_Mensaje(2009, True) 'Retiro parcial
    Me.Toolbar1.Buttons(2).Caption = WinApiFunc.Stellar_Mensaje(2010, True) 'Cierre de caja
    Me.Toolbar1.Buttons(3).Caption = WinApiFunc.Stellar_Mensaje(2014, True) 'Cierre Forzado
    Me.Toolbar1.Buttons(6).Caption = WinApiFunc.Stellar_Mensaje(2016, True) 'Actualizar
    Me.Toolbar1.Buttons(8).Caption = WinApiFunc.Stellar_Mensaje(106, True) 'Reimpresion
    Me.Toolbar1.Buttons(8).ButtonMenus(1).Text = WinApiFunc.Stellar_Mensaje(10156, True) 'facturas
    Me.Toolbar1.Buttons(8).ButtonMenus(2).Text = WinApiFunc.Stellar_Mensaje(10157, True) 'retiros parciales
    Me.Toolbar1.Buttons(8).ButtonMenus(3).Text = WinApiFunc.Stellar_Mensaje(10158, True) 'Cierre Caja
    Me.Toolbar1.Buttons(8).ButtonMenus(4).Text = WinApiFunc.Stellar_Mensaje(10159, True) 'seriales
    Me.Toolbar1.Buttons(8).ButtonMenus(5).Text = WinApiFunc.Stellar_Mensaje(10160, True) 'consumos
    Me.Toolbar1.Buttons(8).ButtonMenus(6).Text = WinApiFunc.Stellar_Mensaje(10161, True) 'cierre mensual
    Me.Toolbar1.Buttons(8).ButtonMenus(7).Text = WinApiFunc.Stellar_Mensaje(2031, True) 'facturas pendientes
    Me.Toolbar1.Buttons(10).Caption = WinApiFunc.Stellar_Mensaje(107, True) 'Salir
    Me.Toolbar1.Buttons(11).Caption = WinApiFunc.Stellar_Mensaje(108, True) 'Opciones
    Me.Toolbar1.Buttons(11).ButtonMenus("Dblq_Cierre").Text = FrmAppLinkADM.StellarMensaje(4133) ' "Desbloquear Cierre"
    Me.Toolbar1.Buttons(11).ButtonMenus("Fondo").Text = FrmAppLinkADM.StellarMensaje(2631) ' "Fondo de Caja"
    Me.Toolbar1.Buttons(11).ButtonMenus("Fondo").Visible = POS_ManejaFondoMultiMoneda
    Me.Toolbar1.Buttons(12).Caption = WinApiFunc.Stellar_Mensaje(10162, True) 'Propinas
    Me.Toolbar1.Buttons(12).Visible = False
    
    Me.Label3.Caption = WinApiFunc.Stellar_Mensaje(2013, True)
    
    Call ClaseCierres.ConstruirGridDelPos(Cajas)
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case UCase(Button.Key)
        
        Case "PARCIAL"
            
            SafeFocus Cajas
            ClaseRutinas.SendKeys Chr(vbKeyF3), True
            
        Case "TOTAL"
            
            SafeFocus Cajas
            ClaseRutinas.SendKeys Chr(vbKeyF4), True
            
        Case "FORZADO"
            
            SafeFocus Cajas
            ClaseRutinas.SendKeys Chr(vbKeyF7), True
            
        Case "MONITOREO"
            
            SafeFocus Cajas
            ClaseRutinas.SendKeys Chr(vbKeyF5), True
            
        Case "ACTUALIZAR"
            
            SafeFocus Cajas
            ClaseRutinas.SendKeys Chr(vbKeyF6), True
            
        Case "REIMPRESION"
            
            SafeFocus Cajas
            ClaseRutinas.SendKeys Chr(vbKeyF8), True
            
        Case "SALIR"
            
            Unload Me
            'SafeFocus Cajas
            'ClaseRutinas.SendKeys Chr(vbKeyF12), True
            
        Case "AYUDA"
            
            SafeFocus Cajas
            ClaseRutinas.SendKeys Chr(vbKeyF1), True
            
        Case "TIPS"
            
            Frm_Tips.Show vbModal
            Set Frm_Tips = Nothing
            
    End Select
    
    DoEvents
    
End Sub

Private Function FunctionAutorizar(ByVal NivelDeUsuario As Integer, _
ByVal NivelRequerido As Integer) As Boolean
    
    Dim Nivel As Long, Autorizar As Object
    
    Set Autorizar = CreateObject("DLLLogin.Login")
    
    SafePropAssign Autorizar, "ObjTeclado", ObjTeclado
    
    Nivel = NivelRequerido
    
    If NivelDeUsuario < Nivel Then
        If Autorizar.Autorizacion(LocalidadUsuario, ConexionAdm) Then
            If Autorizar.NivelUsuario > Nivel Then
                FunctionAutorizar = True
            Else
                FunctionAutorizar = False
            End If
        End If
    Else
        FunctionAutorizar = True
    End If
    
End Function

Private Sub Toolbar1_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    
    Select Case UCase(ButtonMenu.Key)
        
        Case "FACTURAS"
            
            Call ReimprimirFactura(Branch, ConexionPos, ConexionAdm)
            
        Case "CIERRE_CAJA"
            
            Call ReimprimirCierreTotal(Branch, ConexionPos, ConexionAdm)
            
        Case "RETIRO_PARCIAL"
            
            Call ReimprimirRetiroParcial(Branch, ConexionPos, ConexionAdm)
            
        Case "SERIALES"
            
            Call ReimprimirSeriales(Branch, ConexionPos, ConexionAdm)
            
        Case "CONSUMOS"
            
            Call ReimprimirConsumos(Branch, ConexionPos, ConexionAdm)
            
        Case "CIERRE_MENSUAL"
            
            Frm_Cierre_Mensual.Show vbModal
            
        Case UCase("FacFiscalPend")
            
            If (Cajas.RowSel) > 0 Then
                Call MostrarFacturasPendientes(Cajas.TextMatrix(Cajas.RowSel, 0), Localidad, _
                SVal(Cajas.TextMatrix(Cajas.RowSel, 11)))
            End If
            
        Case UCase("Dblq_Cierre")
            
            If ExisteTablaV3("TMP_CIERRES", ConexionPos) _
            And Cajas.Row > 1 Then
                
                Dim StrPregunta As String, mUsuarioSupervisor As String
                
                Dim NivelDesbloquearCierre As Long
                
                NivelDesbloquearCierre = Val(BuscarReglaNegocioStr(ConexionAdm, _
                "FOOD_NivelAdmDesbloquearCierre", "9"))
                
                mRow = Cajas.Row
                mCaja = ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 0)
                mCajero = ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 7)
                mTurno = SDec(ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 11))
                
                'StrPregunta = "Desea desbloquear el turno " & ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 11) & " " & _
                " de la caja " & ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 0) & " que pertenece al cajero " & _
                " " & ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 7) & " - " & _
                " " & ClaseRutinas.MSGridRecover(Cajas, Cajas.Row, 2)
                
                mUsuarioSupervisor = BuscarValorBD("cs_Usuario", _
                "SELECT cs_Usuario " & _
                "FROM TMP_CIERRES " & _
                "WHERE cs_Caja = '" & mCaja & "' " & _
                "AND cs_Turno = " & CDec(mTurno) & " " & _
                "AND cs_Cajero = '" & mCajero & "' " & _
                "AND cs_Localidad = '" & Localidad & "' " & _
                "AND cs_Origen = 'FOOD' ", mCajero, ConexionPos)
                
                StrPregunta = Replace(Replace(Replace(StellarMensaje(10223), _
                "$(Param1)", CDec(mTurno)), _
                "$(Param2)", mCaja), _
                "$(Param3)", mUsuarioSupervisor & " - " & _
                FrmAppLinkADM.Buscar_Usuario(mUsuarioSupervisor))
                
                If ClaseRutinas.Mensajes(StrPregunta, True) Then
                    
                    If FunctionAutorizar(NivelUsuario, NivelDesbloquearCierre) Then
                        
                        If Not ErrorCargando Then
                            
                            mFilaActual = mRow
                            
                            ConexionPos.Execute _
                            "DELETE FROM TMP_CIERRES " & _
                            "WHERE cs_Caja = '" & mCaja & "' " & _
                            "AND cs_Turno = " & CDec(mTurno) & " " & _
                            "AND cs_Cajero = '" & mCajero & "' " & _
                            "AND cs_Localidad = '" & Localidad & "' " & _
                            "AND cs_Origen = 'FOOD' "
                            
                            FrmAppLinkADM.SafeFocus Cajas
                            
                            Cajas.Row = mRow
                            
                            Form_KeyDown vbKeyF4, 0
                            
                            If mFilaActual > 0 Then
                                
                                If mFilaActual <= Cajas.Rows - 1 Then
                                    
                                    Cajas.Row = mFilaActual
                                    Cajas.RowSel = Cajas.Row
                                    Cajas.TopRow = Cajas.Row
                                    Cajas.Col = 0
                                    Cajas.ColSel = Cajas.Cols - 1
                                    
                                    Cajas.Refresh
                                    Cajas.HighLight = flexHighlightAlways
                                    
                                End If
                                
                            End If
                            
                            Exit Sub
                            
                        End If
                        
                    Else
                        
                        ClaseRutinas.Mensajes Stellar_Mensaje(10152), False '"El usuario no tiene el nivel suficiente para realizar esta operaci�n", False
                        
                    End If
                    
                End If
                
            Else
                
                'ClaseRutinas.Mensajes "Debe selecionar un registro valido.", False
                ClaseRutinas.Mensajes Stellar_Mensaje(10224), False
                
            End If
            
        Case UCase("Fondo")
            
            mRow = Cajas.Row
            
            If mRow >= 1 _
            And POS_ManejaFondoMultiMoneda Then
                
                mFilaActual = mRow
                
                mCaja = ClaseRutinas.MSGridRecover(Cajas, mRow, 0)
                mCajero = ClaseRutinas.MSGridRecover(Cajas, mRow, 7)
                mTurno = SDec(ClaseRutinas.MSGridRecover(Cajas, mRow, 11))
                
                Set Form_Ficha_Fondo.ClaseCierres = ClaseCierres
                
                Form_Ficha_Fondo.LCaja = mCaja
                Form_Ficha_Fondo.LCajero = mCajero
                Form_Ficha_Fondo.LLocalidad = Localidad
                Form_Ficha_Fondo.LTurno = mTurno
                
                Form_Ficha_Fondo.Show vbModal
                
                If Form_Ficha_Fondo.pb_Cambio Then
                    
                    Form_KeyDown vbKeyF6, 0
                    
                End If
                
                Set Form_Ficha_Fondo = Nothing
                
            End If
            
    End Select
    
End Sub

Private Sub ObtenerDatosDelUsuario(Usuario As String)
    
    Dim RsUsuarios As New ADODB.Recordset
    
    ClaseRutinas.Apertura_Recordset RsUsuarios, adUseClient
    
    RsUsuarios.Open "SELECT * FROM MA_USUARIOS WHERE CodUsuario = '" & Usuario & "'", _
    ConexionAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not RsUsuarios.EOF Then
        POS.UsuarioDelPos.Codigo = RsUsuarios!CodUsuario
        POS.UsuarioDelPos.Descripcion = RsUsuarios!Descripcion
        POS.UsuarioDelPos.Global = RsUsuarios!TIPO_USUARIO
        POS.UsuarioDelPos.Localidad = RsUsuarios!Localidad
        POS.UsuarioDelPos.LoggedIn = False
        POS.UsuarioDelPos.Nivel = RsUsuarios!Nivel
        POS.UsuarioDelPos.PassWord = RsUsuarios!PassWord
        POS.UsuarioDelPos.Vendedor = RsUsuarios!Vendedor
    End If
    
    RsUsuarios.Close
    
End Sub

Private Sub ObtenerDatosDelVendedor(Vendedor As String)
    
    Dim RsUsuarios As New ADODB.Recordset
    
    ClaseRutinas.Apertura_Recordset RsUsuarios, adUseClient
    
    RsUsuarios.Open "SELECT * FROM MA_VENDEDORES WHERE cu_Vendedor_Cod = '" & Vendedor & "'", _
    ConexionAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not RsUsuarios.EOF Then
        gMesoneros.VendedorCod = RsUsuarios!CU_VENDEDOR_COD
        gMesoneros.VendedorDes = RsUsuarios!CU_VENDEDOR_DES
    Else
        ClaseRutinas.Apertura_Recordset RsUsuarios, adUseClient
        
        RsUsuarios.Open "SELECT * FROM MA_USUARIOS WHERE CodUsuario = '" & Vendedor & "'", _
        ConexionAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
        
        If Not RsUsuarios.EOF Then
            gMesoneros.VendedorCod = RsUsuarios!CodUsuario
            gMesoneros.VendedorDes = RsUsuarios!Descripcion
        End If
    End If
    
    RsUsuarios.Close
    
End Sub

Private Sub txt_montomax_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        txt_montomax_LostFocus
    End If
End Sub

Private Sub txt_montomax_LostFocus()
    
    'If Cajas.Enabled And Cajas.Visible Then
        'Cajas.SetFocus
    'End If
    
    If Not IsNumeric(txt_montomax) Then
        txt_montomax = ""
    Else
        txt_montomax = FormatNumber(txt_montomax, 2)
        ClaseCierres.SetearMontoParaAlerta = CDbl(txt_montomax.Text)
    End If
    
    Call Form_KeyDown(vbKeyF6, 0)
    
End Sub

Private Sub MostrarFacturasPendientes(Caja As String, Localidad As String, Turno As Double)
    
    Dim TmpNivelRequerido
    
    TmpNivelRequerido = BuscarReglaNegocioStr(ConexionAdm, "FOOD_NivelAdmFacturasPendientes", "9")
    
    Dim Autorizado As Boolean
    
    If FunctionAutorizar(NivelUsuario, TmpNivelRequerido) Then
        
        '**AQUI LLAMO A LA FORMA DE MOSTRAR FACTURAS PENDIENTES
        frm_facPendienteProcesar.OrigenDocumento = FFPP_FOOD
        frm_facPendienteProcesar.CajaDato = Caja
        frm_facPendienteProcesar.LocalidadDato = Localidad
        frm_facPendienteProcesar.TurnoDato = CDec(Turno)
        frm_facPendienteProcesar.Show vbModal
        
    Else
        
        ClaseRutinas.Mensajes Stellar_Mensaje(10153) '"El usuario no tiene el nivel suficiente para realizar esta operaci�n", False
        
    End If
    
End Sub
