VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form Ficha_Cierre_total 
   Appearance      =   0  'Flat
   BackColor       =   &H00E7E8E8&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10890
   ClientLeft      =   120
   ClientTop       =   -210
   ClientWidth     =   15330
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "Ficha_cierre_total.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10890
   ScaleWidth      =   15330
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   20
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13150
         TabIndex        =   22
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.PictureBox MenuPrincipal 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1080
      ScaleWidth      =   15570
      TabIndex        =   18
      Top             =   421
      Width           =   15570
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   240
         TabIndex        =   19
         Top             =   120
         Width           =   9000
         _ExtentX        =   15875
         _ExtentY        =   1429
         ButtonWidth     =   1614
         ButtonHeight    =   1429
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "BarraMenu"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   7
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Grabar"
               Key             =   "Grabar"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Salir"
               Key             =   "Salir"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Ayuda"
               Key             =   "Ayuda"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Teclado"
               Key             =   "Teclado"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "(F8)Fondo"
               Key             =   "Fondo"
               ImageIndex      =   3
            EndProperty
         EndProperty
      End
      Begin VB.Label lbl_fecha 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "01/01/2016"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   12240
         TabIndex        =   26
         Top             =   690
         Width           =   2415
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   10350
         TabIndex        =   25
         Top             =   690
         Width           =   735
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Cierre de Caja  N�"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   255
         Left            =   10320
         TabIndex        =   24
         Top             =   210
         Width           =   1935
      End
      Begin VB.Label lbl_consecutivo 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "000000000"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   375
         Left            =   12960
         TabIndex        =   23
         Top             =   210
         Width           =   1830
      End
      Begin VB.Shape Shape1 
         BackStyle       =   1  'Opaque
         BorderStyle     =   0  'Transparent
         Height          =   1080
         Left            =   9960
         Top             =   0
         Width           =   5175
      End
   End
   Begin VB.Frame Montos 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2925
      Left            =   9300
      TabIndex        =   5
      Top             =   1800
      Width           =   5745
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Monto Recibido"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   480
         TabIndex        =   14
         Top             =   2220
         UseMnemonic     =   0   'False
         Width           =   2085
      End
      Begin VB.Label bs_recibidos 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   360
         Left            =   3240
         TabIndex        =   13
         Top             =   2220
         UseMnemonic     =   0   'False
         Width           =   2175
      End
      Begin VB.Label bs_vendido 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   360
         Left            =   3180
         TabIndex        =   12
         Top             =   1425
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Monto Vendido"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   450
         TabIndex        =   11
         Top             =   1425
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   2055
      End
   End
   Begin VB.Frame Caja 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2925
      Left            =   240
      TabIndex        =   1
      Top             =   1800
      Width           =   9105
      Begin VB.Label NoReintegros 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3060
         TabIndex        =   29
         Top             =   960
         Width           =   645
      End
      Begin VB.Label lblFondoCaja 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Fondo de Caja"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   210
         Left            =   480
         TabIndex        =   28
         Top             =   600
         UseMnemonic     =   0   'False
         Width           =   2130
      End
      Begin VB.Label txt_Fondo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3060
         TabIndex        =   27
         Top             =   600
         UseMnemonic     =   0   'False
         Width           =   2415
      End
      Begin VB.Label nocierreparcial 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3060
         TabIndex        =   17
         Top             =   1590
         UseMnemonic     =   0   'False
         Width           =   645
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "N� Cierres"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   16
         Top             =   1590
         UseMnemonic     =   0   'False
         Width           =   2055
      End
      Begin VB.Label montocierreparcial 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3750
         TabIndex        =   15
         Top             =   1590
         UseMnemonic     =   0   'False
         Width           =   1725
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Reintegros"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   10
         Top             =   960
         UseMnemonic     =   0   'False
         Width           =   1785
      End
      Begin VB.Label reintegros 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3900
         TabIndex        =   9
         Top             =   960
         UseMnemonic     =   0   'False
         Width           =   1575
      End
      Begin VB.Label no_devoluciones 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3060
         TabIndex        =   8
         Top             =   2220
         UseMnemonic     =   0   'False
         Width           =   645
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "N� Devoluciones"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   510
         TabIndex        =   7
         Top             =   2220
         UseMnemonic     =   0   'False
         Width           =   2040
      End
      Begin VB.Label monto_devoluciones 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3750
         TabIndex        =   6
         Top             =   2220
         UseMnemonic     =   0   'False
         Width           =   1725
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Caja"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   300
         Left            =   510
         TabIndex        =   4
         Top             =   240
         UseMnemonic     =   0   'False
         Width           =   1215
      End
      Begin VB.Label no_caja 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2040
         TabIndex        =   3
         Top             =   240
         Width           =   645
      End
      Begin VB.Label cajero 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2970
         TabIndex        =   2
         Top             =   240
         Width           =   2500
      End
   End
   Begin MSFlexGridLib.MSFlexGrid monitor 
      Height          =   5565
      Left            =   240
      TabIndex        =   0
      Top             =   4995
      Width           =   14775
      _ExtentX        =   26061
      _ExtentY        =   9816
      _Version        =   393216
      Cols            =   9
      FixedCols       =   0
      RowHeightMin    =   350
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      FocusRect       =   0
      HighLight       =   2
      GridLinesFixed  =   0
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList BarraMenu 
      Left            =   240
      Top             =   1395
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_cierre_total.frx":1CCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_cierre_total.frx":3A5C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_cierre_total.frx":57EE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_cierre_total.frx":7580
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Ficha_Cierre_total"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ClaseCierres             As New Cls_Cierres
Public ClaseRutinas             As New cls_Rutinas
Public ClaseMonedas             As New cls_Monedas
Public ConexionPos              As Object
Public ConexionAdm              As Object
Public CodigoUsuario            As String
Public Actualizar               As Boolean
Public Cancelado                As Boolean

Public LCaja                    As String
Public LCajero                  As String
Public LCajeroDescripcion       As String
Public LTurno                   As Double
Public LLocalidad               As String
Public LOrganizacion            As String
Public LGrupo                   As String
Public LFondo                   As Double
Public RsCaja                   As Object
Public fCambio                  As Boolean ' se agrego para ver si grabo las denominaciones (real=1 y valor=0)

Private EstadoCaja              As String
Private MontoTotal              As Double

Private FormaCargada            As Boolean
Private ErrorCargando           As Boolean
Private PreservarTemp           As Boolean

' Vuelto a nivel general
Private NumVueltos              As Long
Private TotalVueltos            As Variant

' Vueltos dados en opciones digitales / no efectivo
' Estas denominaciones no tienen fondo de caja y dar vuelto _
en ellas no implica que haya sido con dinero entrante en esas formas de pago.
Private NumVueltosNoEfectivo    As Long
Private TotalVueltosNoEfectivo  As Variant

Public Sub Form_Activate()
    
    Cancelado = False
    
    If Not FormaCargada Then
        
        If ErrorCargando Then
            Form_KeyDown vbKeyF12, 0
            Exit Sub
        End If
        
        FormaCargada = True
        
        Me.nocierreparcial.Caption = ClaseCierres.RetirosParciales( _
        LOrganizacion, LLocalidad, LCaja, LTurno, LCajero)
        
        If ClaseCierres.DatosCierreDeCaja(LOrganizacion, LLocalidad, LCaja, _
        LTurno, LCajero, EstadoCaja, RsCaja) Then
            
            Me.montocierreparcial.Caption = FormatNumber(RsCaja!nu_Monto_Retiros, ClaseMonedas.DecMoneda)
            Me.monto_devoluciones.Caption = FormatNumber(RsCaja!ns_Monto_Devuelto, ClaseMonedas.DecMoneda)
            Me.no_devoluciones.Caption = FormatNumber(RsCaja!ns_Numero_Devoluciones, 0)
            Me.bs_vendido.Caption = FormatNumber(RsCaja!ns_Monto_Vendido, ClaseMonedas.DecMoneda)
            Me.NoReintegros.Caption = FormatNumber(RsCaja!nu_Reintegros_Totales, 0)
            Me.reintegros.Caption = FormatNumber(RsCaja!nu_MontoReintegro_Total, ClaseMonedas.DecMoneda)
            
            'Me.bs_recibidos.Caption = FormatNumber(0, 2)
            
        Else
            
            If Not AutoDeclarar And Not ModoAgente Then
                'ClaseRutinas.Mensajes "No se encontr� el registro de transacciones de la caja", False
                ClaseRutinas.Mensajes WinApiFunc.Stellar_Mensaje(10070, True), False
            End If
            
            Unload Me
            
            Exit Sub
            
        End If
        
        If monitor.Enabled And monitor.Visible Then
            monitor.SetFocus
        End If
        
        If AutoDeclarar Or ModoAgente Then
            
            Call SumarColumna(Me.monitor)
            
            Form_KeyDown vbKeyF4, 0
            
            Exit Sub
            
        End If
        
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Static EnEjecucion As Boolean
    
    If EnEjecucion Then
        Exit Sub
    End If
    
    EnEjecucion = True
    
    Select Case Shift
        
        Case vbAltMask
            
            Select Case KeyCode
                
                Case vbKeyG
                    
                    Call Form_KeyDown(vbKeyF4, 0)
                    EnEjecucion = False
                    Exit Sub
                    
                Case vbKeyS
                    
                    Call Form_KeyDown(vbKeyF12, 0)
                    EnEjecucion = False
                    Exit Sub
                    
                Case vbKeyY
                    
                    Call Form_KeyDown(vbKeyF1, 0)
                    EnEjecucion = False
                    Exit Sub
                    
            End Select
            
        Case vbCtrlMask
        Case Else
            
            Select Case KeyCode
                
                Case vbKeyF12
                    
                    Actualizar = False
                    
                    Dim mBoolTempSalir As Boolean
                    
                    If ErrorCargando Then
                        mBoolTempSalir = True
                    Else
                        'GRutinas.Mensajes("Al salir, se perder�n los datos de esta caja, �Est� seguro de querer salir?", True)
                        mBoolTempSalir = gRutinas.Mensajes(WinApiFunc.Stellar_Mensaje(10071, True), True)
                    End If
                    
                    If mBoolTempSalir Then
                        
                        Cancelado = True
                        
                        If Not PreservarTemp Then
                            
                            Call ClaseCierres.BorrarDatosTemporalesDeLaCaja( _
                            LCaja, LCajero, LTurno)
                            
                            If ExisteTablaV3("TMP_CIERRES", ConexionPos) _
                            And Not ErrorCargando Then
                                
                                ConexionPos.Execute _
                                "DELETE FROM TMP_CIERRES " & _
                                "WHERE cs_Caja = '" & Me.LCaja & "' " & _
                                "AND cs_Turno = " & CDec(Me.LTurno) & " " & _
                                "AND cs_Cajero = '" & Me.LCajero & "' " & _
                                "AND cs_Localidad = '" & Me.LLocalidad & "' " & _
                                "AND cs_Origen = 'FOOD' ", TmpRowsAffected
                                
                            End If
                            
                        End If
                        
                        EnEjecucion = False
                        
                        Unload Me
                        
                        Exit Sub
                        
                    End If
                    
                    EnEjecucion = False
                    
                Case vbKeyF4
                    
                    Actualizar = True
                    
                    'ACTUALIZAR Y HACER EL CIERRE TOTAL
                    
                    Dim mImprimir As Boolean
                    
                    If ModoAgente Then
                        mImprimir = False
                    Else
                        mImprimir = True
                    End If
                    
                    ClaseCierres.Prop_NumVueltos = NumVueltos
                    ClaseCierres.Prop_TotalVueltos = TotalVueltos
                    ClaseCierres.Prop_NumVueltosNoEfectivo = NumVueltosNoEfectivo
                    ClaseCierres.Prop_TotalVueltosNoEfectivo = TotalVueltosNoEfectivo
                    
                    If ClaseCierres.CierreTotal(Me, LOrganizacion, LLocalidad, LGrupo, LCaja, LTurno, _
                    LFondo, LCajero, CodigoUsuario, ClaseMonedas.CodMoneda, mImprimir) Then
                        
                        Call ClaseCierres.BorrarDatosTemporalesDeLaCaja( _
                        LCaja, LCajero, LTurno)
                        
                        If ExisteTablaV3("TMP_CIERRES", ConexionPos) Then
                            
                            ConexionPos.Execute _
                            "DELETE FROM TMP_CIERRES " & _
                            "WHERE cs_Caja = '" & Me.LCaja & "' " & _
                            "AND cs_Turno = " & CDec(Me.LTurno) & " " & _
                            "AND cs_Cajero = '" & Me.LCajero & "' " & _
                            "AND cs_Localidad = '" & Me.LLocalidad & "' " & _
                            "AND cs_Origen = 'FOOD' "
                            
                        End If
                        
                        EnEjecucion = False
                        
                        Unload Me
                        
                    ElseIf ModoAgente Then
                        
                        Unload Me
                        
                    End If
                    
                    ClaseCierres.Prop_NumVueltos = 0
                    ClaseCierres.Prop_TotalVueltos = 0
                    ClaseCierres.Prop_NumVueltosNoEfectivo = 0
                    ClaseCierres.Prop_TotalVueltosNoEfectivo = 0
                    
                    EnEjecucion = False
                    
                Case vbKeyF1
                    
                Case vbKeyF8
                    
                    If POS_ManejaFondoMultiMoneda Then
                        
                        Set Form_Ficha_Fondo.ClaseCierres = ClaseCierres
                        
                        Form_Ficha_Fondo.LCaja = LCaja
                        Form_Ficha_Fondo.LCajero = LCajero
                        Form_Ficha_Fondo.LLocalidad = LLocalidad
                        Form_Ficha_Fondo.LTurno = LTurno
                        
                        Form_Ficha_Fondo.Show vbModal
                        
                        If Form_Ficha_Fondo.pb_Cambio Then
                            
                            Me.txt_Fondo.Caption = FormatNumber( _
                            Form_Ficha_Fondo.pb_Montofondo, ClaseMonedaPred.DecMoneda)
                            
                            monitor.Rows = 1
                            monitor.Rows = 2
                            
                            'Call Cargar_Monitor
                            'ClaseCierres.DatosCierreDeCaja(LOrganizacion, LLocalidad, LCaja, LTurno, LCajero, EstadoCaja, RsCaja)
                            Call RecargarDatosCierre
                            
                            If monitor.Rows > 1 Then
                                monitor.Row = 1
                                monitor.Col = 1
                            End If
                            
                        End If
                        
                        Set Form_Ficha_Fondo = Nothing
                        
                    End If
                    
            End Select
            
    End Select
    
    EnEjecucion = False
    
End Sub

Public Sub Form_Load()
    
    On Error GoTo Error
    
    ErrorCargando = False
    PreservarTemp = False
    FormaCargada = False
    
    Dim RsTMPCierre As New ADODB.Recordset
    
    If ExisteTablaV3("TMP_CIERRES", ConexionPos) Then
        
        mSQL = "SELECT * FROM TMP_CIERRES " & _
        "WHERE cs_Caja = '" & Me.LCaja & "' " & _
        "AND cs_Turno = " & CDec(Me.LTurno) & " " & _
        "AND cs_Cajero = '" & Me.LCajero & "' " & _
        "AND cs_Localidad = '" & Me.LLocalidad & "' " & _
        "AND cs_Origen = 'FOOD' "
        
        Call Apertura_Recordset(RsTMPCierre)
        
        RsTMPCierre.Open mSQL, ConexionPos, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If RsTMPCierre.EOF Then
            
            'agrego el registro
            
            RsTMPCierre.AddNew
            
            RsTMPCierre!cs_Localidad = Me.LLocalidad
            RsTMPCierre!cs_Caja = Me.LCaja
            RsTMPCierre!cs_Turno = Me.LTurno
            RsTMPCierre!cs_Cajero = Me.LCajero
            RsTMPCierre!cs_Usuario = CodigoUsuario
            RsTMPCierre!cs_Origen = "FOOD"
            
            RsTMPCierre.UpdateBatch
            
        Else
            
            'no permito seguir y salgo
            
            'ClaseRutinas.Mensajes _
            "El Turno: " & Me.LTurno & " de la Caja: " & Me.LCaja & " " & _
            "ya esta siendo declarado por otro usuario.", False
            
            ClaseRutinas.Mensajes Replace(Replace(StellarMensaje(10222), _
            "$(Param1)", CDec(Me.LTurno)), _
            "$(Param2)", Me.LCaja), False
            
            RsTMPCierre.Close
            
            PreservarTemp = True
            ErrorCargando = True
            
            Exit Sub
            
        End If
        
        RsTMPCierre.Close
        
    End If
    
    'AjustarPantalla Me
    'me.Caption="Cierre Total de Caja"
    
    POS_CP_AutoDeposito_PorLote = _
    Val(BuscarReglaNegocioStr(ConexionAdm, "POS_CP_AutoDeposito_PorLote", 0)) = 1 _
    And ExisteCampoTablaV2("TR_CIERRES", "c_ID_Lote_AutoDeposito", ConexionPos)
    
    POS_ManejaFondoMultiMoneda = _
    (Val(BuscarReglaNegocioStr(ConexionAdm, "POS_ManejaFondoMultiMoneda", "0")) = 1) _
    And ExisteTablaV3("TMP_CAM_ADV_MVD", ConexionAdm)
    
    POS_DeclararEfectivoDesgloseAutomatico = _
    Val(BuscarReglaNegocioStr(ConexionAdm, "POS_DeclararEfectivoDesgloseAutomatico", "0"))
    
    ManejaIGTF = Val(BuscarReglaNegocioStr(ConexionAdm, "ManejaImpuesto_IGTF_Ventas", "0")) = 1
    
    Me.lbl_Organizacion.Caption = WinApiFunc.Stellar_Mensaje(10065, True) ' Cierre Total de Caja
    
    Label3.Caption = WinApiFunc.Stellar_Mensaje(10066, True) ' Cierre de Caja N�
    Label6.Caption = WinApiFunc.Stellar_Mensaje(128, True) ' Fecha
    Label2.Caption = WinApiFunc.Stellar_Mensaje(2017, True) ' Caja
    lblFondoCaja.Caption = WinApiFunc.StellarMensaje(67) & " " ' Fondo de Caja
    Label4.Caption = WinApiFunc.Stellar_Mensaje(10067, True) ' Reintegros
    Label9.Caption = WinApiFunc.Stellar_Mensaje(2021, True) ' N� Retiros
    Label10.Caption = WinApiFunc.Stellar_Mensaje(10069, True) ' N� Devoluciones
    Label1.Caption = WinApiFunc.Stellar_Mensaje(265, True) ' Monto Vendido --> Total Vendido
    Label5.Caption = WinApiFunc.Stellar_Mensaje(266, True) ' Monto Recibido --> Total Recibido
    
    Toolbar1.Buttons(1).Caption = Stellar_Mensaje(103) 'grabar
    Toolbar1.Buttons(3).Caption = Stellar_Mensaje(107) 'salir
    Toolbar1.Buttons(4).Caption = Stellar_Mensaje(7) 'ayuda
    
    Me.no_caja.Caption = Me.LCaja
    Me.cajero.Caption = Me.LCajeroDescripcion
    Me.lbl_fecha.Caption = Date
    
    Me.lbl_consecutivo.Caption = ClaseRutinas.Consecutivos(ConexionPos, "cs_CIERRE_TOTAL", False)
    
    ClaseMonedas.InicializarConexiones ConexionAdm, ConexionPos
    ClaseMonedas.BuscarMonedas , , 1
    
    Set ClaseMonedaPred = ClaseMonedas
    
    Set ClaseMonedaTmp = New cls_Monedas
    ClaseMonedaTmp.InicializarConexiones ConexionAdm, ConexionPos
    
    Me.txt_Fondo.Caption = FormatNumber(LFondo, ClaseMonedaPred.DecMoneda)
    
    Call ClaseCierres.GridCierreParcial(Me.monitor)
    
    If Not ClaseCierres.CargarCierreGridParcial(Me.monitor, Me.LOrganizacion, _
    Me.LLocalidad, Me.LCaja, Me.LTurno, Me.LCajero, MontoTotal) Then
        ErrorCargando = True
        'ClaseRutinas.Mensajes "Ocurri� un error al cargar las denominaciones", False
        If Not ModoAgente Then
            ClaseRutinas.Mensajes WinApiFunc.Stellar_Mensaje(10049, True), False
        End If
    End If
    
    Dim TmpRs
    
    Set TmpRs = ConexionPos.Execute( _
    "SELECT isNULL(SUM(ROUND(CAST(n_Monto AS MONEY), " & _
    "" & ClaseMonedaPred.DecMoneda & ", 0)), 0) AS TotalVueltos, " & _
    "isNULL(SUM(CASE WHEN n_Monto >= 0 THEN 1 ELSE 0 END), 0) AS NumVueltos " & _
    "FROM TR_VENTAS_POS_PAGOS_VUELTO " & _
    "WHERE c_Caja = '" & Me.LCaja & "' " & _
    "AND c_CodCajero = '" & Me.LCajero & "' " & _
    "AND c_Localidad = '" & Me.LLocalidad & "' " & _
    "AND Turno = " & Me.LTurno & " ")
    
    If Not TmpRs.EOF Then
        TotalVueltos = CDec(TmpRs!TotalVueltos)
        NumVueltos = TmpRs!NumVueltos
    End If
    
    TmpRs.Close
    
    Set TmpRs = ConexionPos.Execute( _
    "SELECT isNULL(SUM(ROUND(CAST(n_Monto AS MONEY), " & _
    "" & ClaseMonedaPred.DecMoneda & ", 0)), 0) AS TotalVueltos, " & _
    "isNULL(SUM(CASE WHEN n_Monto >= 0 THEN 1 ELSE 0 END), 0) AS NumVueltos " & _
    "FROM TR_VENTAS_POS_PAGOS_VUELTO " & _
    "WHERE c_Caja = '" & Me.LCaja & "' " & _
    "AND c_CodCajero = '" & Me.LCajero & "' " & _
    "AND c_Localidad = '" & Me.LLocalidad & "' " & _
    "AND Turno = " & Me.LTurno & " " & _
    "AND c_CodDenominacion NOT IN ('Efectivo') ")
    
    If Not TmpRs.EOF Then
        TotalVueltosNoEfectivo = CDec(TmpRs!TotalVueltos)
        NumVueltosNoEfectivo = TmpRs!NumVueltos
    End If
    
    TmpRs.Close
    
    ' Nuevo manejo de fondo de caja.
    
    If POS_ManejaFondoMultiMoneda Then
        
        Me.Toolbar1.Buttons(7).Caption = StellarMensaje(10221) 'fondo
        Me.Toolbar1.Buttons(7).Visible = True
        
        Dim TempFondoCaja  As New ADODB.Recordset
        
        SQL = "SELECT isNULL(SUM(F.n_MontoFondo * F.n_Factor), 0) AS Monto " & _
        "FROM TR_CAJA_FONDO F, " & Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS M " & _
        "WHERE f.c_CodMoneda = m.c_CodMoneda " & _
        "AND f.Turno = " & Me.LTurno & " " & _
        "AND f.c_CodCaja = '" & Me.LCaja & "' " & _
        "AND f.c_Cajero = '" & Me.LCajero & "' "
        
        Call Apertura_Recordset(TempFondoCaja)
        
        TempFondoCaja.Open SQL, ConexionPos, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not TempFondoCaja.EOF Then
            Me.txt_Fondo.Caption = FormatNumber(TempFondoCaja!Monto, ClaseMonedaPred.DecMoneda)
            Me.LFondo = TempFondoCaja!Monto
        Else
            Me.txt_Fondo.Caption = FormatNumber(0, ClaseMonedaPred.DecMoneda)
        End If
        
        TempFondoCaja.Close
        
    Else
        
        Me.Toolbar1.Buttons(7).Visible = False
        'Me.txt_Fondo.Caption = FormatNumber(Ficha_Pos.MontoFondo, ClaseMonedaPred.DecMoneda)
        Me.txt_Fondo.Caption = FormatNumber(LFondo, ClaseMonedaPred.DecMoneda)
        
    End If
    
    Call SumarColumna(Me.monitor)
    
    'Me.bs_recibidos.Caption = FormatNumber(MontoTotal, Clasemonedas.DecMoneda)
    
    Exit Sub
    
Error:
    
    'Resume 'Debug
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    ErrorCargando = True
    
    If Not AutoDeclarar And Not ModoAgente Then
        MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Ficha_Cierre_Total_Load)"
    End If
    
End Sub

Private Sub monitor_DblClick()
    
    Dim MontoLinea As Double, MontoVendido As Double, CodMonedaSel As String
    
    CodMonedaSel = ClaseRutinas.MSGridRecover(monitor, _
    monitor.Row, GridParcial.ParMonedaCodigo)
    
    MontoVendido = CDbl(ClaseRutinas.MSGridLeer(monitor, _
    monitor.Row, GridParcial.ParTotalVendido))
    
    ClaseMonedaTmp.BuscarMonedas , CodMonedaSel, , -1
    
    If DenominacionReal() And ESEfectivo() Then
        
        Set FrmDenominacionesCierres.ConexionAdm = ConexionAdm
        Set FrmDenominacionesCierres.ConexionPos = ConexionPos
        Set FrmDenominacionesCierres.ClaseCierres = ClaseCierres
        Set FrmDenominacionesCierres.ClaseRutinas = ClaseRutinas
        
        FrmDenominacionesCierres.LCaja = LCaja
        FrmDenominacionesCierres.LCajero = LCajero
        FrmDenominacionesCierres.LCajeroDescripcion = LCajeroDescripcion
        FrmDenominacionesCierres.LCodigoMoneda = CodMonedaSel
        FrmDenominacionesCierres.LGrupo = LGrupo
        FrmDenominacionesCierres.LLocalidad = LLocalidad
        FrmDenominacionesCierres.LOrganizacion = LOrganizacion
        FrmDenominacionesCierres.LTurno = LTurno
        
        FrmDenominacionesCierres.Show vbModal
        
        If FrmDenominacionesCierres.Actualizar Then
            
            MontoLinea = FrmDenominacionesCierres.MontoTotal
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParTotalRecibio, _
            FormatNumber(MontoLinea, ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParDiferencia, _
            FormatNumber((MontoLinea - MontoVendido), _
            ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            Call CambiaColorDeLineaGrid(monitor, ModCierres.ColorConfirmado)
            
        Else
            
            MontoLinea = FrmDenominacionesCierres.MontoTotal
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParTotalRecibio, _
            FormatNumber(0, ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParDiferencia, _
            FormatNumber(MontoVendido * -1, _
            ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            Call CambiaColorDeLineaGrid(monitor, ModCierres.ColorNormal)
            
        End If
        
        Set FrmDenominacionesCierres = Nothing
        
    ElseIf DenominacionReal() And Not ESEfectivo() Then
        
        With Lista_Denominacionreal
            
            Set .CnAdm = ConexionAdm
            Set .CnPos = ConexionPos
            Set .Forma = Me
            
            .mMoneda = CodMonedaSel
            .mDenominacion = ClaseRutinas.MSGridRecover(monitor, _
            monitor.Row, GridParcial.ParDenominacionCodigo)
            
            .mDescriDenominacion = ClaseRutinas.MSGridRecover(monitor, _
            monitor.Row, GridParcial.ParDenominacionDescripcion)
            .mFactorMoneda = ClaseRutinas.MSGridRecover(monitor, _
            monitor.Row, GridParcial.ParFactorMoneda)
            
            .mUsuario = CodigoUsuario
            .mCajero = LCajero
            .mTurno = LTurno
            
            .NumCaja = LCaja
            
            .mDenominacionAutoDeclara = ClaseRutinas.MSGridRecover(monitor, _
            monitor.Row, GridParcial.ParDenominacionAutoDeclara)
            
            .Show vbModal
            
            MontoLinea = CDbl(ClaseRutinas.MSGridLeer(monitor, _
            monitor.Row, GridParcial.ParTotalRecibio))
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParDiferencia, _
            FormatNumber((MontoLinea - MontoVendido), _
            ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            Call CambiaColorDeLineaGrid(monitor, _
            IIf(fCambio And MontoLinea > 0, _
            ModCierres.ColorConfirmado, ModCierres.ColorNormal))
            
        End With
        
        Set Lista_Denominacionreal = Nothing
        
    Else
        
        If CDbl(ClaseRutinas.MSGridRecover(monitor, monitor.Row, GridParcial.ParTotalRecibio)) <= 0 Then
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParTotalRecibio, _
            FormatNumber(MontoVendido, ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, _
            GridParcial.ParDiferencia, 0, , flexAlignRightCenter
            
            Call CambiaColorDeLineaGrid(monitor, ModCierres.ColorConfirmado)
            
        Else
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParTotalRecibio, _
            FormatNumber(0, ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            ClaseRutinas.MSGridAsign monitor, monitor.Row, GridParcial.ParDiferencia, _
            FormatNumber(MontoVendido * -1, ClaseMonedaTmp.DecMoneda), , flexAlignRightCenter
            
            Call CambiaColorDeLineaGrid(monitor, ModCierres.ColorNormal)
            
        End If
        
    End If
    
    Call SumarColumna(Me.monitor)
    
End Sub

Private Sub monitor_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
        Case vbCtrlMask
        Case Else
            Select Case KeyCode
                Case vbKeySpace, vbKeyReturn
                    Call monitor_DblClick
            End Select
    End Select
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case "GRABAR"
            ClaseRutinas.SendKeys Chr(vbKeyF4), True
        Case "SALIR"
            ClaseRutinas.SendKeys Chr(vbKeyF12), True
        Case "AYUDA"
            ClaseRutinas.SendKeys Chr(vbKeyF1), True
        Case UCase("Teclado")
            TecladoWindows
        Case "FONDO"
            ClaseRutinas.SendKeys Chr(vbKeyF8), True
    End Select
End Sub

Private Sub SumarColumna(Grid As MSFlexGrid, _
Optional Columna As Integer = GridParcial.ParTotalRecibio)
    
    Dim LTotal As Double, LCont As Integer
    
    LTotal = 0
    
    For LCont = 1 To Grid.Rows - 1
        If ClaseRutinas.MSGridRecover(Grid, LCont, Columna) <> Empty Then
            LTotal = LTotal + _
            (ClaseRutinas.MSGridRecover(Grid, LCont, Columna) * ClaseRutinas.MSGridRecover(Grid, LCont, 1))
            'CMM1N => * => Factor OK
        End If
    Next LCont
    
    Me.bs_recibidos.Caption = FormatNumber(LTotal, ClaseMonedas.DecMoneda)
    
End Sub

Private Function DenominacionReal() As Boolean
    DenominacionReal = ClaseRutinas.MSGridRecover(monitor, monitor.Row, GridParcial.ParValorReal) = "S"
End Function

Private Function ESEfectivo() As Boolean
    ESEfectivo = UCase(ClaseRutinas.MSGridRecover(monitor, _
    monitor.Row, GridParcial.ParDenominacionCodigo)) Like "*EFECTIVO*"
End Function

Private Sub RecargarDatosCierre()
    
    'definir grid
    Call ClaseCierres.GridCierreParcial(Me.monitor)
    
    'cargar grid
    If Not ClaseCierres.CargarCierreGridParcial(Me.monitor, Me.LOrganizacion, _
    Me.LLocalidad, Me.LCaja, Me.LTurno, Me.LCajero, MontoTotal) Then
        'ClaseRutinas.Mensajes "Ocurri� un error al cargar las denominaciones", False
        If Not ModoAgente Then
            ClaseRutinas.Mensajes WinApiFunc.Stellar_Mensaje(10049, True), False
        End If
    End If
    
    'cargar datos parciales
    Me.nocierreparcial.Caption = ClaseCierres.RetirosParciales(LOrganizacion, LLocalidad, LCaja, LTurno, LCajero)
    
    If ClaseCierres.DatosCierreDeCaja(LOrganizacion, LLocalidad, LCaja, LTurno, LCajero, EstadoCaja, RsCaja) Then
        
        Me.montocierreparcial.Caption = FormatNumber(RsCaja!nu_Monto_Retiros, ClaseMonedaPred.DecMoneda)
        Me.monto_devoluciones.Caption = FormatNumber(RsCaja!ns_Monto_Devuelto, ClaseMonedaPred.DecMoneda)
        Me.no_devoluciones.Caption = FormatNumber(RsCaja!ns_Numero_Devoluciones, 0)
        Me.bs_vendido.Caption = FormatNumber(RsCaja!ns_Monto_Vendido, ClaseMonedaPred.DecMoneda)
        Me.NoReintegros.Caption = FormatNumber(RsCaja!nu_Reintegros_Totales, 0)
        Me.reintegros.Caption = FormatNumber(RsCaja!nu_MontoReintegro_Total, ClaseMonedaPred.DecMoneda)
        
        'Me.bs_recibidos.Caption = FormatNumber(0, 2)
        
    End If
    
    'cargar datos del fondo
    
    Dim TempFondoCaja  As New ADODB.Recordset
    
    SQL = "SELECT isNULL(SUM(F.n_Montofondo * F.n_Factor), 0) AS Monto " & _
    "FROM TR_CAJA_FONDO F, " & Srv_Remote_BD_ADM & ".DBO.MA_MONEDAS M " & _
    "WHERE f.c_CodMoneda = m.c_CodMoneda " & _
    "AND f.Turno = " & CDec(Me.LTurno) & " " & _
    "AND f.c_CodCaja = '" & Me.LCaja & "' " & _
    "AND f.c_Cajero = '" & Me.LCajero & "' "
    
    Call Apertura_Recordset(TempFondoCaja)
    
    TempFondoCaja.Open SQL, ConexionPos, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    If Not TempFondoCaja.EOF Then
        Me.txt_Fondo.Caption = FormatNumber(TempFondoCaja!Monto, ClaseMonedaPred.DecMoneda)
        Me.LFondo = TempFondoCaja!Monto
    Else
        Me.txt_Fondo.Caption = FormatNumber(0, ClaseMonedaPred.DecMoneda)
    End If
    
    TempFondoCaja.Close
    
    'cargar datos de cierre de caja
    
    Call SumarColumna(Me.monitor)
    
End Sub
