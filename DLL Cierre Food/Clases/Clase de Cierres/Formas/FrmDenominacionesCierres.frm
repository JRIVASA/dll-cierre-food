VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{A0AD77AC-9E22-49A2-93ED-4F1729AD8BDE}#33.0#0"; "S_Grid_.ocx"
Begin VB.Form FrmDenominacionesCierres 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8010
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   5475
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8010
   ScaleWidth      =   5475
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   3360
         TabIndex        =   8
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.TextBox TxtBuffer 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   360
      Left            =   1395
      TabIndex        =   6
      Top             =   2160
      Visible         =   0   'False
      Width           =   1155
   End
   Begin S_GRID_.S_GRID Denominaciones 
      CausesValidation=   0   'False
      Height          =   5595
      Left            =   135
      TabIndex        =   5
      Top             =   1740
      Width           =   5145
      _ExtentX        =   9075
      _ExtentY        =   9869
      BackColor       =   16448250
      ForeColor       =   4210752
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Appearance      =   0
      BackColorBkg    =   16448250
      BackColorFixed  =   5000268
      BackColorSel    =   16761024
      BackColorUnpopulated=   -2147483633
      Object.CausesValidation=   0   'False
      Sp_AutoIncrementar=   0   'False
      BackColor       =   16448250
      ForeColor       =   4210752
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Appearance      =   0
      BackColorFixed  =   5000268
      BackColorSel    =   16761024
      BackColorBkg    =   16448250
      BackColorUnpopulated=   -2147483633
      Object.CausesValidation=   0   'False
      ColAlignment0   =   9
      ColWidth0       =   -1
      ForeColorSel    =   -2147483634
      ForeColorFixed  =   -2147483630
      GridColor       =   -2147483630
      GridColorFixed  =   0
      GridLinesFixed  =   0
      GridLineWidthBand0=   1
      GridLineWidthHeader0=   1
      GridLineWidthIndent0=   1
      RowHeight0      =   345
      RowHeightMin    =   350
      sp_Autotab      =   0   'False
      sp_Del_EliminarFila=   0   'False
   End
   Begin VB.PictureBox MenuPrincipal 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   5670
      TabIndex        =   0
      Top             =   421
      Width           =   5700
      Begin VB.Frame BarraHerramientas 
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1080
         Left            =   30
         TabIndex        =   1
         Top             =   0
         Width           =   5500
         Begin MSComctlLib.Toolbar Barra 
            Height          =   810
            Left            =   150
            TabIndex        =   2
            Top             =   120
            Width           =   5000
            _ExtentX        =   8811
            _ExtentY        =   1429
            ButtonWidth     =   1561
            ButtonHeight    =   1429
            ToolTips        =   0   'False
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Grabar"
                  Key             =   "Grabar"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Salir"
                  Key             =   "Salir"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Ayuda"
                  Key             =   "Ayuda"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Teclado"
                  Key             =   "Teclado"
                  ImageIndex      =   5
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   795
      Top             =   1515
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDenominacionesCierres.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDenominacionesCierres.frx":1D92
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDenominacionesCierres.frx":3B24
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDenominacionesCierres.frx":58B6
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmDenominacionesCierres.frx":7648
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label LblTotal 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   315
      Left            =   2130
      TabIndex        =   4
      Top             =   7550
      Width           =   3120
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   270
      Left            =   795
      TabIndex        =   3
      Top             =   7545
      Width           =   1335
   End
End
Attribute VB_Name = "FrmDenominacionesCierres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ClaseCierres             As New Cls_Cierres
Public ClaseRutinas             As New cls_Rutinas

Public ConexionPos              As Object
Public ConexionAdm              As Object

Public Actualizar               As Boolean
Public LCaja                    As String
Public LCajero                  As String
Public LCajeroDescripcion       As String
Public LTurno                   As Double
Public LLocalidad               As String
Public LOrganizacion            As String
Public LGrupo                   As String
Public RsCaja                   As Object
Public LCodigoMoneda            As String

Private PermitirGrabar          As Boolean
Private EventoProgramado        As Boolean

Public MontoTotal               As Double
Public CantidadActual           As Long

Private Sub Barra_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Dim NumPad As Double
    Dim PosAct As Variant
    
    PosAct = Denominaciones.sp_CeldaActual
    
    Select Case UCase(Button.Key)
        
        Case "GRABAR"
            ClaseRutinas.SendKeys Chr(vbKeyF4), True
            
        Case "SALIR"
            ClaseRutinas.SendKeys Chr(vbKeyF12), True
            
        Case "AYUDA"
            ClaseRutinas.SendKeys Chr(vbKeyF1), True
            
        Case UCase("Teclado")
            
            NumPad = 0
            
            Call ClaseRutinas.LlamarTecladoNumerico(NumPad, 0)
            
            Denominaciones.S_FijarValor CInt(PosAct(0)), 3, FormatNumber(NumPad, 0)
            
    End Select
    
End Sub

Private Sub Denominaciones_EnterCell()
    
    Dim PosAct As Variant
    
    PosAct = Denominaciones.sp_CeldaActual
    
    CantidadActual = CLng(Denominaciones.S_TomarValor(CInt(PosAct(0)), 3))
    
End Sub

Private Sub Denominaciones_LeaveCell()
    
    If EventoProgramado Then
        Exit Sub
    End If
    
    Dim PosAct As Variant
    
    PosAct = Denominaciones.sp_CeldaActual
    
    If PermitirGrabar Then
        
        EventoProgramado = True
        
        Dim mCantidad As Long
        
        mCantidad = CLng(Denominaciones.S_TomarValor(CInt(PosAct(0)), 3))
        
        If mCantidad <= 0 Then
            mCantidad = 0
            Denominaciones.S_FijarValor CInt(PosAct(0)), 3, CStr(mCantidad)
        End If
        
        Call ClaseCierres.GrabarDatoDelRenglon(CStr(LCodigoMoneda), _
        CStr(Denominaciones.S_TomarValor(CInt(PosAct(0)), 1)), _
        mCantidad, Me.LCaja, Me.LCajero, Me.LTurno)
        
        Me.LblTotal.Caption = FormatNumber(Denominaciones.sm_TotalizarColumna(5), _
        ClaseMonedaTmp.DecMoneda)
        
        If UCase(Denominaciones.S_TomarValor(CInt(PosAct(0)), 6)) = "S" _
        And CantidadActual <> mCantidad Then
            Call EditarSeriales(mCantidad)
        End If
        
        If CInt(PosAct(0)) < Denominaciones.Rows - 1 Then
            Call Denominaciones.sm_MoverACelda(CInt(PosAct(0)) + 1, 3)
        End If
        
        EventoProgramado = False
        
    End If
    
End Sub

Private Sub Form_Activate()
    Me.LblTotal.Caption = FormatNumber(Denominaciones.sm_TotalizarColumna(5), _
    ClaseMonedaTmp.DecMoneda)
End Sub

Private Function GrabarTodo() As Boolean
    
    On Error GoTo Error
    
    Dim LblValorAnt
    
    LblValorAnt = LblTotal.Caption
    
    Dim ActiveTrans As Boolean
    
    ClaseCierres.ConexionPos.BeginTrans
    ActiveTrans = True
    
    LblTotal.Caption = CDec(0)
    
    For i = 1 To Denominaciones.Rows - 1
        
        If Not ClaseCierres.GrabarDatoDelRenglon(LCodigoMoneda, _
        Denominaciones.S_TomarValor(CInt(i), 1), _
        CLng(Denominaciones.S_TomarValor(CInt(i), 3)), _
        Me.LCaja, Me.LCajero, Me.LTurno) Then
            
            ClaseCierres.ConexionPos.RollbackTrans
            ActiveTrans = False
            
            LblTotal.Caption = LblValorAnt
            
            Exit Function
            
        End If
        
        LblTotal.Caption = CDec(LblTotal.Caption) + _
        CDec(CDec(CLng(Denominaciones.S_TomarValor(CInt(i), 3))) * CDec(Denominaciones.S_TomarValor(CInt(i), 4)))
        
    Next
    
    ClaseCierres.ConexionPos.CommitTrans
    ActiveTrans = False
    
    'LblSumTotalizarColumna = FormatNumber(Denominaciones.sm_TotalizarColumna(5), _
    ClaseMonedaTmp.DecMoneda)
    
    LblTotal.Caption = FormatNumber(CDec(LblTotal.Caption), ClaseMonedaTmp.DecMoneda)
    
    GrabarTodo = True
    
    Exit Function
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If ActiveTrans Then
        ClaseCierres.ConexionPos.RollbackTrans
        ActiveTrans = False
    End If
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Grabar)"
    
    LblTotal.Caption = LblValorAnt
    
End Function

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Dim PosAct As Variant
    
    Static EnEjecucion As Boolean
    
    If EnEjecucion Then
        Exit Sub
    End If
    
    EnEjecucion = True
    
    PosAct = Denominaciones.sp_CeldaActual
    
    Select Case Shift
        
        Case vbAltMask
        
        Case vbCtrlMask
        
        Case Else
            
            Select Case KeyCode
                
                Case vbKeyF12
                    
                    Actualizar = False
                    
                    'If ClaseRutinas.Mensajes("� Esta seguro de querer Salir ?", True) Then
                    If ClaseRutinas.Mensajes(WinApiFunc.Stellar_Mensaje(10088, True), True) Then
                        
                        Call ClaseCierres.BorrarDatosTemporales(LCodigoMoneda, _
                        Me.LCaja, Me.LCajero, Me.LTurno)
                        
                        MontoTotal = 0
                        
                        Unload Me
                        
                    End If
                    
                Case vbKeyF4
                    
                    If GrabarTodo() Then ' Por si acaso...
                        
                        Actualizar = True
                        
                        MontoTotal = CDbl(Me.LblTotal.Caption)
                        
                        Unload Me
                        
                        Exit Sub
                        
                    End If
                    
                Case vbKeyF1
                    'AYUDA
                    
                Case vbKeyF2
                    
                    If Denominaciones.S_TomarValor(CInt(PosAct(0)), 6) = "S" _
                    And CantidadActual > 0 Then
                        Call EditarSeriales(CantidadActual)
                    End If
                    
            End Select
            
    End Select
    
    EnEjecucion = False
    
End Sub

Private Sub Form_Load()
    
    PermitirGrabar = False
    
    'Me.lbl_Organizacion.Caption = "Cantidades de denominaciones"
    
    Me.lbl_Organizacion.Caption = WinApiFunc.Stellar_Mensaje(10086, True)
    
    Me.Barra.Buttons(1).Caption = WinApiFunc.Stellar_Mensaje(103, True) 'grabar
    Me.Barra.Buttons(3).Caption = WinApiFunc.Stellar_Mensaje(107, True) 'salir
    Me.Barra.Buttons(4).Caption = WinApiFunc.Stellar_Mensaje(7, True) 'ayuda
    Me.Barra.Buttons(5).Caption = WinApiFunc.Stellar_Mensaje(10080, True) 'teclado
    
    Me.Label1 = WinApiFunc.Stellar_Mensaje(141, True) 'Total
    
    Call ClaseCierres.IniciarGridDenominaciones(Denominaciones, Me.LCodigoMoneda, _
    Me.LOrganizacion, Me.LLocalidad, Me.LGrupo, Me.LCaja, Me.LTurno, Me.LCajero)
    
    PermitirGrabar = True
    
End Sub

Private Sub EditarSeriales(CantidadMaxima As Long)
    
    On Error GoTo Error
    
    Dim PosAct As Variant
    
    PosAct = Denominaciones.sp_CeldaActual
    
    Set FrmSeriales.ConexionAdm = ConexionAdm
    Set FrmSeriales.ConexionPos = ConexionPos
    Set FrmSeriales.ClaseCierres = ClaseCierres
    Set FrmSeriales.ClaseRutinas = ClaseRutinas
    
    FrmSeriales.LCaja = LCaja
    FrmSeriales.LCajero = LCajero
    FrmSeriales.LTurno = LTurno
    FrmSeriales.LCodigoDenominacion = Denominaciones.S_TomarValor(CInt(PosAct(0)), 1)
    FrmSeriales.LCodigoMoneda = LCodigoMoneda
    FrmSeriales.CantidadMaxima = CantidadMaxima
    
    FrmSeriales.Show vbModal
    
    PermitirGrabar = False
    
    Denominaciones.S_FijarValor CInt(PosAct(0)), 3, FrmSeriales.CantidadMaxima
    
    Call ClaseCierres.GrabarDatoDelRenglon(CStr(LCodigoMoneda), _
    CStr(Denominaciones.S_TomarValor(CInt(PosAct(0)), 1)), _
    CLng(Denominaciones.S_TomarValor(CInt(PosAct(0)), 3)), _
    CStr(Me.LCaja), LCajero, Me.LTurno)
    
    PermitirGrabar = True
    
    Set FrmSeriales = Nothing
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(EditarSeriales)"
    
End Sub
