VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{0DB462F1-05B6-41CA-8411-27D636CD48C9}#4.0#0"; "S_GridSf_.ocx"
Begin VB.Form FrmCambioDenominacion 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9975
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   7845
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "FrmCambioDenominacion.frx":0000
   ScaleHeight     =   9975
   ScaleWidth      =   7845
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   26
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   5595
         TabIndex        =   28
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   75
         Width           =   5295
      End
   End
   Begin S_GRIDSF_.S_GRIDSF CAMBIO 
      Height          =   3525
      Left            =   195
      TabIndex        =   12
      Top             =   5640
      Width           =   7395
      _ExtentX        =   13044
      _ExtentY        =   6218
      BackColor       =   16448250
      ForeColor       =   4210752
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Appearance      =   0
      BackColorBkg    =   16448250
      BackColorFixed  =   5000268
      BackColorSel    =   16761024
      BackColorUnpopulated=   -2147483633
      Object.CausesValidation=   0   'False
      Sp_AutoIncrementar=   0   'False
      BackColor       =   16448250
      ForeColor       =   4210752
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Appearance      =   0
      BackColorFixed  =   5000268
      BackColorSel    =   16761024
      BackColorBkg    =   16448250
      BackColorUnpopulated=   -2147483633
      Object.CausesValidation=   0   'False
      ColAlignment0   =   9
      ColWidth0       =   -1
      ForeColorSel    =   -2147483634
      ForeColorFixed  =   -2147483630
      GridColor       =   -2147483630
      GridLinesFixed  =   0
      GridLineWidthBand0=   1
      GridLineWidthHeader0=   1
      GridLineWidthIndent0=   1
      RowHeight0      =   345
      RowHeightMin    =   350
      sp_Autotab      =   0   'False
      sp_Del_EliminarFila=   0   'False
   End
   Begin VB.CommandButton cmd_buscar 
      CausesValidation=   0   'False
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   3375
      Picture         =   "FrmCambioDenominacion.frx":0342
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   5115
      Width           =   360
   End
   Begin VB.TextBox TxtCodigo 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   360
      Left            =   1335
      TabIndex        =   9
      Top             =   5115
      Width           =   1940
   End
   Begin VB.PictureBox CoolBar1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   8055
      TabIndex        =   22
      Top             =   421
      Width           =   8085
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   1080
         Left            =   -90
         TabIndex        =   23
         Top             =   0
         Width           =   7500
         Begin MSComctlLib.Toolbar Barra 
            Height          =   810
            Left            =   300
            TabIndex        =   14
            Top             =   120
            Width           =   7000
            _ExtentX        =   12356
            _ExtentY        =   1429
            ButtonWidth     =   1349
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   5
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Grabar"
                  Key             =   "Grabar"
                  ImageIndex      =   1
                  BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
                     NumButtonMenus  =   1
                     BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                        Key             =   "ING"
                        Text            =   "Ingreso"
                     EndProperty
                  EndProperty
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Style           =   3
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "Salir"
                  Object.ToolTipText     =   "Salir de la Pantalla"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "&Ayuda"
                  Key             =   "Ayuda"
                  Object.ToolTipText     =   "Ayuda del Sistema"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Teclado"
                  Key             =   "Teclado"
                  ImageIndex      =   5
               EndProperty
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame_datos 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   " Datos del Cheque a Cambiar "
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   3285
      Left            =   225
      TabIndex        =   15
      Top             =   1740
      Width           =   7365
      Begin VB.CommandButton view 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5580
         Picture         =   "FrmCambioDenominacion.frx":0B44
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   1200
         Width           =   360
      End
      Begin VB.TextBox tbanco 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2610
         TabIndex        =   4
         Top             =   1200
         Width           =   2895
      End
      Begin VB.TextBox numero_den 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2610
         MaxLength       =   50
         TabIndex        =   6
         Top             =   1650
         Width           =   2895
      End
      Begin VB.TextBox monto_den 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2610
         TabIndex        =   7
         Text            =   "0.00"
         Top             =   2100
         Width           =   2895
      End
      Begin VB.TextBox Emisor 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2610
         MaxLength       =   30
         TabIndex        =   3
         Top             =   750
         Width           =   4275
      End
      Begin VB.TextBox observacion 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   615
         Left            =   2610
         MaxLength       =   100
         MultiLine       =   -1  'True
         TabIndex        =   8
         Top             =   2550
         Width           =   4275
      End
      Begin VB.TextBox txt_denominacion 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   2625
         MaxLength       =   10
         TabIndex        =   0
         Top             =   300
         Width           =   1410
      End
      Begin VB.CommandButton cmd_denominacion 
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   4125
         Picture         =   "FrmCambioDenominacion.frx":1346
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   300
         Width           =   360
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Banco"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   330
         TabIndex        =   21
         Top             =   1200
         Width           =   1470
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Número"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   330
         TabIndex        =   20
         Top             =   1650
         Width           =   1995
      End
      Begin VB.Label label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Monto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   330
         TabIndex        =   19
         Top             =   2100
         Width           =   1845
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Emisor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   330
         TabIndex        =   18
         Top             =   750
         Width           =   1785
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Motivo del Cambio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   330
         TabIndex        =   17
         Top             =   2550
         Width           =   2055
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Denominación"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   330
         TabIndex        =   16
         Top             =   300
         Width           =   2160
      End
      Begin VB.Label lbl_denominacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   4560
         TabIndex        =   2
         Top             =   300
         Width           =   2325
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   7020
      Top             =   1170
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmCambioDenominacion.frx":1B48
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmCambioDenominacion.frx":38DA
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmCambioDenominacion.frx":566C
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmCambioDenominacion.frx":73FE
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmCambioDenominacion.frx":9190
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label LblMoneda 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   360
      Left            =   3845
      TabIndex        =   11
      Top             =   5115
      Width           =   3730
   End
   Begin VB.Label Label8 
      BackStyle       =   0  'Transparent
      Caption         =   "Moneda"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   270
      Left            =   345
      TabIndex        =   25
      Top             =   5115
      Width           =   825
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Monto"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   240
      Left            =   4095
      TabIndex        =   24
      Top             =   9405
      Width           =   525
   End
   Begin VB.Label monto_retirado 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "0.00"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   360
      Left            =   5235
      TabIndex        =   13
      Top             =   9375
      Width           =   2325
   End
End
Attribute VB_Name = "FrmCambioDenominacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public LCaja                    As String
Public LCajero                  As String
Public LCajeroDescripcion       As String
Public LTurno                   As Double
Public LLocalidad               As String
Public LOrganizacion            As String
Public LGrupo                   As String

Public ConexionAdm As Object
Public ConexionPos As Object
Public ClaseRutinas As New cls_Rutinas
Public ClaseCierres As New Cls_Cierres
Public ClaseMonedas As New cls_Monedas
Public ClaseDenominaciones As New cls_Denominaciones
Public ClaseBancos As New Cls_Bancos

Private BuscarBanco As Object 'New recsuna.cls_banco
Private CodigoDelBanco As String
Private TxtObjeto As Object

Private Sub Barra_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Dim mValor As Double
    Dim POS As Variant
    
    Select Case UCase(Button.Key)
        Case "GRABAR"
            Call Grabar
        Case "SALIR"
            Unload Me
        Case UCase("Teclado")
            If TxtObjeto Is Nothing Then Exit Sub
            
            If UCase(TxtObjeto.Name) = "MONTO_DEN" Then
                
                If IsNumeric(TxtObjeto) Then
                    mValor = CDbl(TxtObjeto)
                Else
                    mValor = 0
                End If
                
                Call ClaseRutinas.LlamarTecladoNumerico(mValor, ClaseMonedas.DecMoneda)
                
                TxtObjeto = FormatNumber(mValor, ClaseMonedas.DecMoneda)
                
                ClaseRutinas.SendKeys Chr(vbKeyReturn), True
                
            ElseIf UCase(TxtObjeto.Name) = "CAMBIO" Then
                
                If IsNumeric(TxtObjeto) Then
                    mValor = CDbl(TxtObjeto)
                Else
                    mValor = 0
                End If
                
                POS = CAMBIO.sp_CeldaActual
                
                Call ClaseRutinas.LlamarTecladoNumerico(mValor, ClaseMonedas.DecMoneda)
                
                CAMBIO.S_FijarValor CInt(POS(0)), CInt(POS(1)), CInt(FormatNumber(mValor, 0))
                
                Call CAMBIO_LeaveCell
                
            Else
                AbrirTeclado TxtObjeto
            End If
    End Select
    
End Sub

Private Sub CAMBIO_GotFocus()
    Set TxtObjeto = CAMBIO
    ClaseRutinas.SendKeys Chr(vbKeyHome) & Chr(vbKeyRight), True
End Sub

Private Sub CAMBIO_LeaveCell()
    
    Dim PosAct As Variant
    
    PosAct = CAMBIO.sp_CeldaActual
    
    If Trim(monto_den.Text) = "" Then Exit Sub
    
    If CInt(PosAct(1)) = 3 Then
        Call CAMBIO.S_FijarValor(CInt(PosAct(0)), 5, CDbl(CAMBIO.S_TomarValor(CInt(PosAct(0)), 4)) * CDbl(CAMBIO.S_TomarValor(CInt(PosAct(0)), 3)))
    End If
    
    monto_retirado.Caption = FormatNumber(SumarColumna(CAMBIO, 5), ClaseMonedas.DecMoneda)
    
    If CDbl(monto_retirado.Caption) > CDbl(monto_den.Text) Then
        ClaseRutinas.Mensajes "Superó el monto ingresado en la denominación.", False
        Call CAMBIO.S_FijarValor(CInt(PosAct(0)), 5, 0)
        Call CAMBIO.S_FijarValor(CInt(PosAct(0)), 3, 0)
    End If
    
    'ClaseRutinas.SendKeys Chr(vbKeyLeft) & Chr(vbKeyLeft) & Chr(vbKeyDown), True
    'CAMBIO
    
    Observacion.SetFocus
    
End Sub

Private Sub cmd_denominacion_Click()
    
    Dim ArrayMonedas As Variant
    
    ArrayMonedas = BuscarBanco.Buscar_DenominacionInterfaz(ConexionAdm.ConnectionString, ClaseMonedas.CodMoneda, se_TipoDenominacion_NoReales)
    
    If Not IsEmpty(ArrayMonedas) Then
        If UCase(ArrayMonedas(0)) <> "EFECTIVO" Then
            Me.txt_denominacion.Text = ArrayMonedas(0)
            Me.lbl_denominacion.Caption = ArrayMonedas(1)
        Else
            gRutinas.Mensajes StellarMensaje(262)
        End If
    End If
    
End Sub

Private Sub Emisor_GotFocus()
    Set TxtObjeto = Emisor
End Sub

Private Sub Emisor_LostFocus()
    Set TxtObjeto = Nothing
End Sub

Private Sub Form_Activate()
    
    Me.TxtCodigo.Text = ClaseMonedas.CodMoneda
    Me.LblMoneda.Caption = ClaseMonedas.DesMoneda
    
    DoEvents
    
    ClaseCierres.CargarGridDenominaciones CAMBIO, ClaseMonedas.CodMoneda
    
    DoEvents
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbAltMask
            Case vbKeyG
                Call Grabar
            Case vbKeyS
                Unload Me
        Case vbCtrlMask
        
        Case Else
            Select Case KeyCode
                Case vbKeyReturn
                    ClaseRutinas.SendKeys Chr(vbKeyTab), True
                Case vbKeyEscape, vbKeyF12
                    Unload Me
                Case vbKeyF4
                    Call Grabar
            End Select
    End Select
End Sub

Private Sub Form_Load()
    
    Call ClaseCierres.ConfigGridCambio(CAMBIO)
    
    'Me.Caption = "Cambio de denominación"
    
    Me.lbl_Organizacion.Caption = WinApiFunc.Stellar_Mensaje(10079, True) 'Cambio de denominación
    Me.Barra.Buttons(1).Caption = WinApiFunc.Stellar_Mensaje(103, True) 'grabar
    Me.Barra.Buttons(3).Caption = WinApiFunc.Stellar_Mensaje(107, True) 'salir
    Me.Barra.Buttons(4).Caption = WinApiFunc.Stellar_Mensaje(7, True) 'teclado
    Me.Barra.Buttons(5).Caption = WinApiFunc.Stellar_Mensaje(10080, True) 'ayuda
    
    Label7.Caption = WinApiFunc.Stellar_Mensaje(2008, True) 'Denominación
    Label5.Caption = WinApiFunc.Stellar_Mensaje(10081, True) 'Emisor
    Label1.Caption = WinApiFunc.Stellar_Mensaje(189, True) 'Banco
    Label2.Caption = WinApiFunc.Stellar_Mensaje(10082, True) 'Número
    Label3.Caption = WinApiFunc.Stellar_Mensaje(2517, True) 'Monto
    Label6.Caption = WinApiFunc.Stellar_Mensaje(10083, True) 'Motivo Del Cambio
    Label8.Caption = Replace(WinApiFunc.Stellar_Mensaje(134, True), ":", "") 'Moneda
    Label4.Caption = WinApiFunc.Stellar_Mensaje(2517, True) 'Monto
    
    ClaseDenominaciones.InicializarConexiones ConexionAdm, ConexionPos
    ClaseBancos.InicializarConexiones ConexionAdm, ConexionPos
    
    Set BuscarBanco = CreateObject("recsuna.cls_banco")
    
End Sub

'Private Sub monto_den_Change()
'    If IsNumeric(monto_den.Text) Then
'        If CDbl(monto_den.Text) > 0 And Trim(numero_den.Text) <> "" And Trim(txt_denominacion.Text) <> "" And Trim(Emisor.Text) <> "" And Trim(tbanco.Text) <> "" Then
'            CAMBIO.Enabled = True
'        Else
'            CAMBIO.Enabled = False
'        End If
'    End If
'End Sub

Private Sub monto_den_GotFocus()
    monto_den.SelStart = 0
    monto_den.SelLength = Len(monto_den.Text)
    
    Set TxtObjeto = monto_den
End Sub

Private Sub monto_den_LostFocus()
    If IsNumeric(monto_den.Text) Then
        monto_den.Text = FormatNumber(monto_den.Text, ClaseMonedas.DecMoneda)
    End If
    
    Set TxtObjeto = Nothing
End Sub

Private Sub numero_den_GotFocus()
    Set TxtObjeto = numero_den
End Sub

Private Sub numero_den_LostFocus()
    Set TxtObjeto = Nothing
End Sub

Private Sub observacion_GotFocus()
    Set TxtObjeto = Observacion
End Sub

Private Sub observacion_LostFocus()
    Set TxtObjeto = Nothing
End Sub

Private Sub tbanco_GotFocus()
    Set TxtObjeto = tbanco
End Sub

Private Sub tbanco_LostFocus()
    
    Dim RsBancos  As Object
    
    If Trim(Me.tbanco.Text) = "" Then Exit Sub
    
    If ClaseBancos.BuscarBanco(RsBancos, tbanco.Text) Then
        CodigoDelBanco = RsBancos!c_Codigo
        tbanco.Text = RsBancos!c_Descripcio
    Else
        If Trim(CodigoDelBanco) <> "" Then
            If ClaseBancos.BuscarBanco(RsBancos, CodigoDelBanco) Then
                tbanco.Text = RsBancos!c_Descripcio
            End If
        Else
            CodigoDelBanco = tbanco.Text
        End If
    End If
    
    Set TxtObjeto = Nothing
    
End Sub

Private Sub txt_denominacion_GotFocus()
    Set TxtObjeto = txt_denominacion
    
    txt_denominacion.SelLength = 0
    txt_denominacion.SelStart = Len(txt_denominacion.Text)
End Sub

Private Sub txt_denominacion_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 And Shift = 0 Then
        Call cmd_denominacion_Click
    End If
End Sub

Private Sub txt_denominacion_LostFocus()
    
    Dim RsDenominacion As Object
    
    If Trim(Me.txt_denominacion.Text) = "" Then
        Me.lbl_denominacion.Caption = ""
        Exit Sub
    End If
    
    If ClaseDenominaciones.BuscarDenominacion(RsDenominacion, ClaseMonedas.CodMoneda, Me.txt_denominacion.Text, 2, False) Then
        Me.lbl_denominacion.Caption = RsDenominacion!c_Denominacion
    Else
        'ClaseRutinas.Mensajes "No se pudo acceder a la denominación", False
        ClaseRutinas.Mensajes WinApiFunc.Stellar_Mensaje(10049, True), False
    End If
    
    Set TxtObjeto = Nothing
    
End Sub

Private Sub Grabar()
    
    If Not ClaseCierres.GrabarCambioDenominacion(CAMBIO, LOrganizacion, _
    LLocalidad, LCaja, LTurno, Me.LCajero, LCajeroDescripcion, _
    TxtCodigo.Text, Me.TxtCodigo.Text, ClaseMonedas.FacMoneda, _
    Me.txt_denominacion.Text, Me.numero_den.Text, CodigoDelBanco, _
    Me.Emisor.Text, CDbl(Me.monto_den), Me.Observacion.Text) Then
        'ClaseRutinas.Mensajes "No se pudo grabar...!", False
        ClaseRutinas.Mensajes WinApiFunc.Stellar_Mensaje(10084, True), False
    Else
        Unload Me
    End If
    
End Sub

Private Function SumarColumna(Grid As S_GRIDSF, Columna As Integer) As Double
    
    Dim LCont As Integer
    
    'On Error GoTo Falla_Local
    
    SumarColumna = 0
    
    For LCont = 1 To Grid.Rows - 1
        If Trim(Grid.S_TomarValor(LCont, Columna)) = "" Then
            Exit For
        End If
        SumarColumna = SumarColumna + CDbl(Grid.S_TomarValor(LCont, Columna))
    Next
    
Falla_Local:
    
End Function

Private Sub TxtCodigo_GotFocus()
    Set TxtObjeto = TxtCodigo
End Sub

Private Sub TxtCodigo_LostFocus()
    Set TxtObjeto = Nothing
End Sub

Private Sub view_Click()
    
    Dim ArrayBancos As Variant
    
    ArrayBancos = BuscarBanco.Buscar_BancosInterfaz(ConexionAdm.ConnectionString)
    
    If Not IsEmpty(ArrayBancos) Then
        CodigoDelBanco = ArrayBancos(0)
        Me.tbanco.Text = ArrayBancos(1)
    Else
        CodigoDelBanco = ""
    End If
    
End Sub
