VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form Lista_Denominacionreal 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8280
   ClientLeft      =   15
   ClientTop       =   75
   ClientWidth     =   10350
   ControlBox      =   0   'False
   Icon            =   "Lista_Denominacionreal.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8280
   ScaleWidth      =   10350
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame4 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   8160
         TabIndex        =   9
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   10665
      TabIndex        =   5
      Top             =   421
      Width           =   10695
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   240
         TabIndex        =   6
         Top             =   120
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   1429
         ButtonWidth     =   1376
         ButtonHeight    =   1429
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Icono_Apagado"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   7
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Grabar"
               Key             =   "Grabar"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Salir"
               Key             =   "Salir"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Object.Visible         =   0   'False
               Style           =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Ayuda"
               Key             =   "Ayuda"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Teclado"
               Key             =   "Teclado"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Banco"
               Key             =   "Banco"
               ImageIndex      =   6
            EndProperty
         EndProperty
      End
   End
   Begin VB.TextBox total 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   360
      Left            =   7440
      Locked          =   -1  'True
      TabIndex        =   3
      Text            =   "0.00"
      Top             =   7680
      Width           =   2655
   End
   Begin VB.TextBox totalcantidad 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   360
      Left            =   2760
      Locked          =   -1  'True
      TabIndex        =   2
      Text            =   "0.00"
      Top             =   7680
      Width           =   2655
   End
   Begin MSFlexGridLib.MSFlexGrid grid_denominaciones 
      Height          =   5805
      Left            =   240
      TabIndex        =   1
      Top             =   1620
      Width           =   9915
      _ExtentX        =   17489
      _ExtentY        =   10239
      _Version        =   393216
      Cols            =   5
      FixedCols       =   0
      RowHeightMin    =   350
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   5000268
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   4210752
      AllowBigSelection=   0   'False
      GridLinesFixed  =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   2940
      Top             =   1170
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Lista_Denominacionreal.frx":628A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Lista_Denominacionreal.frx":801C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Lista_Denominacionreal.frx":9DAE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Lista_Denominacionreal.frx":BB40
            Key             =   "Teclado"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Lista_Denominacionreal.frx":10B2C
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Lista_Denominacionreal.frx":10D84
            Key             =   "Buscar"
         EndProperty
      EndProperty
   End
   Begin VB.TextBox txteditor 
      BackColor       =   &H00800000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   2520
      MaxLength       =   20
      TabIndex        =   0
      Top             =   2160
      Width           =   1365
   End
   Begin VB.Label Label1 
      Caption         =   "Totales"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   375
      Left            =   480
      TabIndex        =   4
      Top             =   7680
      Width           =   975
   End
End
Attribute VB_Name = "Lista_Denominacionreal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Enum eColGridDenomina
    dColBanco
    dColNumero
    dColMonto
    dColTotal
    dColCodDenomina
    dColCodBanco
End Enum

Public ClaseRutinas As New cls_Rutinas
Public Forma As Object
Public CnAdm As ADODB.Connection, CnPos As ADODB.Connection
Public NumCaja As String, mUsuario As String, mCajero As String, mTurno As Double
Public mMoneda As String, mDenominacion As String, mDescriDenominacion As String
Public mFactorMoneda As Double
Public mDenominacionAutoDeclara As Boolean

Dim CellRow As Long, CellCol As Long
Dim mGrabo As Boolean

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        Case vbKeyF4
            If Not txteditor.Visible Then
                'If VerificarPaso("Desea Guardar los Datos") Then
                If ClaseRutinas.Mensajes(WinApiFunc.Stellar_Mensaje(10094, True), True) Then
                    Call GrabarDenominaciones
                End If
            End If
        Case vbKeyF12
            If Not txteditor.Visible Then
                Unload Me
            End If
           
    End Select
    
End Sub

Private Sub Form_Load()
    
    Me.txteditor.Visible = False
    
    Call IniciarGridDenominacion
    
    CargarDenominaciones
    
    'Me.Caption = mDescriDenominacion
    
    Me.lbl_Organizacion.Caption = mDescriDenominacion
    
    Me.Toolbar1.Buttons(1).Caption = WinApiFunc.Stellar_Mensaje(103, True) 'grabar
    Me.Toolbar1.Buttons(3).Caption = WinApiFunc.Stellar_Mensaje(107, True) 'salir
    Me.Toolbar1.Buttons(4).Caption = WinApiFunc.Stellar_Mensaje(7, True) 'ayuda
    
    Me.Toolbar1.Buttons("Banco").Caption = _
    StrConv(WinApiFunc.Stellar_Mensaje(10091, True), vbProperCase)
    
    Me.Label1.Caption = WinApiFunc.Stellar_Mensaje(10090, True) 'Totales
    
End Sub

Private Sub IniciarGridDenominacion()

'    With grid_denominaciones
'        .Rows = 2
'        .Cols = 6
'        .Row = 0
'        .ColWidth(eColGridDenomina.dColBanco) = 2800
'        .TextMatrix(.Row, eColGridDenomina.dColBanco) = "BANCO"
'        .ColWidth(eColGridDenomina.dColNumero) = 1500
'        .TextMatrix(.Row, eColGridDenomina.dColNumero) = "CANT"
'        .Col = eColGridDenomina.dColNumero
'        totalcantidad.Move .Left + .CellLeft, totalcantidad.Top, .CellWidth - Me.ScaleX(1, vbPixels, vbTwips), totalcantidad.Height
'        .ColWidth(eColGridDenomina.dColMonto) = 2500
'        .TextMatrix(.Row, eColGridDenomina.dColMonto) = "DENOMINACION"
'        .ColWidth(eColGridDenomina.dColTotal) = 3000
'        .TextMatrix(.Row, eColGridDenomina.dColTotal) = "TOTAL"
'        .Col = eColGridDenomina.dColTotal
'        total.Move .Left + .CellLeft, total.Top, .CellWidth - Me.ScaleX(1, vbPixels, vbTwips), total.Height
'        .ColWidth(eColGridDenomina.dColCodDenomina) = 0
'        .TextMatrix(.Row, eColGridDenomina.dColCodDenomina) = "CODDENOMINA"
'        .ColWidth(eColGridDenomina.dColCodBanco) = 0
'        .TextMatrix(.Row, eColGridDenomina.dColCodBanco) = "CODDBANCO"
'    End With
    
    With grid_denominaciones
        
        .Rows = 2
        .Cols = 6
        .Row = 0
        
        .ColWidth(eColGridDenomina.dColBanco) = 2800 'se deja en 2800 para que no se expanda y no haga scroll horizontal
        .TextMatrix(.Row, eColGridDenomina.dColBanco) = WinApiFunc.Stellar_Mensaje(10091, True)
        .ColWidth(eColGridDenomina.dColNumero) = 1500
        .TextMatrix(.Row, eColGridDenomina.dColNumero) = WinApiFunc.Stellar_Mensaje(10092, True)
        .Col = eColGridDenomina.dColNumero
        totalcantidad.Move .Left + .CellLeft, totalcantidad.Top, .CellWidth - Me.ScaleX(1, vbPixels, vbTwips), totalcantidad.Height
        .ColWidth(eColGridDenomina.dColMonto) = 2500
        .TextMatrix(.Row, eColGridDenomina.dColMonto) = WinApiFunc.Stellar_Mensaje(10053, True)
        .ColWidth(eColGridDenomina.dColTotal) = 3000
        .TextMatrix(.Row, eColGridDenomina.dColTotal) = WinApiFunc.Stellar_Mensaje(10093, True)
        .Col = eColGridDenomina.dColTotal
        total.Move .Left + .CellLeft, total.Top, .CellWidth - Me.ScaleX(1, vbPixels, vbTwips), total.Height
        .ColWidth(eColGridDenomina.dColCodDenomina) = 0
        .TextMatrix(.Row, eColGridDenomina.dColCodDenomina) = "CODDENOMINA"
        .ColWidth(eColGridDenomina.dColCodBanco) = 0
        .TextMatrix(.Row, eColGridDenomina.dColCodBanco) = "CODDBANCO"
        
        .AllowUserResizing = flexResizeColumns
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
    End With
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Not mGrabo And SVal(total.Text) > 0 Then
        'Cancel = IIf(VerificarPaso("Desea Salir sin Guardar"), 0, 1)
        Cancel = Not gRutinas.Mensajes(StellarMensaje(236), True)
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    If Not mGrabo Then
        
        If Not mDenominacionAutoDeclara Then
            
            CnPos.Execute _
            "UPDATE TR_DENOMINA_TEMP SET " & _
            "Cantidad = 0 " & _
            "WHERE CodMoneda = '" & mMoneda & "' " & _
            "AND Caja = '" & NumCaja & "' " & _
            "AND CodDenomina = '" & mDenominacion & "' " & _
            "AND Usuario = '" & mCajero & "' " & _
            "AND Turno = " & CDec(mTurno) & " "
            
            With Forma.monitor
                .Row = .RowSel
                .Col = GridParcial.ParTotalRecibio   '4
                    .Text = FormatNumber(0, ClaseMonedaTmp.DecMoneda)
                .Col = 0
               ' oTeclado.Key_Right
            End With
            
        End If
        
        Forma.fCambio = False
        
    Else
        
        Forma.fCambio = True
        
        With Forma.monitor
           .Row = .RowSel
            '.Col = 3
                'montov = CDbl(.Text)
            .Col = GridParcial.ParTotalRecibio   '4
                Call TotalizarColumna(grid_denominaciones)
                .Text = total.Text
            '.Col = GridParcial.ParDiferencia    '8
                '.Text = FormatNumber(CDbl(total.Text), 2, , , vbTrue)
            .Col = 0
              
            'oTeclado.Key_Right
        End With
        
    End If
    
    'FORMA.Calculo
    
End Sub

Private Sub grid_denominaciones_DblClick()
    
    Dim mValor As Variant
    
    If grid_denominaciones.Row >= 1 Then
        
        Select Case grid_denominaciones.Col
            
            Case eColGridDenomina.dColBanco, eColGridDenomina.dColNumero
                
                Call MostrarEditorTexto(Me, grid_denominaciones, txteditor, CellRow, CellCol)
                
            Case eColGridDenomina.dColMonto
                
                mValor = grid_denominaciones.TextMatrix(grid_denominaciones.Row, 0)
                
                If IsNumeric(mValor) Then
                    
                    If mValor > 0 Then
                        Call MostrarEditorTexto(Me, grid_denominaciones, txteditor, CellRow, CellCol)
                    End If
                    
                End If
                
        End Select
        
    End If
    
End Sub

Private Sub grid_denominaciones_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If grid_denominaciones.Row >= 1 Then
        
        Select Case KeyCode
            
            Case vbKeyDelete
                
                If grid_denominaciones.Col = eColGridDenomina.dColBanco Then
                    
                    If grid_denominaciones.Rows > 2 Then
                        
                        grid_denominaciones.RemoveItem grid_denominaciones.Row
                        
                    Else
                        
                        Call LimpiarFila(grid_denominaciones, grid_denominaciones.Row)
                        
                    End If
                    
                    Call TotalizarColumna(Me.grid_denominaciones)
                    
                End If
                
            Case vbKeyF2
                
                If grid_denominaciones.Col = eColGridDenomina.dColBanco Then
                    
                    BuscarBancos
                    
                    If SVal(grid_denominaciones.TextMatrix( _
                    grid_denominaciones.Row, eColGridDenomina.dColNumero)) <= 0 Then
                        
                        grid_denominaciones.TextMatrix(grid_denominaciones.Row, eColGridDenomina.dColNumero) = "1.00"
                        grid_denominaciones.Col = eColGridDenomina.dColMonto
                        
                        If grid_denominaciones.Visible Then
                            grid_denominaciones.SetFocus
                        End If
                        
                        txteditor.Visible = False
                        
                        DoEvents
                        
                        grid_denominaciones_KeyPress vbKey0
                        
                        Exit Sub
                        
                    End If
                    
                End If
                
        End Select
        
    End If
    
End Sub

Private Sub grid_denominaciones_KeyPress(KeyAscii As Integer)
    
    Dim miVar As String
    
    If grid_denominaciones.Row >= 1 Then
        
        If KeyAscii >= 32 Then
            
            Select Case grid_denominaciones.Col
                
                Case eColGridDenomina.dColBanco
                    
                    Call MostrarEditorTexto(Me, grid_denominaciones, txteditor, CellRow, CellCol)
                    
                    txteditor.Text = Chr$(KeyAscii)
                    txteditor.SelStart = 1
                    
                Case eColGridDenomina.dColMonto, eColGridDenomina.dColNumero
                    
                    Call MostrarEditorTexto(Me, grid_denominaciones, txteditor, CellRow, CellCol)
                    
                    If IsNumeric(Chr(KeyAscii)) Then
                        txteditor.Text = Chr$(KeyAscii)
                        txteditor.SelStart = 1
                    End If
                    
            End Select
            
        ElseIf KeyAscii = vbKeyReturn Then
            
            If grid_denominaciones.Col = eColGridDenomina.dColBanco _
            And grid_denominaciones.Row > 1 Then
                
                If grid_denominaciones.TextMatrix(grid_denominaciones.Row - 1, eColGridDenomina.dColCodBanco) <> "" _
                And grid_denominaciones.TextMatrix(grid_denominaciones.Row - 1, eColGridDenomina.dColBanco) <> "" Then
                    
                    ' Asignamos el banco de la fila de arriba.
                    
                    grid_denominaciones.TextMatrix(grid_denominaciones.Row, eColGridDenomina.dColBanco) = grid_denominaciones.TextMatrix(grid_denominaciones.Row - 1, eColGridDenomina.dColBanco)
                    grid_denominaciones.TextMatrix(grid_denominaciones.Row, eColGridDenomina.dColCodBanco) = grid_denominaciones.TextMatrix(grid_denominaciones.Row - 1, eColGridDenomina.dColCodBanco)
                    grid_denominaciones.TextMatrix(grid_denominaciones.Row, eColGridDenomina.dColCodDenomina) = mDenominacion
                    
                    grid_denominaciones.Col = eColGridDenomina.dColNumero
                    
                    grid_denominaciones.SetFocus
                    
                End If
                
            End If
            
        End If
        
    End If
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case "GRABAR"
            Call Form_KeyDown(vbKeyF4, 0)
        Case "SALIR"
            Call Form_KeyDown(vbKeyF12, 0)
        Case UCase("Teclado")
            TecladoWindows
        Case "BANCO"
            txtEditor_KeyDown vbKeyEscape, 0
            grid_denominaciones_KeyDown vbKeyF2, 0
    End Select
End Sub

Private Sub txtEditor_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Dim mDato As Variant
    Dim Acum As Double
    
    Select Case KeyCode
        Case 13
            Call OcultarEditorTexto(Me.grid_denominaciones, Me.txteditor, CellRow, CellCol)
            mDato = txteditor.Text
        Case 27
            Call OcultarEditorTexto(grid_denominaciones, txteditor, CellRow, CellCol, True)
    End Select
    
End Sub

Private Sub txtEditor_KeyPress(KeyAscii As Integer)
    
    If grid_denominaciones.Col = eColGridDenomina.dColMonto Then
        
        Select Case KeyAscii
            Case 48 To 57
            Case 8
            Case Asc(","), Asc(".")
            Case Else
                KeyAscii = 0
        End Select
        
    End If
    
End Sub

Private Sub txtEditor_LostFocus()
    If IsNumeric(txteditor.Text) Then
        Call OcultarEditorTexto(grid_denominaciones, txteditor, CellRow, CellCol)
    End If
End Sub

Private Sub MostrarEditorTexto(Frm As Form, pGrd As Object, _
ByRef txteditor As Object, ByRef CellRow As Long, ByRef CellCol As Long, _
Optional pForzar As Boolean = False)
    
    With pGrd
        
        .RowSel = .Row
        .ColSel = .Col
        
        txteditor.Move _
        (.Left + .CellLeft), (.Top + .CellTop), _
        .CellWidth - Frm.ScaleX(1, vbPixels, vbTwips), _
        .CellHeight - Frm.ScaleY(1, vbPixels, vbTwips)
        
        txteditor.Text = .Text
        txteditor.Visible = True
        txteditor.ZOrder
        
        txteditor.SetFocus
        
        CellRow = .Row
        CellCol = .Col
        
     End With
     
End Sub

Private Sub OcultarEditorTexto(ByRef pGrd As MSFlexGrid, _
ByRef txteditor As TextBox, pRow As Long, pCol As Long, _
Optional cancelar As Boolean = False)
    
    Dim mTipo As Integer
    Dim mValor As Variant, mValorAux As Variant
    Dim mDiferencia As Double
    
    On Error GoTo Errores
    
    If txteditor.Visible Then
        
        If Not cancelar Then
            
            mValor = txteditor.Text
            
            Select Case pCol
                
                Case eColGridDenomina.dColBanco  ' Banco
                    
                    If mValor <> Empty Then
                        BuscarBancoGrid grid_denominaciones, mValor, pRow
                    End If
                    
                    If SVal(pGrd.TextMatrix(pGrd.Row, eColGridDenomina.dColNumero)) <= 0 Then
                        
                        pGrd.TextMatrix(pGrd.Row, eColGridDenomina.dColNumero) = FormatNumber(1, 2)
                        pGrd.Col = eColGridDenomina.dColMonto
                        
                        If pGrd.Visible Then pGrd.SetFocus
                        
                        txteditor.Visible = False
                        
                        DoEvents
                        
                        grid_denominaciones_KeyPress vbKey0
                        
                        Exit Sub
                        
                    End If
                    
                Case eColGridDenomina.dColNumero  ' Cantidad
                    
                    If pGrd.TextMatrix(pRow, eColGridDenomina.dColBanco) <> Empty _
                    And IsNumeric(mValor) Then
                        
                        pGrd.TextMatrix(pRow, pCol) = FormatNumber(mValor, 2)
                        
                        If IsNumeric(pGrd.TextMatrix(pRow, eColGridDenomina.dColMonto)) Then
                            pGrd.TextMatrix(pRow, eColGridDenomina.dColTotal) = _
                            FormatoDecimalesDinamicos(RoundUp(mValor, 2) _
                            * CDbl(pGrd.TextMatrix(pRow, eColGridDenomina.dColMonto)), _
                            0, ClaseMonedaTmp.DecMoneda)
                        Else
                            pGrd.TextMatrix(pRow, eColGridDenomina.dColTotal) = 0 'FormatNumber(0, 2)
                        End If
                        
                        Call TotalizarColumna(Me.grid_denominaciones)
                        
                        pGrd.Col = eColGridDenomina.dColMonto
                        
                        pGrd.SetFocus
                        
                    Else
                        pGrd.TextMatrix(pRow, pCol) = Empty
                    End If
                    
                Case eColGridDenomina.dColMonto  ' Monto
                    
                    If IsNumeric(mValor) _
                    And pGrd.TextMatrix(pRow, eColGridDenomina.dColBanco) <> Empty Then
                        
                        pGrd.TextMatrix(pRow, pCol) = FormatoDecimalesDinamicos(mValor, , ClaseMonedaTmp.DecMoneda)
                        
                        If IsNumeric(pGrd.TextMatrix(pRow, eColGridDenomina.dColNumero)) Then
                            pGrd.TextMatrix(pRow, eColGridDenomina.dColTotal) = _
                            FormatoDecimalesDinamicos(RoundUp(mValor, ClaseMonedaTmp.DecMoneda) * _
                            CDbl(pGrd.TextMatrix(pRow, eColGridDenomina.dColNumero)), _
                            0, ClaseMonedaTmp.DecMoneda)
                        Else
                            pGrd.TextMatrix(pRow, eColGridDenomina.dColTotal) = 0 'FormatNumber(0, 2)
                        End If
                        
                        Call TotalizarColumna(Me.grid_denominaciones)
                        
                        If ValidarFila(pGrd, pRow) Then
                            If pRow = pGrd.Rows - 1 Then pGrd.Rows = pGrd.Rows + 1
                            pGrd.Row = pGrd.Rows - 1
                            pGrd.Col = eColGridDenomina.dColBanco
                            pGrd.LeftCol = eColGridDenomina.dColBanco
                            pGrd.SetFocus
                        End If
                        
                    Else
                        pGrd.TextMatrix(pRow, pCol) = 0 'FormatNumber(0, 2)
                    End If
                    
            End Select
            
            txteditor.Visible = False
            
            pGrd.SetFocus
            
        End If
        
    End If
    
    Exit Sub
    
Errores:
    
End Sub

Private Function ValidarFila(pGrd As MSFlexGrid, pFila As Long) As Boolean
    
    Dim mCol As Long, mValidar As Boolean
    
    mValidar = True
    
    Do While mCol <= pGrd.Cols - 1 And mValidar
        
        Select Case mCol
            
            Case eColGridDenomina.dColNumero, eColGridDenomina.dColMonto, eColGridDenomina.dColTotal
                If IsNumeric(pGrd.TextMatrix(pFila, mCol)) Then
                    mValidar = CDbl(pGrd.TextMatrix(pFila, mCol)) > 0
                End If
                
            Case Else
                
                mValidar = pGrd.TextMatrix(pFila, mCol) <> Empty
                
        End Select
        
        mCol = mCol + 1
        
    Loop
    
    ValidarFila = mValidar
    
End Function

Private Sub LimpiarFila(pGrd As MSFlexGrid, pFila As Long)
    
    Dim mCol As Long
    
    For mCol = 0 To pGrd.Cols - 1
        pGrd.TextMatrix(pFila, mCol) = Empty
    Next mCol
    
End Sub

Private Sub BuscarBancos()
    
    Dim mResul As Variant, mSQL As String
    
    On Error GoTo Errores
    
    mSQL = "Select c_Codigo, c_Descripcio " & _
    "FROM MA_BANCOS "
    
    With Frm_Super_Consultas
        
        .Inicializar mSQL, WinApiFunc.Stellar_Mensaje(10091, True), CnAdm
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(10096, True), "c_codigo", 1500, 0
        .Add_ItemLabels WinApiFunc.Stellar_Mensaje(143, True), "c_descripcio", 4000, 0
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(143, True), "c_descripcio"
        .Add_ItemSearching WinApiFunc.Stellar_Mensaje(10096, True), "c_codigo"
        
        .txtDato.Text = "%"
        .CmdBuscar(0).Value = True
        
        .Show vbModal
        
        mResul = .ArrResultado
        
    End With
    
    If Not IsEmpty(mResul) Then
        
        If Not (mResul(0) = Empty) Then
            
            grid_denominaciones.TextMatrix(grid_denominaciones.Row, eColGridDenomina.dColBanco) = mResul(1)
            grid_denominaciones.TextMatrix(grid_denominaciones.Row, eColGridDenomina.dColCodBanco) = mResul(0)
            grid_denominaciones.TextMatrix(grid_denominaciones.Row, eColGridDenomina.dColCodDenomina) = mDenominacion
            
            grid_denominaciones.Col = eColGridDenomina.dColNumero
            
            grid_denominaciones.SetFocus
            
        End If
        
    End If
    
    Set Frm_Super_Consultas = Nothing
    
    Exit Sub
    
Errores:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(BuscarBancos)"
    
End Sub

Private Sub BuscarBancoGrid(pGrd As MSFlexGrid, pValor, pFila As Long)
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String, mSqlDet As String
    
    On Error GoTo Errores
    
    mSQL = "Select c_codigo, c_descripcio from ma_bancos " & _
    "where c_codigo = '" & Trim(pValor) & "' " & _
    "or c_descripcio = '" & Trim(pValor) & "' "
    
    mRs.Open mSQL, CnAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        
        pGrd.TextMatrix(pFila, eColGridDenomina.dColBanco) = mRs!c_Descripcio
        pGrd.TextMatrix(pFila, eColGridDenomina.dColCodBanco) = mRs!c_Codigo
        pGrd.TextMatrix(pFila, eColGridDenomina.dColCodDenomina) = mDenominacion
        
        pGrd.Col = eColGridDenomina.dColNumero
        
        pGrd.SetFocus
        
    Else
        LimpiarFila pGrd, pFila
    End If
    
    mRs.Close
    
    Exit Sub
    
Errores:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(BuscarBancoGrid)"
    
End Sub

Private Function VerificarPaso(pMensaje As String) As Boolean
    
    Dim Obj As Object
    
    Set Obj = CreateObject("recsun.OBJ_MENSAJERIA")
    Obj.mensaje pMensaje, True
    VerificarPaso = Obj.PressBoton
    Set Obj = Nothing
    
End Function

Private Sub GrabarDenominaciones()
    
    Dim mCn As New ADODB.Connection
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String, mSqlDet As String
    Dim mFila As Long, mUltimaFila As Long
    Dim mInicio As Boolean
    
    On Error GoTo Err_Manager
    
    mInicio = False
    mUltimaFila = 0
    
    For mFila = 1 To grid_denominaciones.Rows - 1
        
        If mFila = grid_denominaciones.Rows - 1 Then
            
            If grid_denominaciones.TextMatrix(mFila, eColGridDenomina.dColBanco) <> Empty _
            And grid_denominaciones.TextMatrix(mFila, eColGridDenomina.dColTotal) <> Empty Then
                
                If Not ValidarFila(grid_denominaciones, mFila) Then
                    'MsgBox "Faltan por llenar datos en la fila  " & mFila, vbInformation
                    ClaseRutinas.Mensajes WinApiFunc.Stellar_Mensaje(10095) & mFila, True
                    Exit Sub
                End If
                
                mUltimaFila = mFila
                
            End If
            
        Else
            
            If Not ValidarFila(grid_denominaciones, mFila) Then
                'MsgBox "Faltan por llenar datos en la fila  " & mFila, vbInformation
                ClaseRutinas.Mensajes WinApiFunc.Stellar_Mensaje(10095) & mFila, True
                Exit Sub
            End If
            
            mUltimaFila = mFila
            
        End If
        
    Next mFila
    
    mSQL = "SELECT * FROM TR_DENOMINA_TEMP WHERE 1 = 2"
    
    mCn.Open CnPos.ConnectionString
    
    'CnPos.BeginTrans
    
    mCn.BeginTrans
    mInicio = True
    
    mCn.Execute _
    "DELETE FROM TR_DENOMINA_TEMP " & _
    "WHERE CodMoneda = '" & mMoneda & "' " & _
    "AND CodDenomina = '" & mDenominacion & "' " & _
    "AND Caja = '" & NumCaja & "' " & _
    "AND Usuario = '" & mCajero & "' " & _
    "AND Turno = " & CDec(mTurno) & " "
    
    mRs.Open mSQL, mCn, adOpenDynamic, adLockOptimistic, adCmdText
    
    For mFila = 1 To mUltimaFila
        
        mRs.AddNew
            
            mRs!CodMoneda = mMoneda
            mRs!CodDenomina = mDenominacion
            mRs!DesDenomina = mDescriDenominacion
            
            mRs!Cantidad = CDec(grid_denominaciones.TextMatrix(mFila, eColGridDenomina.dColNumero))
            mRs!Valor = CDec(grid_denominaciones.TextMatrix(mFila, eColGridDenomina.dColMonto))
            mRs!Factor = mFactorMoneda
            
            mRs!Caja = NumCaja
            mRs!Usuario = mCajero
            mRs!Turno = CDec(mTurno)
            
            mRs!CodigoBanco = grid_denominaciones.TextMatrix(mFila, eColGridDenomina.dColCodBanco)
            
        mRs.Update
        
    Next mFila
    
    mRs.Close
    mCn.CommitTrans
    
    mInicio = False
    mGrabo = True
    
    Unload Me
    
    Exit Sub
    
Err_Manager:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If mInicio Then
        mCn.RollbackTrans
        mInicio = False
    End If
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(GrabarDenominaciones)"
    
End Sub

Private Sub TotalizarColumna(pGrd As MSFlexGrid)
    
    Dim mCant As Double, mTotal As Double
    Dim mFila As Long
    
    mCant = 0
    mTotal = 0
    
    For mFila = 1 To grid_denominaciones.Rows - 1
        
        If IsNumeric(pGrd.TextMatrix(mFila, eColGridDenomina.dColNumero)) Then
            mCant = mCant + CDbl(pGrd.TextMatrix(mFila, eColGridDenomina.dColNumero))
        End If
        
        If IsNumeric(pGrd.TextMatrix(mFila, eColGridDenomina.dColTotal)) Then
            mTotal = mTotal + CDbl(pGrd.TextMatrix(mFila, eColGridDenomina.dColTotal))
        End If
        
    Next mFila
    
    total.Text = FormatNumber(mTotal, ClaseMonedaTmp.DecMoneda)
    totalcantidad.Text = FormatoDecimalesDinamicos(mCant)

End Sub

Private Sub CargarDenominaciones()
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String, mFila As Integer
    
    mSQL = "SELECT TR.*, " & Srv_Remote_BD_ADM & ".DBO.MA_BANCOS.c_Descripcio AS Banco " & _
    "FROM TR_DENOMINA_TEMP TR " & _
    "LEFT JOIN " & Srv_Remote_BD_ADM & ".DBO.MA_BANCOS " & _
    "ON " & Srv_Remote_BD_ADM & ".DBO.MA_BANCOS.c_Codigo = TR.CodigoBanco " & _
    "WHERE TR.CodMoneda = '" & mMoneda & "' " & _
    "AND TR.CodDenomina = '" & mDenominacion & "' " & _
    "AND TR.Valor > 0 " & _
    "AND TR.Cantidad > 0 " & _
    "AND TR.Caja = '" & NumCaja & "' " & _
    "AND TR.Usuario = '" & mCajero & "' " & _
    "AND TR.Turno = " & CDec(mTurno) & " "
    
    'Debug.Print mSql
    
    mRs.Open mSQL, CnPos, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    With grid_denominaciones
        
        Do While Not mRs.EOF
            
            mFila = mFila + 1
            
            If Not IsNull(mRs!Banco) Then
                
                .TextMatrix(mFila, eColGridDenomina.dColBanco) = mRs!Banco
                .TextMatrix(mFila, eColGridDenomina.dColCodBanco) = mRs!CodigoBanco
                .TextMatrix(mFila, eColGridDenomina.dColCodDenomina) = mDenominacion
                .TextMatrix(mFila, eColGridDenomina.dColMonto) = _
                FormatoDecimalesDinamicos(mRs!Valor, 0, ClaseMonedaTmp.DecMoneda)
                
                .TextMatrix(mFila, eColGridDenomina.dColNumero) = FormatNumber(mRs!Cantidad, 2)
                .TextMatrix(mFila, eColGridDenomina.dColTotal) = FormatoDecimalesDinamicos( _
                mRs!Cantidad * mRs!Valor, 0, ClaseMonedaTmp.DecMoneda)
                
                grid_denominaciones.Rows = grid_denominaciones.Rows + 1
                
            End If
            
            mRs.MoveNext
            
        Loop
        
    End With
    
    Call TotalizarColumna(grid_denominaciones)
    
    grid_denominaciones.Row = grid_denominaciones.Rows - 1
    grid_denominaciones.Col = eColGridDenomina.dColBanco
    
End Sub
