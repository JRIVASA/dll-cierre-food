VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Clase_posMonitor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private Property Get ObjTeclado() As Object
    Set ObjTeclado = ModCierres.ObjTeclado
End Property

Public Property Let ObjTeclado(ByVal pValor As Object)
    Set ModCierres.ObjTeclado = pValor
End Property

Public Property Let AutoDeclarar(ByVal pValor As Boolean)
    ModCierres.AutoDeclarar = pValor
End Property

Public Property Let AnchoImpresoraSetup(ByVal pValor As Integer)
    AnchoSetup = pValor
End Property

Public Property Let HabilitarModoAgente(ByVal pValor As Boolean)
    ModoAgente = pValor
End Property

Public Property Let ImprimirCierreModoAntiguo(ByVal pValor As Boolean)
    ImprimirCierreModoViejo = pValor
End Property

Public Property Let Prop_ImpresoraPOSEspecifica(ByVal pValor As String)
    ImpresoraPOSEspecifica = pValor
End Property

Public Property Let Prop_ImpresoraPOSEspecifica_PorPuerto(ByVal pValor As Boolean)
    ImpresoraPOSEspecifica_PorPuerto = pValor
End Property

Public Property Let Prop_FrmAppLinkADM(ByVal pValor As Object)
    Set FrmAppLinkADM = pValor
End Property

Public Property Let Prop_Srv_Remote_BD_ADM(ByVal pValor As String)
    Srv_Remote_BD_ADM = pValor
End Property

Public Property Let Prop_Srv_Remote_BD_POS(ByVal pValor As String)
    Srv_Remote_BD_POS = pValor
End Property

Public Sub PosMonitor(pBranch As String, pDeposito As String, _
pCodigoUsuario As String, pLocalidadUsuario As String, _
pNivelUsuario As Integer, pConexionAdm As Object, pConexionPos As Object, _
Optional pGrupoCajas As String = "ROOT")
    
    Dim Error As Long
    
    Set ClaseLogin = CreateObject("DLLLogin.Login")
    
    SafePropAssign ClaseLogin, "ObjTeclado", ObjTeclado
    
    GrupoCajas = pGrupoCajas
    Branch = pBranch
    Deposito = pDeposito
    
    Set ConexionAdm = pConexionAdm
    Set ConexionPos = pConexionPos
    
    CodigoUsuario = pCodigoUsuario
    LocalidadUsuario = pLocalidadUsuario
    NivelUsuario = pNivelUsuario
    
    gCierres.InicializarConexiones , , ConexionAdm.ConnectionString, ConexionPos.ConnectionString
    
    Call gCierres.ShowCierreCajas(Empty, Branch, Deposito, GrupoCajas, _
    CodigoUsuario, LocalidadUsuario, NivelUsuario)
    
End Sub

Public Function EjecutarCierreParcial(pCnAdm As Object, _
pCnPos As Object, pCaja As String, pCajero As String, _
pDescriCajero As String, pGrupo As String, pLocalidad As String, _
pOrg As String, pTurno As Double, pUsuario As String, pMensajes As String) As Boolean
    
    Dim mClaseCierres As New Cls_Cierres
    
    mClaseCierres.InicializarConexiones pCnAdm, pCnPos
    
    Set Ficha_Cierre_parcial.ClaseCierres = mClaseCierres
    Set Ficha_Cierre_parcial.ConexionAdm = pCnAdm
    Set Ficha_Cierre_parcial.ConexionPos = pCnPos
    
    Ficha_Cierre_parcial.LCaja = pCaja
    Ficha_Cierre_parcial.LCajero = pCajero
    Ficha_Cierre_parcial.LCajeroDescripcion = pDescriCajero
    Ficha_Cierre_parcial.LGrupo = pGrupo
    Ficha_Cierre_parcial.LLocalidad = pLocalidad
    Ficha_Cierre_parcial.LOrganizacion = Org
    Ficha_Cierre_parcial.LTurno = pTurno
    Ficha_Cierre_parcial.CodigoUsuario = pUsuario
    Ficha_Cierre_parcial.LMensajes = pMensajes
    
    Ficha_Cierre_parcial.Show vbModal
    
    EjecutarCierreParcial = Ficha_Cierre_parcial.EjecutadoRetiroParcial
    
    Set Ficha_Cierre_parcial = Nothing
    
End Function

Public Function EjecutarCierreTotal(pCnAdm As Object, _
pCnPos As Object, pCaja As String, pCajero As String, _
pDescriCajero As String, pGrupo As String, pLocalidad As String, _
pOrg As String, pTurno As Double, pUsuario As String) As Boolean
    
    Dim mClaseCierres As New Cls_Cierres
    
    mClaseCierres.InicializarConexiones pCnAdm, pCnPos
    
    'LlenarDatosTemporales pCnPos, pLocalidad, pCaja, pTurno, pCajero
    
    Set Ficha_Cierre_total.ClaseCierres = mClaseCierres
    Set Ficha_Cierre_total.ConexionAdm = pCnAdm
    Set Ficha_Cierre_total.ConexionPos = pCnPos
    
    Ficha_Cierre_total.LCaja = pCaja
    Ficha_Cierre_total.LCajero = pCajero
    Ficha_Cierre_total.LCajeroDescripcion = pDescriCajero
    Ficha_Cierre_total.LGrupo = pGrupo
    Ficha_Cierre_total.LLocalidad = pLocalidad
    Ficha_Cierre_total.LOrganizacion = Org
    Ficha_Cierre_total.LTurno = pTurno
    Ficha_Cierre_total.CodigoUsuario = pUsuario
    
    'Ficha_Cierre_total.LMensajes = pMensajes
    
    Ficha_Cierre_total.Show vbModal
    
    EjecutarCierreTotal = Ficha_Cierre_total.Actualizar
    
    Set Ficha_Cierre_total = Nothing
    
End Function

Public Sub ReimprimirFacturas(Branch As String, ConexionPos As Object, ConexionAdm As Object)
    Call ModCierres.ReimprimirFactura(Branch, ConexionPos, ConexionAdm)
End Sub

Public Sub ReimprimirSeriales(Branch As String, ConexionPos As Object, ConexionAdm As Object)
    Call ModCierres.ReimprimirSeriales(Branch, ConexionPos, ConexionAdm)
End Sub

Public Sub ReimprimirCierresTotales(Branch As String, ConexionPos As Object, ConexionAdm As Object)
    Call ModCierres.ReimprimirCierreTotal(Branch, ConexionPos, ConexionAdm)
End Sub

Public Sub ReimprimirRetirosParciales(Branch As String, ConexionPos As Object, ConexionAdm As Object)
    Call ModCierres.ReimprimirRetiroParcial(Branch, ConexionPos, ConexionAdm)
End Sub

Public Sub ReimprimirConsumos(Branch As String, ConexionPos As Object, ConexionAdm As Object)
    Call ModCierres.ReimprimirConsumos(Branch, ConexionPos, ConexionAdm)
End Sub

Public Sub TipsManagement(Branch As String, ConexionPos As Object, ConexionAdm As Object, _
cajero As String, Caja As String, Turno As Double)
    
    With Frm_Tips
        
        Set Iniciar.ConexionAdm = ConexionAdm
        Set Iniciar.ConexionPos = ConexionPos
        
        Set .fConexionPos = ConexionPos
        Set .fConexionAdm = ConexionAdm
        
        .fBranch = Branch
        .fCajero = cajero
        .fCaja = Caja
        .fTurno = Turno
        
        .Show vbModal
        
    End With
    
End Sub

'Private Sub LlenarDatosTemporales(CnPos As Object, Localidad As String, NumCaja As String, Turno As Double, cajero As String)
'
'    Dim mRs As ADODB.Recordset, mRsDat As ADODB.Recordset
'    Dim mSQL As String, mDenomina As String
'    Dim mInicio As Boolean
'
'    ' Aqui revisar el multimoneda n
'    On Error GoTo Errores
'
'    mSQL = "SELECT cs_LOCALIDAD,  ns_TURNO, cs_CAJERO, cu_MONEDA,cu_DENOMINACION, cu_CODIGO_BANCO, cu_NUMERO, " _
'    & "ns_FACTOR, nu_MONTO + NU_TIP, D.C_DENOMINACION FROM TR_VENTAS_POS_PAGOS INNER JOIN " & Srv_Remote_BD_ADM & ".DBO.MA_DENOMINACIONES D ON D.C_CODMONEDA=CU_MONEDA AND D.C_CODDENOMINA=CU_DENOMINACION " _
'    & "WHERE  (CU_DENOMINACION<>'EFECTIVO') AND (C_REAL = 1) AND (CS_LOCALIDAD = '" & Localidad & "') AND (CS_TIPO_DOCUMENTO = 'VEN') AND (CS_CAJA = '" & NumCaja & "') and (cu_CLAVE <>'') " _
'    & "AND (CS_CAJERO = '" & cajero & "') AND (NU_MONTO - NU_FALTA) <> 0 AND (NS_TURNO = " & CDec(Turno) & ") "
'
'    Set mRsDat = New ADODB.Recordset
'
'    mRsDat.CursorLocation = adUseClient
'
'    mRsDat.Open mSQL, CnPos, adOpenForwardOnly, adLockReadOnly, adCmdText
'
'    mRsDat.ActiveConnection = Nothing
'
'    CnPos.BeginTrans
'    mInicio = True
'
'    Set mRs = New ADODB.Recordset
'
'    mRs.Open "SELECT * FROM TR_DENOMINA_TEMP WHERE 1 = 2", _
'    CnPos, adOpenKeyset, adLockBatchOptimistic, adCmdText
'
'    Do While Not mRsDat.EOF
'
'        If mDenomina <> mRsDat!cu_Denominacion Then
'
'            mDenomina = mRsDat!cu_Denominacion
'
'            mSQL = "delete from tr_denomina_temp where (caja='" & NumCaja & "') and (usuario='" & cajero & "') " _
'            & " and (coddenomina='" & mDenomina & "')"
'
'            CnPos.Execute mSQL
'
'        End If
'
'        mRs.AddNew
'            mRs!CodMoneda = mRsDat!cu_Moneda
'            mRs!CodDenomina = mDenomina
'            mRs!DesDenomina = mRsDat!c_Denominacion
'            mRs!Cantidad = 1
'            mRs!Valor = mRsDat!nu_Monto
'            mRs!Factor = mRsDat!ns_Factor
'            mRs!Caja = NumCaja
'            mRs!Usuario = cajero
'            mRs!CODIGOBANCO = mRsDat!cu_Codigo_Banco
'        mRs.Update
'
'        mRsDat.MoveNext
'
'    Loop
'
'    mRs.UpdateBatch adAffectAll
'    mRs.Close
'
'    CnPos.CommitTrans
'    mInicio = False
'
'    Exit Sub
'
'Errores:
'
'    If mInicio Then
'        CnPos.RollbackTrans
'        mInicio = False
'    End If
'
'    Err.Clear
'
'End Sub

Public Sub ReimprimirCierre(CnPos As Object, CnAdm As Object, _
NumCierre As String, pLocalidad As String)
    
    Dim mRs As ADODB.Recordset
    Dim mCierre As Cls_Cierres
    Dim mSQL As String
    
    mSQL = "SELECT cs_CAJA, cs_TURNO, cs_DOCUMENTO, cs_CAJERO, c_Relacion FROM MA_CIERRE_POS_TOTAL " _
    & "INNER JOIN MA_CAJA ON c_Codigo = cs_Caja " _
    & "WHERE cs_Documento = '" & NumCierre & "' AND cs_Localidad = '" & pLocalidad & "'"
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSQL, CnPos, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        
        Set mCierre = New Cls_Cierres
        
        mCierre.InicializarConexiones CnAdm, CnPos
        
        ImprimirCierreModoViejo = ImprimirCierreModoViejo Or _
        Val(BuscarReglaNegocioStr(CnAdm, "ImprimirCierreFOODModoAntiguo", "0")) = 1
        
        If ImprimirCierreModoViejo Then
            Call mCierre.ImprimirCierreTotal(POS.Organizacion, pLocalidad, mRs!cs_Caja, _
            POS.CodigoRelacionPos, mRs!cs_Turno, mRs!cs_Cajero, mRs!cs_Documento, False)
        Else
            Call mCierre.ImprimirCierreTotalNuevo(pLocalidad, mRs!cs_Caja, _
            mRs!c_Relacion, mRs!cs_Turno, mRs!cs_Cajero, mRs!cs_Documento, False)
        End If
        
        Set mCierre = Nothing
        
    Else
        
        Call gRutinas.Mensajes(StellarMensaje(237)) ' Cierre de Caja no encontrado.
        
    End If
    
End Sub

Public Sub TotalizarCierrePOE_SANBORNS(pCnAdm As Object, _
pCnPos As Object, pCaja As String, pCajero As String, _
pDescriCajero As String, pGrupo As String, pLocalidad As String, _
pOrg As String, pTurno As Double, pUsuario As String)
    
    Dim mCierre As Cls_Cierres
    Set mCierre = New Cls_Cierres
    mCierre.InicializarConexiones pCnAdm, pCnPos
    mCierre.TotalizarCierrePOE_SANBORNS pLocalidad, pCaja, pGrupo, pTurno, pCajero, vbNullString, True
    Set mCierre = Nothing
    
End Sub

Private Sub Class_Initialize()
    
    Srv_Remote_BD_ADM = "VAD10" ' Valores default que pueden ser cambiados por el DLL Caller
    Srv_Remote_BD_POS = "VAD20" ' Para permitir nombres de BD dinámicos.
    
End Sub
