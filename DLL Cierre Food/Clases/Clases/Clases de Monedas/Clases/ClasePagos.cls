VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_Pagos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'//**************************************************************************//
'// REPUBLICA BOLIVARIANA DE VENEZUELA                          20/08/2003
'// STELLAR POS FOOD REV. 1.0.0
'// SUNLIT CONSULTORES S.A.
'//
'// CLASE DE ENTIDADES BANCARIAS
'// CAPAS: 02 Y 03
'//
'//**************************************************************************//

Private Type TablasCabeceroConsumo
    TablaCabeceroConsumo                As String
    TablaCabeceroImpuestoConsumo        As String
End Type

Private Type TablasTransaccionalConsumo
    TablaTransaccionalConsumo            As String
    TablaTransaccionalImpuestoConsumo    As String
End Type

Private Type TablasCabeceroVentas
    TablaCabeceroVenta                   As String
    TablaCabeceroVentaImpuesto           As String
End Type

Private Type TablasTransaccionalVentas
    TablaTransaccionalVenta              As String
    TablaTransaccionalVentaImpuesto      As String
    TablaTransaccionalVentaPagos         As String
End Type

Private Type TablasGrabarFactura
    Cliente                         As String
    CuentasxCobrar                  As String
    Transaccional                   As String
    TransaccionalCaja               As String
    CabeceroConsumo                 As TablasCabeceroConsumo
    TransaccionalConsumo            As TablasTransaccionalConsumo
    CabeceroVentas                  As TablasCabeceroVentas
    TransaccionalVentas             As TablasTransaccionalVentas
End Type

Private Enum PagColGrid
    PagDescripcionMoneda = 1
    PagCodigoMoneda = 2
    PagFactorMoneda = 3
    PagDescripcionDenominacion = 4
    PagCodigoDenominacion = 5
    PagDescripcionEmisor = 6
    PagCodigoEmisor = 7
    PagNumeroDenominacion = 8
    PagMontoDenominacion = 9
    PagMontoTotal = 10
    PagFactorFactura = 11
    PagEndosaDenominacion = 12
    PagImprimeDenominacion = 13
    PagConformaDenominacion = 14
    PagMontoMinimoDenominacion = 15
    PagOperadorConformacion = 16
    PagClaveConformacion = 17
End Enum

Private Enum FormasPagosPos
    fppmontodescuento = 0
    FPPMontoCredito = 1
    fppmontocontado = 2
    fppmontovuelto = 3
End Enum

Private Const CdNvo As String = "NULL**"
Private Const ClienteContado As String = "9999999999"
Private Rutinas As New cls_Rutinas
Private ClaseMonedas As New cls_Monedas
Private ClaseDenominaciones As New cls_Denominaciones
Private ClaseBancos As New Cls_Bancos
Private ClaseDatosClientes As New clsDatosCliente
Private ClaseDispositivos As New clsDispositivos
Private ClaseReportes   As New Cls_Reportes
Private EstructuraDeTablasFactura As TablasGrabarFactura
Private Const DesCash As String = " Efectivo"
Private DestruirConexion As Boolean

Private ConexionAdm                  As Object
Private ConexionPos                  As Object
Private ConexionAdmLocal             As Object
Private ConexionPosLocal             As Object

Public Banco As String
Public Descripcion As String
Private Const ProgramId As Long = 20389

Public Sub InicializarConexiones(pConexionAdm As Object, pConexionPos As Object, Optional PConexionAdmLocal As Object, Optional PConexionPosLocal As Object, Optional sConexionAdm As String, Optional sConexionPos As String, Optional SConexionAdmLocal As String, Optional SConexionPosLocal As String)
    Call Rutinas.AbrirConexion(pConexionAdm, sConexionAdm, DestruirConexion)        'CREAR CONEXION
    Call Rutinas.AbrirConexion(pConexionPos, sConexionPos, DestruirConexion)         'CREAR CONEXION
    Call Rutinas.AbrirConexion(PConexionAdmLocal, sConexionAdm, DestruirConexion)        'CREAR CONEXION
    Call Rutinas.AbrirConexion(PConexionPosLocal, sConexionPos, DestruirConexion)         'CREAR CONEXION
    
    Set ConexionAdm = pConexionAdm
    Set ConexionPos = pConexionPos
    Set ConexionAdmLocal = PConexionAdmLocal
    Set ConexionPosLocal = PConexionPosLocal
End Sub

Public Function ShowFrmPagos(Organizacion As String, Localidad As String, _
Caja As String, Turno As Double, Moneda As String, CodigoCliente As String, _
CuentaNumero As String, CodigoServicio As String, Relacion As String, _
Mesero As String, Usuario As String, Optional PosFood As Boolean = True) As Boolean
    
    '//********************************************************************************//
    '// PARAMETROS DE LA FORMA
    '//********************************************************************************//
    Set FrmPagos.ConexionAdm = ConexionAdm
    Set FrmPagos.ConexionPos = ConexionPos
    Set FrmPagos.ConexionPosLocal = ConexionPosLocal
    Set FrmPagos.ConexionAdmLocal = ConexionAdmLocal
    
    FrmPagos.CuentaNumero = CuentaNumero
    FrmPagos.ServicioNumero = CodigoServicio
    FrmPagos.CodigoCliente = CodigoCliente
    FrmPagos.Relacion = Relacion
    FrmPagos.Mesero = Mesero
    FrmPagos.MonedaFacturar = Moneda
    FrmPagos.Organizacion = Organizacion
    FrmPagos.Localidad = Localidad
    FrmPagos.Caja = Caja
    FrmPagos.Turno = Turno
    FrmPagos.Usuario = Usuario
    FrmPagos.isPosFood = PosFood
    
    FrmPagos.Show vbModal
    
    ShowFrmPagos = FrmPagos.GrabarOk
    '//********************************************************************************//
    '// RETORNO DE VALORES
    '//********************************************************************************//
    Set FrmPagos = Nothing
    
End Function

Public Sub ConstruirGrid(Grid As S_GRID, MonedaDefault As String, CodigoMoneda As String, Decimales As Integer, Factor As Double, DefaultFactor As Double)
    
    Dim AMonedas As Variant, ABancos As Variant
    Grid.Sm_adicionarColummna 1600, s_mLectura, S_mTexto, "Moneda", False, , MonedaDefault, False, 0
    Grid.Sm_adicionarColummna 0, s_mLectura, S_mTexto, "CodigoMoneda", False, , CodigoMoneda, False, 0
    
    Grid.Sm_adicionarColummna 0, s_mLectura, s_mDecimal, "FactorMoneda", False, , Factor, True, 0
    
    Grid.Sm_adicionarColummna 3000, s_mLectura, S_mTexto, "Denominacion", False, , DesCash, False, 0
    Grid.Sm_adicionarColummna 0, s_mLectura, S_mTexto, "CodigoDenominacion", False, , DesCash, False, 0
    Grid.Sm_adicionarColummna 3000, s_mLectura, S_mTexto, "Emisor", False, , , False, 0
    Grid.Sm_adicionarColummna 0, s_mLectura, S_mTexto, "CodigoEmisor", False, , , False, 0
    Grid.Sm_adicionarColummna 2100, S_mEntrada, S_mTexto, "Numero", True, , , False, 0
    
    Grid.Sm_adicionarColummna 2000, s_mLectura, s_mDecimal, "Monto", True, , 0, True, Decimales
    
    Grid.Sm_adicionarColummna 0, S_mFormula, s_mDecimal, "MontoTotal", True, "(cACT,9 * cACT,3) / cACT,11", , True, Decimales     '"( cACT,9 * cACT,3 ) / cACT,11"
    Grid.Sm_adicionarColummna 0, s_mLectura, s_mDecimal, "FactorDefecto", True, , CStr(DefaultFactor), False, 0
    
    Grid.Sm_adicionarColummna 0, s_mLectura, S_mTexto, "Endoso", False, , "0", False, 0
    Grid.Sm_adicionarColummna 0, s_mLectura, S_mTexto, "Imprime", False, , "0", False, 0
    Grid.Sm_adicionarColummna 0, s_mLectura, S_mTexto, "Conforma", False, , "0", False, 0
    
    Grid.Sm_adicionarColummna 0, s_mLectura, s_mDecimal, "MontoMinimo", False, , 0, True, Decimales
    
    Grid.Sm_adicionarColummna 0, s_mLectura, S_mTexto, "OperadorConformacion", False, , "", False, 0
    Grid.Sm_adicionarColummna 0, s_mLectura, S_mTexto, "ClaveConformacion", False, , "", False, 0
    
End Sub

Private Function ArrayRs(Registros As Object, Campo As String) As Variant
    Dim LCont As Integer, LArreglo As Variant
    ReDim LArreglo(Registros.RecordCount - 1) As Variant
    For LCont = 0 To Registros.RecordCount - 1
        LArreglo(LCont) = Registros.Fields(Campo).Value
        Registros.MoveNext
    Next LCont
    ArrayRs = LArreglo
End Function

Public Function ProcesarTeclas(Grid As S_GRID, Textos As S_text, ActualFactor As Double, ColPos As Integer, Optional ByRef MontoVueltos As Double = 0, Optional POSX As Integer = 0, Optional POSY As Integer = 0, Optional ByRef Teclas As Integer = 0)
    Dim PosAct As Variant, DefaultMoneda As String, RsTemp As Object
    On Error GoTo Falla_Local
    
    PosAct = Grid.sp_CeldaActual
    If PosAct(0) < Grid.Rows - 1 Then
        Exit Function
    End If
    
    Select Case ColPos
        Case 1 'MONEDA
            
            ClaseMonedas.InicializarConexiones ConexionAdm, ConexionPos
            
            DefaultMoneda = Grid.S_TomarValor(Grid.Rows - 1, 2)
            
'            Call BorrarDatosGrid(GRID, True, True, True, True, True, True, True, True, True)
            
            Call ClaseMonedas.ShowFrmMonedas(POSX, POSY)
            
            If ClaseMonedas.CodMoneda = Empty Then
                If Not ClaseMonedas.BuscarMonedas(RsTemp, DefaultMoneda, , -1) Then
                    gRutinas.Mensajes "Error al cargar la moneda.", False
                    gRutinas.ReiniciarWindows EWX_LOGOFF, 0
                    While True
                        DoEvents
                    Wend
                End If
            End If
            
            Grid.S_FijarValor Grid.Rows - 1, 1, ClaseMonedas.DesMoneda
            Grid.S_FijarValor Grid.Rows - 1, 2, ClaseMonedas.CodMoneda
            Grid.S_FijarValor Grid.Rows - 1, 3, ClaseMonedas.FacMoneda
            
            Grid.sm_MoverACelda Grid.Rows - 1, 4
            'GRID.S_FijarValor GRID.Rows - 1, 4, ""
'            Rutinas.SendKeys Chr(vbKeyReturn), True
            
        Case 2 'CODIGO MONEDA
        Case 3 'FACTOR MONEDA
        Case 4 'DENOMINACIONES
            
            ClaseDenominaciones.InicializarConexiones ConexionAdm, ConexionPos
            
            Call BorrarDatosGrid(Grid, , , , True, True, True, True, True, True)
            
            Call ClaseDenominaciones.ShowFrmDenominaciones(POSX, POSY, Grid.S_TomarValor(Grid.Rows - 1, 2))
            
            Grid.S_FijarValor Grid.Rows - 1, 4, ClaseDenominaciones.DesDenomina
            Grid.S_FijarValor Grid.Rows - 1, 5, ClaseDenominaciones.CodDenomina
            
            Grid.S_FijarValor Grid.Rows - 1, 12, IIf(ClaseDenominaciones.RequiereDeEndoso, "1", "0")
            Grid.S_FijarValor Grid.Rows - 1, 13, IIf(ClaseDenominaciones.ImprimeLaForma, "1", "0")
            Grid.S_FijarValor Grid.Rows - 1, 14, IIf(ClaseDenominaciones.RequiereDeConformacion, "1", "0")
            Grid.S_FijarValor Grid.Rows - 1, 15, ClaseDenominaciones.MontoDeCompra
            
            'SI PERMITE VUELTOS
            If ClaseDenominaciones.PermiteDarVuelto Then
                MontoVueltos = MontoVueltos + ClaseDenominaciones.CambioDenominacion(ClaseDenominaciones.MontoDevuelto, Grid.S_TomarValor(Grid.Rows - 1, 3), Grid.S_TomarValor(Grid.Rows - 1, 11), 2)
            End If
            'FIN
            
            If ClaseDenominaciones.DesDenomina <> DesCash Then
                Grid.sm_MoverACelda Grid.Rows - 1, 6
            Else
                Grid.sm_MoverACelda Grid.Rows - 1, 9
            End If
            
'            Rutinas.SendKeys Chr(vbKeyReturn), True
        Case 5 'CODIGO DENOMINACON
            
        Case 6 'EMISOR
        
            If Grid.S_TomarValor(Grid.Rows - 1, 4) <> DesCash And Grid.S_TomarValor(Grid.Rows - 1, 4) <> "" Then
                ClaseBancos.InicializarConexiones ConexionAdm, ConexionPos
                Call ClaseBancos.ShowFrmBancos(POSX, POSY, "")
            Else
                Exit Function
            End If
            Grid.S_FijarValor Grid.Rows - 1, 6, ClaseBancos.DesBanco
            Grid.S_FijarValor Grid.Rows - 1, 7, ClaseBancos.CodBanco
            Grid.sm_MoverACelda Grid.Rows - 1, 8
'            Rutinas.SendKeys Chr(vbKeyReturn), True
            DoEvents
            
        Case 7 'CODIGO EMISOR
        Case 8 'NUMERO
            If Grid.S_TomarValor(Grid.Rows - 1, 4) = DesCash Then
                Rutinas.SendKeys Chr(vbKeyEscape), True
                Grid.S_FijarValor Grid.Rows - 1, 7, ""
                Grid.sm_MoverACelda Grid.Rows - 1, 9
'                Rutinas.SendKeys Chr(vbKeyReturn), True
            ElseIf Grid.S_TomarValor(Grid.Rows - 1, 4) = "" Then
'                Rutinas.SendKeys Chr(vbKeyEscape), True
            End If
        Case 9 'MONTO
            Call ProcesarTeclaSobreColumna(Grid, Textos, CInt(PosAct(0)), CInt(PosAct(1)), Teclas)
            
        Case 10 'MONTO TOTAL
        Case 11 'VALOR POR DEFECTO
    End Select
    DoEvents
    If Grid.Enabled Then
        Grid.SetFocus
    End If
    Exit Function
Falla_Local:
    gRutinas.Mensajes "Error N� [" & Err.Number & "] " & Err.Description & vbNewLine & "Ocurrido en la rutina GrabarDatos de la forma FrmPagos."
    Call gRutinas.GrabarErrores(CStr(Err.Number), Err.Description, POS.UsuarioDelPos.Codigo, Err.Source, "FrmPagos", Err.Source, ConexionPos)
End Function

Private Sub BorrarDatosGrid(Grid As Object, Optional Columna01 As Boolean = False, Optional Columna02 As Boolean = False, Optional Columna03 As Boolean = False, Optional Columna04 As Boolean = False, Optional Columna05 As Boolean = False, Optional Columna06 As Boolean = False, Optional Columna07 As Boolean = False, Optional Columna08 As Boolean = False, _
                            Optional Columna09 As Boolean = False)

    If Columna01 Then Grid.S_FijarValor Grid.Rows - 1, 1, ""
    If Columna02 Then Grid.S_FijarValor Grid.Rows - 1, 2, ""
    If Columna03 Then Grid.S_FijarValor Grid.Rows - 1, 3, 0
    If Columna04 Then Grid.S_FijarValor Grid.Rows - 1, 4, ""
    If Columna05 Then Grid.S_FijarValor Grid.Rows - 1, 5, ""
    If Columna06 Then Grid.S_FijarValor Grid.Rows - 1, 6, ""
    If Columna07 Then Grid.S_FijarValor Grid.Rows - 1, 7, ""
    If Columna08 Then Grid.S_FijarValor Grid.Rows - 1, 8, ""

End Sub

Public Function SalidaDeCelda(ByRef Grid As S_GRID, MonedaDefault As String, CodigoMoneda As String, Factor As Double, FactorDefault As Double, MontoACancelar As Double, Optional MontoVuelto As Double = 0, Optional LastKey As Integer = 0)
    Dim PosAct As Variant
    PosAct = Grid.sp_CeldaActual
    On Error GoTo Falla_Local
    Select Case PosAct(1)
        Case 1      'MONEDA
        Case 2      'DESCRIPCION DE LA MONEDA
        Case 3      'FACTOR DE LA MONEDA
        Case 4      'DESCRIPCION DE LA DENOMINACION
        Case 5      'COGIGO DE LA DENOMINACION
        Case 6      'CODIGO DEL EMISOR
        Case 7      'DESCRIPCION DEL EMISOR
            If Grid.S_TomarValor(Grid.Rows - 1, 7) = "" And Grid.S_TomarValor(Grid.Rows - 1, 5) <> "" Then
                Grid.sm_MoverACelda Grid.Rows - 1, 7
                Rutinas.SendKeys Chr(vbKeyReturn), True
            ElseIf Grid.S_TomarValor(Grid.Rows - 1, 7) = "" And Grid.S_TomarValor(Grid.Rows - 1, 5) = DesCash Then
                Grid.sm_MoverACelda Grid.Rows - 1, 9
                Rutinas.SendKeys Chr(vbKeyReturn), True
            End If
        Case 8      'NUMERO DE LA DENOMINACION SI NO ES REAL
            If Grid.S_TomarValor(Grid.Rows - 1, 8) <> "" And LastKey = vbKeyReturn Then
                Grid.sm_MoverACelda Grid.Rows - 1, 9
                Rutinas.SendKeys Chr(vbKeyReturn), True
            ElseIf Grid.S_TomarValor(Grid.Rows - 1, 8) <> "" Then
                Grid.sm_MoverACelda Grid.Rows - 1, 9
                Rutinas.SendKeys Chr(vbKeyReturn), True
            End If
        Case 9      'MONTO DE LA DENOMINACION
            If Grid.S_TomarValor(Grid.Rows - 1, 5) <> "" And Grid.S_TomarValor(Grid.Rows - 1, 9) > 0 Then
'                If GRID.S_TomarValor(GRID.Rows - 1, 15) >= GRID.S_TomarValor(GRID.Rows - 1, 9) Then
'                    Rutinas.Mensajes "El monto permitido por esta denominaci�n debe ser superior a '" & FormatNumber(GRID.S_TomarValor(GRID.Rows - 1, 15), 2) & "'", False
'                    Exit Function
'                End If
                If Not Grid.Enabled Or Grid.S_TomarValor(Grid.Rows - 1, 10) > MontoACancelar + MontoVuelto Or Grid.sm_TotalizarColumna(10) >= MontoACancelar Then Exit Function
                If Trim(Grid.S_TomarValor(Grid.Rows - 1, 5)) <> Trim(DesCash) And (Trim(Grid.S_TomarValor(Grid.Rows - 1, 7)) = "" Or Trim(Grid.S_TomarValor(Grid.Rows - 1, 8)) = "") Then
                    Rutinas.Mensajes "Debe colocar la entidad emisora y el n�mero antes de continuar.", False
                    Exit Function
                End If
                Grid.Sm_adicionarFila
                Grid.sm_MoverACelda Grid.Rows - 1, 1
                MontoVuelto = 0
            End If
        Case 10     'MONTO TOTAL
        Case 11     'FACTOR DE LA FACTURA
    End Select
    DoEvents
    If Grid.Enabled And Grid.Visible Then
        Grid.SetFocus
    End If
    Exit Function
Falla_Local:
    gRutinas.Mensajes "Error N� [" & Err.Number & "] " & Err.Description & vbNewLine & "Ocurrido en la rutina GrabarDatos de la forma FrmPagos."
    Call gRutinas.GrabarErrores(CStr(Err.Number), Err.Description, POS.UsuarioDelPos.Codigo, Err.Source, "FrmPagos", Err.Source, ConexionPos)
End Function

Public Sub DefaultValues(ByRef Grid As Object, MonedaDefault As String, CodigoMoneda As String, Factor As Double, DefaultFactor As Double)
    Dim PosAct As Variant
    PosAct = Grid.sp_CeldaActual
    Grid.Enabled = False
    If PosAct(0) < Grid.Rows - 1 Then Exit Sub
    Grid.S_FijarValor Grid.Rows - 1, 1, MonedaDefault
    Grid.S_FijarValor Grid.Rows - 1, 2, CodigoMoneda
    Grid.S_FijarValor Grid.Rows - 1, 3, CStr(Factor)
    Grid.S_FijarValor Grid.Rows - 1, 11, CStr(DefaultFactor)
    Grid.Enabled = True
End Sub

Public Sub EscribirConformacion(Grid As S_GRID, Linea As Integer, Operador As String, Clave As String)
    Grid.S_FijarValor Linea, 16, Operador
    Grid.S_FijarValor Linea, 17, Clave
End Sub

Public Sub LeerConfiguracionDenominacion(Grid As S_GRID, Linea As Integer, ByRef ImprimeForma As Boolean, ByRef Conformacion As Boolean, ByRef Endoso As Boolean)
    ImprimeForma = False
    Endoso = False
    Conformacion = False
    Endoso = (Grid.S_TomarValor(Linea, 12) = "1")           'SI VALOR ES IGUAL A '1' => TRUE; DE LO CONTRARIO FALSE
    ImprimeForma = (Grid.S_TomarValor(Linea, 13) = "1")     'SI VALOR ES IGUAL A '1' => TRUE; DE LO CONTRARIO FALSE
    Conformacion = (Grid.S_TomarValor(Linea, 14) = "1")     'SI VALOR ES IGUAL A '1' => TRUE; DE LO CONTRARIO FALSE
End Sub

Public Sub ProcesarTeclaSobreColumna(ByRef Grid As S_GRID, ByRef Textos As S_text, Linea As Integer, Columna As Integer, ByRef Tecla As Integer)
    Dim Texto As String, PosActText As Integer
    On Error GoTo Falla_Local
    If Linea < Grid.Rows - 1 Then Exit Sub
    Select Case Columna
        Case 8              'NUMERO DE DENOMINACION
            If Grid.S_TomarValor(Linea, 4) <> DesCash Then
                Select Case Tecla
                    Case vbKeyReturn
                        Grid.sm_MoverACelda Linea, 9
                        Rutinas.SendKeys Chr(vbKeyReturn), True
                    Case 48 To 57
                        If Textos.SelLength > 0 Then
                            Texto = Chr(Tecla)
                        Else
                            Texto = Grid.S_TomarValor(Linea, 8) & Chr(Tecla)
                        End If
                        Grid.S_FijarValor Linea, 8, Texto
                    Case vbKeyBack
                        Texto = Grid.S_TomarValor(Linea, 8)
                        Texto = Mid(Texto, 1, Len(Texto))
                        Grid.S_FijarValor Linea, 8, Texto
                    Case vbKeyDelete
                        Grid.S_FijarValor Linea, 8, ""
                End Select
            End If
        Case 9              'MONTO
        
            Select Case Tecla
                Case vbKeyReturn
                    Tecla = 0
                    If Not Textos.Visible Then
                    
                        Texto = Grid.S_TomarValor(Linea, 9)
                        Call Grid.sm_MoverACelda(Linea, 9)
                        Call Grid.sm_habilitarTextExt(Textos)
'                        Textos.S_SelGotFocus = True
                        
                        Textos.s_MantenerTextoHabilitado = True
                        Textos.Left = Grid.Left + Grid.CellLeft
                        Textos.Top = Grid.Top + Grid.CellTop
                        Textos.Width = Grid.CellWidth
                        Textos.Height = Grid.CellHeight
                        DoEvents
                        If IsNumeric(Texto) Then
                            If CDbl(Texto) <= 0 Then
                                Texto = ""
                            End If
                        End If
                        Textos.Text = Texto
                        Grid.Enabled = False
                        Textos.Visible = True
                        Textos.SelStart = 0
                        Textos.SelLength = Len(Textos.Text)
                        
                        DoEvents
                        
                    Else
                    
                        Textos.Visible = False
                        Textos.s_MantenerTextoHabilitado = False
                        Grid.Enabled = True
                        DoEvents
                        If Not IsNumeric(Textos.Text) Then Textos.Text = "0"
                        If Grid.S_TomarValor(Grid.Rows - 1, 15) > CDbl(Textos.Text) Then
                            Rutinas.Mensajes "El monto permitido por esta denominaci�n debe ser superior a '" & FormatNumber(Grid.S_TomarValor(Grid.Rows - 1, 15), 2) & "'", False
                            Textos.Text = "0"
                            Grid.S_FijarValor Linea, 9, CDbl(Textos.Text)
                            Grid.SetFocus
                            Grid.sm_MoverACelda Linea, 9
                            DoEvents
                            Exit Sub
                        Else
                            If Textos.Text = "" Then Textos.Text = "0"
                            Grid.S_FijarValor Linea, 9, CDbl(Textos.Text)
                        End If
                        
                    End If
                    
                Case 48 To 57
'                    Tecla = 0
                    Texto = Textos.Text
                    If Trim(Texto) = "" Then Texto = "0"
                    
                    If CDbl(Texto) = 0 Or Textos.SelLength > 0 Then
                        Texto = Chr(Tecla)
                    Else
                        PosActText = Textos.SelStart
                        If PosActText = 0 Then
                            Texto = Chr(Tecla) & Texto
                        Else
                            Texto = Mid(Texto, 1, PosActText) & Chr(Tecla) & Mid(Texto, PosActText + 1)
                        End If
                        
                    End If
                    If IsNumeric(Texto) Then
                        Grid.S_FijarValor Linea, 9, Texto
                        Textos.Text = Texto
                    End If
                    If PosActText = 0 Then
                        Textos.SelStart = PosActText + 1
                    Else
                        Textos.SelStart = PosActText + 1
                    End If
                    
                Case vbKeyBack
                    Tecla = 0
                    Texto = Textos.Text
                    If Len(Texto) < 1 Then Exit Sub
                    Texto = Mid(Texto, 1, Len(Texto) - 1)
                    If Trim(Texto) = "" Then
                        Grid.S_FijarValor Linea, 9, 0
                    End If
                    Textos.Text = Texto
                    
                Case vbKeyEscape
                    Tecla = 0
                    Textos.Text = "0"
                    Texto = Textos.Text
                    Grid.S_FijarValor Linea, 9, Texto
                    
                Case vbKeyDecimal, Asc(","), vbKeyDelete
                    Tecla = 0
                    Texto = Textos.Text
                    If InStr(1, Texto, Rutinas.SDecimal) > 0 Or Trim(Texto) = "" Then Exit Sub
                    Texto = Texto & Rutinas.SDecimal()
                    Grid.S_FijarValor Linea, 9, Texto
                    Textos.Text = Texto
                    Textos.SelStart = Len(Textos.Text)
            End Select
            
'            Textos.SelStart = Len(Textos.Text)
'            Textos.SelLength = Len(Textos.Text)
    End Select
    DoEvents
    If Grid.Enabled Then
        Grid.SetFocus
    End If
    Exit Sub
Falla_Local:
    gRutinas.Mensajes "Error N� [" & Err.Number & "] " & Err.Description & vbNewLine & "Ocurrido en la rutina ProcesarTeclaSobreColumna de la clase cls_Pagos."
End Sub

Public Sub ShowFrmTipoFacturacion(X As Integer, Y As Integer, CodigoCuenta As String, CodigoServicio As String, CodigoCliente As String, Relacion As String, CodigoCaja As String, Turno As Double, Localidad As String, Organizacion As String, Moneda As String, LSubTotal As Double, LDescuento As Double, LImpuestos As Double)
    Set FrmTipoFacturacion.ConexionAdm = ConexionAdm
    Set FrmTipoFacturacion.ConexionPos = ConexionPos
    FrmTipoFacturacion.MonedaFacturar = Moneda
    FrmTipoFacturacion.CodigoCuenta = CodigoCuenta
    FrmTipoFacturacion.CodigoServicio = CodigoServicio
    FrmTipoFacturacion.CodigoCliente = CodigoCliente
    FrmTipoFacturacion.Relacion = Relacion
    FrmTipoFacturacion.Left = X
    FrmTipoFacturacion.Top = Y
    FrmTipoFacturacion.Show vbModal
    Set FrmTipoFacturacion = Nothing
End Sub

Private Sub Class_Initialize()
    
    EstructuraDeTablasFactura.Cliente = "MA_CLIENTES"
    EstructuraDeTablasFactura.CuentasxCobrar = "MA_CXC"
    EstructuraDeTablasFactura.Transaccional = "TR_TRANSACCION_TEMP"
    EstructuraDeTablasFactura.TransaccionalCaja = "TR_CAJA"
    
    EstructuraDeTablasFactura.CabeceroConsumo.TablaCabeceroConsumo = "MA_CONSUMO"
    EstructuraDeTablasFactura.CabeceroConsumo.TablaCabeceroImpuestoConsumo = "MA_CONSUMO_IMPUESTOS"
    EstructuraDeTablasFactura.TransaccionalConsumo.TablaTransaccionalConsumo = "TR_CONSUMO"
    EstructuraDeTablasFactura.TransaccionalConsumo.TablaTransaccionalImpuestoConsumo = "TR_CONSUMO_IMPUESTOS"
    EstructuraDeTablasFactura.CabeceroVentas.TablaCabeceroVenta = "MA_VENTAS_POS"
    EstructuraDeTablasFactura.CabeceroVentas.TablaCabeceroVentaImpuesto = "MA_VENTAS_POS_IMPUESTOS"
    EstructuraDeTablasFactura.TransaccionalVentas.TablaTransaccionalVenta = "TR_VENTAS_POS"
    EstructuraDeTablasFactura.TransaccionalVentas.TablaTransaccionalVentaImpuesto = "TR_VENTAS_POS_IMPUESTOS"
    EstructuraDeTablasFactura.TransaccionalVentas.TablaTransaccionalVentaPagos = "TR_VENTAS_POS_PAGOS"
    
End Sub

Private Sub Class_Terminate()
    If DestruirConexion Then
        ConexionAdm.Close
        ConexionPos.Close
    End If
End Sub

Private Function ExistenTransaccionesPendientes(CuentaNumero As String, pConexionPos As Object)
    Dim RsTrTransaccionTemp As Object
    
    Set RsTrTransaccionTemp = Rutinas.CrearAdoRs
    Call Rutinas.Apertura_Recordset(RsTrTransaccionTemp, adUseServer)
    RsTrTransaccionTemp.Open "SELECT * FROM " & EstructuraDeTablasFactura.Transaccional & " WHERE ns_IDHIJO = '" & CuentaNumero & "'", pConexionPos, adOpenForwardOnly, adLockPessimistic, adCmdText
    ExistenTransaccionesPendientes = Not RsTrTransaccionTemp.EOF
    RsTrTransaccionTemp.Close
    
End Function

Private Function ActualizarTrCaja(Caja As String, Organizacion As String, Localidad As String, Turno As Double, cajero As String, MontoVenta As Double, MontoReintegro As Double, ByRef TrCaja As Object, LConexion As Object, Optional ControlarTransaccion As Boolean = False) As Boolean
    On Error GoTo Falla_Local
    ActualizarTrCaja = False
    If ControlarTransaccion Then
        LConexion.BeginTrans
    End If
    TrCaja.Open "SELECT * FROM TR_CAJA WHERE CS_CAJA = '" & Caja & "' AND NS_TURNO = " & CDec(Turno) & " AND CS_LOCALIDAD = '" & Localidad & "' AND CS_ORGANIZACION = '" & Organizacion & "' AND CS_CAJERO = '" & cajero & "'", LConexion, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    If Not TrCaja.EOF Then
        If MontoReintegro > 0 Then
            TrCaja!nu_MontoReintegro_Total = TrCaja!nu_MontoReintegro_Total + MontoReintegro
            TrCaja!nu_Reintegros_Totales = TrCaja!nu_Reintegros_Totales + 1
        End If
        If MontoVenta > 0 Then
            TrCaja!ns_Monto_Vendido = TrCaja!ns_Monto_Vendido + MontoVenta
            TrCaja!ns_Numero_Operaciones = TrCaja!ns_Numero_Operaciones + 1
        End If
    End If
    TrCaja.UpdateBatch
    ActualizarTrCaja = True
    If ControlarTransaccion Then
        LConexion.CommitTrans
    End If
    Exit Function
Falla_Local:
    If ControlarTransaccion Then
        LConexion.RollbackTrans
    End If
    Exit Function
End Function



Public Function NO_CONSECUTIVO(pCodproducto As Long, pConexion As ADODB.Connection, campo_cons As String, Optional pAvance As Boolean = True, Optional pIncrementar As Long = 0) As String

    Dim rec_cons As New ADODB.Recordset
    Dim Actual As String, Valor As Long, ValorCad As String
    
    Dim MiConexion As New ADODB.Connection, SQL As String
    Dim rsPers As New ADODB.Recordset
      
On Error GoTo Falla_Local

    SQL = "SELECT * From MA_CORRELATIVOS WHERE (CU_Campo = '" & campo_cons & "')"
    rec_cons.CursorLocation = adUseServer
    rec_cons.Open SQL, pConexion, adOpenDynamic, adLockPessimistic, adCmdText
    Actual = rec_cons.Fields("NU_Valor").Value + 1
    
    If pAvance Then
        rec_cons.Update
            rec_cons.Fields("NU_Valor").Value = rec_cons.Fields("NU_Valor").Value + 1 + pIncrementar
        rec_cons.UpdateBatch
    End If
    NO_CONSECUTIVO = Format(Actual, rec_cons.Fields("CU_FORMATO").Value)
    If Trim(NO_CONSECUTIVO) = "" Then
        GoTo Falla_Local
    End If
    Exit Function
Falla_Local:
    Rutinas.Mensajes "Error en los correlativos, el sistema se reiniciar�.", False
    Rutinas.ReiniciarWindows EWX_LOGOFF
End Function
