VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cls_Bancos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'//**************************************************************************//
'// REPUBLICA BOLIVARIANA DE VENEZUELA                          20/08/2003
'// STELLAR POS FOOD REV. 1.0.0
'// SUNLIT CONSULTORES S.A.
'//
'// CLASE DE ENTIDADES BANCARIAS
'// CAPAS: 02 Y 03
'//
'//**************************************************************************//
'//
'// CONSTANTES DE CARACTERES DE ESCAPE
Private Const Escape As Byte = 27

'//
'//**************************************************************************//
Enum EnumDatosEmpresa
    DatosEmpNombre = 0
    DatosEmpDireccion = 1
    DatosEmpLocalidad = 2
    DatosEmpRif = 3
End Enum
Private Const CdNvo As String = "NULL**"
Private Const ClienteContado As String = "9999999999"
Private Rutinas As New cls_Rutinas

Private ConexionAdm                  As Object
Private ConexionPos                  As Object
Public Banco As String
Public Descripcion As String

Property Let CodBanco(Codigo As String)
    Banco = Codigo
End Property

Property Let DesBanco(BDescripcion As String)
    Descripcion = BDescripcion
End Property

Property Get CodBanco() As String
    CodBanco = Banco
End Property

Property Get DesBanco() As String
    DesBanco = Descripcion
End Property

Public Sub InicializarConexiones(Optional ByVal pConexionAdm As Object, Optional ByVal pConexionPos As Object, Optional sConexionAdm As String, Optional sConexionPos As String)
    Call Rutinas.AbrirConexion(pConexionAdm, sConexionAdm)         'CREAR CONEXION
    Call Rutinas.AbrirConexion(pConexionPos, sConexionPos)          'CREAR CONEXION
    Set ConexionAdm = pConexionAdm
    Set ConexionPos = pConexionPos
End Sub

Public Function BuscarBanco(Optional ByRef Registros As Object, Optional Codigo As String = "") As Boolean
    
    'CAPA # 02
    
    BuscarBancos = False
    
    If IsMissing(Registros) Then Exit Function
    
    If Registros Is Nothing Then
        Set Registros = Rutinas.CrearAdoRs()
    End If
    
    BuscarBancos = Buscar_Bancos(Registros, Codigo)
    
End Function

Private Function Buscar_Bancos(ByRef Registros As Object, Codigo As String, Optional Tabla As String = "MA_BANCOS") As Boolean
    'CAPA # 03
    Dim LRec As Object
    On Error GoTo Falla_Local
    Buscar_Bancos = False
    Set LRec = Rutinas.CrearAdoRs()
    Tabla = Tabla & IIf(Codigo <> "", " WHERE C_CODIGO = '" & Codigo & "'", "")
    LRec.Open "select * FROM " & Tabla & " ORDER BY C_DESCRIPCIO ", ConexionAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not LRec.EOF Then
        Banco = LRec!C_CODIGO
        Descripcion = LRec!c_descripcio
    End If
    Rutinas.CopiarRegistro LRec, Registros
    Buscar_Bancos = True
    LRec.Close
    Exit Function
Falla_Local:
    Exit Function
End Function

Public Sub ShowFrmBancos(X As Integer, Y As Integer, ByRef Codigo As String)
    
    Set FrmBancos.ConexionAdm = ConexionAdm
    Set FrmBancos.ConexionPos = ConexionPos
    
    FrmBancos.Codigo = Codigo
    FrmBancos.Left = X
    FrmBancos.Top = Y
    FrmBancos.Show vbModal
    
    Codigo = FrmBancos.ClaseBancos.Banco
    Banco = FrmBancos.ClaseBancos.Banco
    Descripcion = FrmBancos.ClaseBancos.Descripcion
    
    Set FrmBancos = Nothing
    
End Sub

' RUTINAS DE IMPRESI�N DE CHEQUES.

Public Sub Imp_Cheque(pNomBeneficiario As String, pLugar As String, pMontoN As Double, Optional pFecha = "", Optional pPuerto = "LPT1", Optional pNoEndosable = True)
    Dim ESC, FileHand As Long
    On Error GoTo Falla_Local
    ESC = Chr$(27)
    FileHand = FreeFile()
    If pFecha = "" Then pFecha = Date
    PMonto = FormatNumber(pMontoN, 2)
    Open pPuerto For Output As #FileHand
        Print #FileHand, ESC + "@" + ESC + "c0" + Chr$(4)
'        Print #FileHand, ESC + "!" + Chr$(2)
        Print #FileHand, ESC + "L"
        'Print #FileHand, ESC + "W" + Chr$(0) + Chr$(0) + Chr$(0) + Chr$(0) + Chr$(137) + Chr$(1) + Chr$(183) + Chr$(3)
        Print #FileHand, ESC + "W" + Chr$(0) + Chr$(0) + Chr$(0) + Chr$(0) + Chr$(37) + Chr$(1) + Chr$(83) + Chr$(3)
        Print #FileHand, ESC + "T" + Chr$(1)
        Print #FileHand, Space(60) + PMonto + Chr$(10)
        If pNoEndosable Then
            Print #FileHand, Space(65) + "NO ENDOSABLE" + Chr$(10)
        Else
            Print #FileHand, Chr$(10)
        End If
        Print #FileHand, Space(13) + pNomBeneficiario
        Print #FileHand, ESC + "!" + Chr$(48)
        Print #FileHand, Space(10) + "***" + PMonto + "***"
        Print #FileHand, ESC + "!" + Chr$(0)
        Print #FileHand, Space(13) + pLugar & " " & CDate(pFecha)
        Print #FileHand, Chr$(12)
        Print #FileHand, ESC + "q"
    Close #FileHand
    Imp_Inicializar pPuerto
Falla_Local:
End Sub

Public Sub Imp_Validacion(pArrayValores, Optional pPuerto = "LPT1")
    ' El Arreglo debe tener 0-14 posiciones y no mas de 40 caracteres
    ' pArrayValores(x)(cadena,Tama�o)
    Dim ESC, FileHand As Long
    ESC = Chr$(27)
    FileHand = FreeFile()
    Open pPuerto For Output As #FileHand
        Print #FileHand, ESC + "@" + ESC + "c0" + Chr$(8)
        For i = 0 To 14
            Print #FileHand, ESC; "!" + Chr$(CLng(pArrayValores(i)(1))) + CStr(pArrayValores(i)(0)) + ESC + "!" + Chr$(0)
        Next
        Print #FileHand, Chr$(12)
    Close #FileHand
    Imp_Inicializar pPuerto
End Sub

Public Sub Imp_Split(pArrayValores, Optional pPuerto = "LPT1")
    ' pArrayValores(x)(cadena,Tama�o)
    On Error GoTo Falla_Puerto
    Dim ESC, FileHand As Long
    ESC = Chr$(27)
    FileHand = FreeFile()
    Open pPuerto For Output As #FileHand
        Print #FileHand, ESC + "@" + ESC + "c0" + Chr$(4) + ESC + "!" + Chr$(2)
        For i = 0 To UBound(pArrayValores)
            Print #FileHand, ESC; "!" + Chr$(CLng(pArrayValores(i)(1))) + CStr(pArrayValores(i)(0)) + ESC + "!" + Chr$(0)
        Next
        Print #FileHand, Chr$(12)
    Close #FileHand
    Imp_Inicializar pPuerto
Falla_Puerto:
End Sub

Public Sub Imp_Validacion_Standard(pNombreEmpresa, pUsuario, Optional pPosIni = 0, Optional pFecha = "", Optional pHora = "", Optional pPuerto = "LPT1")
    Dim pArray(15)
    If pPosIni > 10 Then pPosIni = 10
    If pFecha = "" Then pFecha = Date
    If pHora = "" Then pHora = Time
    For i = 0 To 14
        pArray(i) = Array("", 0)
    Next
    pArray(0 + pPosIni) = Array(pNombreEmpresa, 9)
    pArray(1 + pPosIni) = Array(pUsuario, 0)
    pArray(2 + pPosIni) = Array(CStr(pFecha) + " " + CStr(pHora), 0)
    Imp_Validacion pArray, pPuerto
End Sub

Public Sub Imp_DatosConformacionCheque_Standard_Slip(pNombre, pTelf, PIdent, pClave, pOperador, Optional pUsuario As String = "", Optional pPosNo As String = "", Optional pPosIni = 10, Optional pFecha = "", Optional pPuerto = "LPT1")
    
    Dim pArray
    
    If pFecha = "" Then pFecha = Date
    
    ReDim pArray(pPosIni + 7)
    
    For i = 0 To UBound(pArray)
        pArray(i) = Array("", 0)
    Next
    
    pArray(0 + pPosIni) = Array("   " & pNombre, 0)
    pArray(1 + pPosIni) = Array("   " & PIdent, 0)
    pArray(2 + pPosIni) = Array("   Telf : " & pTelf, 0)
    pArray(3 + pPosIni) = Array("   Clave: " & pClave, 0)
    pArray(4 + pPosIni) = Array("   Oper.: " & pOperador, 0)
    pArray(5 + pPosIni) = Array("   Usuar: " & pUsuario, 0)
    pArray(6 + pPosIni) = Array("   POS #: " & pPosNo, 0)
    
    Imp_Split pArray, pPuerto
    
End Sub

Public Sub imp_EndosoCheque_Standard_Slip(pNumCuenta, pNombreEmp, pNomBanco, Optional pEncabezado = True, Optional pPosIni = 20, Optional pPuerto = "LPT1")
    
    Dim pArray
    
    If pFecha = "" Then pFecha = Date
    
    ReDim pArray(pPosIni + 4)
    For i = 0 To UBound(pArray)
        pArray(i) = Array("", 0)
    Next
    
    If pEncabezado Then
        pArray(0 + pPosIni) = Array("Depositar Unicamente en la Cuenta", 9)
    End If
    
    pArray(1 + pPosIni) = Array(IIf(pEncabezado, "No. ", "") & pNumCuenta, 0)
    pArray(2 + pPosIni) = Array(IIf(pEncabezado, "De: ", "") & pNombreEmp, 0)
    pArray(3 + pPosIni) = Array(IIf(pEncabezado, "Del Banco: ", "") & pNomBanco, 0)
    
    Imp_Split pArray, pPuerto
    
End Sub

Public Sub Imp_Inicializar(Optional pPuerto = "LPT1")
    Dim ESC, FileHand As Long
    ESC = Chr$(27)
    FileHand = FreeFile()
    Open pPuerto For Output As #FileHand
        Print #FileHand, ESC + "@"
    Close #FileHand
End Sub

Public Function DatosEmpresa() As Variant
    
    Dim RsDatosEmpresa As New ADODB.Recordset, ArregloEmpresa(0 To 3) As String
    
    Set RsDatosEmpresa = Rutinas.CrearAdoRs
    
    RsDatosEmpresa.Open "SELECT * FROM ESTRUC_SIS", _
    ConexionAdm, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    ArregloEmpresa(0) = RsDatosEmpresa!nom_org
    ArregloEmpresa(1) = RsDatosEmpresa!dir_org
    ArregloEmpresa(2) = RsDatosEmpresa!loc_org
    ArregloEmpresa(3) = RsDatosEmpresa!rif_org
    
    RsDatosEmpresa.Close
    
    DatosEmpresa = ArregloEmpresa
    
End Function

Public Sub Imp_ChequeSerial(ObjetoComm As Object, pNomBeneficiario As String, pLugar As String, _
pMontoN As Double, Optional pFecha = "", Optional pNoEndosable = True)

    Dim ESC, FileHand As Long, Buffers As String
    
    ESC = Chr$(27)
    FileHand = FreeFile()
    
    If pFecha = "" Then pFecha = Date
    
    PMonto = FormatNumber(pMontoN, 2)
    
    Buffers = ""
        
    Buffers = Buffers & ESC + "@" + ESC + "c0" + Chr$(4) + ESC + "!" + Chr$(2) & vbNewLine
    Buffers = Buffers & ESC + "L" & vbNewLine
    Buffers = Buffers & ESC + "W" + Chr$(0) + Chr$(0) + Chr$(0) + Chr$(0) + Chr$(137) + Chr$(1) + Chr$(183) + Chr$(3) & vbNewLine
    Buffers = Buffers & ESC + "T" + Chr$(1) & vbNewLine
    Buffers = Buffers & Space(60) + PMonto + Chr$(10) & vbNewLine
    
    If pNoEndosable Then
        Buffers = Buffers & Space(65) + "NO ENDOSABLE" + Chr$(10) & vbNewLine
    Else
        Buffers = Buffers & Chr$(10) & vbNewLine
    End If
    
    Buffers = Buffers & Space(13) + pNomBeneficiario & vbNewLine
    Buffers = Buffers & ESC + "!" + Chr$(48) & vbNewLine
    Buffers = Buffers & Space(10) + "***" + PMonto + "***" & vbNewLine
    Buffers = Buffers & ESC + "!" + Chr$(0) & vbNewLine
    Buffers = Buffers & Space(13) + pLugar & " " & CDate(pFecha) & vbNewLine
    Buffers = Buffers & Chr$(12) & vbNewLine
    Buffers = Buffers & ESC + "q" & vbNewLine
    
    ObjetoComm.Output = Buffers & Imp_InicializarSerial
    
End Sub

Public Function Imp_InicializarSerial() As String
    Dim ESC
    ESC = Chr$(27)
    Imp_InicializarSerial = ESC + "@"
End Function
