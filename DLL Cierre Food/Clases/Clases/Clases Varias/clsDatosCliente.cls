VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDatosCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Const CodigoCliente = 0, DescripcionCliente = 1, LimiteCliente = 2, DiasCliente = 3
Private Const PrecioCliente = 4, RestringidoCliente = 5, CodigoAfiliado = 6, CodigoAutorizado = 7
Const DescuentoCliente = 8
Const RifCliente = 9
Const DireccionCliente = 10
Const TelefonoCliente = 11

Public Enum TiposDeClientes
    TipoCliente = 0
    TipoAfiliado = 1
    TipoAutorizado = 2
End Enum

Public Enum TipoBusquedaCliente
    BCodigoCliente = 0
    BRifCliente = 1
    BNitCliente = 2
    BDescripcionCliente = 3
End Enum

Public Enum TiposDeDescuento
    TipoComercial = 0               'DESCUENTO POR PRODUCTO
    TipoFinanciero = 1              'DESCUENTO POR FACTURA
End Enum

Private Const TablaCuentas_X_Pagar As String = "MA_CXC"
Private Const ConceptoGiro As String = "GIR"
Private Const ConceptoCuota As String = "CUO"
Private Const ConceptoNotaDebito As String = "N_D"
Private Const TablaDeCliente As String = "MA_CLIENTES"

Private objMensaje As Object
Private Datos As Variant
Private DatosEncontrados As Variant
Private Rutinas As New cls_Rutinas
Private ConexionAdm As Object
Private ConexionPos As Object
Private ConexionPosLocal As Object
Private DestruirConexion As Boolean

'PROPIEDADES BÁSICAS
Private CodigoDelCliente            As String
Private DescripcionDelCliente       As String
Private RifDelCliente               As String
Private DireccionDelCliente         As String
Private TelefonoDelCliente          As String
Private LimiteDelCliente            As Double
Private DiasDelCliente              As Integer
Private DescuentoDelCliente         As Double
Private TipoDescuentoDelCliente     As TiposDeDescuento
Private TipoDeCliente               As TiposDeClientes

'PROPIEDADES DE CUENTASxCOBRAR
Private MontoCredito                As Double
Private DiasVencimiento             As Double

Property Let ClienteCodigo(CodCliente As String)
    CodigoDelCliente = CodCliente
End Property

Property Let ClienteDescripcion(DesCliente As String)
    DescripcionDelCliente = DesCliente
End Property

Property Let ClienteRif(RifCliente As String)
    RifDelCliente = RifCliente
End Property

Property Let ClienteDireccion(DirCliente As String)
    DireccionDelCliente = DirCliente
End Property

Property Let ClienteTelefono(TelCliente As String)
    TelefonoDelCliente = TelCliente
End Property

Property Let ClienteLimite(LimCliente As Double)
    LimiteDelCliente = LimCliente
End Property

Property Let ClienteDias(DiasCliente As String)
    DiasDelCliente = DiasCliente
End Property

Property Let ClienteDescuento(DctoCliente As String)
    DescuentoDelCliente = DctoCliente
End Property

Property Let ClienteTipoDescuento(TipoDctoCliente As TiposDeDescuento)
    TipoDescuentoDelCliente = TipoDctoCliente
End Property

Property Let ClienteTipo(TipCliente As TiposDeClientes)
    TipoDeCliente = Tipliente
End Property

Property Let ClienteMontoCredito(MontoCreditoCliente As Double)
    MontoCredito = MontoCreditoCliente
End Property

Property Let ClienteDiasVencimiento(DiasVencimientoCliente As Double)
    DiasVencimiento = DiasVencimientoCliente
End Property

'PROPERTY GETS

Property Get ClienteCodigo() As String
    ClienteCodigo = CodigoDelCliente
End Property

Property Get ClienteDescripcion() As String
    ClienteDescripcion = DescripcionDelCliente
End Property

Property Get ClienteRif() As String
    ClienteRif = RifDelCliente
End Property

Property Get ClienteDireccion() As String
    ClienteDireccion = DireccionDelCliente
End Property

Property Get ClienteTelefono() As String
    ClienteTelefono = TelefonoDelCliente
End Property

Property Get ClienteLimite() As Double
    ClienteLimite = LimiteDelCliente
End Property

Property Get ClienteDias() As String
    ClienteDias = DiasDelCliente
End Property

Property Get ClienteDescuento() As String
    ClienteDescuento = DescuentoDelCliente
End Property

Property Get ClienteTipoDescuento() As TiposDeDescuento
    ClienteTipoDescuento = TipoDescuentoDelCliente
End Property

Property Get ClienteTipo() As TiposDeClientes
    ClienteTipo = TipoDeCliente
End Property

Property Get ClienteMontoCredito() As Double
    ClienteMontoCredito = MontoCredito
End Property

Property Get ClienteDiasVencimiento() As Double
    ClienteDiasVencimiento = DiasVencimiento
End Property

' FIN DE PROPERTY GETS

Public Sub InicializarConexiones(pConexionAdm As Object, pConexionPos As Object, Optional PConexionPosLocal As Object, _
Optional sConexionAdm As String, Optional sConexionPos As String, Optional SConexionPosLocal As String)
    
    Call Rutinas.AbrirConexion(pConexionAdm, sConexionAdm, DestruirConexion)        'CREAR CONEXION
    Call Rutinas.AbrirConexion(pConexionPos, sConexionPos, DestruirConexion)         'CREAR CONEXION
    
    If Not IsMissing(PConexionPosLocal) Then
        Call Rutinas.AbrirConexion(ConexionPosLocal, SConexionPosLocal, DestruirConexion)         'CREAR CONEXION
        Set ConexionPosLocal = PConexionPosLocal
    End If
    
    Set ConexionAdm = pConexionAdm
    Set ConexionPos = pConexionPos
    
End Sub

Public Function MostrarDatosCliente(Conexion As Object, pCodigo As String, Optional CampoDeBusqueda As Integer = 0) As Variant
    
    Dim RecBuscar As New ADODB.Recordset
    Dim SQL As String, Campo As String
    Dim CodigoRelacion As String
        
        Select Case CampoDeBusqueda
            Case BCodigoCliente
                Campo = "MA_CLIENTES.c_codcliente"
            Case BRifCliente
                Campo = "MA_CLIENTES.c_rif"
            Case BNitCliente
                Campo = "MA_CLIENTES.c_nit"
            Case BDescripcionCliente
                Campo = "MA_CLIENTES.c_descripcio"
            Case Else
                Campo = "MA_CLIENTES.c_codcliente"
        End Select
        
    SQL = "Select MA_CLIENTES.*,MA_CLIENTES_CONVENIOS.restringido from MA_CLIENTES Left Join MA_CLIENTES_CONVENIOS ON MA_CLIENTES.c_CODCLIENTE = MA_CLIENTES_CONVENIOS.c_CODCLIENTE where " & Campo & " ='" & pCodigo + "'"
    
    RecBuscar.Open SQL, Conexion, adOpenForwardOnly, adLockReadOnly
    
    If RecBuscar.EOF Then Exit Function
    
    If RecBuscar!C_CODCLIENTE_REL <> "" Then        ' Si el codigo de la relacion es diferente de nada , el cliente buscado puede ser un Afiliado o un Autorizado
        
        SQL = "Select MA_CLIENTES.*,MA_CLIENTES_CONVENIOS.restringido from MA_CLIENTES left join MA_CLIENTES_CONVENIOS ON MA_CLIENTES.c_CODCLIENTE = MA_CLIENTES_CONVENIOS.c_CODCLIENTE where MA_CLIENTES.c_CODCLIENTE='" & RecBuscar!C_CODCLIENTE_REL + "'"
        RecBuscar.Close
        
        '///******************************************** El cliente seleccionado tenia una relacion si tiene otra quiere decir que el cliente era un Autorizado ********************************************************************///
        
        RecBuscar.Open SQL, Conexion, adOpenForwardOnly, adLockReadOnly
        
        If RecBuscar!C_CODCLIENTE_REL <> "" Then
            CodigoRelacion = RecBuscar!c_codcliente             'Si el Codigo de la Relacion es diferente de nada, el cliente buscado es un autorizado por lo tanto el codigoautorizado es igual al parametro pasado, y el codigorelacion es el codigo del afiliado
            SQL = "Select MA_CLIENTES.*,MA_CLIENTES_CONVENIOS.restringido from MA_CLIENTES left join MA_CLIENTES_CONVENIOS ON MA_CLIENTES.c_CODCLIENTE = MA_CLIENTES_CONVENIOS.c_CODCLIENTE where MA_CLIENTES.c_CODCLIENTE='" & RecBuscar!C_CODCLIENTE_REL + "'"
            RecBuscar.Close
            
            RecBuscar.Open SQL, Conexion, adOpenForwardOnly, adLockReadOnly
            Call AsignarDatos(RecBuscar, CodigoRelacion, pCodigo)
        Else
            Call AsignarDatos(RecBuscar, pCodigo)        ' Como no se encontro otra relacion era un Afiliado y el parametro pasado es el codigo del afiliado
        End If
        
    Else
        Call AsignarDatos(RecBuscar)                        'Se busco un cliente
    End If
    
    MostrarDatosCliente = DatosEncontrados
      
End Function

Private Sub AsignarDatos(ByRef Record As ADODB.Recordset, Optional Afiliado As String, Optional Autorizado As String)
    
    Dim DatosDevueltos(11) As Variant
    
    '///********************************************** Asignando los valores del recoraset encontrado *******************************************///
    
    DatosDevueltos(CodigoCliente) = Record!c_codcliente
    DatosDevueltos(DescripcionCliente) = Record!c_descripcio
    DatosDevueltos(LimiteCliente) = Record!N_LIMITE
    DatosDevueltos(DiasCliente) = Record!N_DIAS
    DatosDevueltos(PrecioCliente) = Record!N_PRECIO
    DatosDevueltos(RestringidoCliente) = Record!restringido
    DatosDevueltos(DescuentoCliente) = Record!N_DESCUENTO
    DatosDevueltos(CodigoAfiliado) = Afiliado
    DatosDevueltos(CodigoAutorizado) = Autorizado
    DatosDevueltos(RifCliente) = Record!C_RIF
    DatosDevueltos(DireccionCliente) = Record!c_direccion
    DatosDevueltos(TelefonoCliente) = Record!c_telefono
    
    DatosEncontrados = DatosDevueltos
    
End Sub

Public Sub LlenarListaClientes(ByRef LV As Object, Conexion As Object, pTipo As Integer, _
Optional pCriterio As String, Optional pCampo As String)
    
    Dim RecCliente As New ADODB.Recordset
    Dim SQL As String
    Dim ListaItem As ListItem
    
    '///***************************************Filtrando datos por Tipo,por codigo o nombre y ordenandolos *******************************************************///
    
    If pCriterio <> "" And pCampo <> "" Then
        SQL = "Select c_CODCLIENTE,c_DESCRIPCIO,n_TIPO,c_rif from MA_CLIENTES where n_TIPO=" & pTipo
        SQL = SQL + " and " & pCampo + " like '" & Replace(pCriterio, "'", "''") + "%' order by " & pCampo
    Else
        SQL = "Select c_CODCLIENTE,c_DESCRIPCIO,N_TIPO,c_RIF from MA_CLIENTES  where N_TIPO=" & pTipo
    End If
    
    '///***************************************** Abriendo conexion y recorsets, dependiendo del parametro pasado *************************************************///

    RecCliente.Open SQL, Conexion
    
    '///************************************************** Llenando List View ******************************************************************///
    
    LV.ListItems.Clear
    If Not RecCliente.EOF Then
        
        Do Until RecCliente.EOF
            
            Set ListaItem = LV.ListItems.Add(, , RecCliente!c_codcliente)
            ListaItem.SubItems(1) = RecCliente!c_descripcio
            ListaItem.SubItems(2) = IIf(IsNull(RecCliente!C_RIF), "", RecCliente!C_RIF)
            RecCliente.MoveNext
        Loop
    Else
        'objMensaje.mensaje "No se encontraron clientes de este tipo"
        Call gRutinas.Mensajes(StellarMensaje(260))
    End If

End Sub

Private Sub Class_Initialize()
    Set objMensaje = CreateObject("recsun.obj_MENSAJERIA")
End Sub

Private Sub Class_Terminate()
    
    Set objMensaje = Nothing
    If DestruirConexion Then
        ConexionAdm.Close
        Set ConexionAdm = Nothing
        ConexionPos.Close
        Set ConexionPos = Nothing
    End If
    
End Sub

' MANTENIMIENTO, NECESITA LA MONEDA.

Public Function CreditosVencidosYPorVencerse(pCodCliente, LConexion As Object) As Variant
    'ASUME QUE LA MONEDA CON LA CUAL FUE ESCRITA LA TANSACCION ES LA PREDETERMINADA
    If LConexion Is Nothing Then Exit Function
    Dim mRstmp As Object
    Set mRstmp = Rutinas.CrearAdoRs
    SQL = " SELECT MAX(DATEDIFF(dd, f_fechae, f_fechav)) AS diasCredito, sum(n_total - n_Pagado) as montoCredito " & _
        " From " & TablaCuentas_X_Pagar & " WHERE (n_total - n_Pagado <> 0) AND (c_concepto = N'VEN' OR " & _
                          " c_concepto = '" & ConceptoCuota & "' OR " & _
                          " c_concepto = '" & ConceptoGiro & "' OR " & _
                          " c_concepto = '" & ConceptoNotaDebito & "') AND (C_Codigo = '" & pCodCliente & "')"
    mRstmp.CursorLocation = adUseServer
    mRstmp.Open SQL, LConexion, adOpenForwardOnly, adLockReadOnly
    If Not mRstmp.EOF Then
        CreditosVencidosYPorVencerse = Array(CDbl(IIf(IsNull(mRstmp!diasCredito), 0, mRstmp!diasCredito)), CDbl(IIf(IsNull(mRstmp!MontoCredito), 0, mRstmp!MontoCredito)))
        DiasVencimiento = CDbl(IIf(IsNull(mRstmp!diasCredito), 0, mRstmp!diasCredito))
        MontoCredito = CDbl(IIf(IsNull(mRstmp!MontoCredito), 0, mRstmp!MontoCredito))
    Else
        CreditosVencidosYPorVencerse = Array(0, 0)
        MontoCredito = 0
        DiasVencimiento = 0
    End If
End Function

Public Function PropiedadesCliente(Codigo As String, LConexion As Object, Optional BusquedaLocal As Boolean = False, Optional BuscarClientePor As Integer = 0) As Boolean
    Dim RsCliente As Object, Campo As String
    PropiedadesCliente = False
    If LConexion Is Nothing Then Exit Function
    Set RsCliente = CreateObject("ADODB.recordset")
    
    Select Case BuscarClientePor
        Case BCodigoCliente
            Campo = "C_CODCLIENTE"
        Case BRifCliente
            Campo = "C_RIF"
        Case BNitCliente
            Campo = "C_NIT"
        Case BDescripcionCliente
            Campo = "C_DESCRIPCIO"
        Case Else
            Campo = "C_CODCLIENTE"
    End Select
    RsCliente.Open "SELECT * FROM " & TablaDeCliente & " WHERE " & Campo & " = '" & Codigo & "'", LConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    
    If Not RsCliente.EOF Then
        CodigoDelCliente = RsCliente!c_codcliente
        DescripcionDelCliente = RsCliente!c_descripcio
        RifDelCliente = RsCliente!C_RIF
        DireccionDelCliente = RsCliente!c_direccion
        TelefonoDelCliente = RsCliente!c_telefono
        LimiteDelCliente = RsCliente!N_LIMITE
        DiasDelCliente = RsCliente!N_DIAS
        DescuentoDelCliente = RsCliente!N_DESCUENTO
        TipoDescuentoDelCliente = RsCliente!NU_TIPODESCUENTO
        TipoDeCliente = RsCliente!N_TIPO
        If Not BusquedaLocal Then
            Call CreditosVencidosYPorVencerse(Codigo, LConexion)
        End If
        PropiedadesCliente = True
    End If
    RsCliente.Close
End Function

Public Function ShowFrmClientes(CodigoCuenta As String, Relacion As String, Mesero As String) As Variant
    On Error GoTo Falla_Local
    Set FrmCambioClientes.ConexionAdm = ConexionAdm
    Set FrmCambioClientes.ConexionPos = ConexionPos
    Set FrmCambioClientes.ConexionPosLocal = ConexionPosLocal
    
    FrmCambioClientes.MCodigoMesero = Mesero
    FrmCambioClientes.MCuenta = CodigoCuenta
    FrmCambioClientes.MRelacion = Relacion
    
    FrmCambioClientes.Show vbModal
    
    ShowFrmClientes = FrmCambioClientes.DatosDelCliente
Falla_Local:
    Set FrmCambioClientes = Nothing
End Function

Public Function ShowFrmBuscarClientes() As Variant
    Set view_clientes.ConexionAdm = ConexionAdm
    view_clientes.Show vbModal
    ShowFrmBuscarClientes = view_clientes.DatosClienteSel
    Set view_clientes = Nothing
End Function

Public Function EscribirDatosClientes(CuentaNumero As String, Relacion As String, Mesero As String, CodigoCliente As String, DescripcionCliente As String, RifCliente As String, PrecioCliente As String, DireccionCliente As String, TelefonoCliente As String, Optional ConexionPosRemoto As Object) As Boolean
    
    'CAPA 2
    
    On Error GoTo Falla_Local
    
    EscribirDatosClientes = False
    
    If Trim(CuentaNumero) = "" Then
        Rutinas.Mensajes "No se puede grabar sin tener información de la cuenta.", False
        Exit Function
    End If
    
    If Trim(Relacion) = "" Then
        Rutinas.Mensajes "No se puede grabar sin tener información sobre la relacion de la mesa o cuenta.", False
        Exit Function
    End If
    
    If Trim(Mesero) = "" Then
        Rutinas.Mensajes "No se puede grabar sin tener información sobre el mesero.", False
        Exit Function
    End If
    
    If Trim(CodigoCliente) = "" Then
        Rutinas.Mensajes "No se puede grabar sin tener información sobre el cliente.", False
        Exit Function
    End If
    
    If Trim(PrecioCliente) = "" Then
        Rutinas.Mensajes "No se puede grabar sin tener información sobre el cliente.", False
        Exit Function
    End If
    
    EscribirDatosClientes = Escribir_Datos_Clientes(CuentaNumero, Relacion, Mesero, CodigoCliente, DescripcionCliente, RifCliente, PrecioCliente, DireccionCliente, TelefonoCliente, ConexionPosRemoto)
    
Falla_Local:
    
End Function

Private Function Escribir_Datos_Clientes(CuentaNumero As String, Relacion As String, Mesero As String, CodigoCliente As String, DescripcionCliente As String, RifCliente As String, PrecioCliente As String, DireccionCliente As String, TelefonoCliente As String, Optional ConexionPosRemoto As Object, Optional Tabla As String = "MA_TRANSACCION_TEMP") As Boolean
    'CAPA 3
    Dim RsCuentas As Object
    Escribir_Datos_Clientes = False
    On Error GoTo Falla_Al_Grabar
    Set RsCuentas = CreateObject("ADODB.recordset")
    
    RsCuentas.Open "SELECT * FROM " & Tabla & _
                   " WHERE cs_IDPADRE = '" & CuentaNumero & "'" _
                   , ConexionPosLocal, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    If Not RsCuentas.EOF Then
'        If RsCuentas!nu_monto_total > 0 Then Exit Function
        RsCuentas!cu_codigo_cliente = CodigoCliente
        RsCuentas!cu_NOMBRE_CLIENTE = DescripcionCliente
        RsCuentas!cu_RIFCLIENTE = RifCliente
        RsCuentas!cu_DIRECCION_CLIENTE = Trim(DireccionCliente)
        RsCuentas!cu_TELEFONO_CLIENTE = Trim(TelefonoCliente)
        RsCuentas!ns_TIPO_PRECIO = CInt(PrecioCliente)
        RsCuentas.UpdateBatch
        Escribir_Datos_Clientes = True
    End If
    RsCuentas.Close
    
    If Not ConexionPosRemoto Is Nothing Then
        
        RsCuentas.Open "SELECT * FROM MA_CONSUMO " & _
                       " WHERE cs_CODIGOCUENTA = '" & CuentaNumero & "'" & _
                       " AND CS_RELACION = '" & Relacion & "' AND cs_CODIGOMESERO = '" & Mesero & "' " _
                       , ConexionPosRemoto, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
                       
        If Not RsCuentas.EOF Then
            RsCuentas!cu_codigocliente = CodigoCliente
            RsCuentas!CU_NOMBRECLIENTE = DescripcionCliente
            RsCuentas!cu_RIFCLIENTE = RifCliente
            RsCuentas!cu_DIRECCION_CLIENTE = Trim(DireccionCliente)
            RsCuentas!cu_TELEFONO_CLIENTE = Trim(TelefonoCliente)
            RsCuentas!cs_PRECIOCLIENTE = CInt(PrecioCliente)
            RsCuentas.UpdateBatch
            Escribir_Datos_Clientes = True
        End If
        RsCuentas.Close
        
    End If
    
    Exit Function
Falla_Al_Grabar:
    Exit Function
End Function

Public Sub AsignarClienteAlPos(DatosClienteSel As Variant)
     If Not IsEmpty(DatosClienteSel) Then
        POS.ClienteActual.Codigo = IIf(IsNull(DatosClienteSel(0)), "", DatosClienteSel(0))
        POS.ClienteActual.Nombre = IIf(IsNull(DatosClienteSel(1)), "", DatosClienteSel(1))
        POS.ClienteActual.limite = IIf(IsNull(DatosClienteSel(2)), 0, DatosClienteSel(2))
        POS.ClienteActual.Dias = IIf(IsNull(DatosClienteSel(3)), "", DatosClienteSel(3))
        POS.ClienteActual.Precio = IIf(IsNull(DatosClienteSel(4)), "", DatosClienteSel(4))
        POS.ClienteActual.CodigoAfiliado = IIf(IsNull(DatosClienteSel(5)), "", DatosClienteSel(5))
        POS.ClienteActual.CodigoAutorizado = IIf(IsNull(DatosClienteSel(6)), "", DatosClienteSel(6))
        POS.ClienteActual.Descuento = IIf(IsNull(DatosClienteSel(8)), 0, DatosClienteSel(8))
        POS.ClienteActual.Rif = IIf(IsNull(DatosClienteSel(9)), "", DatosClienteSel(9))
        GLimitedeCompra = POS.ClienteActual.limite
    End If
End Sub

Public Sub ImagenDelCliente(ByRef PictureObj As Object, CodigoCliente As String, Campo As String, PConexion As Object, Optional CampoDeBusqueda As Integer = 0)
    Dim RsCliente As New ADODB.Recordset, Extraer As New recsun.cls_datos, Campos As String
    On Error GoTo Falla_Local
    Set RsCliente = Rutinas.CrearAdoRs
    Set Extraer = CreateObject("recsun.cls_datos")
    
    Select Case CampoDeBusqueda
        Case BCodigoCliente
            Campos = "c_codcliente"
        Case BRifCliente
            Campos = "c_rif"
        Case BNitCliente
            Campos = "c_nit"
        Case BDescripcionCliente
            Campos = "c_descripcio"
        Case Else
            Campos = "c_codcliente"
    End Select
    
    RsCliente.Open "SELECT * FROM MA_CLIENTES WHERE " & Campos & " = '" & CodigoCliente & "'", PConexion, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    If Not RsCliente.EOF Then
        Call Extraer.PopImageOfDb(PictureObj, RsCliente, Campo)
    End If
    RsCliente.Close
Falla_Local:
    Reset
    Close
End Sub
