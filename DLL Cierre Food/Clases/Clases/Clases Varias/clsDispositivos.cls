VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDispositivos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'//*****************************************************************************//
'// CLASE DE DISPOSITIVOS
'// 03-09-2003
'//
'//
'//*****************************************************************************//
'    PNEMOTECNICO       DESCRIPCION                     CODIGO
'======================================================================
'    IMPRESORA          IMPRESORA                       DSP0000001
'    DISPLAY            DISPLAY                         DSP0000002
'    SCANNER            SCANNER                         DSP0000003
'    GAVETA             GAVETA                          DSP0000004
'    BALANZA            BALANZA                         DSP0000005
'    MRS                LECTOR DE BANDA MAGN�TICA       DSP0000006
'    MIR                LECTOR DE CHEQUES               DSP0000007
'    LLAVES             LLAVES DEL POS                  DSP0000008

Public ClaseRutinas As New cls_Rutinas
Private StrPNemotecnicos(0 To 7) As String

Enum PNemotecnicoDispositivos
    PNemotecnicoImpresora = 0
    PNemotecnicoDisplay = 1
    PNemotecnicoScanner = 2
    PNemotecnicoGaveta = 3
    PNemotecnicoBalanza = 4
    PNemotecnicoMrs = 5
    PNemotecnicoMir = 6
    PNemotecnicoLlaves = 7
End Enum
                                                                                                                                                     
Public Function SelectDispositivo(PNemotecnico As String) As Long
    Dim Cont As Long
    SelectDispositivo = -1
    On Error GoTo Falla_Local
    If Trim(PNemotecnico) = "" Then Exit Function
    For Cont = LBound(POS.Propiedades.MisDispositivos) To UBound(POS.Propiedades.MisDispositivos)
        If UCase(PNemotecnico) = UCase(POS.Propiedades.MisDispositivos(Cont).PNemotecnico) Then
            SelectDispositivo = Cont
            Exit Function
        End If
    Next Cont
Falla_Local:
End Function

Public Function AbrirDispositivo(NumeroDispositivo As Long, Optional ByRef ControlOpos As Object, Optional ByRef ControlSerial As MSComm, Optional FileHand As Long) As Boolean
    
    Dim EsteDispositivo As Dispositivos, Datos As String
    
    EsteDispositivo = POS.Propiedades.MisDispositivos(NumeroDispositivo)
    
    AbrirDispositivo = False
    
    On Error GoTo Falla_Dispositivo
    
    If EsteDispositivo.NombreOPOS <> "" Then
        
        ' EL DISPOSITIVO TIENE NOMBRE OPOS.
        
        If ControlOpos Is Nothing Then
            ClaseRutinas.Mensajes "No se especific� un objeto OPOS en el c�digo para el dispositivo.", False
            Exit Function
        End If
        
        If ControlOpos.Open(EsteDispositivo.NombreOPOS) <> OPOS_SUCCESS Then
            ControlOpos.Close
            Set EsteDispositivo.OOPOSApuntador = Nothing
        Else
            ControlOpos.DataEventEnabled = True
            ControlOpos.DeviceEnabled = False
            Set EsteDispositivo.OOPOSApuntador = ControlOpos
        End If
        
        EsteDispositivo.FApuntador = -1
        
        Set EsteDispositivo.OComApuntador = Nothing
        
    Else
        
        'EL DISPOSITIVO NO USA OPOS
        If InStr(1, EsteDispositivo.Puerto, "COM") Then
            'TIPO SERIAL
            Datos = EsteDispositivo.Baudios & "," & EsteDispositivo.Paridad & "," & EsteDispositivo.BitDatos & "," & EsteDispositivo.Parada
            ControlSerial.Settings = Datos
            ControlSerial.CommPort = Right(EsteDispositivo.Puerto, 1)
            ControlSerial.PortOpen = True
            Set EsteDispositivo.OComApuntador = ControlSerial
            Set EsteDispositivo.OOPOSApuntador = Nothing
            EsteDispositivo.FApuntador = -1
            
            AbrirDispositivo = True
            
        Else
            'TIPO PARALELO
            FileHand = FreeFile()
'            Open EsteDispositivo.Puerto For Output Access Write As #FileHand
            AbrirDispositivo = True
            EsteDispositivo.FApuntador = FileHand
            Set EsteDispositivo.OComApuntador = Nothing
            Set EsteDispositivo.OOPOSApuntador = Nothing
        End If
    End If
    
    POS.Propiedades.MisDispositivos(NumeroDispositivo) = EsteDispositivo
    
    Exit Function
    
Falla_Dispositivo:
    
    FileHand = -1
    
End Function

Public Function SettingOPOS(NumeroDispositivo As Long, Optional DspEnabled As Boolean = True, Optional DspEvent As Boolean = True, Optional DspRelease As Boolean = True, Optional DspClose As Boolean = False) As Boolean
    Dim EsteDispositivo As Dispositivos, Datos As String
    SettingOPOS = False
    On Error GoTo Falla_Local
    EsteDispositivo = POS.Propiedades.MisDispositivos(NumeroDispositivo)
    If EsteDispositivo.NombreOPOS <> "" Then
        
        If Not EsteDispositivo.OOPOSApuntador Is Nothing Then
            EsteDispositivo.OOPOSApuntador.DeviceEnabled = DspEnabled
            If DspRelease Then EsteDispositivo.OOPOSApuntador.Release
            If DspClose Then EsteDispositivo.OOPOSApuntador.Close
            If UCase(EsteDispositivo.OOPOSApuntador.Name) = "OSCANNER" Then EsteDispositivo.OOPOSApuntador.DataEventEnabled = DspEvent
        End If
        
    End If
    SettingOPOS = True
    Exit Function
Falla_Local:
    Exit Function
End Function

Private Sub ConfigurarDispositivos(numero As Long, forma As Object)
    Dim EsteDispositivo As Dispositivos, FHandVar As Long
    EsteDispositivo = POS.Propiedades.MisDispositivos(numero)
    Select Case UCase(EsteDispositivo.PNemotecnico)
        Case "IMPRESORA"
            If Not gDispositivos.AbrirDispositivo(numero, forma.OImpresora, forma.Impresora, FHandVar) Then
                gRutinas.Mensajes "Error al configurar el dispositivo nombrado 'IMPRESORA'", False
            End If
        Case "DISPLAY"
            If Not gDispositivos.AbrirDispositivo(numero, forma.ODisplay, forma.Display, FHandVar) Then
                gRutinas.Mensajes "Error al configurar el dispositivo nombrado 'DISPLAY'", False
            End If
        
        Case "SCANNER"
            If Not gDispositivos.AbrirDispositivo(numero, forma.Oscanner, forma.Scanner, FHandVar) Then
                gRutinas.Mensajes "Error al configurar el dispositivo nombrado 'SCANNER'", False
            End If
        
        Case "GAVETA"
            If Not gDispositivos.AbrirDispositivo(numero, forma.OGaveta, forma.Gaveta, FHandVar) Then
                gRutinas.Mensajes "Error al configurar el dispositivo nombrado 'GAVETA'", False
            End If
        
        Case "BALANZA"
            If Not gDispositivos.AbrirDispositivo(numero, , forma.Balanza, FHandVar) Then
                gRutinas.Mensajes "Error al configurar el dispositivo nombrado 'BALANZA'", False
            End If
        
        Case "MRS"
            If Not gDispositivos.AbrirDispositivo(numero, forma.OMrs, forma.mRs, FHandVar) Then
                gRutinas.Mensajes "Error al configurar el dispositivo nombrado 'MRS'", False
            End If
        
        Case "MIR"
            If Not gDispositivos.AbrirDispositivo(numero, forma.OMir, forma.MIR, FHandVar) Then
                gRutinas.Mensajes "Error al configurar el dispositivo nombrado 'MIR'", False
            End If
        
        Case "LLAVES"
            If Not gDispositivos.AbrirDispositivo(numero, forma.OLLaves, forma.LLAVES, FHandVar) Then
                gRutinas.Mensajes "Error al configurar el dispositivo nombrado 'LLAVES'", False
            End If
    End Select
    If Not gDispositivos.SettingOPOS(numero) Then
        gRutinas.Mensajes "Error al inicializar el dispositivo nombrado '" & UCase(EsteDispositivo.PNemotecnico) & "'", False
    End If
End Sub

Public Sub InicializarDispositivos(forma As Object)
    Dim ContDevice As Long, ActualDevices() As Dispositivos
    On Error GoTo Falla
    For ContDevice = 0 To UBound(POS.Propiedades.MisDispositivos)
        ConfigurarDispositivos ContDevice, forma
        DoEvents
    Next ContDevice
    Exit Sub
Falla:
End Sub

Private Sub Class_Initialize()
    StrPNemotecnicos(0) = "IMPRESORA"
    StrPNemotecnicos(1) = "DISPLAY"
    StrPNemotecnicos(2) = "SCANNER"
    StrPNemotecnicos(3) = "GAVETA"
    StrPNemotecnicos(4) = "BALANZA"
    StrPNemotecnicos(5) = "MRS"
    StrPNemotecnicos(6) = "MIR"
    StrPNemotecnicos(7) = "LLAVES"
End Sub

Public Function Str_PNemotecnico(numero As PNemotecnicoDispositivos) As String
    Str_PNemotecnico = StrPNemotecnicos(numero)
End Function

Public Sub CortarPapel(Optional pVarImpresora As String = "")
    
    On Error GoTo Error
    
    Dim EsteDispositivo As Dispositivos, UbicacionDispositivo As Long, Cadena As String
    
    UbicacionDispositivo = SelectDispositivo(Str_PNemotecnico(PNemotecnicoImpresora))
    
    If UbicacionDispositivo >= 0 Then 'SE ENCONTRO LA CONFIURACION DEL DISPOSITIVO
        EsteDispositivo = POS.Propiedades.MisDispositivos(UbicacionDispositivo)
        ' Si marcaron Cortar Papel al Finalizar
        If EsteDispositivo.UsaHojilla And EsteDispositivo.CarCorte <> vbNullString Then
            If EsteDispositivo.Puerto = "N/A" Then
                ESC_POS_CortarPapelImp pVarImpresora
            Else
                FileHand = FreeFile()
                Open EsteDispositivo.Puerto For Output Access Write As #FileHand
                Cadena = ConvertirEnAscii(EsteDispositivo.CarCorte)
                Call EnviarCadenaADispositivo(Cadena, UbicacionDispositivo)
            End If
        End If
    End If
    
    Exit Sub
    
Error:
    
End Sub

Public Sub AbrirGaveta()
    Dim EsteDispositivo As Dispositivos, UbicacionDispositivo As Long, Cadena As String
    
    UbicacionDispositivo = SelectDispositivo(Str_PNemotecnico(PNemotecnicoGaveta))
    If UbicacionDispositivo >= 0 Then 'SE ENCONTRO LA CONFIURACION DEL DISPOSITIVO
        EsteDispositivo = POS.Propiedades.MisDispositivos(UbicacionDispositivo)
        Call EnviarCadenaADispositivo(EsteDispositivo.CarControl, UbicacionDispositivo)
    Else
        UbicacionDispositivo = SelectDispositivo(Str_PNemotecnico(PNemotecnicoImpresora))
        If UbicacionDispositivo >= 0 Then 'SE ENCONTRO LA CONFIURACION DEL DISPOSITIVO
            EsteDispositivo = POS.Propiedades.MisDispositivos(NumeroDispositivo)
            FileHand = FreeFile()
            Open EsteDispositivo.Puerto For Output Access Write As #FileHand
            EsteDispositivo = POS.Propiedades.MisDispositivos(UbicacionDispositivo)
            Cadena = ConvertirEnAscii(EsteDispositivo.CarControl)
            Call EnviarCadenaADispositivo(Cadena, UbicacionDispositivo)
        End If
    End If
End Sub

Public Function EnviarCadenaADispositivo(Cadena As String, NumeroDeDispositivo As Long) As Boolean
    Dim EsteDispositivo As Dispositivos, UbicacionDispositivo As Long
    EnviarCadenaADispositivo = False
    On Error GoTo Falla_Local
    If UbicacionDispositivo >= 0 Then 'SE ENCONTRO LA CONFIURACION DEL DISPOSITIVO
        EsteDispositivo = POS.Propiedades.MisDispositivos(NumeroDeDispositivo)
        If Not EsteDispositivo.OComApuntador Is Nothing Then
            'USA PUERTO SERIAL
            EsteDispositivo.OComApuntador.Output Cadena
        ElseIf Not EsteDispositivo.OOPOSApuntador Is Nothing Then
            'USA OPOS
            
        ElseIf EsteDispositivo.FApuntador >= 0 Then
            'USA LPT
            
            Print #EsteDispositivo.FApuntador, Cadena
            Close #EsteDispositivo.FApuntador
        End If
'        Call AbrirDispositivo(UbicacionDispositivo, EsteDispositivo.OOPOSApuntador, EsteDispositivo.OComApuntador, EsteDispositivo.FApuntador)
    End If
    EnviarCadenaADispositivo = True
    Exit Function
Falla_Local:
End Function

Private Function ConvertirEnAscii(Cadena As String) As String
    Dim SplitCadena As Variant, LCont As Integer
    On Error GoTo Falla_Local
    SplitCadena = Split(Cadena, ",")
    ConvertirEnAscii = ""
    For LCont = LBound(SplitCadena) To UBound(SplitCadena)
        If IsNumeric(SplitCadena(LCont)) Then
            If CLng(SplitCadena(LCont)) > 255 Then
                ConvertirEnAscii = ConvertirEnAscii & Chr(CLng(SplitCadena(LCont)) Mod 255)
            Else
                ConvertirEnAscii = ConvertirEnAscii & Chr(CLng(SplitCadena(LCont)))
            End If
        Else
            ConvertirEnAscii = ConvertirEnAscii & SplitCadena(LCont)
        End If
    Next LCont
    Exit Function
Falla_Local:
End Function

Public Function HayImpresoraSeleccionada(Optional ByRef NombreDispositivo As String) As Boolean
    On Error GoTo Falla_Impresora
    NombreDispositivo = Printer.DeviceName
    HayImpresoraSeleccionada = True
    Exit Function
Falla_Impresora:
    HayImpresoraSeleccionada = False
End Function


