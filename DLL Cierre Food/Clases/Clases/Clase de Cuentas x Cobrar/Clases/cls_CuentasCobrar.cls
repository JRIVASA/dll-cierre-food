VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cls_CuentasCobrar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'/**************************************************************************** _
 /* CLASE DE CUENTAS POR COBRAR _
 /* HECHO POR: EDGAR SANCHEZ _
 /* FECHA: 04/10/2003 _
 /* _
 /* MODIFICADO POR: _
 /* FECHA:
Private TipoCuenta(0 To 1) As String
Private NTipoCuenta As CuentaEnum
'DECLARACIONES PRIVADAS
Private ConexionAdm                     As Object
Private ConexionPos                     As Object
Private ClaseClientes                   As New clsDatosCliente
Private ClaseMoneda                     As New cls_Monedas
Private ClaseDenominacion               As New cls_Denominaciones
Private ClaseRutinas                    As New cls_Rutinas

Public Enum CuentaEnum
    EnumCuentaCobrar = 0
    EnumCuentaPagar = 1
End Enum

Public Property Let UsarTipoCuenta(UsarCuenta As CuentaEnum)
    NTipoCuenta = UsarCuenta
End Property

'DECLARACIONES PUBLICAS

'RUTINAS Y PROCEDIMIENTOS

' MANTENIMIENTO, NECESITA LA MONEDA.

Public Function CreditosVencidosYPorVencerse(pCodCliente As String, Optional LConexion As Object, Optional LConexionString As String = "") As Variant
    'ASUME QUE LA MONEDA CON LA CUAL FUE ESCRITA LA TANSACCION ES LA PREDETERMINADA
    On Error GoTo Falla_Local
    If LConexion Is Nothing Then
        If Trim(LConexionString) = "" Then
            Exit Function
        End If
        LConexion = ClaseRutinas.CrearAdoCn
        LConexion.ConnectionString = LConexionString
        LConexion.Open
    End If
    Dim mRstmp As Object
    Set mRstmp = Rutinas.CrearAdoRs
    SQL = " SELECT MAX(DATEDIFF(dd, f_fechae, f_fechav)) AS diasCredito, sum(n_total - n_Pagado) as montoCredito " & _
        " From " & TablaCuentas_X_Pagar & " WHERE (n_total - n_Pagado <> 0) AND (c_concepto = N'VEN' OR " & _
                          " c_concepto = '" & ConceptoCuota & "' OR " & _
                          " c_concepto = '" & ConceptoGiro & "' OR " & _
                          " c_concepto = '" & ConceptoNotaDebito & "') AND (C_Codigo = '" & pCodCliente & "')"
    mRstmp.CursorLocation = adUseServer
    mRstmp.Open SQL, LConexion, adOpenForwardOnly, adLockReadOnly
    If Not mRstmp.EOF Then
        CreditosVencidosYPorVencerse = Array(CDbl(IIf(IsNull(mRstmp!diasCredito), 0, mRstmp!diasCredito)), CDbl(IIf(IsNull(mRstmp!MontoCredito), 0, mRstmp!MontoCredito)))
        DiasVencimiento = CDbl(IIf(IsNull(mRstmp!diasCredito), 0, mRstmp!diasCredito))
        MontoCredito = CDbl(IIf(IsNull(mRstmp!MontoCredito), 0, mRstmp!MontoCredito))
    Else
        CreditosVencidosYPorVencerse = Array(0, 0)
        MontoCredito = 0
        DiasVencimiento = 0
    End If
    Exit Function
Falla_Local:
End Function

Public Function CrearCabecero(ConexionAdmin As Object, FacturaNumero As String, CodigoCliente As String, Concepto As String, MontoTotal As Double, MontoPagado As Double, MontoBaseImpuesto As Double, MontoImpuesto As Double, MontoSubtotal As Double, MontoDescuento As Double, FechaEmision As Date, DiasVencimiento As Integer, CodigoLocalidad As String, CodigoUsuario As String, observacion As String, CodigoMoneda As String, FactorMoneda As Double, DetallePagos As Variant, Optional DetalleDenominacion As Variant, Optional Decimales As Long = 2) As Boolean
    Dim RsCuentaCobrar As Object, Consecutivo As String
    On Error GoTo Falla_Local
    CrearCabecero = False
    
    Set RsCuentaCobrar = ClaseRutinas.CrearAdoRs
    
    RsCuentaCobrar.Open "SELECT * FROM " & TipoCuenta(NTipoCuenta) & " WHERE C_DOCUMENTO = '" & FacturaNumero & "' AND C_CONCEPTO = '" & Concepto & "' AND C_CODIGO = '" & CodigoCliente & "'", ConexionAdmin, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    If Not RsCuentaCobrar.EOF Then
        Exit Function
    End If
    RsCuentaCobrar.AddNew
    RsCuentaCobrar!c_Documento = FacturaNumero
    RsCuentaCobrar!C_CODIGO = CodigoCliente
    RsCuentaCobrar!c_Localidad = CodigoLocalidad
    RsCuentaCobrar!c_usuario = CodigoUsuario
    RsCuentaCobrar!c_Concepto = Concepto
    RsCuentaCobrar!c_Detalle = observacion
    RsCuentaCobrar!f_FechaE = FechaEmision
    RsCuentaCobrar!f_FechaV = FechaEmision + DiasVencimiento
    RsCuentaCobrar!n_Impuesto = FormatNumber(MontoImpuesto, Decimales)
    RsCuentaCobrar!n_bimpuesto = FormatNumber(MontoBaseImpuesto, Decimales)
    RsCuentaCobrar!n_Total = FormatNumber(MontoTotal, Decimales)
    If MontoPagado = 0 Then
        RsCuentaCobrar!n_PagadoImp = 0
    Else
        RsCuentaCobrar!n_PagadoImp = FormatNumber((MontoTotal / MontoPagado) * MontoImpuesto, Decimales)            'REVISAR ESTE CAMPO FORMULA = (MontoTotal/MontoPagado) * MontoImpuesto
    End If
    RsCuentaCobrar!n_Pagado = 0 'FormatNumber(MontoPagado, Decimales)
    RsCuentaCobrar!n_Subtotal = FormatNumber(MontoSubtotal, Decimales)
    RsCuentaCobrar!n_pimpuesto = FormatNumber((MontoImpuesto / MontoTotal) * 100, Decimales)
    RsCuentaCobrar!c_CodMoneda = CodigoMoneda
    RsCuentaCobrar!n_Factor = FactorMoneda
    RsCuentaCobrar!n_Descuento = MontoDescuento
    RsCuentaCobrar.UpdateBatch
    
    CrearCabecero = True
    Exit Function
Falla_Local:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")"
    
End Function

Public Function CrearCabeceroPgNdNc(ConexionAdmin As Object, FacturaNumero As String, CodigoCliente As String, Concepto As String, MontoTotal As Double, MontoPagado As Double, MontoBaseImpuesto As Double, MontoImpuesto As Double, MontoSubtotal As Double, MontoDescuento As Double, FechaEmision As Date, CodigoLocalidad As String, CodigoUsuario As String, observacion As String, CodigoMoneda As String, FactorMoneda As Double, DetallePagos As Variant, Optional DetalleDenominaciones As Variant, Optional Decimales As Long = 2) As Boolean
    
    Dim RsCuentaCobrar As Object, Consecutivo As String, Proporcion As Double
    On Error GoTo Falla_Local
    CrearCabeceroPgNdNc = False
    
    Select Case UCase(Concepto)
        Case "PAG"  'PAGOS
            Consecutivo = ClaseRutinas.Consecutivos(ConexionAdmin, "cxc_pagos", True)
        Case "N_D"  'NOTA DE DEBITO
            Consecutivo = ClaseRutinas.Consecutivos(ConexionAdmin, "cxc_notad", True)
        Case "N_C"  'NOTA DE CREDITO
            Consecutivo = ClaseRutinas.Consecutivos(ConexionAdmin, "cxc_notac", True)
    End Select
    
    Set RsCuentaCobrar = ClaseRutinas.CrearAdoRs
    
    RsCuentaCobrar.Open "SELECT * FROM " & TipoCuenta(NTipoCuenta) & " WHERE C_DOCUMENTO = '" & Consecutivo & "' AND C_CONCEPTO = '" & Concepto & "' AND C_CODIGO = '" & CodigoCliente & "'", ConexionAdmin, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    If Not RsCuentaCobrar.EOF Then
        Exit Function
    End If
    
    RsCuentaCobrar.AddNew
    
    Proporcion = FormatNumber(MontoPagado / MontoTotal, Decimales)
    
    RsCuentaCobrar!c_Documento = Consecutivo
    RsCuentaCobrar!C_CODIGO = CodigoCliente
    RsCuentaCobrar!c_Localidad = CodigoLocalidad
    RsCuentaCobrar!c_usuario = CodigoUsuario
    RsCuentaCobrar!c_Concepto = Concepto
    RsCuentaCobrar!c_Detalle = observacion
    
    RsCuentaCobrar!f_FechaE = FechaEmision
    RsCuentaCobrar!f_FechaV = FechaEmision
    RsCuentaCobrar!f_Cancelacion = FechaEmision
    
    RsCuentaCobrar!n_Impuesto = FormatNumber(Proporcion * MontoImpuesto, Decimales)
    RsCuentaCobrar!n_bimpuesto = FormatNumber(Proporcion * MontoBaseImpuesto, Decimales)
    RsCuentaCobrar!n_Total = FormatNumber(MontoPagado, Decimales)
    RsCuentaCobrar!n_PagadoImp = FormatNumber(Proporcion * MontoImpuesto, Decimales)             'REVISAR ESTE CAMPO FORMULA = (MontoTotal/MontoPagado) * MontoImpuesto
    RsCuentaCobrar!n_Pagado = FormatNumber(MontoPagado, Decimales)
    RsCuentaCobrar!n_Subtotal = FormatNumber(Proporcion * MontoSubtotal, Decimales)
    RsCuentaCobrar!n_pimpuesto = FormatNumber((RsCuentaCobrar!n_Impuesto / MontoPagado) * 100, Decimales)
    RsCuentaCobrar!c_CodMoneda = CodigoMoneda
    RsCuentaCobrar!n_Factor = FactorMoneda
    RsCuentaCobrar!n_Descuento = MontoDescuento
    RsCuentaCobrar.UpdateBatch
    
    If Not CrearCabeceroDeDetalle(ConexionAdmin, Consecutivo, CodigoCliente, Concepto, DetallePagos, DetalleDenominaciones, observacion, CodigoLocalidad, CodigoUsuario, True, 2) Then
        Exit Function
    End If
    
    CrearCabeceroPgNdNc = True
    Exit Function
Falla_Local:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")"
    
End Function

Private Function CrearCabeceroDeDetalle(ConexionAdmin As Object, Documento As String, CodigoCliente As String, Concepto As String, DetallePagos As Variant, DetalleDenominaciones As Variant, observacion As String, CodigoLocalidad As String, CodigoUsuario As String, Optional EjecutarCancelacion As Boolean = False, Optional Decimales As Long = 2) As Boolean
    
    Dim RsCuentaCobrar As Object, Consecutivo As String, ContArray As Integer
    Dim RsCuentaPadre As Object, Proporcion As Double
    On Error GoTo Falla_Local
    
    Set RsCuentaCobrar = ClaseRutinas.CrearAdoRs
    Set RsCuentaPadre = ClaseRutinas.CrearAdoRs
    
    CrearCabeceroDeDetalle = False
    
    If IsEmpty(DetallePagos) Then Exit Function
    
    For ContArray = LBound(DetallePagos) To UBound(DetallePagos)
    
        RsCuentaCobrar.Open "SELECT * FROM " & TipoCuenta(NTipoCuenta) & " WHERE C_DOCUMENTO = '" & Documento & "' AND C_CONCEPTO = '" & Concepto & "x" & DetallePagos(ContArray)(1) & "' AND C_CODIGO = '" & CodigoCliente & "'", ConexionAdmin, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
        
        If Not RsCuentaCobrar.EOF Then
            Exit Function
        End If
        
        RsCuentaCobrar.AddNew
        RsCuentaCobrar!c_Documento = Documento
        RsCuentaCobrar!c_Relacion = DetallePagos(ContArray)(0)
        RsCuentaCobrar!C_CODIGO = CodigoCliente
        RsCuentaCobrar!c_Localidad = CodigoLocalidad
        RsCuentaCobrar!c_usuario = CodigoUsuario
        RsCuentaCobrar!c_Concepto = UCase(Concepto) & "x" & UCase(DetallePagos(ContArray)(1))
        RsCuentaCobrar!c_Detalle = observacion
        
        RsCuentaCobrar!f_FechaE = CDate(DetallePagos(ContArray)(2))
        RsCuentaCobrar!f_FechaV = RsCuentaCobrar!f_FechaE
        RsCuentaCobrar!f_Cancelacion = DetallePagos(ContArray)(2) & " " & CStr(Time)
        
        ClaseRutinas.Apertura_Recordset RsCuentaPadre, adUseClient
        RsCuentaPadre.Open "SELECT * FROM " & TipoCuenta(NTipoCuenta) & " WHERE C_DOCUMENTO = '" & DetallePagos(ContArray)(0) & "' AND C_CODIGO = '" & CodigoCliente & "' AND C_CONCEPTO = '" & DetallePagos(ContArray)(1) & "'", ConexionAdmin, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
        
        If Not RsCuentaPadre.EOF Then
            
            Proporcion = FormatNumber(CDbl(DetallePagos(ContArray)(3)) / RsCuentaPadre!n_Total, Decimales)
            RsCuentaCobrar!n_Total = FormatNumber(CDbl(DetallePagos(ContArray)(3)), Decimales)
            RsCuentaCobrar!n_Pagado = FormatNumber(CDbl(DetallePagos(ContArray)(3)), Decimales)
            
            RsCuentaCobrar!n_Impuesto = FormatNumber(Proporcion * RsCuentaPadre!n_Impuesto, Decimales)
            RsCuentaCobrar!n_bimpuesto = FormatNumber(Proporcion * RsCuentaPadre!n_bimpuesto, Decimales)
            RsCuentaCobrar!n_pimpuesto = FormatNumber(RsCuentaPadre!n_pimpuesto, Decimales)
            RsCuentaCobrar!n_PagadoImp = FormatNumber(Proporcion * RsCuentaPadre!n_PagadoImp, Decimales)
            
            RsCuentaCobrar!n_Subtotal = FormatNumber(RsCuentaCobrar!n_Total - RsCuentaCobrar!n_Impuesto, Decimales)
            
            RsCuentaCobrar!c_CodMoneda = DetallePagos(ContArray)(4)
            RsCuentaCobrar!n_Factor = FormatNumber(CDbl(DetallePagos(ContArray)(5)), Decimales)
            RsCuentaCobrar!n_Descuento = 0
            RsCuentaCobrar.UpdateBatch
            
            RsCuentaPadre!n_Pagado = RsCuentaPadre!n_Pagado + FormatNumber(CDbl(DetallePagos(ContArray)(3)), Decimales)
            
            If RsCuentaPadre!n_Total <= RsCuentaPadre!n_Pagado Then
                RsCuentaPadre!n_Pagado = RsCuentaPadre!n_Total
                RsCuentaPadre!f_Cancelacion = Now
            End If
            RsCuentaPadre.UpdateBatch
             
            Call CrearDetallesPagos(ConexionAdmin, Documento, UCase(Concepto) & "x" & UCase(DetallePagos(ContArray)(1)), DetalleDenominaciones)
             
        Else
            Exit Function
        End If
        
    Next ContArray
    
    CrearCabeceroDeDetalle = True
    Exit Function
Falla_Local:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")"
    
End Function

Private Function CrearDetallesPagos(ConexionAdmin As Object, Documento As String, Concepto As String, DetalleDenominaciones As Variant) As Boolean
    
    Dim RsCuentaCobrarDetPag As New ADODB.Recordset, Consecutivo As String, ContArray As Integer
    
    On Error GoTo Falla_Local
    
    Set RsCuentaCobrarDetPag = ClaseRutinas.CrearAdoRs
    CrearDetallesPagos = False
    
    If IsEmpty(DetalleDenominaciones) Then Exit Function
    
    'DetalleDenominaciones (c_codmoneda, c_codbanco, c_num_cheque, f_fecha, n_monto, n_factor, n_cantidad, n_vuelto)
    RsCuentaCobrarDetPag.Open "SELECT * FROM " & TipoCuenta(NTipoCuenta) & "_DETPAG WHERE C_DOCUMENTO = '" & Documento & "' AND C_CONCEPTO = '" & Concepto & "'", ConexionAdmin, adOpenForwardOnly, adLockBatchOptimistic, adCmdText
    
    For ContArray = LBound(DetalleDenominaciones) To UBound(DetalleDenominaciones)
        
        RsCuentaCobrarDetPag.AddNew
        RsCuentaCobrarDetPag!c_Documento = Documento
        RsCuentaCobrarDetPag!c_Concepto = Concepto
        RsCuentaCobrarDetPag!c_CodMoneda = DetalleDenominaciones(ContArray)(0)
        RsCuentaCobrarDetPag!c_CodBanco = DetalleDenominaciones(ContArray)(1)
        RsCuentaCobrarDetPag!c_Num_Cheque = DetalleDenominaciones(ContArray)(2)
        RsCuentaCobrarDetPag!f_Fecha = DetalleDenominaciones(ContArray)(3)
        RsCuentaCobrarDetPag!n_Monto = DetalleDenominaciones(ContArray)(4)
        RsCuentaCobrarDetPag!n_Factor = DetalleDenominaciones(ContArray)(5)
        RsCuentaCobrarDetPag!n_Cantidad = DetalleDenominaciones(ContArray)(6)
        RsCuentaCobrarDetPag!n_Vuelto = DetalleDenominaciones(ContArray)(7)
        RsCuentaCobrarDetPag.UpdateBatch
    
    Next ContArray
    
    RsCuentaCobrarDetPag.Close
    
    CrearDetallesPagos = True
    
    Exit Function
    
Falla_Local:
    
End Function

Private Sub Class_Initialize()
    TipoCuenta(0) = "MA_CXC"
    TipoCuenta(1) = "MA_CXP"
End Sub
